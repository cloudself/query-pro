package cn.cloudself;

import cn.cloudself.helpers.Helpers;
import cn.cloudself.helpers.query.User;
import cn.cloudself.helpers.query.UserQueryPro;
import cn.cloudself.query.QueryProSql;
import cn.cloudself.query.config.QueryProConfig;
import org.intellij.lang.annotations.Language;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static cn.cloudself.helpers.Helpers.*;

public class DeleteTest {
    static {
        Helpers.initLogger();
    }

    private void prepareData() {
        @Language("SQL")
        String sql =
                "TRUNCATE TABLE user;\n" +
                "INSERT INTO user (id, name, age) VALUES (1, 'delete-test', 18);\n" +
                "INSERT INTO user (id, name, age) VALUES (2, 'delete-test', 10);\n" +
                "INSERT INTO user (id, name, age) VALUES (3, 'delete-test2', 18);\n" +
                "INSERT INTO user (id, name, age) VALUES (4, 'delete-test3', 18);"
                ;
        QueryProSql.createBatch().autoSplit(sql).exec();
    }

    @Test
    public void test() { QueryProConfig.context.use(config -> {
        config.beautifySql(false);
        config.logicDelete(false);
        config.dataSource(getDataSource());

        prepareData();

        final User user1 = UserQueryPro.selectBy().id().equalTo(1).runLimit1();
        Assertions.assertNotNull(user1);

        expectSql("DELETE FROM `user` WHERE `user`.`id` = ?", listOf(1));
        final Boolean success = UserQueryPro.deleteBy().id().equalTo(1).run();
        Assertions.assertTrue(success);

        final User user2 = UserQueryPro.selectBy().id().equalTo(1).runLimit1();
        Assertions.assertNull(user2);

        final boolean success2 = UserQueryPro.deleteByPrimaryKey(2);
        Assertions.assertTrue(success2);
    });}

    @Test
    public void testLogicDelete() { QueryProConfig.context.use(config -> {
        config.beautifySql(false);
        config.logicDelete(true);
        config.dataSource(getDataSource());

        prepareData();

        final User user3 = UserQueryPro.selectBy().id().equalTo(3).runLimit1();
        Assertions.assertNotNull(user3);

        expectSql("UPDATE `user` SET `deleted` = ? WHERE `user`.`id` = ?", listOf(true, 3));
        final Boolean success = UserQueryPro.deleteBy().id().equalTo(3).run();
        Assertions.assertTrue(success);

        final User user5 = UserQueryPro.selectBy().id().equalTo(3).runLimit1();
        Assertions.assertNull(user5);
    });}
}
