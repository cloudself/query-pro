package cn.cloudself;

import cn.cloudself.helpers.Helpers;
import cn.cloudself.helpers.query.Setting;
import cn.cloudself.helpers.query.User;
import cn.cloudself.query.QueryProSql;
import cn.cloudself.query.QueryProSession;
import cn.cloudself.query.config.QueryProConfig;
import org.intellij.lang.annotations.Language;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

import static cn.cloudself.helpers.Helpers.*;

public class QueryProSqlTest {
    static {
        Helpers.initLogger();
    }

    private void prepareData() {
        @Language("SQL")
        String sql = "TRUNCATE TABLE user;\n" +
                "INSERT INTO user (id, name, age) VALUES (1, 'hb', 18);\n" +
                "INSERT INTO user (id, name, age) VALUES (2, 'hb', 10);\n" +
                "TRUNCATE TABLE setting;\n" +
                "INSERT INTO setting (id, user_id, kee, value) VALUES (1, 1, 'language', 'English');";
                ;
        QueryProSql.createBatch().autoSplit(sql).exec();

        final File file = new File("temp.sql");
        try (final OutputStream os = Files.newOutputStream(file.toPath())) {
            // language=SQL
            os.write("SELECT * FROM setting".getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        file.deleteOnExit();
    }

    @Test
    public void test() { QueryProConfig.context.use(config -> {
        config.beautifySql(false);
        config.dataSource(getDataSource());

        prepareData();

        final User user1 = new User().setId(1L).setName("hb").setAge(18).setDeleted(false);
        final User user2 = new User().setId(2L).setName("hb").setAge(10).setDeleted(false);

        final Setting setting1 = new Setting().setId(1L).setUserId(1L).setKee("language").setValue("English").setDeleted(false);

        System.out.println("单条查询语句");
        assertEquals(
                QueryProSql.create("SELECT * FROM user").query(User.class),
                listOf(user1, user2)
        );

        System.out.println("单条查询语句 含分号");
        assertEquals(
                QueryProSql.create("SELECT * FROM user;").query(User.class),
                listOf(user1, user2)
        );

        System.out.println("单条查询语句 空结果");
        assertEquals(
                QueryProSql.create("SELECT * FROM user WHERE age > 500;").query(User.class),
                listOf()
        );

        System.out.println("单条查询语句 (文件模式)");
        assertEquals(
                QueryProSql.create(Files.newInputStream(new File("temp.sql").toPath())).query(Setting.class),
                listOf(setting1)
        );

        System.out.println("单条更新语句");
        assertEquals(
                QueryProSql.create("INSERT INTO user (id, name, age) VALUES (3, 'herb', 18)").exec(),
                1
        );

        System.out.println("单条更新语句, 含分号");
        assertEquals(
                QueryProSql.create("INSERT INTO user (id, name, age) VALUES (4, 'herb', 20);").exec(),
                1
        );

        System.out.println("单条查询语句 查出单条数据");
        assertEquals(
                QueryProSql.create("SELECT * FROM user WHERE id = ?", 3).queryOne(),
                new HashMap<String, Object>(){{ put("id", 3L); put("name", "herb"); put("age", 18); put("deleted", false); put("sex", null); }}
        );
        assertEquals(
                QueryProSql.create("SELECT * FROM user WHERE id = 4").queryOne(User.class),
                new User().setId(4L).setName("herb").setAge(20).setDeleted(false)
        );
        assertEquals(
                QueryProSql.create("SELECT * FROM user WHERE age > 500").queryOne(),
                null
        );
        assertEquals(
                QueryProSql.create("SELECT * FROM user WHERE age > 500").queryOne(User.class),
                null
        );

        QueryProSql.create().insert("user", new HashMap<String, Object>() {{
            put("name", "hb-new");
            put("age", 8);
        }});

        assertEquals(
                QueryProSql.create("SELECT age FROM user WHERE name = ?", "hb-new").queryOne().get("age"),
                8
        );

        QueryProSession.useTransaction(() -> {
            QueryProSql.create(
                    "drop temporary table if exists tmp;\n" +
                    "create temporary table tmp select 1 as a, 'b' as b;"
            ).autoSplit().exec();

            final Map<String, Object> row = QueryProSql.create("select * from tmp;").queryOne();
            assertEquals(row, new HashMap<String, Object>() {{ put("a", 1); put("b", "b"); }});

            QueryProSql.create("set @v1 = ?;set @v2 = ?;create temporary table tmp2 select @v1 as a, @v2 as b;", 1L, 2L).autoSplit().exec();

            final Map<String, Object> row2 = QueryProSql.create("select * from tmp2;").queryOne();
            assertEquals(row2, new HashMap<String, Object>() {{ put("a", 1L); put("b", 2L); }});
        });

    });}
}
