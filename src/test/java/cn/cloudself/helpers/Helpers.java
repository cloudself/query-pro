package cn.cloudself.helpers;

import cn.cloudself.query.config.QueryProConfig;
import cn.cloudself.util.structure.Result;
import cn.cloudself.util.ext.SqlPro;
import com.mysql.cj.jdbc.MysqlDataSource;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.builder.api.*;
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration;
import org.intellij.lang.annotations.Language;
import org.junit.jupiter.api.Assertions;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class Helpers {
    @SafeVarargs
    public static <T> List<T> listOf(T ...objs) {
        List<T> res = new ArrayList<>(objs.length);
        Collections.addAll(res, objs);
        return res;
    }

    private static boolean dbInitialized = false;
    private static final String DB_NAME = "query_pro_test";

    private static void initDb() {
        if (dbInitialized) {
            return;
        }

        @Language("SQL") final String sqlGroup =
                "CREATE DATABASE IF NOT EXISTS " + DB_NAME + " CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;\n" +
                "USE " + DB_NAME + ";\n" +
                "CREATE TABLE IF NOT EXISTS user (\n" +
                "    id   bigint not null auto_increment primary key,\n" +
                "    name varchar(55) null,\n" +
                "    age  int         null,\n" +
                "    sex  enum ('man', 'not_man') null,\n" +
                "    deleted tinyint(1) null default false\n" +
                ");\n" +
                "CREATE TABLE IF NOT EXISTS setting (\n" +
                "    id      bigint auto_increment primary key,\n" +
                "    user_id bigint      null,\n" +
                "    kee     varchar(55) null,\n" +
                "    value   varchar(55) null,\n" +
                "    deleted tinyint(1) not null default false\n" +
                ");";

        final DataSource dataSource = getDataSource(null, true);
        try (
                final Connection connection = dataSource.getConnection();
                final Statement statement = connection.createStatement();
        ) {
            final List<String> split = SqlPro.of(sqlGroup).splitter().bySemicolon().split();
            for (String sql : split) {
                statement.addBatch(sql);
                statement.executeBatch();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


        dbInitialized = true;
    }

    public static DataSource getDataSource() {
        return getDataSource(DB_NAME, false);
    }

    public static DataSource getDataSource(String dbName, boolean skipInit) {
        if (!skipInit) {
            initDb();
        }

        final MysqlDataSource dataSource = new MysqlDataSource();
        dbName = dbName == null ? "" : dbName;
        dataSource.setUrl("jdbc:mysql://127.0.0.1:3306/" + dbName + "?useUnicode=true&characterEncoding=utf-8&useSSL=false&serverTimezone=UTC&allowPublicKeyRetrieval=true");
        dataSource.setUser("root");
        dataSource.setPassword(Password.password1);
        try {
            dataSource.setConnectTimeout(1024);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return dataSource;
    }

    public static void initLogger() {
        final ConfigurationBuilder<BuiltConfiguration> builder = ConfigurationBuilderFactory.newConfigurationBuilder();

        final RootLoggerComponentBuilder rootLogger = builder.newRootLogger(Level.ALL).add(builder.newAppenderRef("stdout"));
        builder.add(rootLogger);

        final LayoutComponentBuilder layout = builder.newLayout("PatternLayout").addAttribute("pattern", "%d{yyyy-MM-dd HH:mm:ss.SSS} [%t] %highlight{%-5level}{FATAL=red blink, ERROR=red, WARN=yellow bold, INFO=green, DEBUG=gray, TRACE=blue} %style{%40.40C{1.}-%-4L}{cyan}: %msg%n%ex");
        final AppenderComponentBuilder console = builder.newAppender("stdout", "Console");
        console.add(layout);
        builder.add(console);

        //noinspection resource
        Configurator.initialize(builder.build());
    }

    public static void assertEquals(Object actual, Object expect) {
        if (actual instanceof List && expect instanceof List) {
            Assertions.assertEquals(new ArrayList<>((List<?>) expect), new ArrayList<>((List<?>) actual));
        } else if (actual instanceof Map && expect instanceof Map) {
            Assertions.assertEquals(new HashMap<>((Map<?, ?>) expect), new HashMap<>((Map<?, ?>) actual));
        } else {
            Assertions.assertEquals(expect, actual);
        }
    }

    private static boolean isNumber(Object obj) {
        return obj instanceof Integer || obj instanceof Float || obj instanceof Double || obj instanceof Long || obj instanceof BigDecimal || obj instanceof Short;
    }

    private static boolean sqlExpectInit = false;
    private static String expectSql = null;
    private static List<Object> expectParams = null;
    private static void initSqlExpect() {
        sqlExpectInit = true;
        QueryProConfig.global.lifecycle().beforeRunSql(sqlAndParams -> {
            if (expectSql == null || expectParams == null) {
                return Result.ok(sqlAndParams);
            }

            final String relSql = sqlAndParams.sql();
            final Object[] relParams = sqlAndParams.params();
            assertEquals(relSql.trim(), expectSql.trim());
            for (int i = 0; i < expectParams.size(); i++) {
                final Object expectParam = expectParams.get(i);
                final Object relParam = relParams[i];
                if (isNumber(expectParam)) {
                    assertEquals(expectParam.toString(), relParam.toString());
                } else {
                    assertEquals(expectParam, relParam);
                }
            }
            expectSql = null;
            expectParams = null;
            return Result.ok(sqlAndParams);
        });
    }


    public static void expectSql(@Language("SQL") String sql, List<Object> params) {
        if (!sqlExpectInit) {
            initSqlExpect();
        }
        expectSql = sql;
        expectParams = params;
    }
}
