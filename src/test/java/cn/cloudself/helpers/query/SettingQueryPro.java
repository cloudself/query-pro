package cn.cloudself.helpers.query;

import cn.cloudself.util.log.Log;
import cn.cloudself.util.log.LogFactory;
import cn.cloudself.helpers.query.Setting;
import cn.cloudself.query.*;
import cn.cloudself.query.psi.*;
import cn.cloudself.query.psi.structure.*;
import cn.cloudself.exception.IllegalCall;
import cn.cloudself.util.structure.ListEx;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.function.Function;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unused")
public class SettingQueryPro {
    private static final Log logger = LogFactory.getLog(__Impl.class);

    private static QueryStructure defQueryStructure() {
        final QueryStructure queryStructure = new QueryStructure();
        queryStructure.from(new QueryStructureFrom(__Impl.TABLE_NAME));
        return queryStructure;
    }

    private static QueryPro<
            Setting,
            Long,
            __Impl.WhereField<Setting, List<Setting>>,
            __Impl.OrderByField<Setting, List<Setting>>,
            __Impl.UpdateSetField,
            __Impl.WhereField<Boolean, Boolean>,
            __Impl.WhereField<Boolean, Boolean>
    > createQuery() {
        return new QueryPro<>(
                SettingQueryPro.class,
                Setting.class,
                defQueryStructure(),
                (qs, payload) -> new __Impl.WhereField<>(qs, payload, Setting.class),
                (qs, payload) -> new __Impl.OrderByField<>(qs, payload, Setting.class),
                __Impl.UpdateSetField::new,
                (qs, payload) -> new __Impl.WhereField<>(qs, payload, Boolean.class),
                (qs, payload) -> new __Impl.WhereField<>(qs, payload, Boolean.class)
        );
    }

    public static cn.cloudself.query.QueryPro<Setting, Long, __Impl.WhereField<Setting, List<Setting>>, __Impl.OrderByField<Setting, List<Setting>>, __Impl.UpdateSetField, __Impl.WhereField<Boolean, Boolean>, __Impl.WhereField<Boolean, Boolean>> beautifySql(boolean arg0) {
        return createQuery().beautifySql(arg0);
    }

    public static cn.cloudself.query.QueryPro<Setting, Long, __Impl.WhereField<Setting, List<Setting>>, __Impl.OrderByField<Setting, List<Setting>>, __Impl.UpdateSetField, __Impl.WhereField<Boolean, Boolean>, __Impl.WhereField<Boolean, Boolean>> connection(java.sql.Connection arg0) {
        return createQuery().connection(arg0);
    }

    public static cn.cloudself.query.QueryPro<Setting, Long, __Impl.WhereField<Setting, List<Setting>>, __Impl.OrderByField<Setting, List<Setting>>, __Impl.UpdateSetField, __Impl.WhereField<Boolean, Boolean>, __Impl.WhereField<Boolean, Boolean>> dataSource(javax.sql.DataSource arg0) {
        return createQuery().dataSource(arg0);
    }

    public static cn.cloudself.query.QueryPro<Setting, Long, __Impl.WhereField<Setting, List<Setting>>, __Impl.OrderByField<Setting, List<Setting>>, __Impl.UpdateSetField, __Impl.WhereField<Boolean, Boolean>, __Impl.WhereField<Boolean, Boolean>> dbType(cn.cloudself.query.config.IQueryProConfig.DatabaseType arg0) {
        return createQuery().dbType(arg0);
    }

    public static boolean deleteByPrimaryKey(Object arg0) {
        return createQuery().deleteByPrimaryKey(arg0);
    }

    public static __Impl.WhereField<Boolean, Boolean> deleteBy() {
        return createQuery().deleteBy();
    }

    public static cn.cloudself.query.QueryPro<Setting, Long, __Impl.WhereField<Setting, List<Setting>>, __Impl.OrderByField<Setting, List<Setting>>, __Impl.UpdateSetField, __Impl.WhereField<Boolean, Boolean>, __Impl.WhereField<Boolean, Boolean>> dryRun(boolean arg0) {
        return createQuery().dryRun(arg0);
    }

    public static Long insert(Setting arg0) {
        return createQuery().insert(arg0);
    }

    public static java.util.List<Long> insert(Setting ...arg0) {
        return createQuery().insert(arg0);
    }

    public static java.util.List<Long> insert(java.util.Collection<Setting> arg0) {
        return createQuery().insert(arg0);
    }

    @SafeVarargs
    public static java.util.List<Long> insert(java.util.Map<String, ?> ...arg0) {
        return createQuery().insert(arg0);
    }

    public static cn.cloudself.query.QueryPro<Setting, Long, __Impl.WhereField<Setting, List<Setting>>, __Impl.OrderByField<Setting, List<Setting>>, __Impl.UpdateSetField, __Impl.WhereField<Boolean, Boolean>, __Impl.WhereField<Boolean, Boolean>> logicDelete(boolean arg0, String arg1, Object arg2, Object arg3) {
        return createQuery().logicDelete(arg0, arg1, arg2, arg3);
    }

    public static cn.cloudself.query.QueryPro<Setting, Long, __Impl.WhereField<Setting, List<Setting>>, __Impl.OrderByField<Setting, List<Setting>>, __Impl.UpdateSetField, __Impl.WhereField<Boolean, Boolean>, __Impl.WhereField<Boolean, Boolean>> logicDelete(boolean arg0) {
        return createQuery().logicDelete(arg0);
    }

    public static cn.cloudself.query.QueryPro<Setting, Long, __Impl.WhereField<Setting, List<Setting>>, __Impl.OrderByField<Setting, List<Setting>>, __Impl.UpdateSetField, __Impl.WhereField<Boolean, Boolean>, __Impl.WhereField<Boolean, Boolean>> maxParameterSize(int arg0) {
        return createQuery().maxParameterSize(arg0);
    }

    public static __Impl.OrderByField<Setting, List<Setting>> orderBy() {
        return createQuery().orderBy();
    }

    public static cn.cloudself.query.plus.QueryProPlus<Setting, cn.cloudself.query.plus.Alias> plus(String arg0) {
        return createQuery().plus(arg0);
    }

    public static cn.cloudself.query.plus.QueryProPlus<Setting, Setting> plus() {
        return createQuery().plus();
    }

    public static cn.cloudself.query.QueryPro<Setting, Long, __Impl.WhereField<Setting, List<Setting>>, __Impl.OrderByField<Setting, List<Setting>>, __Impl.UpdateSetField, __Impl.WhereField<Boolean, Boolean>, __Impl.WhereField<Boolean, Boolean>> printCallByInfo(boolean arg0, cn.cloudself.util.log.LogLevel arg1) {
        return createQuery().printCallByInfo(arg0, arg1);
    }

    public static cn.cloudself.query.QueryPro<Setting, Long, __Impl.WhereField<Setting, List<Setting>>, __Impl.OrderByField<Setting, List<Setting>>, __Impl.UpdateSetField, __Impl.WhereField<Boolean, Boolean>, __Impl.WhereField<Boolean, Boolean>> printCallByInfo(boolean arg0) {
        return createQuery().printCallByInfo(arg0);
    }

    public static cn.cloudself.query.QueryPro<Setting, Long, __Impl.WhereField<Setting, List<Setting>>, __Impl.OrderByField<Setting, List<Setting>>, __Impl.UpdateSetField, __Impl.WhereField<Boolean, Boolean>, __Impl.WhereField<Boolean, Boolean>> printLargeElementWholly(boolean arg0) {
        return createQuery().printLargeElementWholly(arg0);
    }

    public static cn.cloudself.query.QueryPro<Setting, Long, __Impl.WhereField<Setting, List<Setting>>, __Impl.OrderByField<Setting, List<Setting>>, __Impl.UpdateSetField, __Impl.WhereField<Boolean, Boolean>, __Impl.WhereField<Boolean, Boolean>> printLog(boolean arg0, cn.cloudself.util.log.LogLevel arg1) {
        return createQuery().printLog(arg0, arg1);
    }

    public static cn.cloudself.query.QueryPro<Setting, Long, __Impl.WhereField<Setting, List<Setting>>, __Impl.OrderByField<Setting, List<Setting>>, __Impl.UpdateSetField, __Impl.WhereField<Boolean, Boolean>, __Impl.WhereField<Boolean, Boolean>> printLog(boolean arg0) {
        return createQuery().printLog(arg0);
    }

    public static cn.cloudself.query.QueryPro<Setting, Long, __Impl.WhereField<Setting, List<Setting>>, __Impl.OrderByField<Setting, List<Setting>>, __Impl.UpdateSetField, __Impl.WhereField<Boolean, Boolean>, __Impl.WhereField<Boolean, Boolean>> printResult(boolean arg0, cn.cloudself.util.log.LogLevel arg1) {
        return createQuery().printResult(arg0, arg1);
    }

    public static cn.cloudself.query.QueryPro<Setting, Long, __Impl.WhereField<Setting, List<Setting>>, __Impl.OrderByField<Setting, List<Setting>>, __Impl.UpdateSetField, __Impl.WhereField<Boolean, Boolean>, __Impl.WhereField<Boolean, Boolean>> printResult(boolean arg0) {
        return createQuery().printResult(arg0);
    }

    public static void replace(Setting ...arg0) {
        createQuery().replace(arg0);
    }

    public static void replace(java.util.Collection<Setting> arg0) {
        createQuery().replace(arg0);
    }

    @SafeVarargs
    public static void replace(java.util.Map<String, ?> ...arg0) {
        createQuery().replace(arg0);
    }

    public static java.util.List<Setting> selectAll() {
        return createQuery().selectAll();
    }

    public static __Impl.WhereField<Setting, List<Setting>> selectByObj(Setting arg0) {
        return createQuery().selectByObj(arg0);
    }

    public static Setting selectByPrimaryKey(Object arg0) {
        return createQuery().selectByPrimaryKey(arg0);
    }

    public static __Impl.WhereField<Setting, List<Setting>> selectBy() {
        return createQuery().selectBy();
    }

    @Contract(pure = true)
    public static cn.cloudself.query.psi.UpdateSetDefinedExpression<__Impl.WhereField<Boolean, Boolean>> updateSet(Setting arg0, boolean arg1) {
        return createQuery().updateSet(arg0, arg1);
    }

    @Contract(pure = true)
    public static cn.cloudself.query.psi.UpdateSetDefinedExpression<__Impl.WhereField<Boolean, Boolean>> updateSet(Setting arg0) {
        return createQuery().updateSet(arg0);
    }

    @Contract(pure = true)
    public static cn.cloudself.query.psi.UpdateSetDefinedExpression<__Impl.WhereField<Boolean, Boolean>> updateSet(java.util.Map<String, ?> arg0) {
        return createQuery().updateSet(arg0);
    }

    @Contract(pure = true)
    public static __Impl.UpdateSetField updateSet() {
        return createQuery().updateSet();
    }

    public static class __Impl {
        private static final Class<Setting> CLAZZ = Setting.class;
        public static final String TABLE_NAME = "setting";
        private static Field createField(String column) { return new Field(TABLE_NAME, column); }

        public static class WhereField<T, RUN_RES> extends AbstractWhereExpressionOperators<T, RUN_RES, WhereField<T, RUN_RES>, OrderByField<T, RUN_RES>, ColumnLimiterField<T, RUN_RES>, ColumnsLimiterField<T, RUN_RES>> {
            private final QueryStructure queryStructure;
            private final QueryPayload payload;
            private final Class<T> clazz;
            public WhereField(QueryStructure queryStructure, QueryPayload payload, Class<T> clazz) {
                super();
                this.queryStructure = queryStructure;
                this.payload = payload;
                this.clazz = clazz;
            }
            @NotNull @Override protected QueryStructure getQueryStructure() { return queryStructure; }
            @NotNull @Override public QueryPayload getPayload() { return payload; }
            @NotNull @Override public Class<T> getClazz() { return clazz; }
            @NotNull @Override protected WhereField<T, RUN_RES> createWhereField(@NotNull QueryStructure qs, @NotNull QueryPayload payload) { return new WhereField<>(qs, payload, clazz); }
            @NotNull @Override protected OrderByField<T, RUN_RES> createOrderByField(@NotNull QueryStructure qs, @NotNull QueryPayload payload) { return new OrderByField<>(qs, payload, clazz); }
            @NotNull @Override protected ColumnLimiterField<T, RUN_RES> createColumnLimitField(@NotNull QueryStructure qs, @NotNull QueryPayload payload) { return new ColumnLimiterField<>(qs, payload, clazz); }
            @NotNull @Override protected ColumnsLimiterField<T, RUN_RES> createColumnsLimitField(@NotNull QueryStructure qs, @NotNull QueryPayload payload) { return new ColumnsLimiterField<>(qs, payload, clazz); }
            @NotNull @Override protected ExpressionType getFieldType() { return ExpressionType.WHERE; }
            private Keywords<WhereField<T, RUN_RES>> createWhereField(String column) {
                return new Keywords<>(new Field(TABLE_NAME, column), queryStructure, payload, this::createWhereField);
            }
            private WhereField<T, RUN_RES> createWhereField(String column, Object[] objs) {
                final Keywords<WhereField<T, RUN_RES>> whereField = createWhereField(column);
                return objs.length == 1 ? whereField.equalTo(objs[0]) : whereField.in(objs);
            }

            @Contract(pure = true)
            public Keywords<WhereField<T, RUN_RES>> id() { return createWhereField("id"); }
            @Contract(pure = true)
            public WhereField<T, RUN_RES> id(List<Long> idList) { return createWhereField("id", idList.toArray(new Object[0])); }
            @Contract(pure = true)
            public WhereField<T, RUN_RES> id(Long... ids) { return createWhereField("id", ids); }
            @Contract(pure = true)
            public Keywords<WhereField<T, RUN_RES>> userId() { return createWhereField("user_id"); }
            @Contract(pure = true)
            public WhereField<T, RUN_RES> userId(List<Long> userIdList) { return createWhereField("user_id", userIdList.toArray(new Object[0])); }
            @Contract(pure = true)
            public WhereField<T, RUN_RES> userId(Long... userIds) { return createWhereField("user_id", userIds); }
            @Contract(pure = true)
            public Keywords<WhereField<T, RUN_RES>> kee() { return createWhereField("kee"); }
            @Contract(pure = true)
            public WhereField<T, RUN_RES> kee(List<String> keeList) { return createWhereField("kee", keeList.toArray(new Object[0])); }
            @Contract(pure = true)
            public WhereField<T, RUN_RES> kee(String... kees) { return createWhereField("kee", kees); }
            @Contract(pure = true)
            public Keywords<WhereField<T, RUN_RES>> value() { return createWhereField("value"); }
            @Contract(pure = true)
            public WhereField<T, RUN_RES> value(List<String> valueList) { return createWhereField("value", valueList.toArray(new Object[0])); }
            @Contract(pure = true)
            public WhereField<T, RUN_RES> value(String... values) { return createWhereField("value", values); }
            @Contract(pure = true)
            public Keywords<WhereField<T, RUN_RES>> deleted() { return createWhereField("deleted"); }
            @Contract(pure = true)
            public WhereField<T, RUN_RES> deleted(List<Boolean> deletedList) { return createWhereField("deleted", deletedList.toArray(new Object[0])); }
            @Contract(pure = true)
            public WhereField<T, RUN_RES> deleted(Boolean... deleteds) { return createWhereField("deleted", deleteds); }

            @Contract(pure = true)
            public WhereField<T, RUN_RES> take(Function<WhereField<T, RUN_RES>, WhereField<T, RUN_RES>> factor) {
                return factor.apply(this);
            }
        }

        public abstract static class CommonField<T, RUN_RES> extends AbstractExpressionOperators<T, RUN_RES, WhereField<T, RUN_RES>, OrderByField<T, RUN_RES>, ColumnLimiterField<T, RUN_RES>, ColumnsLimiterField<T, RUN_RES>> {
            protected final QueryStructure queryStructure;
            protected final QueryPayload payload;
            protected final Class<T> clazz;
            CommonField(QueryStructure queryStructure, QueryPayload payload, Class<T> clazz) {
                super();
                this.queryStructure = queryStructure;
                this.payload = payload;
                this.clazz = clazz;
            }
            @NotNull @Override protected QueryStructure getQueryStructure() { return queryStructure; }
            @NotNull @Override public QueryPayload getPayload() { return payload; }
            @NotNull @Override public Class<T> getClazz() { return clazz; }
            @NotNull @Override protected WhereField<T, RUN_RES> createWhereField(@NotNull QueryStructure qs, @NotNull QueryPayload payload) { return new WhereField<>(qs, payload, clazz); }
            @NotNull @Override protected OrderByField<T, RUN_RES> createOrderByField(@NotNull QueryStructure qs, @NotNull QueryPayload payload) { return new OrderByField<>(qs, payload, clazz); }
            @NotNull @Override protected ColumnLimiterField<T, RUN_RES> createColumnLimitField(@NotNull QueryStructure qs, @NotNull QueryPayload payload) { return new ColumnLimiterField<>(qs, payload, clazz); }
            @NotNull @Override protected ColumnsLimiterField<T, RUN_RES> createColumnsLimitField(@NotNull QueryStructure qs, @NotNull QueryPayload payload) { return new ColumnsLimiterField<>(qs, payload, clazz); }
        }

        public static class OrderByField<T, RUN_RES> extends CommonField<T, RUN_RES> {
            public OrderByField(QueryStructure qs, QueryPayload payload, Class<T> clazz) { super(qs, payload, clazz); }
            @NotNull @Override protected ExpressionType getFieldType() { return ExpressionType.ORDER_BY; }
            private KeywordsOrderBy<OrderByField<T, RUN_RES>> createOrderByField(String column) {
                return new KeywordsOrderBy<>(new Field(TABLE_NAME, column), queryStructure, payload, super::createOrderByField);
            }

            @Contract(pure = true)
            public KeywordsOrderBy<OrderByField<T, RUN_RES>> id() { return createOrderByField("id"); }
            @Contract(pure = true)
            public KeywordsOrderBy<OrderByField<T, RUN_RES>> userId() { return createOrderByField("user_id"); }
            @Contract(pure = true)
            public KeywordsOrderBy<OrderByField<T, RUN_RES>> kee() { return createOrderByField("kee"); }
            @Contract(pure = true)
            public KeywordsOrderBy<OrderByField<T, RUN_RES>> value() { return createOrderByField("value"); }
            @Contract(pure = true)
            public KeywordsOrderBy<OrderByField<T, RUN_RES>> deleted() { return createOrderByField("deleted"); }

            @Contract(pure = true)
            public OrderByField<T, RUN_RES> take(Function<OrderByField<T, RUN_RES>, OrderByField<T, RUN_RES>> factor) {
                return factor.apply(this);
            }
        }

        public static class ColumnLimiterField<T, RUN_RES> extends CommonField<T, RUN_RES> {
            public ColumnLimiterField(QueryStructure qs, QueryPayload payload, Class<T> clazz) { super(qs, payload, clazz); }
            @NotNull @Override protected ExpressionType getFieldType() { return ExpressionType.COLUMN_LIMITER; }
            private <R> ListEx<R> createColumnLimiterField(String column, Class<R> clazz) {
                return new ListEx<>(getColumn(new Field(TABLE_NAME, column), clazz));
            }

            public ListEx<Long> id() { return createColumnLimiterField("id", Long.class); }
            public ListEx<Long> userId() { return createColumnLimiterField("user_id", Long.class); }
            public ListEx<String> kee() { return createColumnLimiterField("kee", String.class); }
            public ListEx<String> value() { return createColumnLimiterField("value", String.class); }
            public ListEx<Boolean> deleted() { return createColumnLimiterField("deleted", Boolean.class); }

            public <R> R take(Function<ColumnLimiterField<T, RUN_RES>, R> factor) {
                return factor.apply(this);
            }
        }

        public static class ColumnsLimiterField<T, RUN_RES> extends CommonField<T, RUN_RES> {
            public ColumnsLimiterField(QueryStructure qs, QueryPayload payload, Class<T> clazz) { super(qs, payload, clazz); }
            @NotNull @Override protected ExpressionType getFieldType() { return ExpressionType.COLUMNS_LIMITER; }

            @SuppressWarnings("DuplicatedCode")
            private ColumnsLimiterField<T, RUN_RES> createColumnsLimiterField(String column) {
                final QueryStructure queryStructure = getQueryStructure();
                queryStructure.appendField(new Field(TABLE_NAME, column));
                return new ColumnsLimiterField<>(queryStructure, payload, clazz);
            }

            @Contract(pure = true)
            public ColumnsLimiterField<T, RUN_RES> id() { return createColumnsLimiterField("id"); }
            @Contract(pure = true)
            public ColumnsLimiterField<T, RUN_RES> userId() { return createColumnsLimiterField("user_id"); }
            @Contract(pure = true)
            public ColumnsLimiterField<T, RUN_RES> kee() { return createColumnsLimiterField("kee"); }
            @Contract(pure = true)
            public ColumnsLimiterField<T, RUN_RES> value() { return createColumnsLimiterField("value"); }
            @Contract(pure = true)
            public ColumnsLimiterField<T, RUN_RES> deleted() { return createColumnsLimiterField("deleted"); }

            @Contract(pure = true)
            public ColumnsLimiterField<T, RUN_RES> take(Function<ColumnsLimiterField<T, RUN_RES>, ColumnsLimiterField<T, RUN_RES>> factor) {
                return factor.apply(this);
            }
        }

        public static class UpdateSetField extends UpdateSetDefinedExpression<WhereField<Boolean, Boolean>> {
            private final QueryStructure queryStructure;
            public UpdateSetField(QueryStructure qs, QueryPayload payload) {
                super(qs, payload, (q, p) -> new WhereField<>(q, p, Boolean.class));
                this.queryStructure = qs;
            }

            @SuppressWarnings("DuplicatedCode")
            private UpdateSetField createUpdateSetField(String key, Object value) {
                final Update update = queryStructure.update();
                if (update == null) {
                    throw new IllegalCall("usage like: UserQueryPro.updateSet().id(1).name(name).run()");
                }
                if (value == null) {
                    logger.error("null���������������updateSet, ������Const.NULL������, usage like: UserQueryPro.updateSet().name(Const.NULL).where().id(1).run()");
                }
                @SuppressWarnings("unchecked") final Map<String, Object> map = (Map<String, Object>) update.data();
                assert map != null;
                map.put(key, value);
                return this;
            }

            @Contract(pure = true)
            public UpdateSetField id(Object id) { return createUpdateSetField("id", id); }
            @Contract(pure = true)
            public UpdateSetField userId(Object userId) { return createUpdateSetField("user_id", userId); }
            @Contract(pure = true)
            public UpdateSetField kee(Object kee) { return createUpdateSetField("kee", kee); }
            @Contract(pure = true)
            public UpdateSetField value(Object value) { return createUpdateSetField("value", value); }
            @Contract(pure = true)
            public UpdateSetField deleted(Object deleted) { return createUpdateSetField("deleted", deleted); }

            @Contract(pure = true)
            public UpdateSetField take(Function<UpdateSetField, UpdateSetField> factor) {
                return factor.apply(this);
            }
        }
    }
}
