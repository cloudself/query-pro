package cn.cloudself;

import cn.cloudself.helpers.Helpers;
import cn.cloudself.query.QueryProSql;
import cn.cloudself.query.config.QueryProConfig;
import cn.cloudself.query.resolver.impl.JdbcQSR;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;

public class ResultSetWalkerTest {
    static {
        Helpers.initLogger();
    }

    private static class ResultSetWalker implements JdbcQSR.IResultSetWalker {
        final private List<String> columnNames = new ArrayList<>();

        @Override
        public void walk(@NotNull ResultSet rs) throws Exception {
            final ResultSetMetaData metaData = rs.getMetaData();
            final int columnCount = metaData.getColumnCount();
            for (int i = 1; i <= columnCount; i++) {
                final String columnLabel = metaData.getColumnLabel(i);
                columnNames.add(columnLabel);
            }
        }
    }

    @Test
    public void test() { QueryProConfig.context.use(conf -> {
        // 测试用例中，需要初始化QueryPro
        conf.dataSource(Helpers.getDataSource());

        // 提供一个实现了JdbcQSR.IResultSetWalker的类
        final ResultSetWalker walker = QueryProSql.create("select * from user left join setting on user.id = setting.user_id").queryOne(ResultSetWalker.class);
        assert walker != null;
        final List<String> columnNames = walker.columnNames;
        Helpers.assertEquals(columnNames, Helpers.listOf("id", "name", "age", "sex", "deleted", "id", "user_id", "kee", "value", "deleted"));
    });}

}
