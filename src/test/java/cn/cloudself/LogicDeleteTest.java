package cn.cloudself;

import cn.cloudself.helpers.Helpers;
import cn.cloudself.helpers.query.SettingQueryPro;
import cn.cloudself.helpers.query.User;
import cn.cloudself.helpers.query.UserQueryPro;
import cn.cloudself.query.QueryProSql;
import cn.cloudself.query.config.QueryProConfig;
import org.intellij.lang.annotations.Language;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static cn.cloudself.helpers.Helpers.*;

public class LogicDeleteTest {
    static {
        Helpers.initLogger();
    }

    private void prepareData() {
        @Language("SQL")
        String sql =
                "TRUNCATE TABLE setting;\n" +
                "INSERT INTO setting (id, user_id, kee, value, deleted) VALUES (1, 1, 'lang', 'en', false);\n" +
                "INSERT INTO setting (id, user_id, kee, value, deleted) VALUES (2, 2, 'lang', 'english', false);\n" +
                "INSERT INTO setting (id, user_id, kee, value, deleted) VALUES (3, 3, 'lang', 'zh-cn', false);\n" +
                "INSERT INTO setting (id, user_id, kee, value, deleted) VALUES (4, 4, 'lang', '简体中文', false);"
                ;
        QueryProSql.createBatch().autoSplit(sql).exec();
    }

    @Test
    public void test() { QueryProConfig.context.use(config -> {
        config.beautifySql(false);
        config.logicDelete(true);
        config.dataSource(getDataSource());

        prepareData();

        expectSql("UPDATE `setting` SET `deleted` = ? WHERE `setting`.`id` = ?", listOf(true, 1));
        assertEquals(SettingQueryPro.deleteBy().id().equalTo(1).run(), true);
        assertEquals(SettingQueryPro.selectBy().id().equalTo(1).runLimit1(), null);

        expectSql("SELECT * FROM `setting` WHERE  ( `setting`.`id` = ? OR `setting`.`kee` = ? )  AND `setting`.`deleted` = ? LIMIT 1", listOf(1, "lang", false));
        SettingQueryPro.selectBy().id().equalTo(1).or().kee().equalTo("lang").runLimit1();
    });}
}
