package cn.cloudself;

import cn.cloudself.helpers.Helpers;
import cn.cloudself.helpers.query.User;
import cn.cloudself.helpers.query.UserQueryPro;
import cn.cloudself.query.QueryProSql;
import cn.cloudself.query.QueryProSession;
import cn.cloudself.query.config.QueryProConfig;
import org.intellij.lang.annotations.Language;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static cn.cloudself.helpers.Helpers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TransactionTest {
    static {
        Helpers.initLogger();
    }

    private void prepareData() {
        @Language("SQL")
        String sql =
                "TRUNCATE TABLE user;\n" +
                "INSERT INTO user (id, name, age) VALUES (1, 'hb', 18);\n" +
                "INSERT INTO user (id, name, age) VALUES (2, 'herb', 19);"
                ;
        QueryProSql.createBatch().autoSplit(sql).exec();
    }

    @Test
    public void testTransaction() { QueryProConfig.context.use(config -> {
        config.beautifySql(false);
        config.logicDelete(false);
        config.dataSource(getDataSource());

        prepareData();

        final User user1 = new User().setId(1L).setName("hb").setAge(18).setDeleted(false);
        final User user1_19 = new User().setId(1L).setName("hb").setAge(19).setDeleted(false);
        final User user1_20 = new User().setId(1L).setName("hb").setAge(20).setDeleted(false);

        final User user2_19 = new User().setId(2L).setName("herb").setAge(19).setDeleted(false);
        final User user2_20 = new User().setId(2L).setName("herb").setAge(20).setDeleted(false);

        assertEquals(
                UserQueryPro.selectBy().id().equalTo(1).run(),
                listOf(user1)
        );

        assertThrows(Exception.class, () -> {
            QueryProSession.useTransaction(() -> {
                expectSql("UPDATE `user` SET `age` = ? WHERE `user`.`id` = ?", listOf(19, 1));
                Assertions.assertTrue(UserQueryPro.updateSet(new User().setAge(19), false).where().id().equalTo(1).run());
                assertEquals(
                        UserQueryPro.selectBy().id().equalTo(1).run(),
                        listOf(user1_19)
                );

                expectSql("UPDATE `user` SET `age` = ? WHERE `user`.`id` = ?", listOf(20, 2));
                Assertions.assertTrue(UserQueryPro.updateSet(new User().setAge(20), false).where().id().equalTo(2).run());
                assertEquals(
                        UserQueryPro.selectByPrimaryKey(2),
                        user2_20
                );

                expectSql("UPDATE `user` SET `age` = ? WHERE `user`.`id` = ?", listOf(20, 1));
                Assertions.assertTrue(UserQueryPro.updateSet(new User().setAge(20), false).where().id().equalTo(1).run());
                assertEquals(
                        UserQueryPro.selectBy().id().equalTo(1).run(),
                        listOf(user1_20)
                );

                throw new RuntimeException("test");
            });
        });

        assertEquals(
                UserQueryPro.selectBy().id().equalTo(1).run(),
                listOf(user1)
        );
        assertEquals(
                UserQueryPro.selectBy().id().equalTo(2).run(),
                listOf(user2_19)
        );

        boolean done = QueryProSession.useTransaction(() -> {
            expectSql("UPDATE `user` SET `age` = ? WHERE `user`.`id` = ?", listOf(19, 1));
            Assertions.assertTrue(UserQueryPro.updateSet().age(19).where().id().equalTo(1).run());
            assertEquals(UserQueryPro.selectBy().id().equalTo(1).run(), listOf(user1_19));

            expectSql("UPDATE `user` SET `age` = ? WHERE `user`.`id` = ?", listOf(20, 2));
            Assertions.assertTrue(UserQueryPro.updateSet().age(20).where().id().equalTo(2).run());
            assertEquals(UserQueryPro.selectBy().id().equalTo(2).run(), listOf(user2_20));

            expectSql("UPDATE `user` SET `age` = ? WHERE `user`.`id` = ?", listOf(20, 1));
            Assertions.assertTrue(UserQueryPro.updateSet().age(20).where().id().equalTo(1).run());
            assertEquals(UserQueryPro.selectBy().id().equalTo(1).run(), listOf(user1_20));

            return true;
        });
        Assertions.assertTrue(done);

        assertEquals(UserQueryPro.selectBy().id().equalTo(1).run(), listOf(user1_20));
        assertEquals(UserQueryPro.selectBy().id().equalTo(2).run(), listOf(user2_20));
    });}
}
