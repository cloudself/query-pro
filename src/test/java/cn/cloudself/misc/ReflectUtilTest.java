package cn.cloudself.misc;

import cn.cloudself.helpers.Helpers;
import cn.cloudself.helpers.query.UserQueryPro;
import cn.cloudself.query.config.QueryProConfig;
import cn.cloudself.util.ext.ReflectPro;
import org.junit.jupiter.api.Test;

import static cn.cloudself.helpers.Helpers.getDataSource;
import static cn.cloudself.helpers.Helpers.initLogger;

public class ReflectUtilTest {
    @Test
    public void test() throws Exception {
        initLogger();

        QueryProConfig.global.beautifySql(false);
        QueryProConfig.global.dataSource(getDataSource());

        final Object user1 = ReflectPro.of(UserQueryPro.class)
                .invoke("selectBy")
                .invoke("id", 3L)
                .invoke("run")
                .getResult();

        final Object user2 = ReflectPro.of(UserQueryPro.class)
                .invoke("selectBy")
                .invoke("id")
                .invoke("equalTo", 3L)
                .invoke("run")
                .getResult();

        Helpers.assertEquals(user1, user2);
    }
}
