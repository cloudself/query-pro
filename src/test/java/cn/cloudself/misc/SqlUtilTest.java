package cn.cloudself.misc;

import cn.cloudself.util.ext.SqlPro;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

import static cn.cloudself.helpers.Helpers.assertEquals;

public class SqlUtilTest {

    @Test
    public void test() {
        final List<SqlPro.StatementSplitter.Split> splits = SqlPro.of(
                "SELECT * FROM user WHERE id = '?';\n" +
                        "\n" +
                        "SELECT * \n" +
                        "FROM user\n" +
                        "WHERE id = '?';\n" +
                        "\n" +
                        "SELECT id AS '\"`?;' FROM user WHERE id = ?;\n" +
                        "SELECT id AS 'a\"a`a?a;a' FROM user WHERE id = ? AND name = '?';\n" +
                        "SELECT id AS \"'`?;\" FROM user;\n" +
                        "SELECT id AS \"a'a`a?a;a\" FROM user;\n" +
                        "SELECT id AS `'\"?;` FROM user;\n" +
                        "SELECT id AS `a\"a'a?a;a'` FROM user;\n" +
                        "\n" +
                        "SELECT id AS `a\"a'a?a;a'` /*?*/ FROM user;\n" +
                        "SELECT id AS `a\"a'a?a;a'` /*?*/ FROM user WHERE id = ?;\n" +
                        "\n" +
                        "SELECT id AS `a\"a'a?a;a'` /*?*/ FROM user; # ?\n" +
                        "SELECT id AS `a\"a'a?a;a'` /*?*/ FROM user; # */ ?\n" +
                        "SELECT id AS `a\"a'a?a;a'` /*?*/ FROM user; -- */ ?\n" +
                        "SELECT id AS `a\"a'a?a;a'` /*#?*/ FROM user; -- */ ?\n" +
                        "SELECT id AS `a\"a'a?a;a'` /*--?*/ FROM user; -- */ ?\n" +
                        "SELECT id AS `a\"a'a?a;a'` /**?*/ FROM user; -- */ ?\n" +
                        "SELECT id AS `a\"a'a?a;a'` /**?**/ FROM user; -- */ ?"
        ).splitter().bySemicolon().registerCounter('?').splitAndCount();

        System.out.println(splits);

        int i = 0;
        assertEquals(splits.size(), 17);

        assertEquals(splits.get(i++).count('?'), 0);

        assertEquals(splits.get(i++).count('?'), 0);

        assertEquals(splits.get(i++).count('?'), 1);
        assertEquals(splits.get(i++).count('?'), 1);
        assertEquals(splits.get(i++).count('?'), 0);
        assertEquals(splits.get(i++).count('?'), 0);
        assertEquals(splits.get(i++).count('?'), 0);
        assertEquals(splits.get(i++).count('?'), 0);

        assertEquals(splits.get(i++).count('?'), 0);
        assertEquals(splits.get(i++).count('?'), 1);

        assertEquals(splits.get(i++).count('?'), 0);
        assertEquals(splits.get(i++).count('?'), 0);
        assertEquals(splits.get(i++).count('?'), 0);
        assertEquals(splits.get(i++).count('?'), 0);
        assertEquals(splits.get(i++).count('?'), 0);
        assertEquals(splits.get(i++).count('?'), 0);
        assertEquals(splits.get(i++).count('?'), 0);

        System.out.println(i);
    }

    @Test
    public void test2() {
        List<SqlPro.StatementSplitter.Split> splits = SqlPro.of(
                "-- aaaaa\n" +
                        "select 1;\n" +
                        "-- bbb\n" +
                        "-- ccccc\n" +
                        "select 2;\n"
        ).splitter().bySemicolon().keepEmptyStatement().splitAndCount();
        for (SqlPro.StatementSplitter.Split split : splits) {
            System.out.println("---------");
            System.out.println(split.sql());
            System.out.println("\n");
        }
    }
}
