package cn.cloudself;

import cn.cloudself.helpers.Helpers;
import cn.cloudself.helpers.query.Setting;
import cn.cloudself.helpers.query.SettingQueryPro;
import cn.cloudself.helpers.query.User;
import cn.cloudself.query.QueryProSql;
import cn.cloudself.query.config.QueryProConfig;
import cn.cloudself.util.structure.ListEx;
import org.intellij.lang.annotations.Language;
import org.junit.jupiter.api.Test;

import java.util.List;

public class QueryProPlusTest {
    static {
        Helpers.initLogger();
    }

    private void prepareData() {
        @Language("SQL")
        String sql = "TRUNCATE TABLE user;" +
                "INSERT INTO user (id, name, age) VALUES (1, 'hb', 18);" +
                "INSERT INTO user (id, name, age) VALUES (2, 'hb', 10);" +
                "INSERT INTO user (id, name, age) VALUES (3, 'herb', 18);" +
                "INSERT INTO user (id, name, age) VALUES (4, 'l', null);" +
                "TRUNCATE TABLE setting;" +
                "INSERT INTO setting (id, user_id, kee, value) VALUES (1, 1, 'lang', '简体中文');" +
                "INSERT INTO setting (id, user_id, kee, value) VALUES (2, 1, 'theme', 'dark');" +
                "INSERT INTO setting (id, user_id, kee, value) VALUES (3, 2, 'lang', '繁体中文');" +
                "INSERT INTO setting (id, user_id, kee, value) VALUES (4, 4, 'lang', 'English');"
                ;
        QueryProSql.createBatch().autoSplit(sql).exec();
    }

    @Test
    public void test() { QueryProConfig.context.use(conf -> {
        conf.beautifySql(false);
        conf.logicDelete(false);
        conf.dataSource(Helpers.getDataSource());

        prepareData();

        User user1 = new User().setId(1L).setName("hb").setAge(18).setDeleted(false);
        User user2 = new User().setId(2L).setName("hb").setAge(10).setDeleted(false);
        User user3 = new User().setId(3L).setName("herb").setAge(18).setDeleted(false);
        User user4 = new User().setId(4L).setName("l").setAge(null).setDeleted(false);

        Setting setting1 = new Setting().setId(1L).setUserId(1L).setKee("lang").setValue("简体中文").setDeleted(false);
        Setting setting2 = new Setting().setId(2L).setUserId(1L).setKee("theme").setValue("dark").setDeleted(false);
        Setting setting3 = new Setting().setId(3L).setUserId(2L).setKee("lang").setValue("繁体中文").setDeleted(false);
        Setting setting4 = new Setting().setId(4L).setUserId(4L).setKee("lang").setValue("English").setDeleted(false);

        // normal 2 table
        Helpers.expectSql("SELECT `setting`.* FROM `setting` LEFT JOIN `user` ON `setting`.`user_id` = `user`.`id` WHERE `user`.`name` = ? AND `setting`.`kee` = ?", Helpers.listOf("l", "lang"));
        final List<Setting> settings1 = SettingQueryPro.plus()
                .leftJoin(User.class).on(Setting::getUserId, User::getId)
                .where().column(User::getName).eq("l")
                .and().column(Setting::getKee).eq("lang")
                .run();
        Helpers.assertEquals(settings1, Helpers.listOf(setting4));

        // normal 2 table with order by
        Helpers.expectSql("SELECT `setting`.* FROM `setting` LEFT JOIN `user` ON `setting`.`user_id` = `user`.`id` WHERE `user`.`name` = ? ORDER BY `setting`.`id` DESC", Helpers.listOf("hb"));
        final List<Setting> settings2 = SettingQueryPro.plus()
                .leftJoin(User.class).on(Setting::getUserId, User::getId)
                .where().column(User::getName).eq("hb")
                .orderBy().column(Setting::getId).desc()
                .run();
        Helpers.assertEquals(settings2, Helpers.listOf(setting3, setting2, setting1));

        Helpers.expectSql("SELECT `setting`.`value` FROM `setting` LEFT JOIN `user` ON `setting`.`user_id` = `user`.`id` WHERE `user`.`name` = ?", Helpers.listOf("hb"));
        final ListEx<String> settings3 = SettingQueryPro.plus()
                .leftJoin(User.class).on(Setting::getUserId, User::getId)
                .where().column(User::getName).eq("hb")
                .columnLimiter().column(Setting::getValue);
        Helpers.assertEquals(settings3, Helpers.listOf("简体中文", "dark", "繁体中文"));

        Helpers.expectSql("SELECT DISTINCT `user`.`age` FROM `setting` LEFT JOIN `user` ON `setting`.`user_id` = `user`.`id` WHERE `user`.`name` = ?", Helpers.listOf("hb"));
        final ListEx<Integer> users1 = SettingQueryPro.plus()
                .leftJoin(User.class).on(Setting::getUserId, User::getId)
                .where().column(User::getName).eq("hb")
                .distinct()
                .columnLimiter().column(User::getAge);
        Helpers.assertEquals(users1, Helpers.listOf(18, 10));

        Helpers.expectSql("SELECT `setting`.* FROM `setting` LEFT JOIN `user` ON `setting`.`user_id` = `user`.`id` LEFT JOIN `setting` `par` ON `setting`.`id` = `par`.`id` WHERE `setting`.`id` = ? AND `user`.`id` = ? AND `par`.`id` = ?", Helpers.listOf(1, 1, 1));
        final List<Setting> settings = SettingQueryPro.plus()
                .leftJoin(User.class).on(Setting::getUserId, User::getId)
                .leftJoin(Setting.class, "par").on("par", Setting::getId, Setting::getId)
                .where().column(Setting::getId).eq(1)
                .and().column(User::getId).eq(1)
                .and().column("par", Setting::getId).eq(1)
                .run();
        Helpers.assertEquals(settings, Helpers.listOf(setting1));

        final List<Setting> settings_j4 = SettingQueryPro.plus()
                .leftJoin(User.class).on(Setting::getUserId, User::getId)
                .leftJoin(Setting.class, "par").on("par", Setting::getId, Setting::getId)
                .leftJoin(Setting.class, "s2").on("s2", Setting::getId, Setting::getId)
                .where().column(Setting::getId).eq(1)
                .and().column(User::getId).eq(1)
                .and().column("par", Setting::getId).eq(1)
                .run();

        final List<Setting> settings_j5 = SettingQueryPro.plus()
                .leftJoin(User.class).on(Setting::getUserId, User::getId)
                .leftJoin(Setting.class, "par").on("par", Setting::getId, Setting::getId)
                .leftJoin(Setting.class, "s2").on("s2", Setting::getId, Setting::getId)
                .leftJoin(Setting.class, "s3").on("s3", Setting::getId, Setting::getId)
                .where().column(Setting::getId).eq(1)
                .and().column(User::getId).eq(1)
                .and().column("par", Setting::getId).eq(1)
                .run();

        final List<Setting> settings_j6 = SettingQueryPro.plus()
                .leftJoin(User.class).on(Setting::getUserId, User::getId)
                .leftJoin(Setting.class, "par").on("par", Setting::getId, Setting::getId)
                .leftJoin(Setting.class, "s2").on("s2", Setting::getId, Setting::getId)
                .leftJoin(Setting.class, "s3").on("s3", Setting::getId, Setting::getId)
                .leftJoin(Setting.class, "s4").on("s4", Setting::getId, Setting::getId)
                .where().column(Setting::getId).eq(1)
                .and().column(User::getId).eq(1)
                .and().column("par", Setting::getId).eq(1)
                .run();

        SettingQueryPro.plus()
                .leftJoin(User.class).on(Setting::getUserId, User::getId)
                .leftJoin(Setting.class, "par").on(Setting::getId, "par", Setting::getId);

        SettingQueryPro.plus()
                .leftJoin(Setting.class, "par").on("par", Setting::getId, Setting::getId)
                .leftJoin(User.class).on(Setting::getUserId, User::getId);
//                .where();

        SettingQueryPro.plus()
                .leftJoin(Setting.class, "par").on(Setting::getId, "par", Setting::getId)
                .leftJoin(User.class).on(User::getId, Setting::getUserId);

        SettingQueryPro.plus()
                .leftJoin(Setting.class, "par").on("par", Setting::getId, Setting::getId)
                .leftJoin(User.class).on("par", Setting::getUserId, User::getId);

        SettingQueryPro.plus()
                .leftJoin(Setting.class, "par").on(Setting::getId, "par", Setting::getId)
                .leftJoin(User.class).on(User::getId, "par", Setting::getUserId)
                .leftJoin(User.class, "u").on(User::getId, "u", User::getId);

//        SettingQueryPro.plus("par")
//                .leftJoin(Setting.class).on("par", Setting::getId, Setting::getId)
//                .leftJoin(User.class).on(Setting::getUserId, User::getId);

        SettingQueryPro.plus("par")
                .leftJoin(Setting.class).on("par", Setting::getId, Setting::getId)
                .leftJoin(User.class).on("par", Setting::getUserId, User::getId);

//        SettingQueryPro.plus("par")
//                .leftJoin(Setting.class).on(Setting::getId, "par", Setting::getId)
//                .leftJoin(User.class).on(User::getId, Setting::getUserId);

        SettingQueryPro.plus("par")
                .leftJoin(Setting.class).on(Setting::getId, "par", Setting::getId)
                .leftJoin(User.class).on(User::getId, "par", Setting::getUserId);


        SettingQueryPro.plus("s1")
                .leftJoin(Setting.class, "s2").on("s1", Setting::getId, "s2", Setting::getUserId)
                .leftJoin(User.class).on(User::getId, "s1", Setting::getUserId);

//                .leftJoinOn(Setting::getId, Setting::getId, "s2")
//                .leftJoinOn(Setting::getId, Setting::getId, "s3")
//                .leftJoinOn("s2", Setting::getUserId, User::getId)
//                .leftJoinOn(User::getId, SettingQueryPro::beautifySql)
//                .leftJoinOn(User::getId, SettingQueryPro::beautifySql)
//                .leftJoinOn(SettingQueryPro::beautifySql, User::getId)

    });}
}
