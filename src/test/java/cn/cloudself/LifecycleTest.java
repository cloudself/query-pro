package cn.cloudself;

import cn.cloudself.exception.UnSupportException;
import cn.cloudself.helpers.Helpers;
import cn.cloudself.helpers.query.User;
import cn.cloudself.helpers.query.UserQueryPro;
import cn.cloudself.query.QueryProSql;
import cn.cloudself.query.config.QueryProConfig;
import cn.cloudself.util.structure.Result;
import org.intellij.lang.annotations.Language;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

public class LifecycleTest {
    static {
        Helpers.initLogger();
    }

    private void prepareData() {
        @Language("SQL")
        String sql = "TRUNCATE TABLE user;" +
                "INSERT INTO user (id, name, age) VALUES (1, 'hb', 18);" +
                "INSERT INTO user (id, name, age) VALUES (2, 'hb', 10);" +
                "INSERT INTO user (id, name, age) VALUES (3, 'herb', 18);" +
                "INSERT INTO user (id, name, age) VALUES (4, 'l', null);" +
                "TRUNCATE TABLE setting;" +
                "INSERT INTO setting (id, user_id, kee, value) VALUES (1, 1, 'lang', '简体中文');" +
                "INSERT INTO setting (id, user_id, kee, value) VALUES (2, 1, 'theme', 'dark');" +
                "INSERT INTO setting (id, user_id, kee, value) VALUES (3, 2, 'lang', '繁体中文');";
        QueryProSql.createBatch().autoSplit(sql).exec();
    }

    @Test
    public void test() { QueryProConfig.context.use(conf -> {
        conf.beautifySql(false);
        conf.logicDelete(false);
        conf.dataSource(Helpers.getDataSource());

        prepareData();

        final AtomicInteger beforeSelect = new AtomicInteger(0);
        QueryProConfig.global
                .lifecycle()
                .beforeInsert(builder -> builder.addField("deleted", Boolean.class, () -> false).overrideField("age", Integer.class, () -> 18))
                .afterInsert(builder -> builder.addTransformer(((resultClass, result, qs, payload) -> Result.ok(result))))
                .beforeUpdate(
                        builder -> builder
                                .overrideField("update_time", Date.class, Date::new)
                                .overrideField("update_by", String.class, () -> "test")
                )
                .beforeSelect(builder -> builder.addTransformer(((resultClass, qs, payload) -> {
                    beforeSelect.getAndIncrement();
                    return Result.ok(qs);
                })));

        final String name = UserQueryPro.selectBy().id().eq(1).columnLimiter().name().findAnyOrNull();
        Assertions.assertEquals(1, beforeSelect.get());
        Assertions.assertEquals("hb", name);

        final User user = new User();
        user.setName("insert test");
        UserQueryPro.insert(user);

        final Integer age = UserQueryPro.selectBy().name().eq("insert test").columnLimiter().age().findAnyOrNull();
        Assertions.assertEquals(18, age);

    });}
}
