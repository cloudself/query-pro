package cn.cloudself;

import cn.cloudself.helpers.Helpers;
import cn.cloudself.helpers.query.Setting;
import cn.cloudself.helpers.query.SettingQueryPro;
import cn.cloudself.query.QueryProSql;
import cn.cloudself.query.config.QueryProConfig;
import org.intellij.lang.annotations.Language;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import static cn.cloudself.helpers.Helpers.*;

public class ConfigTest {
    static {
        Helpers.initLogger();
    }

    private void prepareData() {
        @Language("SQL")
        String sql = "TRUNCATE TABLE setting;\n" +
                "INSERT INTO setting (id, user_id, kee, value, deleted) VALUES (1, 1, 'lang', 'en', false);\n" +
                "INSERT INTO setting (id, user_id, kee, value, deleted) VALUES (2, 2, 'lang', 'english', false);\n" +
                "INSERT INTO setting (id, user_id, kee, value, deleted) VALUES (3, 3, 'lang', 'zh-cn', false);\n" +
                "INSERT INTO setting (id, user_id, kee, value, deleted) VALUES (4, 4, 'lang', '简体中文', false);"
                ;
        QueryProSql.createBatch().autoSplit(sql).exec();
    }

    @Test
    // 单线程配置信息
    public void singleThread() {
        QueryProConfig.global.beautifySql(false);
        QueryProConfig.global.dataSource(getDataSource());

        prepareData();

        System.out.println("code 级别的逻辑删除");
        SettingQueryPro.deleteBy().id().equalTo(1).run();
        assertEquals(
                SettingQueryPro.logicDelete(false).selectBy().id().equalTo(1).runLimit1(),
                new Setting().setId(1L).setUserId(1L).setKee("lang").setValue("en").setDeleted(true)
        );

        System.out.println("作用域仅在code");
        assertEquals(SettingQueryPro.selectBy().id().equalTo(1).runLimit1(), null);
        assertEquals(true, QueryProConfig.computed.logicDelete());
        assertEquals(SettingQueryPro.logicDelete(true).selectBy().id().equalTo(1).runLimit1(), null);
        assertEquals(true, QueryProConfig.computed.logicDelete());

        System.out.println("code + global的逻辑删除");
        QueryProConfig.global.logicDelete(true);
        SettingQueryPro.logicDelete(false).deleteBy().id().equalTo(2).run();
        assertEquals(SettingQueryPro.logicDelete(false).selectBy().id().equalTo(2).runLimit1(), null);
        assertEquals(SettingQueryPro.selectBy().id().equalTo(2).runLimit1(), null);
        assertEquals(true, QueryProConfig.computed.logicDelete());

        QueryProConfig.global.logicDelete(false);
        assertEquals(false, QueryProConfig.computed.logicDelete());
        QueryProConfig.context.use(it -> {
            assertEquals(false, QueryProConfig.computed.logicDelete());
            it.logicDelete(true);
            assertEquals(true, QueryProConfig.computed.logicDelete());
            SettingQueryPro.logicDelete(false).deleteBy().id().equalTo(3).run();
            assertEquals(SettingQueryPro.logicDelete(false).selectBy().id().equalTo(3).runLimit1(), null);
            assertEquals(SettingQueryPro.selectBy().id().equalTo(3).runLimit1(), null);
            final List<Setting> ss = QueryProSql.create("SELECT * FROM setting").query(Setting.class);
            assertEquals(ss.size(), 2);
            final Setting setting1 = new Setting().setId(1L).setUserId(1L).setKee("lang").setValue("en").setDeleted(true);
            final Setting setting2 = new Setting().setId(4L).setUserId(4L).setKee("lang").setValue("简体中文").setDeleted(false);
            assertEquals(ss, listOf(setting1, setting2));
        });

        assertEquals(false, QueryProConfig.computed.logicDelete());

        QueryProConfig.context.use(it -> {
            assertEquals(false, QueryProConfig.computed.logicDelete());
            it.logicDelete(true);
            assertEquals(true, QueryProConfig.computed.logicDelete());
        });
    }

    // 多线程测试
    @Test
    public void multiThread() throws InterruptedException {
        final List<Thread> threads = new ArrayList<>();

        for (int i = 0; i < 10000; i++) {
            final Thread thread = new Thread(() -> {
                if ((0.5) > Math.random()) {
                    QueryProConfig.context.use(it -> {
                        final boolean logicDelete = (0.5) > Math.random();
                        it.logicDelete(logicDelete);
                        Thread.sleep((long) (1000 * Math.random()));
                        assertEquals(logicDelete, QueryProConfig.computed.logicDelete());
                    });
                    assertEquals(true, QueryProConfig.computed.logicDelete());
                }
                assertEquals(true, QueryProConfig.computed.logicDelete());
            });
            threads.add(thread);
            thread.start();
        }

        for (Thread thread : threads) {
            thread.join();
        }
    }

    @Test
    public void nestedContext() {
        QueryProConfig.context.use(it -> {
            Assertions.assertTrue(QueryProConfig.computed.logicDelete());
            it.logicDelete(false);
            Assertions.assertFalse(QueryProConfig.computed.logicDelete());
            Assertions.assertFalse(QueryProConfig.computed.printLargeElementWholly());

            QueryProConfig.context.use(nested -> {
                Assertions.assertTrue(QueryProConfig.computed.logicDelete());
                Assertions.assertFalse(QueryProConfig.computed.printLargeElementWholly());
                nested.printLargeElementWholly(true);
                Assertions.assertTrue(QueryProConfig.computed.printLargeElementWholly());
            });

            Assertions.assertFalse(QueryProConfig.computed.printLargeElementWholly());
        });
    }

    @Test
    public void subThread() {
        Assertions.assertTrue(QueryProConfig.computed.logicDelete());
        QueryProConfig.global.beautifySql(true);

        // 父线程已经初始化过ThreadLocal，能正常传递至子线程
        QueryProConfig.context.use(it -> {
            Assertions.assertTrue(QueryProConfig.computed.logicDelete());
            it.logicDelete(false);
            Assertions.assertFalse(QueryProConfig.computed.logicDelete());
            Assertions.assertTrue(QueryProConfig.computed.beautifySql());
            Assertions.assertFalse(QueryProConfig.computed.printLargeElementWholly());

            final AtomicInteger passedCounts = new AtomicInteger(0);
            final Thread[] threads = new Thread[] {
                    new Thread(() -> {
                        Assertions.assertFalse(QueryProConfig.computed.logicDelete());
                        it.logicDelete(true);
                        Assertions.assertTrue(QueryProConfig.computed.logicDelete());
                        passedCounts.getAndIncrement();
                        it.beautifySql(false);
                    }),
                    new Thread(() -> {
                        try {
                            Thread.sleep(100L);
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                        Assertions.assertTrue(QueryProConfig.computed.printLargeElementWholly());
                        passedCounts.getAndIncrement();
                    })
            };
            for (Thread thread : threads) {
                thread.start();
            }
            it.printLargeElementWholly(true);
            Assertions.assertTrue(QueryProConfig.computed.printLargeElementWholly());
            for (Thread thread : threads) {
                thread.join();
            }
            assertEquals(passedCounts.get(), 2);
            Assertions.assertTrue(QueryProConfig.computed.logicDelete());
            Assertions.assertFalse(QueryProConfig.computed.beautifySql());
        });

        // 父线程未初始化过ThreadLocal，能正常传递至子线程
        QueryProConfig.context.use(it -> {
            Assertions.assertTrue(QueryProConfig.computed.beautifySql());
            Assertions.assertFalse(QueryProConfig.computed.printLargeElementWholly());
            final AtomicInteger passedCounts = new AtomicInteger(0);
            final Thread[] threads = new Thread[] {
                    new Thread(() -> {
                        Assertions.assertTrue(QueryProConfig.computed.beautifySql());
                        it.beautifySql(false);
                        Assertions.assertFalse(QueryProConfig.computed.beautifySql());
                        passedCounts.getAndIncrement();
                    }),
                    new Thread(() -> {
                        try {
                            Thread.sleep(100L);
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                        Assertions.assertTrue(QueryProConfig.computed.printLargeElementWholly());
                        passedCounts.getAndIncrement();
                    })
            };
            for (Thread thread : threads) {
                thread.start();
            }
            it.printLargeElementWholly(true);
            Assertions.assertTrue(QueryProConfig.computed.printLargeElementWholly());
            for (Thread thread : threads) {
                thread.join();
            }
            Assertions.assertFalse(QueryProConfig.computed.beautifySql());
            assertEquals(passedCounts.get(), 2);
        });
    }

    @Test
    public void subThreadGroup() {
        QueryProConfig.context.use(it -> {
            Assertions.assertTrue(QueryProConfig.computed.logicDelete());
            it.logicDelete(false);
            Assertions.assertFalse(QueryProConfig.computed.logicDelete());
            Assertions.assertTrue(QueryProConfig.computed.beautifySql());

            final int result = IntStream.range(0, 100).parallel().peek(i -> {
                it.linkToCurrentThread();
                Assertions.assertFalse(QueryProConfig.computed.logicDelete());
                it.beautifySql(false);
                Assertions.assertFalse(QueryProConfig.computed.beautifySql());
            }).sum();

            Assertions.assertFalse(QueryProConfig.computed.beautifySql());

            System.out.println(result);
        });
    }
}
