package cn.cloudself;

import cn.cloudself.helpers.Helpers;
import cn.cloudself.helpers.query.User;
import cn.cloudself.helpers.query.UserQueryPro;
import cn.cloudself.query.QueryProSql;
import cn.cloudself.query.config.QueryProConfig;
import cn.cloudself.exception.MissingParameter;
import cn.cloudself.query.psi.Const;
import org.intellij.lang.annotations.Language;
import org.junit.jupiter.api.Test;

import static cn.cloudself.helpers.Helpers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UpdateTest {
    static {
        Helpers.initLogger();
    }

    private void prepareData() {
        @Language("SQL")
        String sql =
                "TRUNCATE TABLE user;\n" +
                "INSERT INTO user (id, name, age) VALUES (1, 'hb', 18);\n" +
                "INSERT INTO user (id, name, age) VALUES (2, 'hb', 10);\n" +
                "INSERT INTO user (id, name, age) VALUES (3, 'herb', 18);\n" +
                "INSERT INTO user (id, name, age) VALUES (4, 'l', 18);\n" +
                "INSERT INTO user (id, name, age) VALUES (5, 'l', 18);\n" +
                "INSERT INTO user (id, name, age) VALUES (6, 'lhb', 18);\n" +
                "INSERT INTO user (id, name, age) VALUES (7, 'lhb', 20);"
                ;
        QueryProSql.createBatch().autoSplit(sql).exec();
    }

    @Test
    public void test() { QueryProConfig.context.use(config -> {
        config.beautifySql(false);
        config.logicDelete(false);
        config.dataSource(getDataSource());

        prepareData();

        // 如果没有传入primary key, 且无where条件, 则报错
        assertThrows(
                MissingParameter.class,
                () -> UserQueryPro.updateSet(new User().setName("hb"), false).run()
        );

        assertThrows(
                Exception.class,
                () -> UserQueryPro.updateSet().age(null).where().id().equalTo(1).run()
        );

        UserQueryPro.updateSet().age(null).name("hb").where().id().equalTo(1).run();

        assertEquals(
                UserQueryPro.selectBy().id().equalTo(1).run(),
                listOf(new User().setId(1L).setName("hb").setAge(18).setDeleted(false))
        );
        expectSql("UPDATE `user` SET `age` = ? WHERE `user`.`id` = ?", listOf(19, 1));
        assertTrue(UserQueryPro.updateSet(new User().setAge(19), false).where().id().equalTo(1).run());
        assertEquals(
                UserQueryPro.selectBy().id().equalTo(1).run(),
                listOf(new User().setId(1L).setName("hb").setAge(19).setDeleted(false))
        );


        assertEquals(
                UserQueryPro.selectBy().id().equalTo(2).run(),
                listOf(new User().setId(2L).setName("hb").setAge(10).setDeleted(false))
        );

        expectSql("UPDATE `user` SET `age` = ? WHERE `user`.`id` = ?", listOf(18, 2));
        assertTrue(UserQueryPro.updateSet(new User().setId(2L).setAge(18), false).run());

        assertEquals(
                UserQueryPro.selectBy().id().equalTo(2).run(),
                listOf(new User().setId(2L).setName("hb").setAge(18).setDeleted(false))
        );

        assertEquals(
                UserQueryPro.selectBy().id().equalTo(3).run(),
                listOf(new User().setId(3L).setName("herb").setAge(18).setDeleted(false))
        );
        expectSql("UPDATE `user` SET `age` = ? WHERE `user`.`id` = ?", listOf(19, 3));
        assertTrue(UserQueryPro.updateSet(new User().setId(3L).setAge(19), false).run());
        assertEquals(
                UserQueryPro.selectBy().id().equalTo(3).run(),
                listOf(new User().setId(3L).setName("herb").setAge(19).setDeleted(false))
        );


        assertEquals(
                UserQueryPro.selectBy().id().equalTo(4).run(),
                listOf(new User().setId(4L).setName("l").setAge(18).setDeleted(false))
        );


        expectSql("UPDATE `user` SET `deleted` = ?, `sex` = ?, `name` = ?, `age` = ? WHERE `user`.`id` = ?", listOf(false, Const.NULL, "hb", Const.NULL, 4));
        assertTrue(UserQueryPro.updateSet(new User().setId(4L).setName("hb").setDeleted(false), true).run());


        assertEquals(
                UserQueryPro.selectBy().id().equalTo(4).run(),
                listOf(new User().setId(4L).setName("hb").setAge(null).setDeleted(false))
        );

        assertEquals(
                UserQueryPro.selectBy().id().equalTo(5).run(),
                listOf(new User().setId(5L).setName("l").setAge(18).setDeleted(false))
        );
        expectSql("UPDATE `user` SET `age` = ? WHERE `user`.`id` = ?", listOf(Const.NULL, 5));
        assertTrue(UserQueryPro.updateSet().id(5).age(Const.NULL).run());
        assertEquals(
                UserQueryPro.selectBy().id().equalTo(5).run(),
                listOf(new User().setId(5L).setName("l").setAge(null).setDeleted(false))
        );

        assertEquals(
                UserQueryPro.selectBy().id().equalTo(6).run(),
                listOf(new User().setId(6L).setName("lhb").setAge(18).setDeleted(false))
        );
        assertEquals(
                UserQueryPro.selectBy().id().equalTo(7).run(),
                listOf(new User().setId(7L).setName("lhb").setAge(20).setDeleted(false))
        );
        expectSql("UPDATE `user` SET `age` = ? WHERE `user`.`name` = ?", listOf(22, "lhb"));
        assertTrue(UserQueryPro.updateSet().age(22).where().name().equalTo("lhb").run());
        assertEquals(
                UserQueryPro.selectBy().id().equalTo(6).run(),
                listOf(new User().setId(6L).setName("lhb").setAge(22).setDeleted(false))
        );
        assertEquals(
                UserQueryPro.selectBy().id().equalTo(7).run(),
                listOf(new User().setId(7L).setName("lhb").setAge(22).setDeleted(false))
        );
    });}
}
