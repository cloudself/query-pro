package cn.cloudself;

import cn.cloudself.helpers.Helpers;
import cn.cloudself.helpers.query.SettingQueryPro;
import cn.cloudself.helpers.query.User;
import cn.cloudself.helpers.query.UserQueryPro;
import cn.cloudself.query.QueryProSql;
import cn.cloudself.query.config.QueryProConfig;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static cn.cloudself.helpers.Helpers.*;

public class InsertTest {
    static {
        Helpers.initLogger();
    }

    private void prepareData() {
        QueryProSql.create("TRUNCATE table user;TRUNCATE table setting;").autoSplit().exec();
    }

    @Test
    public void test() { QueryProConfig.context.use(config -> {
        config.beautifySql(false);
        config.dataSource(getDataSource());
        prepareData();

        UserQueryPro.insert(new ArrayList<>());

        QueryProSql.create()
                .maxParameterSize(2000)
                .insert("user", new ArrayList<>());

        assertEquals(
                UserQueryPro.insert(new User().setName("hb").setAge(18)),
                1L
        );

        assertEquals(
                UserQueryPro.insert(new User().setName("hb2").setAge(18)),
                2L
        );

        // 自动分离 有id和没id的
        assertEquals(
                UserQueryPro.insert(
                        new User().setName("hb").setAge(19),
                        new User().setName("hb").setAge(20),
                        new User().setId(7L).setName("hb").setAge(21)
                ),
                listOf(3L, 4L, 7L)
        );

        assertEquals(
                UserQueryPro.insert(
                        new HashMap<String, Object>() {{ put("name", "hb"); put("age", 18); }},
                        new HashMap<String, Object>() {{ put("name", "hb2"); put("age", 18); }}
                ),
                listOf(8L, 9L)
        );

        SettingQueryPro.insert(
                new HashMap<String, Object>() {{ put("user_id", 999L); put("kee", "k1"); put("value", "v1"); put("deleted", false); }},
                new HashMap<String, Object>() {{ put("userId", 999L); put("kee", "k2"); put("value", "v2"); put("deleted", false); }}
        );
        assertEquals(
                SettingQueryPro.selectBy().userId().equalTo(999).count(),
                2L
        );

        QueryProSql.create().insert(
                "setting",
                listOf(
                        new HashMap<String, Object>() {{ put("user_id", 999L); put("kee", "k3"); put("value", "v3"); put("deleted", false); }},
                        new HashMap<String, Object>() {{ put("userId", 999L); put("kee", "k4"); put("value", "v4"); }}
                )
        );

        final List<User> users = new ArrayList<>();
        for (int i = 1; i <= 100000; i++) {
            users.add(new User().setName("u" + i).setAge(18));
        }
        UserQueryPro.insert(users);

        users.clear();
        for (int i = 1; i <= 20000; i++) {
            users.add(new User().setId((long) (i + 200000)).setName("u" + i).setAge(18));
        }
        UserQueryPro.insert(users);
    });}
}
