package cn.cloudself;

import cn.cloudself.helpers.Helpers;
import cn.cloudself.helpers.query.SettingQueryPro;
import cn.cloudself.helpers.query.User;
import cn.cloudself.helpers.query.UserQueryPro;
import cn.cloudself.query.QueryProSql;
import cn.cloudself.query.config.QueryProConfig;
import org.intellij.lang.annotations.Language;
import org.junit.jupiter.api.Test;

import java.util.List;

import static cn.cloudself.helpers.Helpers.*;

public class SelectTest {
    static {
        Helpers.initLogger();
    }

    private void prepareData() {
        @Language("SQL")
        String sql = "TRUNCATE TABLE user;" +
                "INSERT INTO user (id, name, age) VALUES (1, 'hb', 18);" +
                "INSERT INTO user (id, name, age) VALUES (2, 'hb', 10);" +
                "INSERT INTO user (id, name, age) VALUES (3, 'herb', 18);" +
                "INSERT INTO user (id, name, age) VALUES (4, 'l', null);" +
                "TRUNCATE TABLE setting;" +
                "INSERT INTO setting (id, user_id, kee, value) VALUES (1, 1, 'lang', '简体中文');" +
                "INSERT INTO setting (id, user_id, kee, value) VALUES (2, 1, 'theme', 'dark');" +
                "INSERT INTO setting (id, user_id, kee, value) VALUES (3, 2, 'lang', '繁体中文');";
        QueryProSql.createBatch().autoSplit(sql).exec();
    }

    public Boolean True = true;

    @Test
    public void test() { QueryProConfig.context.use(conf -> {
        conf.beautifySql(false);
        conf.logicDelete(false);
        conf.dataSource(Helpers.getDataSource());

        prepareData();

        User user1 = new User().setId(1L).setName("hb").setAge(18).setDeleted(false);
        User user2 = new User().setId(2L).setName("hb").setAge(10).setDeleted(false);
        User user3 = new User().setId(3L).setName("herb").setAge(18).setDeleted(false);
        User user4 = new User().setId(4L).setName("l").setAge(null).setDeleted(false);


        // primary key
        expectSql("SELECT * FROM `user` WHERE `id` = ? LIMIT 1", listOf(1));
        final User user0 = UserQueryPro.selectByPrimaryKey(1);
        assertEquals(user0, user1);

        // =
        expectSql("SELECT * FROM `user` WHERE `user`.`id` = ?", listOf(1));
        final List<User> usersRun1 = UserQueryPro.selectBy().id().equalTo(1).run();
        assertEquals(usersRun1, listOf(user1));

        // in
        expectSql("SELECT * FROM `user` WHERE `user`.`name` IN (?, ?)", listOf("hb", "herb"));
        List<User> usersRun2 = UserQueryPro.selectBy().name().in("hb", "herb").run();
        assertEquals(usersRun2, listOf(user1, user2, user3));
        expectSql("SELECT * FROM `user` WHERE `user`.`name` IN (?, ?)", listOf("hb", "herb"));
        usersRun2 = UserQueryPro.selectBy().name().in(listOf("hb", "herb")).run();
        assertEquals(usersRun2, listOf(user1, user2, user3));
        expectSql("SELECT * FROM `user` WHERE `user`.`name` IN (?, ?)", listOf("hb", "herb"));
        usersRun2 = UserQueryPro.selectBy().name("hb", "herb").run();
        assertEquals(usersRun2, listOf(user1, user2, user3));

        // >, <, >=, <=, like, between
        expectSql("SELECT * FROM `user` WHERE `user`.`age` > ?", listOf(18));
        List<User> usersRun3 = UserQueryPro.selectBy().age().graterThan(18).run();
        assertEquals(usersRun3, listOf());
        expectSql("SELECT * FROM `user` WHERE `user`.`age` < ?", listOf(10));
        usersRun3 = UserQueryPro.selectBy().age().lessThan(10).run();
        assertEquals(usersRun3, listOf());
        expectSql("SELECT * FROM `user` WHERE `user`.`age` >= ?", listOf(18));
        usersRun3 = UserQueryPro.selectBy().age().graterThanOrEqual(18).run();
        assertEquals(usersRun3, listOf(user1, user3));
        expectSql("SELECT * FROM `user` WHERE `user`.`age` <= ?", listOf(10));
        usersRun3 = UserQueryPro.selectBy().age().lessThanOrEqual(10).run();
        assertEquals(usersRun3, listOf(user2));
        expectSql("SELECT * FROM `user` WHERE `user`.`age` BETWEEN ? AND ?", listOf(18, 20));
        usersRun3 = UserQueryPro.selectBy().age().between(18, 20).run();
        assertEquals(usersRun3, listOf(user1, user3));

        // count
        expectSql("SELECT count(*) FROM `user` WHERE `user`.`id` = ?", listOf(1));
        assertEquals(
                UserQueryPro.selectBy().id().equalTo(1).count(),
                1L
        );

        // and
        expectSql("SELECT * FROM `user` WHERE `user`.`name` = ? AND `user`.`age` = ?", listOf("hb", 18));
        assertEquals(
                UserQueryPro.selectBy().name().is().equalTo("hb").and().age().is().equalTo(18).run(),
                listOf(user1)
        );

        // or
        expectSql("SELECT * FROM `user` WHERE `user`.`id` = ? OR `user`.`age` = ?", listOf(1, 10));
        final List<User> usersRun4 = UserQueryPro
                .selectBy().id().is().equalTo(1)
                .or().age().equalTo(10)
                .run();
        assertEquals(usersRun4, listOf(user1, user2));

        // not
        expectSql("SELECT * FROM `user` WHERE `user`.`id` <> ?", listOf(2));
        assertEquals(
                UserQueryPro.selectBy().id().is().not().equalTo(2).run(),
                listOf(user1, user3, user4)
        );

        // is null
        expectSql("SELECT * FROM `user` WHERE `user`.`age` IS NULL", listOf());
        assertEquals(
                UserQueryPro.selectBy().age().is().nul().run(),
                listOf(user4)
        );

        // like
        expectSql("SELECT * FROM `user` WHERE UPPER(`user`.`name`) LIKE UPPER(?)", listOf("%H%"));
        assertEquals(
                UserQueryPro.selectBy().name().ignoreCase().like("%H%").run(),
                listOf(user1, user2, user3)
        );

        expectSql("SELECT * FROM `user` WHERE `user`.`id` = ? OR (`user`.`age` = ? AND `user`.`name` LIKE ?)", listOf(1, 18, "%rb%"));
        final List<User> usersRun8 = UserQueryPro
                .selectBy().id().is().equalTo(1)
                .or((it) -> it.age().equalTo(18).and().name().like("%rb%"))
                .run();
        assertEquals(usersRun8, listOf(user1, user3));

        expectSql("SELECT * FROM `user` WHERE `user`.`name` LIKE ? ORDER BY `user`.`id` DESC", listOf("%h%"));
        assertEquals(
                UserQueryPro.selectBy().name().like("%h%").orderBy().id().desc().run(),
                listOf(user3, user2, user1)
        );

        // order by
        expectSql("SELECT * FROM `user` ORDER BY `user`.`id` DESC", listOf());
        final List<User> usersRun10 = UserQueryPro.orderBy().id().desc().run();
        assertEquals(usersRun10, listOf(user4, user3, user2, user1));

        expectSql("SELECT * FROM `user` ORDER BY `user`.`age` ASC, `user`.`id` DESC", listOf());
        final List<User> usersRun11 = UserQueryPro.orderBy().age().asc().id().desc().run();
        assertEquals(usersRun11, listOf(user4, user2, user3, user1));

        expectSql("SELECT * FROM `user` ORDER BY `user`.`age` DESC, `user`.`id` ASC LIMIT 1", listOf());
        final List<User> usersRun12 = UserQueryPro.orderBy().age().desc().id().asc().limit(1).run();
        Helpers.assertEquals(usersRun12, listOf(user1));

        // run limit 1
        expectSql("SELECT * FROM `user` LIMIT 1", listOf());
        final User userRun1 = UserQueryPro.selectBy().runLimit1();
        Helpers.assertEquals(userRun1, user1);

        // column limiter
        expectSql("SELECT `setting`.`id` FROM `setting` WHERE `setting`.`id` = ?", listOf(1));
        final List<Long> ids1 = SettingQueryPro.selectBy().id().equalTo(1).columnLimiter().id();
        assertEquals(ids1, listOf(1L));

        // columns limiter
        expectSql("SELECT `user`.`id`, `user`.`age` FROM `user` WHERE `user`.`id` = ?", listOf(1));
        final List<User> usersRun13 = UserQueryPro.selectBy().id().equalTo(1).columnsLimiter().id().age().run();
        assertEquals(usersRun13, listOf(new User().setId(user1.getId()).setName(null).setAge(user1.getAge())));

        // order by and limit 1
        expectSql("SELECT `user`.`id`, `user`.`name` FROM `user` ORDER BY `user`.`age` DESC, `user`.`id` DESC LIMIT 1", listOf());
        final List<User> usersRun14 = UserQueryPro
                .orderBy().age().desc().id().desc().limit(1)
                .columnsLimiter().id().name()
                .run();
        assertEquals(usersRun14, listOf(new User().setId(user3.getId()).setName(user3.getName()).setAge(null)));

        // take
        expectSql("SELECT * FROM `user` WHERE `user`.`id` = ? AND `user`.`name` = ?", listOf(1, "hb"));
        final List<User> usersRun15 = UserQueryPro
                .selectBy().id().equalTo(1)
                .take(it -> True ? it.name().equalTo("hb") : it.name().equalTo("hb2"))
                .run();
        assertEquals(usersRun15, listOf(user1));
    });}
}
