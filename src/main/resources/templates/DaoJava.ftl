<#-- @ftlvariable name="m" type="cn.cloudself.query.generator.TemplateModel" -->
<#assign ClassName = m._ClassName/>
<#assign EntityName = m._EntityName/>
<#assign IdType = (m.id.javaTypeStr)!"Long"/>
package ${m.packagePath};

import cn.cloudself.util.log.Log;
import cn.cloudself.util.log.LogFactory;
import ${m.entityPackage}.${EntityName};
<#if m.hasBigDecimal>import java.math.BigDecimal;
</#if><#if m.hasDate>import java.util.Date;
</#if>import cn.cloudself.query.*;
import cn.cloudself.query.psi.*;
import cn.cloudself.query.psi.structure.*;
import cn.cloudself.exception.IllegalCall;
import cn.cloudself.util.structure.ListEx;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.function.Function;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unused")
public class ${ClassName} {
    private static final Log logger = LogFactory.getLog(__Impl.class);

    private static QueryStructure defQueryStructure() {
        final QueryStructure queryStructure = new QueryStructure();
        queryStructure.from(new QueryStructureFrom(__Impl.TABLE_NAME));
        return queryStructure;
    }

    private static QueryPro<
            ${EntityName},
            ${IdType},
            __Impl.WhereField${"<"}${EntityName}, List${"<"}${EntityName}>>,
            __Impl.OrderByField${"<"}${EntityName}, List${"<"}${EntityName}>>,
            __Impl.UpdateSetField,
            __Impl.WhereField${"<"}Boolean, Boolean>,
            __Impl.WhereField${"<"}Boolean, Boolean>
    > createQuery() {
        return new QueryPro<>(
                ${ClassName}.class,
                ${EntityName}.class,
                defQueryStructure(),
                (qs, payload) -> new __Impl.WhereField<>(qs, payload, ${EntityName}.class),
                (qs, payload) -> new __Impl.OrderByField<>(qs, payload, ${EntityName}.class),
                __Impl.UpdateSetField::new,
                (qs, payload) -> new __Impl.WhereField<>(qs, payload, Boolean.class),
                (qs, payload) -> new __Impl.WhereField<>(qs, payload, Boolean.class)
        );
    }

<#list m.queryProDelegate as di>
<#list di.annotations as annotation>
    ${annotation}
</#list>
    <#--noinspection FtlReferencesInspection-->
    ${di.modifiers} <@di.returnType?interpret /> ${di.method}(<#list di.args as arg><@arg.variableType?interpret /> <#if arg.vararg>...</#if>${arg.variableName}<#sep>, </#list>) {
        <#if di.returnType != 'void'>return </#if>createQuery().${di.method}(<#list di.args as arg>${arg.variableName}<#sep>, </#list>);
    }

</#list>
    public static class __Impl {
        private static final Class${"<"}${EntityName}> CLAZZ = ${EntityName}.class;
        public static final String TABLE_NAME = "${m.db_name}";
        private static Field createField(String column) { return new Field(TABLE_NAME, column); }

        public static class WhereField${"<"}T, RUN_RES> extends AbstractWhereExpressionOperators${"<"}T, RUN_RES, WhereField${"<"}T, RUN_RES>, OrderByField${"<"}T, RUN_RES>, ColumnLimiterField${"<"}T, RUN_RES>, ColumnsLimiterField${"<"}T, RUN_RES>> {
            private final QueryStructure queryStructure;
            private final QueryPayload payload;
            private final Class${"<"}T> clazz;
            public WhereField(QueryStructure queryStructure, QueryPayload payload, Class${"<"}T> clazz) {
                super();
                this.queryStructure = queryStructure;
                this.payload = payload;
                this.clazz = clazz;
            }
            @NotNull @Override protected QueryStructure getQueryStructure() { return queryStructure; }
            @NotNull @Override public QueryPayload getPayload() { return payload; }
            @NotNull @Override public Class${"<"}T> getClazz() { return clazz; }
            @NotNull @Override protected WhereField${"<"}T, RUN_RES> createWhereField(@NotNull QueryStructure qs, @NotNull QueryPayload payload) { return new WhereField<>(qs, payload, clazz); }
            @NotNull @Override protected OrderByField${"<"}T, RUN_RES> createOrderByField(@NotNull QueryStructure qs, @NotNull QueryPayload payload) { return new OrderByField<>(qs, payload, clazz); }
            @NotNull @Override protected ColumnLimiterField${"<"}T, RUN_RES> createColumnLimitField(@NotNull QueryStructure qs, @NotNull QueryPayload payload) { return new ColumnLimiterField<>(qs, payload, clazz); }
            @NotNull @Override protected ColumnsLimiterField${"<"}T, RUN_RES> createColumnsLimitField(@NotNull QueryStructure qs, @NotNull QueryPayload payload) { return new ColumnsLimiterField<>(qs, payload, clazz); }
            @NotNull @Override protected ExpressionType getFieldType() { return ExpressionType.WHERE; }
            private Keywords${"<"}WhereField${"<"}T, RUN_RES>> createWhereField(String column) {
                return new Keywords<>(new Field(TABLE_NAME, column), queryStructure, payload, this::createWhereField);
            }
            private WhereField${"<"}T, RUN_RES> createWhereField(String column, Object[] objs) {
                final Keywords${"<"}WhereField${"<"}T, RUN_RES>> whereField = createWhereField(column);
                return objs.length == 1 ? whereField.equalTo(objs[0]) : whereField.in(objs);
            }

        <#list m.columns as field>
            @Contract(pure = true)
            public Keywords${"<"}WhereField${"<"}T, RUN_RES>> ${field.propertyName}() { return createWhereField("${field.db_name}"); }
            @Contract(pure = true)
            public WhereField${"<"}T, RUN_RES> ${field.propertyName}(List<${field.javaTypeStr}> ${field.propertyName}List) { return createWhereField("${field.db_name}", ${field.propertyName}List.toArray(new Object[0])); }
            @Contract(pure = true)
            public WhereField${"<"}T, RUN_RES> ${field.propertyName}(${field.javaTypeStr}... ${field.propertyName}s) { return createWhereField("${field.db_name}", ${field.propertyName}s); }
        </#list>

            @Contract(pure = true)
            public WhereField${"<"}T, RUN_RES> take(Function${"<"}WhereField${"<"}T, RUN_RES>, WhereField${"<"}T, RUN_RES>> factor) {
                return factor.apply(this);
            }
        }

        public abstract static class CommonField${"<"}T, RUN_RES> extends AbstractExpressionOperators${"<"}T, RUN_RES, WhereField${"<"}T, RUN_RES>, OrderByField${"<"}T, RUN_RES>, ColumnLimiterField${"<"}T, RUN_RES>, ColumnsLimiterField${"<"}T, RUN_RES>> {
            protected final QueryStructure queryStructure;
            protected final QueryPayload payload;
            protected final Class${"<"}T> clazz;
            CommonField(QueryStructure queryStructure, QueryPayload payload, Class${"<"}T> clazz) {
                super();
                this.queryStructure = queryStructure;
                this.payload = payload;
                this.clazz = clazz;
            }
            @NotNull @Override protected QueryStructure getQueryStructure() { return queryStructure; }
            @NotNull @Override public QueryPayload getPayload() { return payload; }
            @NotNull @Override public Class${"<"}T> getClazz() { return clazz; }
            @NotNull @Override protected WhereField${"<"}T, RUN_RES> createWhereField(@NotNull QueryStructure qs, @NotNull QueryPayload payload) { return new WhereField<>(qs, payload, clazz); }
            @NotNull @Override protected OrderByField${"<"}T, RUN_RES> createOrderByField(@NotNull QueryStructure qs, @NotNull QueryPayload payload) { return new OrderByField<>(qs, payload, clazz); }
            @NotNull @Override protected ColumnLimiterField${"<"}T, RUN_RES> createColumnLimitField(@NotNull QueryStructure qs, @NotNull QueryPayload payload) { return new ColumnLimiterField<>(qs, payload, clazz); }
            @NotNull @Override protected ColumnsLimiterField${"<"}T, RUN_RES> createColumnsLimitField(@NotNull QueryStructure qs, @NotNull QueryPayload payload) { return new ColumnsLimiterField<>(qs, payload, clazz); }
        }

        public static class OrderByField${"<"}T, RUN_RES> extends CommonField${"<"}T, RUN_RES> {
            public OrderByField(QueryStructure qs, QueryPayload payload, Class${"<"}T> clazz) { super(qs, payload, clazz); }
            @NotNull @Override protected ExpressionType getFieldType() { return ExpressionType.ORDER_BY; }
            private KeywordsOrderBy${"<"}OrderByField${"<"}T, RUN_RES>> createOrderByField(String column) {
                return new KeywordsOrderBy<>(new Field(TABLE_NAME, column), queryStructure, payload, super::createOrderByField);
            }

        <#list m.columns as field>
            @Contract(pure = true)
            public KeywordsOrderBy${"<"}OrderByField${"<"}T, RUN_RES>> ${field.propertyName}() { return createOrderByField("${field.db_name}"); }
        </#list>

            @Contract(pure = true)
            public OrderByField${"<"}T, RUN_RES> take(Function${"<"}OrderByField${"<"}T, RUN_RES>, OrderByField${"<"}T, RUN_RES>> factor) {
                return factor.apply(this);
            }
        }

        public static class ColumnLimiterField${"<"}T, RUN_RES> extends CommonField${"<"}T, RUN_RES> {
            public ColumnLimiterField(QueryStructure qs, QueryPayload payload, Class${"<"}T> clazz) { super(qs, payload, clazz); }
            @NotNull @Override protected ExpressionType getFieldType() { return ExpressionType.COLUMN_LIMITER; }
            private ${"<"}R> ListEx${"<"}R> createColumnLimiterField(String column, Class${"<"}R> clazz) {
                return new ListEx${"<"}>(getColumn(new Field(TABLE_NAME, column), clazz));
            }

        <#list m.columns as field>
            public ListEx${"<"}${field.javaTypeStr}> ${field.propertyName}() { return createColumnLimiterField("${field.db_name}", ${field.javaTypeStr}.class); }
        </#list>

            public ${"<"}R> R take(Function${"<"}ColumnLimiterField${"<"}T, RUN_RES>, R> factor) {
                return factor.apply(this);
            }
        }

        public static class ColumnsLimiterField${"<"}T, RUN_RES> extends CommonField${"<"}T, RUN_RES> {
            public ColumnsLimiterField(QueryStructure qs, QueryPayload payload, Class${"<"}T> clazz) { super(qs, payload, clazz); }
            @NotNull @Override protected ExpressionType getFieldType() { return ExpressionType.COLUMNS_LIMITER; }

            @SuppressWarnings("DuplicatedCode")
            private ColumnsLimiterField${"<"}T, RUN_RES> createColumnsLimiterField(String column) {
                final QueryStructure queryStructure = getQueryStructure();
                queryStructure.appendField(new Field(TABLE_NAME, column));
                return new ColumnsLimiterField${"<"}>(queryStructure, payload, clazz);
            }

        <#list m.columns as field>
            @Contract(pure = true)
            public ColumnsLimiterField${"<"}T, RUN_RES> ${field.propertyName}() { return createColumnsLimiterField("${field.db_name}"); }
        </#list>

            @Contract(pure = true)
            public ColumnsLimiterField${"<"}T, RUN_RES> take(Function${"<"}ColumnsLimiterField${"<"}T, RUN_RES>, ColumnsLimiterField${"<"}T, RUN_RES>> factor) {
                return factor.apply(this);
            }
        }

        public static class UpdateSetField extends UpdateSetDefinedExpression${"<"}WhereField${"<"}Boolean, Boolean>> {
            private final QueryStructure queryStructure;
            public UpdateSetField(QueryStructure qs, QueryPayload payload) {
                super(qs, payload, (q, p) -> new WhereField<>(q, p, Boolean.class));
                this.queryStructure = qs;
            }

            @SuppressWarnings("DuplicatedCode")
            private UpdateSetField createUpdateSetField(String key, Object value) {
                final Update update = queryStructure.update();
                if (update == null) {
                    throw new IllegalCall("usage like: UserQueryPro.updateSet().id(1).name(name).run()");
                }
                if (value == null) {
                    logger.error("null值不能直接updateSet, 使用Const.NULL代替, usage like: UserQueryPro.updateSet().name(Const.NULL).where().id(1).run()");
                }
                @SuppressWarnings("unchecked") final Map${"<"}String, Object> map = (Map${"<"}String, Object>) update.data();
                assert map != null;
                map.put(key, value);
                return this;
            }

        <#list m.columns as field>
            @Contract(pure = true)
            public UpdateSetField ${field.propertyName}(Object ${field.propertyName}) { return createUpdateSetField("${field.db_name}", ${field.propertyName}); }
        </#list>

            @Contract(pure = true)
            public UpdateSetField take(Function${"<"}UpdateSetField, UpdateSetField> factor) {
                return factor.apply(this);
            }
        }
    }
}
