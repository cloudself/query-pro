package cn.cloudself.exception;

import java.text.MessageFormat;

/**
 * The type Un support exception.
 */
public class UnSupportException extends RuntimeException {
    /**
     * Instantiates a new Un support exception.
     *
     * @param message the message
     * @param args    the args
     */
    public UnSupportException(String message, Object... args) {
        super(MessageFormat.format(message, args));
    }

    /**
     * Instantiates a new Un support exception.
     *
     * @param cause   the cause
     * @param message the message
     * @param args    the args
     */
    public UnSupportException(Throwable cause, String message, Object... args) {
        super(MessageFormat.format(message, args), cause);
    }
}