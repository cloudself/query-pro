package cn.cloudself.exception;

import java.text.MessageFormat;

/**
 * The type Illegal call.
 */
public class IllegalCall extends RuntimeException {
    /**
     * Instantiates a new Illegal call.
     *
     * @param message the message
     * @param args    the args
     */
    public IllegalCall(String message, Object... args) {
        super(MessageFormat.format(message, args));
    }

    /**
     * Instantiates a new Illegal call.
     *
     * @param cause   the cause
     * @param message the message
     * @param args    the args
     */
    public IllegalCall(Throwable cause, String message, Object... args) {
        super(MessageFormat.format(message, args), cause);
    }
}