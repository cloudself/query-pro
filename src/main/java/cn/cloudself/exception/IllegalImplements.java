package cn.cloudself.exception;

import java.text.MessageFormat;

/**
 * The type Illegal implements.
 */
public class IllegalImplements extends RuntimeException {
    /**
     * Instantiates a new Illegal implements.
     *
     * @param message the message
     * @param args    the args
     */
    public IllegalImplements(String message, Object... args) {
        super(MessageFormat.format(message, args));
    }

    /**
     * Instantiates a new Illegal implements.
     *
     * @param cause   the cause
     * @param message the message
     * @param args    the args
     */
    public IllegalImplements(Throwable cause, String message, Object... args) {
        super(MessageFormat.format(message, args), cause);
    }
}