package cn.cloudself.exception;

import java.text.MessageFormat;

/**
 * The type Illegal template.
 */
public class IllegalTemplate extends RuntimeException {
    /**
     * Instantiates a new Illegal template.
     *
     * @param message the message
     * @param args    the args
     */
    public IllegalTemplate(String message, Object... args) {
        super(MessageFormat.format(message, args));
    }

    /**
     * Instantiates a new Illegal template.
     *
     * @param cause   the cause
     * @param message the message
     * @param args    the args
     */
    public IllegalTemplate(Throwable cause, String message, Object... args) {
        super(MessageFormat.format(message, args), cause);
    }
}