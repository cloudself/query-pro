package cn.cloudself.exception;

import java.text.MessageFormat;

/**
 * The type Illegal parameters.
 */
public class IllegalParameters extends RuntimeException {
    /**
     * Instantiates a new Illegal parameters.
     *
     * @param message the message
     * @param args    the args
     */
    public IllegalParameters(String message, Object... args) {
        super(MessageFormat.format(message, args));
    }

    /**
     * Instantiates a new Illegal parameters.
     *
     * @param cause   the cause
     * @param message the message
     * @param args    the args
     */
    public IllegalParameters(Throwable cause, String message, Object... args) {
        super(MessageFormat.format(message, args), cause);
    }
}