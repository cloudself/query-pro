package cn.cloudself.exception;

import java.text.MessageFormat;

/**
 * The type Config exception.
 */
public class ConfigException extends RuntimeException {
    /**
     * Instantiates a new Config exception.
     *
     * @param message the message
     * @param args    the args
     */
    public ConfigException(String message, Object... args) {
        super(MessageFormat.format(message, args));
    }

    /**
     * Instantiates a new Config exception.
     *
     * @param cause   the cause
     * @param message the message
     * @param args    the args
     */
    public ConfigException(Throwable cause, String message, Object... args) {
        super(MessageFormat.format(message, args), cause);
    }
}