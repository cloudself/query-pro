package cn.cloudself.exception;

import java.text.MessageFormat;

/**
 * The type Missing parameter.
 */
public class MissingParameter extends RuntimeException {
    /**
     * Instantiates a new Missing parameter.
     *
     * @param message the message
     * @param args    the args
     */
    public MissingParameter(String message, Object... args) {
        super(MessageFormat.format(message, args));
    }

    /**
     * Instantiates a new Missing parameter.
     *
     * @param cause   the cause
     * @param message the message
     * @param args    the args
     */
    public MissingParameter(Throwable cause, String message, Object... args) {
        super(MessageFormat.format(message, args), cause);
    }
}