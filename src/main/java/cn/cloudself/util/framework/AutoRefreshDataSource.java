package cn.cloudself.util.framework;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.lang.NonNull;

import cn.cloudself.util.log.Log;
import cn.cloudself.util.log.LogFactory;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.function.Supplier;
import java.util.logging.Logger;

public class AutoRefreshDataSource implements DataSource {

    @Configuration
    static class Cfg implements ApplicationContextAware, ApplicationListener<ContextRefreshedEvent> {
        private static ApplicationContext applicationContext;
        private static final Log logger = LogFactory.getLog(AutoRefreshDataSource.class);
        private static DataSource dataSource;

        @Override
        public void setApplicationContext(@NonNull ApplicationContext applicationContext) throws BeansException {
            Cfg.applicationContext = applicationContext;
        }

        @Override
        public void onApplicationEvent(@NonNull ContextRefreshedEvent event) {
            dataSource = null;
        }

        public static DataSource getDataSource() {
            if (dataSource == null) {
                try {
                    dataSource = applicationContext.getBean(DataSource.class);
                } catch (BeansException e) {
                    logger.warn("获取bean失败" + e.getMessage());
                }
            }
            return dataSource;
        }
    }

    private final Supplier<DataSource> onNull;

    public AutoRefreshDataSource(Supplier<DataSource> onNull) {
        this.onNull = onNull;
    }

    private DataSource get() {
        DataSource dataSource = Cfg.getDataSource();
        if (dataSource == null) {
            return onNull.get();
        }
        return dataSource;
    }

    @Override
    public Connection getConnection() throws SQLException {
        return get().getConnection();
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return get().getConnection(username, password);
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return get().getLogWriter();
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {
        get().setLogWriter(out);
    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {
        get().setLoginTimeout(seconds);
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return get().getLoginTimeout();
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return get().getParentLogger();
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return get().unwrap(iface);
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return get().isWrapperFor(iface);
    }
}
