package cn.cloudself.util.iterator;

import java.util.*;

public class ConcatenatedIterator<T> implements Iterator<T> {
    /* The last iterator to return an element.  Calls to remove() go to this iterator. */
    private Iterator<? extends T> toRemove;

    /* The iterator currently returning elements. */
    private Iterator<? extends T> iterator;

    /*
     * We track the "meta iterators," the iterators-of-iterators, below.  Usually, topMetaIterator
     * is the only one in use, but if we encounter nested concatenations, we start a deque of
     * meta-iterators rather than letting the nesting get arbitrarily deep.  This keeps each
     * operation O(1).
     */

    private Iterator<? extends Iterator<? extends T>> topMetaIterator;

    // Only becomes nonnull if we encounter nested concatenations.
    private Deque<Iterator<? extends Iterator<? extends T>>> metaIterators;

    public static <T> T checkNotNull(T reference) {
        if (reference == null) {
            throw new NullPointerException();
        }
        return reference;
    }

    public ConcatenatedIterator(Iterator<? extends Iterator<? extends T>> metaIterator) {
        iterator = Collections.emptyIterator();
        topMetaIterator = checkNotNull(metaIterator);
    }

    // Returns a nonempty meta-iterator or, if all meta-iterators are empty, null.
    private Iterator<? extends Iterator<? extends T>> getTopMetaIterator() {
        while (topMetaIterator == null || !topMetaIterator.hasNext()) {
            if (metaIterators != null && !metaIterators.isEmpty()) {
                topMetaIterator = metaIterators.removeFirst();
            } else {
                return null;
            }
        }
        return topMetaIterator;
    }

    @Override
    public boolean hasNext() {
        while (!checkNotNull(iterator).hasNext()) {
            // this weird checkNotNull positioning appears required by our tests, which expect
            // both hasNext and next to throw NPE if an input iterator is null.

            topMetaIterator = getTopMetaIterator();
            if (topMetaIterator == null) {
                return false;
            }

            iterator = topMetaIterator.next();

            if (iterator instanceof ConcatenatedIterator) {
                // Instead of taking linear time in the number of nested concatenations, unpack
                // them into the queue
                @SuppressWarnings("unchecked")
                ConcatenatedIterator<T> topConcat = (ConcatenatedIterator<T>) iterator;
                iterator = topConcat.iterator;

                // topConcat.topMetaIterator, then topConcat.metaIterators, then this.topMetaIterator,
                // then this.metaIterators

                if (this.metaIterators == null) {
                    this.metaIterators = new ArrayDeque<>();
                }
                this.metaIterators.addFirst(this.topMetaIterator);
                if (topConcat.metaIterators != null) {
                    while (!topConcat.metaIterators.isEmpty()) {
                        this.metaIterators.addFirst(topConcat.metaIterators.removeLast());
                    }
                }
                this.topMetaIterator = topConcat.topMetaIterator;
            }
        }
        return true;
    }

    @Override
    public T next() {
        if (hasNext()) {
            toRemove = iterator;
            return iterator.next();
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void remove() {
        if (toRemove == null) {
            throw new IllegalStateException("no calls to next() since the last call to remove()");
        }
        toRemove.remove();
        toRemove = null;
    }
}