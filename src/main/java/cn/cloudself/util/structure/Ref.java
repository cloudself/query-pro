package cn.cloudself.util.structure;

import java.util.Objects;

public class Ref<R> {
    public static <R> Ref<R> of(R value) {
        return new Ref<>(value);
    }

    private R value;

    public Ref(R value) {
        this.value = value;
    }

    public R getValue() {
        return value;
    }

    public void setValue(R value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ref<?> ref = (Ref<?>) o;
        return Objects.equals(value, ref.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return "Ref<" + value + '>';
    }
}