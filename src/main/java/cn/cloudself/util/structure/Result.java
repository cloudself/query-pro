package cn.cloudself.util.structure;

import cn.cloudself.util.log.Log;
import cn.cloudself.util.log.LogFactory;

import java.io.Serializable;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Result
 */
public class Result<S, F> implements Serializable {
    private static final Log log = LogFactory.getLog(Result.class);
    private static final long serialVersionUID = 1L;

    private final boolean ok;
    private final S success;
    private final F failed;

    public Result(boolean ok, S success, F failed) {
        this.ok = ok;
        this.success = success;
        this.failed = failed;
    }

    public static <T> Result<T, Throwable> ok(T data) {
        return new Result<>(true, data, null);
    }

    public static <S> Result<S, Throwable> err(Throwable e) {
        return new Result<>(false, null, e);
    }

    /**
     * Try all.
     * <p>Each of the supplier will be called.</p>
     * <p><b>Examples:</b></p>
     * <pre>
     * {@code
     *   // all the suppliers will be called.
     *   final Result<?, Throwable> result = Result.tryAll(Arrays.asList(
     *           () -> Result.ok(123),
     *           () -> Result.err(new RuntimeException("1st error info.")),
     *           () -> Result.ok(567),
     *           () -> new Result&lt;>(false, null, "2nd error info.")
     *   ));
     *   assert !result.ok();
     *   // both 1st error info & 2nd error info will be combined to the new result.
     *   result.error();
     * }</pre>
     * @see #tryAllParallel(Iterable)
     */
    public static Result<?, Throwable> tryAll(Iterable<Supplier<Result<?, ?>>> runnable) {
        Throwable e = null;
        Result<?, Throwable> result = Result.ok(null);
        for (Supplier<Result<?, ?>> fun : runnable) {
            try {
                final Result<?, ?> res = fun.get();
                if (!res.ok()) {
                    log.info("item of try all returns an error. {}", res.failed);
                    if (e == null) {
                        e = res.errorToThrowable();
                        result = Result.err(e);
                    } else {
                        e.addSuppressed(res.errorToThrowable());
                    }
                }
            } catch (Exception ex) {
                log.error("item of try all catch an error. ", ex);
                if (e == null) {
                    e = ex;
                    result = Result.err(e);
                } else {
                    e.addSuppressed(ex);
                }
            }
        }
        return result;
    }

    /**
     * Try all's parallel version.
     * @see #tryAll(Iterable)
     */
    public static Result<?, Throwable> tryAllParallel(Iterable<Supplier<Result<?, ?>>> workers) {
        final Stream.Builder<Supplier<Result<?, ?>>> builder = Stream.builder();
        for (Supplier<Result<?, ?>> worker : workers) {
            builder.add(worker);
        }
        final List<Result<?, ?>> results = builder.build().parallel().map(fun -> {
            try {
                return fun.get();
            } catch (Exception e) {
                log.error("item of try all parallel catch an error.", e);
                return Result.err(e);
            }
        }).collect(Collectors.toList());
        Throwable e = null;
        Result<?, Throwable> result = Result.ok(null);
        for (Result<?, ?> res : results) {
            if (!res.ok()) {
                if (e == null) {
                    e = res.errorToThrowable();
                    result = Result.err(e);
                } else {
                    e.addSuppressed(res.errorToThrowable());
                }
            }
        }
        return result;
    }

    /**
     * wrapper code segment to result. any throwable will be caught and transformed.
     * <p><b>Examples:</b></p>
     * <pre>
     * {@code
     *   final Result<Object, Throwable> result = Result.fromTryCatch(() -> {
     *       throw new RuntimeException("123");
     *   });
     *   assert !result.ok();
     *   assert result.error() instanceof RuntimeException;
     *   assert result.error().getMessage().equals("123");
     * }</pre>
     */
    public static <S, RES extends Result<S, ?>> Result<S, Throwable> fromTryCatch(Supplier<RES> fun) {
        try {
            log.info("trying");
            final RES res = fun.get();
            log.info("success tried.");
            return res.toThrowable();
        } catch (Throwable e) {
            log.warn("error catch.", e);
            if (Result.configThreadLocal.get().printError) {
                System.out.println("error catch.");
                //noinspection CallToPrintStackTrace
                e.printStackTrace();
            }
            return Result.err(e);
        }
    }

    public boolean ok() {
        return ok;
    }

    /**
     * Maps a Result&lt;S, F> to Result&lt;T, F> by applying a function to a contained Ok value, leaving an Err value untouched.
     * <p><b>Examples:</b></p>
     * <pre>
     * {@code
     *   assert Result.ok(1).map(it -> it + 1).unwrap() == 2;
     *   final RuntimeException ex = new RuntimeException("ex");
     *   assert Result.err(ex).map(it -> it + "").error() == ex;
     * }</pre>
     */
    public <T> Result<T, F> map(Function<S, T> fun) {
        if (!ok()) {
            return new Result<>(ok, null, this.failed);
        }
        return new Result<>(ok, fun.apply(success), this.failed);
    }

    /**
     * Returns res if the result is Ok, otherwise returns the Err value of self.
     * <p></p>Arguments passed to and are eagerly evaluated; if you are passing the result of a function call, it is recommended to use {@link #andThen(Function)}, which is lazily evaluated.
     * <p><b>Examples:</b></p>
     * <pre>
     * {@code
     *   assert Result.ok(1).and(Result.ok(2)).unwrap() == 2;
     *   final Throwable ex1 = new Throwable();
     *   final Throwable ex2 = new Throwable();
     *   assert !Result.err(ex1).and(Result.ok(2)).ok();
     *   assert Result.err(ex1).and(Result.ok(2)).error() == ex1;
     *   assert Result.err(ex1).and(Result.err(ex2)).error() == ex1;
     * }</pre>
     * @param res another {@link Result}
     * @return Composed Result
     * @see #andThen(Function)
     */
    public <NS> Result<NS, F> and(Result<NS, ? extends F> res) {
        if (!ok()) {
            return new Result<>(ok, null, this.failed);
        }
        return res.mapError(e -> e);
    }

    /**
     * Calls `op` if the result is `{@code ok()}`, otherwise returns the [`failed`] value of `self`.
     * <p>The opposite operator is {@link #catchError(Function)}.</p>
     * <p><b>Examples:</b></p>
     * <pre>
     * {@code
     *   assert Result.ok(1).andThen(r -> Result.ok(2)).unwrap() == 2;
     *   final Throwable ex = new Throwable();
     *   assert !Result.err(ex).andThen(r -> {
     *       assert false; // will never be called.
     *       return Result.ok(2);
     *   }).ok();
     * }</pre>
     * @see #and(Result)
     * @see #catchError(Function)
     */
    public <NS> Result<NS, F> andThen(Function<S, Result<NS, ? extends F>> op) {
        if (!ok()) {
            return new Result<>(ok, null, this.failed);
        }
        final Result<NS, ? extends F> newRes = op.apply(success);
        if (newRes == null) {
            return null;
        }
        return new Result<>(true, newRes.success, newRes.failed);
    }

    /**
     * Maps a Result&lt;S, F> to Result&lt;S, NF> by applying a function to a contained Err value, leaving an ok() value untouched.
     * <p></p>This function can be used to pass through a successful result while handling an error.
     * <p><b>Examples:</b></p>
     * <pre>
     * {@code
     *   assert Result.ok(1).mapError(r -> 2).unwrap() == 1;
     *   assert Result.err(new Throwable("")).mapError(t -> 3).error() == 3;
     * }</pre>
     */
    public <NF> Result<S, NF> mapError(Function<F, NF> op) {
        if (ok()) {
            return new Result<>(ok, success, null);
        }
        final NF newFailed = op.apply(this.failed);
        return new Result<>(false, success, newFailed);
    }

    /**
     * Calls `op` if the result is `{@code !ok()}`, otherwise returns the [`success`] value of `self`.
     * <p>The opposite operator is {@link #andThen(Function)}.</p>
     * <pre>
     * {@code
     *   final Throwable ex1 = new Throwable("ex1");
     *   final Throwable ex2 = new Throwable("ex2");
     *   assert Result.err(ex1).catchError(throwable -> Result.ok(throwable.getMessage())).unwrap().equals("ex1");
     *   assert Result.err(ex1).catchError(throwable -> Result.err(ex2)).error() == ex2;
     * }</pre>
     * @see #andThen(Function)
     * @see #unwrapOrElse(Function)
     */
    public <NF> Result<S, NF> catchError(Function<F, Result<S, NF>> op) {
        if (ok()) {
            return new Result<>(ok, success, null);
        }
        return op.apply(this.failed);
    }

    public interface WithThrowFunction<T, R> {
        R apply(T t) throws Exception;
    }

    public <T> Result<T, Throwable> mapWithCatch(WithThrowFunction<S, T> fun) {
        if (!ok()) {
            return new Result<>(ok, null, this.errorToThrowable());
        }
        try {
            return new Result<>(ok, fun.apply(success), null);
        } catch (Exception e) {
            log.info("error catch when mapping ", e);
            return Result.err(e);
        }
    }

    public <NS> Result<NS, Throwable> andThenWithCatch(WithThrowFunction<S, Result<NS, ?>> fun) {
        if (!ok()) {
            return new Result<>(ok, null, this.errorToThrowable());
        }
        try {
            return fun.apply(success).toThrowable();
        } catch (Exception e) {
            log.info("error catch when merging ", e);
            return Result.err(e);
        }
    }

    public Result<S, Throwable> catchErrorWithCatch(WithThrowFunction<F, Result<S, ?>> transformer) {
        if (ok()) {
            return new Result<>(ok, success, null);
        }
        try {
            return transformer.apply(this.failed).toThrowable();
        } catch (Exception e) {
            log.info("error catch when catching ", e);
            return Result.err(e);
        }
    }

    public String toMessage(int maxLengthOfMessage) {
        return toMessage("it's ok, nil error.", maxLengthOfMessage);
    }

    public String toMessage(String defaultMsg, int maxLengthOfMessage) {
        if (ok()) {
            return defaultMsg;
        }
        final F error = this.failed;
        log.info("the result of res is an error: {}", error);
        if (error == null) {
            return "type: null";
        }
        final String message = error instanceof Throwable
                ? ((Throwable) error).getMessage()
                : String.valueOf(error);
        if (message == null) {
            return "error message: null";
        }
        if (maxLengthOfMessage == -1 || message.length() < maxLengthOfMessage) {
            return message;
        }
        final String prefix = "error catch：";
        return prefix + message.substring(0, maxLengthOfMessage - prefix.length());
    }

    public Result<S, F> copy() {
        return new Result<>(ok, success, failed);
    }

    public String toMessage() {
        return toMessage( -1);
    }

    /**
     * Returns the contained ok value, consuming the self value.
     * <p></p>Because this function may throw exception, its use is generally discouraged. Instead, prefer to use pattern matching and handle the Err case explicitly, or call {@link #unwrapOr(Object)}, {@link #unwrapOrElse(Function)}
     * <p><b>Examples:</b></p>
     * <pre>
     * {@code
     *   assert Result.ok(1).unwrap() == 1;
     *
     *   final Throwable ex = new RuntimeException("ex");
     *   try {
     *       Result.err(ex).unwrap();
     *   } catch (Throwable t) {
     *       assert t == ex;
     *   }
     * }</pre>
     * @throws RuntimeException if the result is `{@code !ok()}`
     * @see #unwrapOr(Object)
     * @see #unwrapOrElse(Function)
     */
    public S unwrap() {
        if (ok()) {
            return success;
        }
        if (failed instanceof RuntimeException) {
            throw (RuntimeException) this.failed;
        }
        if (this.failed instanceof Throwable) {
            throw new RuntimeException((Throwable) this.failed);
        }
        throw new RuntimeException(String.valueOf(this.failed));
    }

    /**
     * Returns the contained Ok value or a provided default.
     * <p></p>Arguments passed to unwrap_or are eagerly evaluated; if you are passing the result of a function call, it is recommended to use {@link #unwrapOrElse(Function)}, which is lazily evaluated.
     * <p><b>Examples:</b></p>
     * <pre>
     * {@code
     *   assert Result.ok(1).unwrapOr(2) == 1;
     *   assert Result.&lt;Integer>err(new Throwable()).unwrapOr(3) == 3;
     * }</pre>
     *
     * @see #unwrap()
     * @see #unwrapOrElse(Function)
     */
    public S unwrapOr(S def) {
        if (ok()) {
            return success;
        }
        return def;
    }

    /**
     * Returns the contained Ok value or computes it from a closure.
     * <p><b>Examples:</b></p>
     * <pre>
     * {@code
     *    assert Result.ok(1).unwrapOrElse(e -> {
     *        assert false; // will never be called.
     *        return 2;
     *    }) == 1;
     *
     *    assert Result.<Integer>err(new Throwable()).unwrapOrElse(e -> 3) == 3;
     * }</pre>
     * @see #unwrap()
     * @see #unwrapOr(Object)
     * @see #catchError(Function)
     */
    public S unwrapOrElse(Function<F, S> op) {
        if (ok()) {
            return success;
        }
        return op.apply(this.failed);
    }

    public F error() {
        return failed;
    }

    public Throwable errorToThrowable() {
        if (this.failed == null) {
            return null;
        }
        if (this.failed instanceof Throwable) {
            return (Throwable) this.failed;
        }
        return new RuntimeException("error, data: " + this.failed);
    }

    public Result<S, Throwable> toThrowable() {
        if (this.ok) {
            return Result.ok(this.success);
        }
        return Result.err(errorToThrowable());
    }

    /**
     * JavaBean field
     */
    public int getOk() {
        return ok ? 1 : 0;
    }

    /**
     * JavaBean field
     */
    public Object getData() {
        if (ok) {
            return success;
        }
        return failed;
    }

    @Override
    public String toString() {
        return ok ? ("success: " + success) : ("failed: " + failed);
    }

    private static class Cfg {
        private boolean printError = true;
    }
    private static final ThreadLocal<Cfg> configThreadLocal = ThreadLocal.withInitial(Cfg::new);
    public static void printErrorThreadLocal(boolean enable) {
        Result.configThreadLocal.get().printError = enable;
    }
}