package cn.cloudself.util.log;

import java.text.MessageFormat;

public interface Log {
    boolean isFatalEnabled();

    boolean isErrorEnabled();

    boolean isWarnEnabled();

    boolean isInfoEnabled();

    boolean isDebugEnabled();

    boolean isTraceEnabled();



    void fatal(Object message);

    void fatal(Object message, Throwable throwable);

    default void fatal(String message, Object arg1, Object... arg) {
        if (isFatalEnabled()) {
            fatal(LogHelper.format(message, arg1, arg));
        }
    }

    void error(Object message);

    void error(Object message, Throwable throwable);

    default void error(String message, Object arg1, Object... arg) {
        if (isErrorEnabled()) {
            error(LogHelper.format(message, arg1, arg));
        }
    }

    void warn(Object message);

    void warn(Object message, Throwable throwable);

    default void warn(String message, Object arg1, Object... arg) {
        if (isWarnEnabled()) {
            warn(LogHelper.format(message, arg1, arg));
        }
    }

    void info(Object message);

    void info(Object message, Throwable throwable);

    default void info(String message, Object arg1, Object... arg) {
        if (isInfoEnabled()) {
            info(LogHelper.format(message, arg1, arg));
        }
    }

    void debug(Object message);

    void debug(Object message, Throwable throwable);

    default void debug(String message, Object arg1, Object... arg) {
        if (isDebugEnabled()) {
            debug(LogHelper.format(message, arg1, arg));
        }
    }

    void trace(Object message);

    void trace(Object message, Throwable throwable);

    default void trace(String message, Object arg1, Object... arg) {
        if (isTraceEnabled()) {
            trace(LogHelper.format(message, arg1, arg));
        }
    }

    default void level(LogLevel level, String message, Object... args) {
        final String formattedMessage = MessageFormat.format(message, args);
        switch (level) {
            case TRACE: trace(formattedMessage); break;
            case DEBUG: debug(formattedMessage); break;
            case INFO: info(formattedMessage); break;
            case WARN: warn(formattedMessage); break;
            case ERROR: error(formattedMessage); break;
            case FATAL: fatal(formattedMessage); break;
        }
    }
}
