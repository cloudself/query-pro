package cn.cloudself.util.log;

import java.text.MessageFormat;

public class LogHelper {
    public static String format(String message, Object arg1, Object[] arg) {
        final Object[] objects = new Object[arg.length + 1];
        System.arraycopy(arg, 0, objects, 1, arg.length);
        objects[0] = arg1;
        return MessageFormat.format(message, objects);
    }
}
