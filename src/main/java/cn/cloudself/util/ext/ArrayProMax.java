package cn.cloudself.util.ext;

import java.nio.charset.Charset;
import java.util.Arrays;

@SuppressWarnings("DuplicatedCode")
public class ArrayProMax {
    public static ByteArrayView of(byte[] array) {
        return new ByteArrayView(array, 0, array.length);
    }
    public static ByteArrayView of(byte[] array, int offset) {
        return new ByteArrayView(array, offset, array.length - offset);
    }
    public static ByteArrayView of(byte[] array, int offset, int length) {
        return new ByteArrayView(array, offset, length);
    }
    public static class ByteArrayView {
        private final byte[] array;
        private final int offset;
        private final int length;
        private Charset charset = null;
        private boolean asString = false;
        private ByteArrayView(byte[] array, int offset, int length) {
            this.array = array;
            this.offset = offset;
            this.length = length;
        }
        public ByteArrayView charset(Charset charset) {
            this.charset = charset;
            return this;
        }
        public ByteArrayView asString(Charset charset) {
            this.charset = charset;
            this.asString = true;
            return this;
        }
        public boolean startsWith(byte... prefix) {
            final int prefixLength = prefix.length;
            if (prefixLength > length) {
                return false;
            }
            for (int i = 0; i < prefixLength; i++) {
                int j = i + offset;
                final byte a = array[j];
                final byte b = prefix[i];
                if (a != b) {
                    return false;
                }
            }
            return true;
        }
        public boolean startsWith(char... prefix) {
            final int prefixLength = prefix.length;
            if (prefixLength > length) {
                return false;
            }
            for (int i = 0; i < prefixLength; i++) {
                int j = i + offset;
                final byte a = array[j];
                final char b = prefix[i];
                if (a != b) {
                    return false;
                }
            }
            return true;
        }
        public int indexOf(byte b) {
            int iMax = offset + length;
            if (iMax > array.length) {
                iMax = array.length;
            }
            for (int i = offset; i < iMax; i++) {
                if (array[i] == b) {
                    return i;
                }
            }
            return -1;
        }

        public byte[] copy() {
            return Arrays.copyOfRange(array, offset, offset + length);
        }

        @Override
        public String toString() {
            final String main;
            if (charset != null) {
                main = new String(array, offset, length, charset);
            } else {
                main = toString(array, offset, length);
            }
            return asString ? main : ("ByteArray{" + "array=" + main + '}');
        }

        public static String toString(byte[] a, int offset, int length) {
            if (a == null)
                return "null";
            if (offset < 0 || length <= 0 || offset >= a.length) {
                return "[]";
            }
            int iMax = offset + length - 1;

            StringBuilder b = new StringBuilder();
            b.append('[');
            for (int i = offset; ; i++) {
                b.append(a[i]);
                if (i == iMax)
                    return b.append(']').toString();
                b.append(", ");
            }
        }
    }
}
