package cn.cloudself.util.ext;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Iterator;
import java.util.stream.Stream;

public class IterablePlus {
    @Nullable
    public static <E> E first(@NotNull Iterable<E> iterable) {
        final Iterator<E> iterator = iterable.iterator();
        if (iterator.hasNext()) {
            return iterator.next();
        }
        return null;
    }

    public static <E> Stream<E> downgradeToStream(@NotNull Iterator<E> iterator) {
        final Stream.Builder<E> builder = Stream.builder();
        while (iterator.hasNext()) {
            builder.add(iterator.next());
        }
        return builder.build();
    }
}
