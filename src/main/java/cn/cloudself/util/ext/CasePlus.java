package cn.cloudself.util.ext;

import java.util.regex.Pattern;

public class CasePlus {
    public static String to_snake_case(String javaName) {
        return to_snake_case_pattern.matcher(javaName).replaceAll("$1_$2").toLowerCase();
    }

    private final static Pattern to_snake_case_pattern = Pattern.compile("([a-z])([A-Z]+)");
}
