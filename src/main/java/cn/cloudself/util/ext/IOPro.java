package cn.cloudself.util.ext;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.function.Consumer;
import java.util.function.Function;

public class IOPro {

    private static final int DEFAULT_BUFFER_SIZE = 16384;

    public static IOPro fromInputStream(InputStream inputStream) {
        return fromInputStream(inputStream, false);
    }
    public static IOPro fromInputStream(InputStream inputStream, boolean autoClose) {
        return new IOPro(inputStream, autoClose);
    }
    public static IOPro fromClassPath(String path) {
        final InputStream inputStream = IOPro.class.getClassLoader().getResourceAsStream(path);
        if (inputStream == null) {
            throw new RuntimeException("either the resource could not be found, the resource is in a package that is not opened unconditionally, or access to the resource is denied by the security manager.\n" + path);
        }
        return new IOPro(inputStream, true);
    }
    @NotNull
    private final InputStream inputStream;
    private final boolean autoClose;
    private boolean closed = false;
    private IOPro(@NotNull InputStream inputStream, boolean autoClose) {
        this.inputStream = inputStream;
        this.autoClose = autoClose;
    }

    public byte[] transferToBytes() {
        try {
            return transferByBytes(ByteArrayOutputStream::toByteArray);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String transferToString() {
        return transferToString(StandardCharsets.UTF_8);
    }

    public String transferToString(Charset charset) {
        try {
            return transferByBytes(it -> {
                try {
                    return it.toString(charset.name());
                } catch (UnsupportedEncodingException e) {
                    throw new RuntimeException(e);
                }
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public <T> T transferByBytes(Function<ByteArrayOutputStream, T> transformer) throws IOException {
        try (final ByteArrayOutputStream os = new ByteArrayOutputStream()) {
            transferTo(os);
            return transformer.apply(os);
        }
    }

    public ToFileTransformer transferToFile() {
        return new ToFileTransformer(this);
    }

    public static class ToFileTransformer {
        private final IOPro ioPro;
        ToFileTransformer(IOPro ioPro) {
            this.ioPro = ioPro;
        }
        public CouldTransfer file(File file) {
            return new CouldTransfer(this, file);
        }
        public ByPath byPath(@NotNull String path) {
            return new ByPath(this, path);
        }

        public static class CouldTransfer {
            ToFileTransformer toFileTransformer; File file;
            CouldTransfer(ToFileTransformer toFileTransformer, File file) {
                this.toFileTransformer = toFileTransformer;
                this.file = file;
            }
            public void transform() throws IOException {
                final FileOutputStream fos;
                try {
                    fos = new FileOutputStream(file);
                } catch (Throwable e) {
                    toFileTransformer.ioPro.close(e);
                    throw e;
                }
                try {
                    toFileTransformer.ioPro.transferTo(fos);
                } finally {
                    fos.close();
                }
            }
        }

        public static class ByPath {
            ToFileTransformer toFileTransformer; String path;
            ByPath(ToFileTransformer toFileTransformer, String path) {
                this.toFileTransformer = toFileTransformer;
                this.path = path;
            }
            public CouldTransfer override() {
                return transfer(file -> {
                    //noinspection ResultOfMethodCallIgnored
                    file.mkdirs();
                    if (file.exists()) {
                        if (!file.delete()) {
                            throw new RuntimeException("override failed.");
                        }
                    }
                });
            }
            private CouldTransfer transfer(Consumer<File> fileConsumer) {
                if (path == null) {
                    final NullPointerException e = new NullPointerException("path");
                    toFileTransformer.ioPro.close(e);
                    throw e;
                }
                final File file = new File(path);
                try {
                    fileConsumer.accept(file);
                } catch (Throwable e) {
                    toFileTransformer.ioPro.close(e);
                    throw e;
                }
                return new CouldTransfer(toFileTransformer, file);
            }
        }
    }

    @SuppressWarnings("UnusedReturnValue")
    public long transferTo(@NotNull OutputStream out) throws IOException {
        long transferred = 0;
        final byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        Throwable e = null;
        try {
            int read;
            while ((read = inputStream.read(buffer, 0, DEFAULT_BUFFER_SIZE)) >= 0) {
                out.write(buffer, 0, read);
                if (transferred < Long.MAX_VALUE) {
                    try {
                        transferred = Math.addExact(transferred, read);
                    } catch (ArithmeticException ignore) {
                        transferred = Long.MAX_VALUE;
                    }
                }
            }
        } catch (Throwable t) {
            e = t;
            throw t;
        } finally {
            close(e);
        }
        return transferred;
    }

    private void close(@Nullable Throwable e) {
        if (!autoClose || closed) {
            return;
        }
        try {
            inputStream.close();
            closed = true;
        } catch (IOException ex) {
            if (e != null) {
                e.addSuppressed(ex);
            } else {
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    @SuppressWarnings("removal")
    protected void finalize() throws Throwable {
        super.finalize();
        if (autoClose && !closed) {
            inputStream.close();
        }
    }
}
