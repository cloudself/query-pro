package cn.cloudself.util.ext;

import cn.cloudself.exception.UnSupportException;
import cn.cloudself.util.log.Log;
import cn.cloudself.util.log.LogFactory;
import cn.cloudself.util.structure.Ref;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Bean代理，
 * 支持生成三种类型的数据:
 * <ul>
 *   <li>Map</li>
 *   <li>基本对象</li>
 *   <li>JavaBean</li>
 * </ul>
 * <i>BeanProxy</i>的主要目的是虚拟出一个对象.
 * <br/>
 *
 * <p>Usages:</p>
 * <ul>
 *   <li>使用{@link EntityProxy}.{@link EntityProxy#fromClass(Class)}或{@link EntityProxy}.{@link EntityProxy#fromBean}构造该对象</li>
 *   <li>使用{@link EntityProxy}.{@link EntityProxy#newInstance()}创建临时对象{@link BeanInstance}</li>
 *   <li>使用{@link BeanInstance}.{@link BeanInstance#setProperty}设置目标对象的属性</li>
 *   <li>使用{@link BeanInstance}.{@link BeanInstance#getPropertyType}获取目标某字段的类型</li>
 *   <li>使用{@link BeanInstance}.{@link BeanInstance#toBean}将临时对象转为目标对象</li>
 * </ul>
 * <p>Examples:</p>
 * <pre>{@code
 * // from bean
 * EntityProxy.BeanInstance<T, T> instance = EntityProxy.fromBean(bean_or_map);
 * String no = (String) instance.getProperty("no");
 * instance.setProperty("no", no == null ? genNo() : no);
 *
 * // from class
 * final EntityProxy<ZzUser> entityProxy = EntityProxy.fromClass(ZzUser.class);
 * final BeanInstance<ZzUser, ZzUser> beanInstance = entityProxy.newInstance();
 * beanInstance.setProperty("user_name", "my name");
 * final ZzUser zzUser = beanInstance.toBean();
 *
 * // parsed object
 * final Map<String, EntityProxy.Parser.Parsed.Column> columns = EntityProxy.fromClass(ZzUser.class).parsed().columns;
 * }</pre>
 *
 * <p>注意点：</p>
 * <p>内置一个HashMap&lt;Class, EntityProxy>作为缓存，该缓存是不会自动垃圾回收，常规场景(例如SpringBoot)下不会存在问题，因为
 * <ol>
 *     <li>虚拟机本来就很少回收.class对象, 往往发生回收(FullGC)时，内存已经不足了，该优化了</li>
 *     <li>本身Entity.class对象和EntityProxy不会占用很多内存。</li>
 * </ol>
 * <br/>
 * 但是以下场景需要手动清空缓存: <br/>
 * <ul>
 *   <li>Entity是动态生成的，而且很多</li>
 *   <li>单Tomcat存在多个应用，且应用存在退出行为，而此时，Tomcat容器仍在运行。该场景会存在一定的内存泄漏，如果应用退出行为非常频繁，可能会发生内存不足。可以实现ServletContextListener，在contextDestroyed时调用EntityProxy.removeCaches(k -> true)清空缓存</li>
 * </ul>
 */
public abstract class EntityProxy<RES> {
    private static final LruCache<Class<?>, EntityProxy<?>> beanProxyCaches = new LruCache<>(512);
    public static void setLruCacheSize(int size) {
        beanProxyCaches.resize(size);
    }

    private static final Log logger = LogFactory.getLog(EntityProxy.class);

    public static Predicate<Class<?>> areBasicType;

    public static Supplier<Set<String>> shouldIgnoreFields = HashSet::new;

//    public static <T> EntityProxy<T> fromType() {
//
//    }

    @NotNull
    public static <T> EntityProxy<T> fromClass(@NotNull final Class<T> clazz) {
        final EntityProxy<?> cachedEntityProxy = beanProxyCaches.get(clazz);
        if (cachedEntityProxy != null) {
            //noinspection unchecked
            return (EntityProxy<T>) cachedEntityProxy;
        }

        final EntityProxy<T> entityProxy;
        if (Map.class.isAssignableFrom(clazz)) {
            //noinspection rawtypes,unchecked
            entityProxy = new MapProxy<>((Class) clazz);
        } else if (areBasicType != null && areBasicType.test(clazz)) {
            entityProxy = new BasicTypeProxy<>(clazz);
        } else {
            entityProxy = new JavaBeanProxy<>(clazz);
        }

        beanProxyCaches.put(clazz, entityProxy);
        return entityProxy;
    }

    /**
     * 构造一个BeanInstance
     *
     * @param instance 只能为`Java Bean`, 不能为`Map`, 如需为`Map`, 参考{@link #fromBean(Map, Class)}
     */
    public static <BEAN> BeanInstance<BEAN, BEAN> fromBean(BEAN instance) {
        //noinspection unchecked
        final Class<BEAN> clazz = (Class<BEAN>) instance.getClass();
        return new BeanInstance<>(instance, fromClass(clazz));
    }

    /**
     * 构造一个BeanInstance
     *
     * @param instance 可以为Map或者JavaBean
     * @param refer 该Map参考的`Bean Class`
     */
    public static <M extends Map<? super String, ?>, BEAN> BeanInstance<BEAN, M> fromBean(M instance, Class<BEAN> refer) {
        return fromClass(refer).newMapInstance(instance);
    }

    protected final Class<RES> clazz;
    public abstract BeanInstance<RES, RES> newInstance();
    public abstract <INSTANCE> BeanInstance<RES, INSTANCE> newInstance(INSTANCE instance);
    public <M extends Map<? super String, ?>> BeanInstance<RES, M> newMapInstance(M map) {
        return newInstance(map);
    }
    public BeanInstance<RES, Map<String, Object>> newMapInstance() {
        return newMapInstance(new HashMap<>());
    }

    /**
     * <h2>代理实例对象</h2>
     * 一般通过{@link #getProperty(String)}, {@link #setProperty(Object, String, Object)}操作实例对象，通过{@link #toBean()} 解代理以获取最终对象
     *
     * @param <BEAN> 被代理的对象，比如某个JavaBean
     * @param <RES> 返回结果，比如Map
     */
    public static class BeanInstance<BEAN, RES> {
        /**
         * instance 可能为BEAN的实例，也可能为map
         */
        private final RES instance;
        private final EntityProxy<BEAN> proxy;

        public BeanInstance(@NotNull RES instance, @NotNull EntityProxy<BEAN> proxy) {
            this.instance = instance;
            this.proxy = proxy;
        }

        /**
         * 设置一个属性，该方法不会自动转换value的类型。
         * <br/>
         * 如代理对象的`id`是`Long`或者`Int`类型，现在有一个`BigDecimal`类型的值，如何将这个`BigDecimal`设置到`id`上呢？
         * <br/>
         * 参考如下代码：
         * <pre>
         * {@code
         *     BeanInstance&lt;Object> instance = BeanProxy.fromBean(bean);
         *     String field_name = "id";
         *
         *     Object typedValue = new ObjectMapper().convertValue(bigDecimalId, instance.getPropertyType(field_name));
         *     instance.setProperty(field_name, typedValue);
         * }</pre>
         */
        @NotNull
        public BeanInstance<BEAN, RES> setProperty(@NotNull String db_field_name, @Nullable Object value) {
            proxy.setProperty(instance, db_field_name, value);
            return this;
        }

        /**
         * get prop
         */
        @Nullable
        public Object getProperty(@NotNull String db_field_name) {
            return proxy.getProperty(instance, db_field_name);
        }

        /**
         * 获取某属性的类型
         */
        @Nullable
        public Class<?> getPropertyType(@NotNull String db_field_name) {
            if (proxy instanceof MapProxy) {
                final Object val = ((Map<?, ?>) instance).get(db_field_name);
                if (val == null) {
                    return null;
                }
                return val.getClass();
            }
            return proxy.getPropertyType(db_field_name);
        }

        /**
         * 获取bean
         */
        @Nullable
        public RES toBean() {
            //noinspection unchecked
            return (RES) proxy.toResult(instance);
        }

        /**
         * 获取bean的类型
         */
        public Class<BEAN> getBeanType(){
            return proxy.clazz;
        }

        public Parser.Parsed parsed() {
            if (proxy instanceof JavaBeanProxy) {
                return ((JavaBeanProxy<?>) proxy).parsed();
            }
            throw new RuntimeException("非JavaBean不支持获取parsed对象");
        }

        /**
         * 以Map.Entry的形式迭代。
         */
        @NotNull
        public Iterator<Map.Entry<String/*db_name*/, Object>> iterator() {
            if (proxy instanceof JavaBeanProxy) {
                final Map<String, Parser.Parsed.Column> columns = ((JavaBeanProxy<?>) proxy).parsed().columns;
                final Iterator<Map.Entry<String, Parser.Parsed.Column>> iterator = columns.entrySet().iterator();
                return new Iterator<Map.Entry<String, Object>>() {
                    @Override
                    public boolean hasNext() {
                        return iterator.hasNext();
                    }

                    @Override
                    public Map.Entry<String, Object> next() {
                        final Map.Entry<String, Parser.Parsed.Column> next = iterator.next();
                        return new AbstractMap.SimpleEntry<>(next.getKey(), next.getValue().getter().get(instance));
                    }
                };
            }
            if (proxy instanceof BasicTypeProxy) {
                final Map.Entry<String, Object> entry = new AbstractMap.SimpleEntry<>("value", proxy.getProperty(instance, "value"));
                return Collections.singleton(entry).iterator();
            }
            if (instance instanceof Map) {
                final Map<?, ?> mapInstance = (Map<?, ?>) instance;
                final Iterator<? extends Map.Entry<?, ?>> iterator = mapInstance.entrySet().iterator();
                return new Iterator<Map.Entry<String, Object>>() {
                    @Override
                    public boolean hasNext() {
                        return iterator.hasNext();
                    }

                    @Override
                    public Map.Entry<String, Object> next() {
                        final Map.Entry<?, ?> next = iterator.next();
                        final Object key = next.getKey();
                        if (key instanceof String) {
                            return new AbstractMap.SimpleEntry<>((String) key, next.getValue());
                        }
                        throw new UnSupportException("不支持非Map<String, ?>类型的Map");
                    }
                };
            }

            throw new UnSupportException("不支持的Proxy类型");
        }
    }

    public static void flushCaches(Class<?>... classes) {
        for (Class<?> clazz : classes) {
            beanProxyCaches.remove(clazz);
            Parser.removeCaches(classes);
        }
    }

    /* *******************
     * private functions *
     ******************* */

    /**
     * 设置属性
     */
    protected abstract void setProperty(@NotNull Object obj, @NotNull String prop, @Nullable Object val);
    /**
     * 获取属性
     */
    @Nullable
    protected abstract Object getProperty(@NotNull Object obj, @NotNull String prop);

    /**
     * 获取某属性的类型
     * 当clazz为Map类型时，返回null
     */
    @Nullable
    protected abstract Class<?> getPropertyType(@NotNull String prop);
    /**
     * 转为目标对象
     */
    @Nullable
    protected abstract RES toResult(@NotNull Object obj);

    protected EntityProxy(Class<RES> clazz) {
        this.clazz = clazz;
    }

    private static class MapProxy<MAP extends Map<? super String, ?>> extends EntityProxy<MAP> {
        protected MapProxy(@NotNull Class<MAP> clazz) {
            super(clazz);
        }

        @NotNull
        @Override
        public BeanInstance<MAP, MAP> newInstance() {
            if (clazz.isAssignableFrom(LinkedHashMap.class)) {
                //noinspection unchecked
                return newMapInstance((MAP) new LinkedHashMap<String, Object>());
            }
            if (clazz.isAssignableFrom(HashMap.class)) {
                //noinspection unchecked
                return newMapInstance((MAP) new HashMap<String, Object>());
            }
            try {
                return newMapInstance(clazz.getDeclaredConstructor().newInstance());
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException |
                     NoSuchMethodException e) {
                throw new UnSupportException(e, "{0} 没有找到无参构造函数，该类是一个JavaBean吗, " +
                        "对于Kotlin，需使用kotlin-maven-noarg生成默认的无参构造函数" +
                        "或在生成工具中配置QueryProFileMaker.disableKtNoArgMode()来生成默认的构造函数；" +
                        "对于Java，需检查是否存在类似@Nonnull的注解，lombok等工具可能对其有特殊处理", clazz);
            }
        }

        @Override
        public <INSTANCE> BeanInstance<MAP, INSTANCE> newInstance(INSTANCE instance) {
            return new BeanInstance<>(instance, this);
        }

        @SuppressWarnings("rawtypes")
        private Map asMap(Object obj) {
            if (obj == null) {
                throw new RuntimeException("被代理对象不能为null ");
            }
            if (!(obj instanceof Map)) {
                throw new RuntimeException("非map类型的实例不能使用MapProxy");
            }
            return (Map) obj;
        }

        @Override
        protected void setProperty(@NotNull Object obj, @NotNull String prop, @Nullable Object val) {
            //noinspection unchecked
            asMap(obj).put(prop, val);
        }

        @Nullable
        @Override
        protected Object getProperty(@NotNull Object obj, @NotNull String prop) {
            return asMap(obj).get(prop);
        }

        @Nullable
        @Override
        protected Class<?> getPropertyType(@NotNull String prop) {
            return null;
        }

        @Nullable
        @Override
        protected MAP toResult(@NotNull Object obj) {
            //noinspection unchecked
            return (MAP) obj;
        }
    }

    private static class BasicTypeProxy<BASIC> extends EntityProxy<BASIC> {

        protected BasicTypeProxy(@NotNull Class<BASIC> clazz) {
            super(clazz);
        }

        @Override
        public BeanInstance<BASIC, BASIC> newInstance() {
            //noinspection RedundantCast,unchecked
            return (BeanInstance<BASIC, BASIC>) new BeanInstance<>(new Ref<>(null), this);
        }

        @Override
        public <INSTANCE> BeanInstance<BASIC, INSTANCE> newInstance(INSTANCE instance) {
            return new BeanInstance<>(instance, this);
        }

        @Override
        protected void setProperty(@NotNull Object obj, @NotNull String prop, @Nullable Object val) {
            if (obj instanceof Ref<?>) {
                //noinspection unchecked,rawtypes
                ((Ref) obj).setValue(val);
            } else if (obj instanceof Map) {
                //noinspection rawtypes,unchecked
                ((Map) obj).put("value", val);
            } else {
                throw new RuntimeException("BasicTypeProxy.setProperty不支持除了Ref和Map以外的类型");
            }
        }

        @Nullable
        @Override
        protected Object getProperty(@NotNull Object obj, @NotNull String prop) {
            if (obj instanceof Ref<?>) {
                return ((Ref<?>) obj).getValue();
            } else if (obj instanceof Map) {
                //noinspection rawtypes
                return ((Map) obj).get("value");
            } else {
                throw new RuntimeException("BasicTypeProxy.getProperty不支持除了Ref和Map以外的类型");
            }
        }

        @Nullable
        @Override
        protected Class<?> getPropertyType(@NotNull String prop) {
            return clazz;
        }

        @Nullable
        @Override
        @SuppressWarnings("unchecked")
        protected BASIC toResult(@NotNull Object obj) {
            if (obj instanceof Ref<?>) {
                return (BASIC) ((Ref<?>) obj).getValue();
            }
            return (BASIC) obj;
        }
    }

    private static class JavaBeanProxy<UNKNOWN> extends EntityProxy<UNKNOWN> {
        private final Parser.Parsed parsed;
        private final Map<String, Parser.Parsed.Column> columns;

        protected JavaBeanProxy(@NotNull Class<UNKNOWN> clazz) {
            super(clazz);
            parsed = Parser.of(clazz).parse();
            columns = parsed.columns;
        }

        @Override
        public BeanInstance<UNKNOWN, UNKNOWN> newInstance() {
            try {
                return new BeanInstance<>(clazz.getDeclaredConstructor().newInstance(), this);
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException |
                     NoSuchMethodException e) {
                throw new UnSupportException(e, "{0} 没有找到无参构造函数，该类是一个JavaBean吗, " +
                        "对于Kotlin，需使用kotlin-maven-noarg生成默认的无参构造函数" +
                        "或在生成工具中配置QueryProFileMaker.disableKtNoArgMode()来生成默认的构造函数；" +
                        "对于Java，需检查是否存在类似@Nonnull的注解，lombok等工具可能对其有特殊处理", clazz);
            }
        }

        @Override
        public <INSTANCE> BeanInstance<UNKNOWN, INSTANCE> newInstance(INSTANCE instance) {
            return new BeanInstance<>(instance, this);
        }

        @NotNull
        protected Parser.Parsed parsed() {
            return parsed;
        }

        @Override
        protected void setProperty(@NotNull Object obj, @NotNull String prop, @Nullable Object val) {
            final Parser.Parsed.Column column = columns.get(prop);
            if (column == null) {
                logger.warn("给定的类型" + clazz.getSimpleName() + "中不存在字段" + prop + ", 该字段的赋值已跳过。");
                return;
            }
            column.setter().set(obj, val);
        }

        @Nullable
        @Override
        protected Object getProperty(@NotNull Object obj, @NotNull String prop) {
            final Parser.Parsed.Column column = columns.get(prop);
            if (column == null) {
                logger.warn("没有找到该字段 {0}", prop);
                return null;
            }
            return column.getter().get(obj);
        }

        @Nullable
        @Override
        protected Class<?> getPropertyType(@NotNull String prop) {
            final Parser.Parsed.Column column = columns.get(prop);
            if (column == null) {
                return null;
            }
            return column.javaType;
        }

        @Nullable
        @Override
        protected UNKNOWN toResult(@NotNull Object obj) {
            //noinspection unchecked
            return (UNKNOWN) obj;
        }
    }

    /**
     * <h2>Entity or Bean's parser</h2>
     * <p>参考Jvm {@link Introspector#getBeanInfo(Class)}</p>
     * <p>相比jvm.beans更适合解析Entity对象</p>
     *
     * <p>注意点：</p>
     * <p>内置一个HashMap&lt;Class, Parsed>实现的缓存，该缓存不会自动回收，常规场景(例如SpringBoot)下不会存在问题，因为本身Entity.class对象和Parsed不会占用很多内存。</p>
     * <br/>
     *
     * 但是以下场景需要手动清空缓存: <br/>
     * <ul>
     * <li>Entity是动态生成的，而且很多</li>
     * <li>单Tomcat存在多个应用，且应用存在退出行为，而此时，Tomcat容器仍在运行。该场景可以实现ServletContextListener，在contextDestroyed时调用Parser.removeCaches(k -> true)清空缓存</li>
     * </ul>
     */
    public static class Parser {
        /**
         * 解析一个JavaBean，<br/>
         * 获取它的字段名，字段类型，各字段的Getter和Setter的代理方法等信息。<br/>
         * 对于一个符合的JPA规范的Entity，还可以额外获得：<br/>
         * <ul>
         *   <li>id字段, 默认是id或者@{@link javax.persistence.Id}注解标注的字段，不支持复合主键。</li>
         *   <li>数据库字段名(如无@{@link javax.persistence.Column}注解，默认给出下划线风格的字段名)等信息，你可以配置：{@link #defaultPropertyNameToDbName(Function)}}更改这一行为。</li>
         * </ul>
         * @param clazz 不能为{@code Map.class}
         */
        public static Parser of(Class<?> clazz) {
            if (clazz == null) {
                throw new IllegalArgumentException("参数clazz不能为空");
            }
            if (Map.class.isAssignableFrom(clazz)) {
                throw new RuntimeException("不支持解析Map.class, 使用Parser.of(map object)替换当前的Parser.of(Map.class)");
            }
            return new Parser(clazz);
        }

        /**
         * 解析一个map
         *  <p> Examples:
         *  <blockquote><pre>
         *  Parser.of(User.class).parse().getColumnByDbFieldName("name").setter().set(map, "hb");
         *  </pre></blockquote>
         * @deprecated 不建议使用Parser.of(map)，可以尝试解析具体的Bean,解析结果的Getter和Setter是同时支持map类型的:Parser.of(BeanClass.class).parse().getColumnByDbFieldName("create_by").setter().set(map, username)
         * @see #of(Class)
         */
        @Deprecated
        public static MapParser of(Map<?, ?> map) {
            return new MapParser(map);
        }

        @NotNull
        private final Class<?> clazz;
        @Nullable
        private Parsed parsedEntity;
        public Parsed parse() {
            if (parsedEntity == null) {
                final Parsed parsed = parseEntityClass(clazz);
                parsedEntity = parsed;
                CACHES.put(clazz, parsed);
            }
            return parsedEntity;
        }
        public static class MapParser {
            private final @NotNull Map<?, ?> map;
            private Function<Object, String> nonStringKeyToString = null;
            private MapParser(@NotNull Map<?, ?> map) {
                this.map = map;
            }
            public MapParser nonStringKeyToString(Function<Object, String> nonStringKeyToString) {
                this.nonStringKeyToString = nonStringKeyToString;
                return this;
            }
            public Map<String, EntityProxy.Parser.Parsed.Column> parse() {
                final Map<String, EntityProxy.Parser.Parsed.Column> parsed = new HashMap<>();
                for (Map.Entry<?, ?> entry : map.entrySet()) {
                    final Object key = entry.getKey();
                    final String javaName;
                    if (key instanceof String) {
                        javaName = (String) key;
                    } else if (nonStringKeyToString != null) {
                        javaName = nonStringKeyToString.apply(key);
                    } else {
                        javaName = null;
                    }
                    if (javaName == null) {
                        throw new RuntimeException("Parser仅支持Map<String, ?>");
                    }
                    final String dbName = CasePlus.to_snake_case(javaName);
                    parsed.put(dbName, getColumn(entry, dbName, javaName));
                }
                return parsed;
            }

            @NotNull
            private static Parsed.Column getColumn(Map.Entry<?, ?> entry, String dbName, String javaName) {
                final Object value = entry.getValue();
                final Class<?> javaType = value == null ? Void.class : value.getClass();
                return new Parsed.Column(
                        dbName,
                        javaName,
                        javaType,
                        (obj, val) -> {
                            if (!(obj instanceof Map)) {
                                throw new RuntimeException("ParsedMap不支持非map类型的setter");
                            }
                            //noinspection unchecked
                            ((Map<Object, Object>) obj).put(dbName, val);
                        },
                        (obj) -> {
                            if (!(obj instanceof Map)) {
                                throw new RuntimeException("ParsedMap不支持非map类型的getter");
                            }
                            //noinspection unchecked
                            return ((Map<Object, Object>) obj).get(dbName);
                        }
                );
            }
        }

        private Parser(@NotNull Class<?> clazz) {
            this.clazz = clazz;
        }

        public static class Parsed {
            public interface Setter {
                void set(Object o, Object v);
            }
            public interface Getter {
                Object get(Object o);
            }
            public static class Column {
                @NotNull private final String name;
                @NotNull private final String javaName;
                @NotNull private final Class<?> javaType;
                @NotNull private final Setter setter;
                @NotNull private final Getter getter;
                @Nullable private Boolean primary;
                @Nullable private Boolean hasDefault;

                public Column(@NotNull String name, @NotNull String javaName, @NotNull Class<?> javaType, @NotNull Setter setter, @NotNull Getter getter) {
                    this.name = name;
                    this.javaName = javaName;
                    this.javaType = javaType;
                    this.setter = setter;
                    this.getter = getter;
                }

                @NotNull
                public String name() {
                    return name;
                }

                @NotNull
                public String javaName() {
                    return javaName;
                }

                @NotNull
                public Class<?> javaType() {
                    return javaType;
                }

                @NotNull
                public Setter setter() {
                    return setter;
                }

                @NotNull
                public Getter getter() {
                    return getter;
                }

                public boolean isPrimary() {
                    return primary == null ? "id".equals(name) : primary;
                }

                public Column setPrimary(@Nullable Boolean primary) {
                    this.primary = primary;
                    return this;
                }

                @Nullable
                public Boolean getHasDefault() {
                    return hasDefault;
                }

                public Column setHasDefault(@Nullable Boolean hasDefault) {
                    this.hasDefault = hasDefault;
                    return this;
                }

                @Override
                public String toString() {
                    return "Column{" +
                            "name='" + name + '\'' +
                            ", javaType=" + javaType +
                            '}';
                }
            }
            @Nullable private final Column id;
            @NotNull private final String name;
            @NotNull private final Map<String/*dbName*/, Column> columns;
            @NotNull private final Map<String/*dbName*/, Column> columnsByJavaName;

            public Parsed(@Nullable Column id, @NotNull String name, @NotNull Map<String, Column> columns) {
                this.id = id;
                this.name = name;
                this.columns = columns;
                this.columnsByJavaName = new HashMap<>();
                for (Column value : columns.values()) {
                    columnsByJavaName.put(value.javaName, value);
                }
            }

            @NotNull
            public Map<String, Column> columns() {
                return columns;
            }

            @Nullable
            public Column id() {
                return id;
            }

            @NotNull
            public String name() {
                return name;
            }

            @Nullable
            public Column getColumnByDbFieldName(String name) {
                return columns.get(name);
            }
            @Nullable
            public Column getColumnByJavaPropertyName(String name) {
                return columnsByJavaName.get(name);
            }

            @Override
            public String toString() {
                return "Parsed{" +
                        "columns=" + columns +
                        '}';
            }
        }

        private static final LruCache<Class<?>, Parsed> CACHES = new LruCache<>(512);
        private static final String KEYWORDS_SUFFIX = "_$col$umn";

        public static void removeCaches(Class<?>... classes) {
            for (Class<?> clazz : classes) {
                CACHES.remove(clazz);
            }
        }

        private Parsed parseEntityClass(Class<?> clazz) {
            final Map<String, Parsed.Column> columns = new HashMap<>();
            String idColumn = null;
            String idColumnMay = null;

            final Table tableAnnotation = clazz.getAnnotation(Table.class);

            final String tableAnnotationName = tableAnnotation == null ? null : tableAnnotation.name();

            final String dbNameForTable = tableAnnotationName == null ? classNameToDbName.apply(clazz.getName()) : tableAnnotationName;

            final Map<String, PropertyDescriptor> propertyDescriptorMap = new HashMap<>();
            try {
                final PropertyDescriptor[] propertyDescriptors = Introspector.getBeanInfo(clazz).getPropertyDescriptors();
                for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
                    propertyDescriptorMap.put(propertyDescriptor.getName(), propertyDescriptor);
                }
            } catch (IntrospectionException e) {
                throw new RuntimeException(e);
            }

            Class<?> classOrSuperClass = clazz;
            while (classOrSuperClass != null) {
                for (Field field : classOrSuperClass.getDeclaredFields()) {
                    final String fieldName = field.getName();
                    if (ignoreFields().contains(fieldName)) {
                        continue;
                    }

                    @Nullable final Id idAnnotation = field.getAnnotation(Id.class);
                    @Nullable final Column columnAnnotation = field.getAnnotation(Column.class);
                    final String columnName = columnAnnotation == null ? null : columnAnnotation.name();
                    final String dbName = columnName == null ? propertyNameToDbName.apply(fieldName) : columnName;
                    if (idAnnotation != null) {
                        if (idColumn != null) {
                            // TODO 这里不能抛异常
                            throw new UnSupportException("不支持联合主键");
                        }
                        idColumn = dbName;
                    }
                    if ("id".equals(dbName)) {
                        idColumnMay = dbName;
                    }

                    final Method setter = getSetterMethod(clazz, fieldName, field.getType(), propertyDescriptorMap);
                    final Method getter = getGetterMethod(clazz, fieldName, propertyDescriptorMap);

                    final Parsed.Column column = new Parsed.Column(
                            dbName,
                            fieldName,
                            field.getType(),
                            (o, v) -> {
                                if (o instanceof Map) {
                                    //noinspection unchecked,rawtypes
                                    ((Map) o).put(dbName, v);
                                    return;
                                }
                                if (setter != null) {
                                    try {
                                        setter.invoke(o, v);
                                    } catch (InvocationTargetException | IllegalAccessException e) {
                                        throw new RuntimeException(e);
                                    }
                                } else {
                                    try {
                                        field.set(o, v);
                                    } catch (IllegalAccessException e) {
                                        throw new UnSupportException("无法访问私有且无setter的属性 {0}", fieldName);
                                    }
                                }
                            },
                            o -> {
                                if (o instanceof Map) {
                                    final Map<?, ?> map = (Map<?, ?>) o;
                                    if (map.containsKey(fieldName)) {
                                        return map.get(fieldName);
                                    }
                                    return map.get(dbName);
                                }
                                if (getter != null) {
                                    try {
                                        return getter.invoke(o);
                                    } catch (IllegalAccessException | InvocationTargetException e) {
                                        throw new RuntimeException(e);
                                    }
                                } else {
                                    try {
                                        return field.get(o);
                                    } catch (IllegalAccessException e) {
                                        throw new UnSupportException("无法访问私有且无getter的属性 {0}", fieldName);
                                    }
                                }
                            }
                    ).setPrimary(false);
                    columns.put(dbName, column);
                }
                classOrSuperClass = classOrSuperClass.getSuperclass();
            }

            Introspector.flushCaches();

            final Parsed.Column id = columns.get(idColumn == null ? idColumnMay : idColumn);
            id.setPrimary(true);
            return new Parsed(id, dbNameForTable, columns);
        }

        private Function<String, String> classNameToDbName = CasePlus::to_snake_case;
        private Function<String, String> propertyNameToDbName = CasePlus::to_snake_case;

        public Parser defaultClassNameToDbName(Function<String, String> classNameToDbName) {
            this.classNameToDbName = classNameToDbName;
            return this;
        }

        public Parser defaultPropertyNameToDbName(Function<String, String> propertyNameToDbName) {
            this.propertyNameToDbName = propertyNameToDbName;
            return this;
        }

        private final Set<String> _addedIgnoreFields = new HashSet<>();
        @Nullable private Set<String> ignoreFields;
        @NotNull
        private Set<String> ignoreFields() {
            if (ignoreFields == null) {
                ignoreFields = new HashSet<>(EntityProxy.shouldIgnoreFields.get());
                ignoreFields.addAll(_addedIgnoreFields);
            }
            return ignoreFields;
        }

        public Parser addIgnoreFields(String[] fields) {
            Collections.addAll(_addedIgnoreFields, fields);
            return this;
        }

        @Nullable
        private Method getSetterMethod(Class<?> clazz, String fieldName, Class<?> fieldType, Map<String, PropertyDescriptor> propertyDescriptorMap) {
            Method setterMethod = null;
            // normal style
            final PropertyDescriptor propertyDescriptor = propertyDescriptorMap.get(fieldName);
            if (propertyDescriptor != null) {
                final Method writeMethod = propertyDescriptor.getWriteMethod();
                if (writeMethod != null) {
                    return writeMethod;
                }
            }
            try {
                setterMethod = clazz.getMethod("set" + Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1), fieldType);
            } catch (Exception ignored) {
            }
            if (setterMethod != null) {
                return setterMethod;
            }
            // record style
            try {
                setterMethod = clazz.getMethod(fieldName, fieldType);
            } catch (Exception ignored) {
            }
            if (setterMethod != null) {
                return setterMethod;
            }
            // filed name = keywords
            if (!fieldName.endsWith(KEYWORDS_SUFFIX)) {
                return null;
            }
            final String newSuffix = Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1, fieldName.length() - KEYWORDS_SUFFIX.length());
            try {
                setterMethod = clazz.getMethod("set" + newSuffix, fieldType);
            } catch (Exception ignored) {
            }
            if (setterMethod != null) {
                return setterMethod;
            }
            try {
                setterMethod = clazz.getMethod(fieldName.substring(0, fieldName.length() - KEYWORDS_SUFFIX.length()), fieldType);
            } catch (Exception ignored) {
            }
            return setterMethod;
        }

        

        @Nullable
        private Method getGetterMethod(Class<?> clazz, String fieldName, Map<String, PropertyDescriptor> propertyDescriptorMap) {
            Method getter = null;
            // normal style
            final PropertyDescriptor propertyDescriptor = propertyDescriptorMap.get(fieldName);
            if (propertyDescriptor != null) {
                final Method readMethod = propertyDescriptor.getReadMethod();
                if (readMethod != null) {
                    return readMethod;
                }
            }
            // record style
            try {
                getter = clazz.getMethod(fieldName);
            } catch (Exception ignored) {
            }
            if (getter != null) {
                return getter;
            }
            // filed name = keywords
            if (!fieldName.endsWith(KEYWORDS_SUFFIX)) {
                return null;
            }
            final String newSuffix = Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1, fieldName.length() - KEYWORDS_SUFFIX.length());
            try {
                getter = clazz.getMethod("get" + newSuffix);
            } catch (Exception ignored) {
            }
            if (getter != null) {
                return getter;
            }
            try {
                getter = clazz.getMethod("is" + newSuffix);
            } catch (Exception ignored) {
            }
            if (getter != null) {
                return getter;
            }
            try {
                getter = clazz.getMethod(fieldName.substring(0, fieldName.length() - KEYWORDS_SUFFIX.length()));
            } catch (Exception ignored) {
            }
            return getter;
        }
    }
}
