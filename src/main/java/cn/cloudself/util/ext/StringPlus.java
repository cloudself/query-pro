package cn.cloudself.util.ext;

public class StringPlus {
    public static String onlyAZaz_(String str) {
        final char[] chars = str.toCharArray();
        final char[] newChars = new char[chars.length];
        for (int i = chars.length - 1; i >= 0; i--) {
            final char c = chars[i];
            newChars[i] = (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') ? c : '_';
        }
        return new String(newChars);
    }

    public static void main(String[] args) {
        final String str = StringPlus.onlyAZaz_("aAaMaa_%+aaa_a*_b");
        System.out.println("aAaMaa___aaa_a__b".equals(str));
    }
}
