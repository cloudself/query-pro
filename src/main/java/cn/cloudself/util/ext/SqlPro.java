package cn.cloudself.util.ext;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.stream.Collectors;

public class SqlPro {
    private final String sql;
    private SqlPro(String sql) {
        this.sql = sql;
    }
    public static SqlPro of(String sql) {
        return new SqlPro(sql);
    }

    private enum CommentTag {
        NormalOneLine,
        HashOneLine,
        MultiLine,
    }

    public static class SplitterUnInit {
        private final String sql;

        public SplitterUnInit(String sql) {
            this.sql = sql;
        }

        public StatementSplitter bySemicolon() {
            return new StatementSplitter(sql, ';');
        }

        public MsSchemaAndTable msTableToSchemaAndTable() {
            return MsSchemaAndTable.bySqlPart(sql);
        }
    }

    public static class MsSchemaAndTable {
        public static MsSchemaAndTable bySqlPart(String name) {
            final int len = name.length();
            int i = 0;
            int first = name.charAt(i++);
            if (first == '[') {
                while (i < len) {
                    if (']' == name.charAt(i++)) {
                        if (i == len) {
                            return new MsSchemaAndTable(null, name);
                        }
                        final char c = name.charAt(i);
                        if (c == '.') {
                            return new MsSchemaAndTable(name.substring(0, i), name.substring(i + 1));
                        }
                        if (c == ']') {
                            i++;
                        }
                    }
                }
                return new MsSchemaAndTable(null, name);
            }
            final int dotIndex = name.indexOf('.');
            return new MsSchemaAndTable(name.substring(0, dotIndex), name.substring(dotIndex + 1));
        }
        @Nullable
        private final String schema;
        @NotNull
        private final String table;
        private MsSchemaAndTable(@Nullable String schema, @NotNull String table) {
            this.schema = schema;
            this.table = table;
        }

        @Nullable
        public String schema() {
            return schema;
        }

        @NotNull
        public String table() {
            return table;
        }

        @Override
        public String toString() {
            return "MsSchemaAndTable{" +
                    "schema='" + schema + '\'' +
                    ", table='" + table + '\'' +
                    '}';
        }
    }

    public static class StatementSplitter {
        public static class Split {
            private final boolean empty;
            private final String sql;
            private final Map<Character, Integer> counts;

            private Split(String sql, Map<Character, Integer> counts, boolean empty) {
                this.sql = sql;
                this.counts = counts;
                this.empty = empty;
            }
            public String sql() {
                return sql;
            }
            public int count(char c) {
                return counts.get(c);
            }
            public boolean empty() {
                return empty;
            }

            @Override
            public String toString() {
                return "Split{" +
                        (empty ? "empty, " : "") +
                        "sql='" + sql + '\'' +
                        ", counts=" + counts +
                        '}';
            }
        }
        private final String sql;
        private boolean keepEmptyStatement = false;
        private final Character splitter;
        private final List<Character> charsForCounter = new ArrayList<>();
        public StatementSplitter(String sql, Character splitter) {
            this.sql = sql;
            this.splitter = splitter;
        }

        public StatementSplitter registerCounter(char c) {
            charsForCounter.add(c);
            return this;
        }

        public StatementSplitter keepEmptyStatement() {
            this.keepEmptyStatement = true;
            return this;
        }

        public List<String> split() {
            return splitAndCount().stream().map(Split::sql).collect(Collectors.toList());
        }

        private boolean lookbehindIsNotEscape(int i) {
            if (i == 0) {
                return true;
            }
            if (i == 1) {
                return sql.charAt(0) != '\\';
            }
            return sql.charAt(i - 1) != '\\' || sql.charAt(i - 2) == '\\';
        }

        /**
         * 按照;切割sql，返回值是 sql与sql中?个数的键值对。
         * 注意，默认会自动合并纯注释语句到上一个sql语句中，纯注释sql会返回空集。可以使用keepEmptyStatement阻止这一行为
         */
        public List<Split> splitAndCount() {
            final boolean keepEmptyStatement = this.keepEmptyStatement;
            final List<Split> splits = new ArrayList<>();

            StringBuilder chars = new StringBuilder();
            Map<Character, Integer> counts = new HashMap<>();
            for (Character c : charsForCounter) {
                counts.put(c, 0);
            }

            boolean emptySql = true;
            boolean endOfComment;
            boolean inComment = false;
            CommentTag commentTag = null;
            boolean inString = false;
            Character quota = null;

            final char[] charsOfSql = sql.toCharArray();
            final int charsLength = charsOfSql.length;
            for (int i = 0; i < charsLength; i++) {
                final char c = charsOfSql[i];
                chars.append(c);

                endOfComment = false;
                switch (c) {
                    case '\'': case '"': case '`':
                        if (!inComment) {
                            if (!inString) {
                                inString = true;
                                quota = c;
                            } else {
                                if (quota == c && lookbehindIsNotEscape(i)) {
                                    inString = false;
                                    quota = null;
                                }
                            }
                        }
                        break;
                    case '/':
                        if (!inString) {
                            if (!inComment) {
                                if (charsLength == i + 1) {
                                    throw new RuntimeException("unexpect eof after char '/', check the sql statements.");
                                }
                                if (charsOfSql[i + 1] == '*') {
                                    inComment = true;
                                    commentTag = CommentTag.MultiLine;
                                }
                            } else {
                                if (commentTag == CommentTag.MultiLine) {
                                    if (charsOfSql[i - 1] == '*') {
                                        if (lookbehindIsNotEscape(i - 1)) {
                                            inComment = false;
                                            commentTag = null;
                                            endOfComment = true;
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    case '-':
                        if (!inString && !inComment) {
                            if (i > 0 && charsOfSql[i - 1] == '-') {
                                inComment = true;
                                commentTag = CommentTag.NormalOneLine;
                            }
                        }
                        break;
                    case '#':
                        if (!inString && !inComment) {
                            inComment = true;
                            commentTag = CommentTag.HashOneLine;
                        }
                        break;
                    case '\n':
                        if (inComment && commentTag != CommentTag.MultiLine) {
                            endOfComment = true;
                            inComment = false;
                            commentTag = null;
                        }
                        break;
                }

                for (Character character : charsForCounter) {
                    if (c == character) {
                        if (!inComment && !inString) {
                            counts.put(character, counts.get(character) + 1);
                        }
                    }
                }

                if ((c == splitter) || (emptySql && endOfComment)) {
                    if (!inComment && !inString) {
                        splits.add(new Split(chars.toString(), counts, emptySql));

                        chars = new StringBuilder();

                        emptySql = true;
                        // inComment, inString must be false, and commentTag will reassign while inComment to be true
                        // quota must be null

                        counts = new HashMap<>();
                        for (Character character : charsForCounter) {
                            counts.put(character, 0);
                        }
                    }
                } else {
                    if (emptySql && !inComment && c > ' '/*0x20*/ && c != '-') {
                        emptySql = false;
                    }
                }
            }

            splits.add(new Split(chars.toString(), counts, emptySql));

            if (!keepEmptyStatement) {
                boolean allEmpty = true;
                final LinkedList<Split> merged = new LinkedList<>();
                for (Split split : splits) {
                    if (split.empty && !merged.isEmpty()) {
                        final Split last = merged.removeLast();
                        final String lastSql = last.sql;
                        final String lastSqlOmitSplitter = lastSql.substring(0, lastSql.length() - 1);
                        merged.add(new Split(lastSqlOmitSplitter + split.sql, last.counts, false));
                    } else {
                        if (!split.empty) {
                            allEmpty = false;
                        }
                        merged.add(split);
                    }
                }
                if (allEmpty) {
                    return new ArrayList<>();
                } else {
                    return merged;
                }
            }
            return splits;
        }
    }

    public SplitterUnInit splitter() {
        return new SplitterUnInit(sql);
    }

    public static void main(String[] args) {
        System.out.println(SqlPro.of("dbo.test").splitter().msTableToSchemaAndTable());
        System.out.println(SqlPro.of("[test]").splitter().msTableToSchemaAndTable());
        System.out.println(SqlPro.of("[dbo].[test]").splitter().msTableToSchemaAndTable());
        System.out.println(SqlPro.of("[d.bo].[test]").splitter().msTableToSchemaAndTable());
        System.out.println(SqlPro.of("[d.b]]o].[test]").splitter().msTableToSchemaAndTable());
    }
}
