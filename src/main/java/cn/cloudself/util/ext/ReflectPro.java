package cn.cloudself.util.ext;

import cn.cloudself.util.structure.Result;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class ReflectPro {
    @Nullable
    private final Object obj;
    @Nullable
    private final Class<?> clazz;
    private ReflectPro(@Nullable Object obj) {
        this(obj, null);
    }
    private ReflectPro(@Nullable Object obj, @Nullable Class<?> clazz) {
        this.obj = obj;
        this.clazz = clazz;
    }

    public static ReflectPro of(Object obj) {
        return new ReflectPro(obj);
    }

    public static ReflectPro of(Class<?> clazz) {
        return new ReflectPro(null, clazz);
    }

    public static FieldExt ofField(Field field) {
        return new FieldExt(field);
    }

    public ReflectPro invoke(String methodName, Object... args) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        final Object result;
        if (clazz != null) {
            result = invokePro(getMethod(clazz, methodName, args), null, args);
        } else {
            if (obj == null) {
                throw new NullPointerException();
            }
            result = invokePro(getMethod((obj).getClass(), methodName, args), obj, args);
        }
        return new ReflectPro(result);
    }

    public Object getResult() {
        return obj;
    }

    @Nullable
    private Object invokePro(@NotNull Method method, @Nullable Object obj, Object... args) throws InvocationTargetException, IllegalAccessException {
        method.setAccessible(true);
        final Parameter[] parameters = method.getParameters();
        Parameter lastParameter = null;
        final boolean isVarArgs;
        if (parameters.length == 0) {
            isVarArgs = false;
        } else {
            lastParameter = parameters[parameters.length - 1];
            isVarArgs = lastParameter.isVarArgs();
        }

        if (isVarArgs) {
            final List<Object> argList = Arrays.stream(args).collect(Collectors.toList());
            final List<Object> newArgs = argList.subList(0, parameters.length - 1);

            final Class<?> lastParameterType = lastParameter.getType().getComponentType();
            final int argListSize = argList.size();
            final Object varArgs = Array.newInstance(lastParameterType, argListSize - parameters.length + 1);

            int j = 0;
            for (int i = parameters.length - 1; i < argListSize; i++, j++) {
                Array.set(varArgs, j, argList.get(i));
            }
            newArgs.add(varArgs);
            return method.invoke(obj, newArgs.toArray());
        } else {
            return method.invoke(obj, args);
        }
    }

    private Method getMethod(Class<?> clazz, String methodName, Object... args) throws NoSuchMethodException {
        // 假设args是参数列表，查找对应的方法
        AtomicBoolean findByMethodsMode = new AtomicBoolean(false);
        final Class<?>[] argsType = Arrays.stream(args).map(it -> {
            if (it == null) {
                findByMethodsMode.set(true);
                return null;
            }
            return it.getClass();
        }).toArray(Class[]::new);

        if (findByMethodsMode.get()) {
            return getMethodUseMethodsMode(clazz, methodName, argsType);
        }
        try {
            return clazz.getMethod(methodName, argsType);
        } catch (NoSuchMethodException e) {
            return getMethodUseMethodsMode(clazz, methodName, argsType);
        }
    }

    private Method getMethodUseMethodsMode(
            @NotNull Class<?> clazz,@NotNull String methodName, Class<?>... args
    ) throws NoSuchMethodException {
        final List<Method> methods = Arrays.stream(clazz.getMethods())
                .filter(it -> methodName.equals(it.getName()))
                .collect(Collectors.toList());
        if (methods.isEmpty()) {
            throw new NoSuchMethodException(clazz.getName() + "." + methodName);
        }
        if (methods.size() == 1) {
            return methods.get(0);
        }
        final List<Method> filteredMethods = methods.stream().filter(it -> {
            final LinkedList<Class<?>> argTypes = new LinkedList<>(Arrays.asList(args));
            for (Parameter parameter : it.getParameters()) {
                if (parameter.isVarArgs()) { // 肯定是最后一个参数了
                    final Class<?> parameterType = parameter.getType().getComponentType();
                    for (Class<?> argType : argTypes) {
                        if (!isArgMatchParam(argType, parameterType)) {
                            return false;
                        }
                    }
                    return true;
                }
                if (argTypes.isEmpty()) {
                    return false;
                }
                final Class<?> argType = argTypes.get(0);
                argTypes.removeFirst();
                if (!isArgMatchParam(argType, parameter.getType())) {
                    return false;
                }
            }
            return argTypes.isEmpty(); // 空了，完全匹配了，则保留
        }).collect(Collectors.toList());

        if (filteredMethods.isEmpty()) {
            final String s = Arrays.stream(args).map(Class::toString).collect(Collectors.joining(", "));
            throw new NoSuchMethodException(clazz.getName() + "." + methodName + "(" +  s + ")");
        }
        if (filteredMethods.size() == 1) {
            return filteredMethods.get(0);
        }
        return filteredMethods.stream().filter(it -> {
                    final Parameter[] parameters = it.getParameters();
                    return !parameters[parameters.length - 1].isVarArgs();
                })
                .findAny().orElseGet(() -> filteredMethods.get(0));
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean isArgMatchParam(@Nullable Class<?> argType, @NotNull Class<?> parameterType) {
        if (argType == null) {
            return true;
        }
        if (parameterType.isAssignableFrom(argType)) {
            return true;
        }
        if (argType.isPrimitive() || parameterType.isPrimitive()) {
            //noinspection RedundantIfStatement
            if (toPrimitiveType(argType) == toPrimitiveType(parameterType)) {
                return true;
            }
        }
        return false;
    }

    public static class FieldExt {
        private final Field field;
        private FieldExt(Field field) {
            this.field = field;
        }
        public Result<Object, Throwable> tryGet(@Nullable Object o) {
            try {
                return Result.ok(this.field.get(o));
            } catch (IllegalAccessException e) {
                return Result.err(e);
            }
        }
        public Result<Void, Throwable> trySet(@Nullable Object o, @Nullable Object v) {
            try {
                this.field.set(o, v);
                return Result.ok(null);
            } catch (IllegalAccessException e) {
                return Result.err(e);
            }
        }
    }

    @SuppressWarnings("DuplicatedCode")
    private Class<?> toPrimitiveType(Class<?> clazz) {
        switch (clazz.getName()) {
            case "java.lang.Boolean"   : return boolean.class;
            case "java.lang.Character" : return char.class;
            case "java.lang.Byte"      : return byte.class;
            case "java.lang.Short"     : return short.class;
            case "java.lang.Integer"   : return int.class;
            case "java.lang.Float"     : return float.class;
            case "java.lang.Long"      : return long.class;
            case "java.lang.Double"    : return double.class;
            case "java.lang.Void"      : return void.class;
            default                    : return null;
        }
    }
}
