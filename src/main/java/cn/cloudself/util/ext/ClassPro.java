package cn.cloudself.util.ext;

import org.jetbrains.annotations.Nullable;

import java.lang.annotation.Annotation;

/**
 * ClassPro
 */
public class ClassPro {
    public static ClassPro of(@Nullable Class<?> clazz) {
        return new ClassPro(clazz);
    }

    @Nullable
    private final Class<?> clazz;
    private ClassPro(@Nullable Class<?> clazz) {
        this.clazz = clazz;
    }

    /**
     * 是否能强制转换为整型
     * @return true 能, false 不能
     */
    public boolean compatibleWithInt() {
        if (clazz == null) {
            return false;
        }
        return Integer.class.isAssignableFrom(clazz) || int.class.isAssignableFrom(clazz);
    }

    /**
     * 是否能强制转换为布尔值
     * @return true 能, false 不能
     */
    public boolean compatibleWithBool() {
        if (clazz == null) {
            return false;
        }
        return Boolean.class.isAssignableFrom(clazz) || boolean.class.isAssignableFrom(clazz);
    }

    /**
     * 将包装类转为基本类型
     * @return 包装类: 基本类型, 其它: null,
     */
    public Class<?> toPrimitiveType() {
        if (clazz == null) {
            return null;
        }
        //noinspection DuplicatedCode
        switch (clazz.getName()) {
            case "java.lang.Boolean"   : return boolean.class;
            case "java.lang.Character" : return char.class;
            case "java.lang.Byte"      : return byte.class;
            case "java.lang.Short"     : return short.class;
            case "java.lang.Integer"   : return int.class;
            case "java.lang.Float"     : return float.class;
            case "java.lang.Long"      : return long.class;
            case "java.lang.Double"    : return double.class;
            case "java.lang.Void"      : return void.class;
            default                    : return null;
        }
    }

    /**
     * 将基本类型转为包装类
     * @return 基本类型：包装类，其它：类本身
     */
    public Class<?> toObjectType() {
        if (clazz == null) {
            return null;
        }
        switch (clazz.getName()) {
            case "boolean" : return Boolean.class;
            case "char"    : return Character.class;
            case "byte"    : return Byte.class;
            case "short"   : return Short.class;
            case "int"     : return Integer.class;
            case "float"   : return Float.class;
            case "long"    : return Long.class;
            case "double"  : return Double.class;
            case "void"    : return Void.class;
            default        : return clazz;
        }
    }
}
