package cn.cloudself.query;

import cn.cloudself.query.config.*;
import cn.cloudself.query.config.store.HashMapStore;
import cn.cloudself.query.config.IQueryProConfig;
import cn.cloudself.query.config.store.Store;
import cn.cloudself.exception.IllegalCall;
import cn.cloudself.query.plus.Alias;
import cn.cloudself.query.plus.QueryProPlus;
import cn.cloudself.query.psi.AbstractExpressionOperators;
import cn.cloudself.query.psi.Const;
import cn.cloudself.query.psi.QueryFieldCreator;
import cn.cloudself.query.psi.UpdateSetDefinedExpression;
import cn.cloudself.query.psi.annotations.PureContract;
import cn.cloudself.query.psi.structure.*;
import cn.cloudself.util.ext.EntityProxy;
import cn.cloudself.util.ext.IterablePlus;
import cn.cloudself.util.structure.ListEx;
import cn.cloudself.util.log.Log;
import cn.cloudself.util.log.LogFactory;
import cn.cloudself.util.log.LogLevel;
import cn.cloudself.util.structure.Pair;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.*;
import java.util.stream.Collectors;

public class QueryPro<
        T,
        ID,
        SELECT_BY_FIELD extends AbstractExpressionOperators<?, ?, ?, ?, ?, ?>,
        ORDER_BY_FIELD extends AbstractExpressionOperators<?, ?, ?, ?, ?, ?>,
        UPDATE_SET_FIELD extends UpdateSetDefinedExpression<UPDATE_BY_FIELD>,
        UPDATE_BY_FIELD extends AbstractExpressionOperators<?, ?, ?, ?, ?, ?>,
        DELETE_BY_FIELD extends AbstractExpressionOperators<?, ?, ?, ?, ?, ?>
> implements IQueryProConfig.Writeable<QueryPro<T, ID, SELECT_BY_FIELD, ORDER_BY_FIELD, UPDATE_SET_FIELD, UPDATE_BY_FIELD, DELETE_BY_FIELD>> {
    private final static Log logger = LogFactory.getLog(QueryPro.class);
    private final Class<T> clazz;
    private final QueryStructure queryStructure;
    private final EntityProxy.Parser.Parsed parsed;
    private final QueryFieldCreator<SELECT_BY_FIELD> createSelectByField;
    private final QueryFieldCreator<ORDER_BY_FIELD> createOrderByField;
    private final QueryFieldCreator<UPDATE_SET_FIELD> createUpdateSetField;
    private final QueryFieldCreator<UPDATE_BY_FIELD> createUpdateByField;
    private final QueryFieldCreator<DELETE_BY_FIELD> createDeleteByField;
    private final QueryPayload payload;
    private final CodeStoreDbWriteOnly<QueryPro<T, ID, SELECT_BY_FIELD, ORDER_BY_FIELD, UPDATE_SET_FIELD, UPDATE_BY_FIELD, DELETE_BY_FIELD>> codeStoreDbWriteOnly;
    public QueryPro(
            Class<?> queryProClass,
            Class<T> clazz,
            QueryStructure queryStructure,
            QueryFieldCreator<SELECT_BY_FIELD> createSelectByField,
            QueryFieldCreator<ORDER_BY_FIELD> createOrderByField,
            QueryFieldCreator<UPDATE_SET_FIELD> createUpdateSetField,
            QueryFieldCreator<UPDATE_BY_FIELD> createUpdateByField,
            QueryFieldCreator<DELETE_BY_FIELD> createDeleteByField
    ) {
        this.clazz = clazz;
        this.queryStructure = queryStructure;
        this.createSelectByField = createSelectByField;
        this.createOrderByField = createOrderByField;
        this.createUpdateSetField = createUpdateSetField;
        this.createUpdateByField = createUpdateByField;
        this.createDeleteByField = createDeleteByField;
        this.parsed = EntityProxy.Parser.of(clazz).parse();
        this.payload = new QueryPayload(clazz, queryProClass, new HashMapStore());
        this.codeStoreDbWriteOnly = new CodeStoreDbWriteOnly<>(payload.configs(), this);
    }

    @NotNull
    @Override
    public QueryPro<T, ID, SELECT_BY_FIELD, ORDER_BY_FIELD, UPDATE_SET_FIELD, UPDATE_BY_FIELD, DELETE_BY_FIELD> dbType(@NotNull IQueryProConfig.DatabaseType dbType) {
        return this.codeStoreDbWriteOnly.dbType(dbType);
    }

    @NotNull
    @Override
    public QueryPro<T, ID, SELECT_BY_FIELD, ORDER_BY_FIELD, UPDATE_SET_FIELD, UPDATE_BY_FIELD, DELETE_BY_FIELD> maxParameterSize(int size) {
        return this.codeStoreDbWriteOnly.maxParameterSize(size);
    }

    @NotNull
    @Override
    public QueryPro<T, ID, SELECT_BY_FIELD, ORDER_BY_FIELD, UPDATE_SET_FIELD, UPDATE_BY_FIELD, DELETE_BY_FIELD> connection(@NotNull Connection connection) {
        return this.codeStoreDbWriteOnly.connection(connection);
    }

    @NotNull
    @Override
    public QueryPro<T, ID, SELECT_BY_FIELD, ORDER_BY_FIELD, UPDATE_SET_FIELD, UPDATE_BY_FIELD, DELETE_BY_FIELD> dataSource(@NotNull DataSource dataSource) {
        return this.codeStoreDbWriteOnly.dataSource(dataSource);
    }

    @NotNull
    @Override
    public QueryPro<T, ID, SELECT_BY_FIELD, ORDER_BY_FIELD, UPDATE_SET_FIELD, UPDATE_BY_FIELD, DELETE_BY_FIELD> beautifySql(boolean enable) {
        return this.codeStoreDbWriteOnly.beautifySql(enable);
    }

    @NotNull
    @Override
    public QueryPro<T, ID, SELECT_BY_FIELD, ORDER_BY_FIELD, UPDATE_SET_FIELD, UPDATE_BY_FIELD, DELETE_BY_FIELD> printLog(boolean enable) {
        return this.codeStoreDbWriteOnly.printLog(enable);
    }

    @NotNull
    @Override
    public QueryPro<T, ID, SELECT_BY_FIELD, ORDER_BY_FIELD, UPDATE_SET_FIELD, UPDATE_BY_FIELD, DELETE_BY_FIELD> printLog(boolean enable, @NotNull LogLevel level) {
        return this.codeStoreDbWriteOnly.printLog(enable, level);
    }

    @NotNull
    @Override
    public QueryPro<T, ID, SELECT_BY_FIELD, ORDER_BY_FIELD, UPDATE_SET_FIELD, UPDATE_BY_FIELD, DELETE_BY_FIELD> printLargeElementWholly(boolean print) {
        return this.codeStoreDbWriteOnly.printLargeElementWholly(print);
    }

    @NotNull
    @Override
    public QueryPro<T, ID, SELECT_BY_FIELD, ORDER_BY_FIELD, UPDATE_SET_FIELD, UPDATE_BY_FIELD, DELETE_BY_FIELD> printCallByInfo(boolean enable) {
        return this.codeStoreDbWriteOnly.printCallByInfo(enable);
    }

    @NotNull
    @Override
    public QueryPro<T, ID, SELECT_BY_FIELD, ORDER_BY_FIELD, UPDATE_SET_FIELD, UPDATE_BY_FIELD, DELETE_BY_FIELD> printCallByInfo(boolean enable, @NotNull LogLevel level) {
        return this.codeStoreDbWriteOnly.printCallByInfo(enable, level);
    }

    @NotNull
    @Override
    public QueryPro<T, ID, SELECT_BY_FIELD, ORDER_BY_FIELD, UPDATE_SET_FIELD, UPDATE_BY_FIELD, DELETE_BY_FIELD> printResult(boolean enable) {
        return this.codeStoreDbWriteOnly.printResult(enable);
    }

    @NotNull
    @Override
    public QueryPro<T, ID, SELECT_BY_FIELD, ORDER_BY_FIELD, UPDATE_SET_FIELD, UPDATE_BY_FIELD, DELETE_BY_FIELD> printResult(boolean enable, @NotNull LogLevel level) {
        return this.codeStoreDbWriteOnly.printResult(enable, level);
    }

    @NotNull
    @Override
    public QueryPro<T, ID, SELECT_BY_FIELD, ORDER_BY_FIELD, UPDATE_SET_FIELD, UPDATE_BY_FIELD, DELETE_BY_FIELD> dryRun(boolean enable) {
        return this.codeStoreDbWriteOnly.dryRun(enable);
    }

    @NotNull
    @Override
    public QueryPro<T, ID, SELECT_BY_FIELD, ORDER_BY_FIELD, UPDATE_SET_FIELD, UPDATE_BY_FIELD, DELETE_BY_FIELD> logicDelete(boolean enable) {
        return this.codeStoreDbWriteOnly.logicDelete(enable);
    }

    @NotNull
    @Override
    public QueryPro<T, ID, SELECT_BY_FIELD, ORDER_BY_FIELD, UPDATE_SET_FIELD, UPDATE_BY_FIELD, DELETE_BY_FIELD> logicDelete(boolean enable, @NotNull String field, @Nullable Object trueValue, @Nullable Object falseValue) {
        return this.codeStoreDbWriteOnly.logicDelete(enable, field, trueValue, falseValue);
    }

    private static class CodeStoreDbWriteOnly<T extends IQueryProConfig.Writeable<T>> extends IQueryProConfig.Writeable.Default<T> {
        private final T that;
        CodeStoreDbWriteOnly(Store store, T that) {
            super(store);
            this.that = that;
        }
        @NotNull @Override protected T that() { return that; }
    }

    /**
     * 查询操作
     */
    public SELECT_BY_FIELD selectBy() {
        return createSelectByField.create(queryStructure.action(QueryStructureAction.SELECT), payload);
    }

    /**
     * 查询全部数据
     */
    public List<T> selectAll() {
        //noinspection unchecked
        return (List<T>) selectBy().run();
    }

    public SELECT_BY_FIELD selectByObj(T obj) {
        final List<WhereClause> whereClauses = IterablePlus.downgradeToStream(EntityProxy.fromBean(obj).iterator())
                .filter(entry -> entry.getValue() != null)
                .map(entry -> new WhereClause(new Field(entry.getKey()), Const.EQUAL, entry.getValue()))
                .collect(Collectors.toList());

        queryStructure.action(QueryStructureAction.SELECT).where(whereClauses).limit(Pair.of(0L, 1L));
        return createSelectByField.create(queryStructure, payload);
    }

    /**
     * 使用主键查询
     */
    public T selectByPrimaryKey(Object value) {
        final EntityProxy.Parser.Parsed.Column idColumn = parsed.id();
        if (idColumn == null) {
            throw new IllegalCall("Class {0} 没有找到主键", clazz.getName());
        }
        final WhereClause whereClause = new WhereClause(new Field(idColumn.name()), Const.EQUAL, value);
        queryStructure.action(QueryStructureAction.SELECT).appendWhere(whereClause).limit(Pair.of(0L, 1L));
        //noinspection unchecked
        return (T) createSelectByField.create(queryStructure, payload).runLimit1();
    }

    /**
     * 排序操作
     */
    public ORDER_BY_FIELD orderBy() {
        return createOrderByField.create(queryStructure.action(QueryStructureAction.SELECT), payload);
    }

    /**
     * 更新操作 例子：updateSet().kee("new-key").where.id.equalTo(1).run()
     * 注意如果要更新的值传入null(例子中是kee), 则会报错,
     * 如确实需要更新为null, 使用
     * java: updateSet().kee(NULL).where.id.equalTo(1).run()
     * java: updateSet().kee(QueryProConstKt.NULL).where.id().equalTo(1).run()
     */
    @PureContract
    @Contract(pure = true)
    public UPDATE_SET_FIELD updateSet() {
        final EntityProxy.Parser.Parsed.Column idColumn = parsed.id();
        queryStructure.action(QueryStructureAction.UPDATE).update(new Update().data(new HashMap<>()).override(false).id(idColumn == null ? null : idColumn.name()));
        return createUpdateSetField.create(queryStructure, payload);
    }

    /**
     * 更新操作
     * updateSet(Apple(id = 2021, name = "iPhone13", type = null)).run()
     * 如果 需要更新的值为null, 则跳过该字段不更新
     * 如确实需要更新, 使用
     * updateSet(Apple(id = 2021, name = "iPhone13", type = null), true).run()
     * 如果需要更新的值更新的值为null, 会将其更新为null
     */
    @PureContract
    @Contract(pure = true)
    public UpdateSetDefinedExpression<UPDATE_BY_FIELD> updateSet(T obj) {
        return updateSet(obj, false);

    }

    /**
     * 更新操作
     * updateSet(Apple(id = 2021, name = "iPhone13", type = null)).run()
     * 如果 需要更新的值为null, 则跳过该字段不更新
     * 如确实需要更新, 使用
     * updateSet(Apple(id = 2021, name = "iPhone13", type = null), true).run()
     * 如果需要更新的值更新的值为null, 会将其更新为null
     */
    @PureContract
    @Contract(pure = true)
    public UpdateSetDefinedExpression<UPDATE_BY_FIELD> updateSet(T obj, boolean override) {
        final Set<String> columns = parsed.columns().keySet();
        final EntityProxy.Parser.Parsed.Column idColumn = parsed.id();
        final Update update = new Update().data(obj).columns(new ArrayList<>(columns)).override(override).id(idColumn == null ? null : idColumn.name());
        queryStructure.action(QueryStructureAction.UPDATE).update(update);
        return new UpdateSetDefinedExpression<>(queryStructure, payload, createUpdateByField);
    }

    @PureContract
    @Contract(pure = true)
    public UpdateSetDefinedExpression<UPDATE_BY_FIELD> updateSet(Map<String, ?> obj) {
        final Map<String, Object> data = new HashMap<>();
        for (Map.Entry<String, ?> entry : obj.entrySet()) {
            final String key = entry.getKey();
            final Object value = entry.getValue();
            if (parsed.getColumnByDbFieldName(key) != null) {
                data.put(key, value);
            } else {
                final EntityProxy.Parser.Parsed.Column column = parsed.getColumnByJavaPropertyName(key);
                if (column == null) {
                    logger.warn("字段{0}已被跳过", key);
                    continue;
                }
                data.put(column.name(), value);
            }
        }
        final Update update = new Update().data(data).override(true);
        queryStructure.action(QueryStructureAction.UPDATE).update(update);
        return new UpdateSetDefinedExpression<>(queryStructure, payload, createUpdateByField);
    }

    /**
     * 删除操作
     */
    public DELETE_BY_FIELD deleteBy() {
        return createDeleteByField.create(queryStructure.action(QueryStructureAction.DELETE), payload);
    }

    /**
     * 使用主键删除
     */
    public boolean deleteByPrimaryKey(Object keyValue) {
        final EntityProxy.Parser.Parsed.Column idColumn = parsed.id();
        queryStructure.action(QueryStructureAction.DELETE).appendWhere(new WhereClause(new Field(idColumn == null ? "id" : idColumn.name()), Const.EQUAL, keyValue));
        final Object run = createDeleteByField.create(queryStructure, payload).run();
        return Boolean.TRUE.equals(run);
    }

    /**
     * 插入操作
     */
    @Nullable
    public ID insert(T obj) {
        final List<ID> insertResult = insert(ListEx.of(obj));
        if (insertResult.isEmpty()) {
            return null;
        }
        return insertResult.get(0);
    }

    /**
     * 插入操作
     */
    @SuppressWarnings("unchecked")
    public final List<ID> insert(T... objs) {
        return insert(Arrays.asList(objs));
    }

    @SafeVarargs
    public final List<ID> insert(Map<String, ?>... objs) {
        //noinspection unchecked
        return insert((Collection<T>) Arrays.asList(objs));
    }

    /**
     * 批量插入
     */
    public List<ID> insert(Collection<T> collection) {
        queryStructure.action(QueryStructureAction.INSERT);
        queryStructure.insert(new Insert(collection));
        //noinspection unchecked
        return (List<ID>) QueryProConfig.computed.queryStructureResolver()
            .insert(queryStructure, payload, clazz);
    }

    /**
     * replace操作
     */
    @SuppressWarnings("unchecked")
    public final void replace(T... objs) {
        replace(Arrays.asList(objs));
    }

    @SafeVarargs
    public final void replace(Map<String, ?>... objs) {
        //noinspection unchecked
        replace((Collection<T>) Arrays.asList(objs));
    }

    /**
     * 批量replace
     */
    public void replace(Collection<T> collection) {
        queryStructure.action(QueryStructureAction.REPLACE);
        queryStructure.insert(new Insert(collection));
        QueryProConfig.computed.queryStructureResolver().insert(queryStructure, payload, clazz);
    }

    public QueryProPlus<T, T> plus() {
        return new QueryProPlus<>(queryStructure, payload, clazz);
    }

    public QueryProPlus<T, Alias> plus(String alias) {
        return new QueryProPlus<>(queryStructure, payload, clazz, alias);
    }
}
