package cn.cloudself.query.util;

import cn.cloudself.query.config.QueryProConfig;
import cn.cloudself.query.config.SqlAndParams;
import cn.cloudself.util.log.Log;
import cn.cloudself.util.log.LogLevel;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.util.*;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SqlLog {
    public static <R> R withQuery(Log logger, SqlAndParams sqlAndParams, Supplier<R> query) {
        final R result;
        try {
            result = query.get();
        } catch (Exception e) {
            doLog(logger, sqlAndParams, e.getClass().getSimpleName());
            throw e;
        }
        doLog(logger, sqlAndParams, result);
        return result;
    }

    private static <P> void doLog(Log logger, SqlAndParams sqlAndParams, @Nullable Object result) {
        final Boolean printLog = QueryProConfig.computed.printLog();
        final LogLevel level = QueryProConfig.computed.printLogLevel();

        if (!printLog && level == LogLevel.DEBUG) {
            return;
        }

        final boolean printLargeElementWholly = Boolean.TRUE.equals(QueryProConfig.computed.printLargeElementWholly());

        final int resultSize;
        final Object firstResult;
        if (result instanceof List) {
            List<?> list = (List<?>) result;
            resultSize = list.size();
            firstResult = resultSize == 0 ? null : list.get(0);
        } else if (result instanceof Collection) {
            Collection<?> collection = (Collection<?>) result;
            resultSize = collection.size();
            firstResult = null;
        } else if (isArray(result)) {
            resultSize = Array.getLength(result);
            firstResult = resultSize == 0 ? null : Array.get(result, 0);
        } else {
            resultSize = 1;
            firstResult = result;
        }

        final String sql = sqlAndParams.sql();
        final Object[] params = sqlAndParams.params();

        if (!printLog) {
            logger.debug("{0}\n{1}", getCallInfo(), sql);
            logger.debug(params);
            if (resultSize > 256 && !printLargeElementWholly) {
                logger.debug("very large result, type: {0}, size: {1}, first result(maybe): {2}", result.getClass().getName(), resultSize, firstResult);
            } else {
                logger.debug(result);
            }
            return;
        }

        final Boolean printCallByInfo = QueryProConfig.computed.printCallByInfo();
        final Boolean printResult = QueryProConfig.computed.printResult();

        final boolean isBatch = params.length != 0 && isArray(params[0]);
        final boolean autoReplaceParams = !isBatch && sql.chars().filter(it -> it == '?').count() == params.length;
        final String paramsReplacedSql;
        if (autoReplaceParams) {
            int i = 0;

            final StringBuffer newSqlSB = new StringBuffer();
            final Matcher matcher = SQL_ARG_PATTERN.matcher(sql.trim());
            while (matcher.find()) {
                final String replaceTo;
                final Object param = params[i++];
                if (param == null) {
                    replaceTo = "null";
                } else if (param instanceof String) {
                    replaceTo = "'" + Matcher.quoteReplacement((String) param) + "'";
                } else if (param instanceof Date) {
                    replaceTo = "'" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(param) + "'";
                } else if (param instanceof TemporalAccessor) {
                    final TemporalAccessor ta = (TemporalAccessor) param;
                    final boolean hourSupported = ta.isSupported(ChronoField.HOUR_OF_DAY);
                    replaceTo = DateTimeFormatter.ofPattern(hourSupported ? "yyyy-MM-dd hh:mm:ss" : "yyyy-MM-dd").format(ta);
                } else if (param instanceof Boolean || param instanceof Number) {
                    replaceTo = param.toString();
                } else {
                    replaceTo = "'" + param + "'";
                }
                matcher.appendReplacement(newSqlSB, replaceTo);
            }
            matcher.appendTail(newSqlSB);
            paramsReplacedSql = newSqlSB.toString();
        } else {
            paramsReplacedSql = sql.trim();
        }

        final String toPrintSql = paramsReplacedSql.startsWith("/*BATCH MODE ") && paramsReplacedSql.length() > 256 && !printLargeElementWholly
                ? paramsReplacedSql.substring(0, 255) + "..."
                : paramsReplacedSql;

        final String toPrintParams;
        if (autoReplaceParams) {
            toPrintParams = "";
        } else if (!isBatch) {
            toPrintParams = Arrays.toString(params);
        } else if (params.length > 32 && !printLargeElementWholly) {
            toPrintParams = "very large params, size: " + params.length + ", first: " + params[0] + "\n";
        } else {
            final StringJoiner sj = new StringJoiner("\n");
            for (Object param : params) {
                sj.add(isArray(sj) ? arrayToString(sj) : param + "");
            }
            toPrintParams = sj.toString();
        }

        final Object toPrintResult;
        if (resultSize > 128 && !printLargeElementWholly) {
            toPrintResult = "very large result, type: " + result.getClass().getName() + ", first: " + firstResult;
        } else {
            toPrintResult = result;
        }

        logger.level(
                level,
                "{0}⭣⭣⭣\n" +
                "--------------------------------\n" +
                "{1}\n" +
                "{2}\n" +
                "result(size: {3}):\n" +
                "{4}\n" +
                "--------------------------------",

                printCallByInfo ? getCallInfo() : "",

                toPrintSql,

                toPrintParams,

                resultSize,

                toPrintResult
        );

        if (!printResult && QueryProConfig.computed.printLogLevel() != LogLevel.DEBUG) {
            logger.debug("result: {0}", result);
        }
    }

    private static final Pattern SQL_ARG_PATTERN = Pattern.compile("\\?");

    private static String getCallInfo() {
        if (!QueryProConfig.computed.printCallByInfo()) {
            return "";
        }
        final StackTraceElement[] stacks = Thread.currentThread().getStackTrace();
        String callByInfo = "";
        for (StackTraceElement stack : stacks) {
            final String className = stack.getClassName();
            final String methodName = stack.getMethodName();
            if (className.startsWith("cn.cloudself.query.") ||
                    className.startsWith("java.lang.") ||
                    className.endsWith("ColumnLimiterField") ||
                    "selectByPrimaryKey".equals(methodName) ||
                    "deleteByPrimaryKey".equals(methodName)
            ) {
                continue;
            } else {
                callByInfo = className + "." + methodName + "(" + stack.getLineNumber() + ")";
                break;
            }
        }
        return callByInfo;
    }

    private static boolean isArray(Object obj) {
        return obj != null && obj.getClass().isArray();
    }

    private static String arrayToString(Object obj) {
        if (!isArray(obj)) {
            return obj + "";
        }
        final int length = Array.getLength(obj);
        final StringJoiner sj = new StringJoiner(",");
        for (int i = 0; i < length; i++) {
            sj.add(Array.get(obj, i) + "");
        }
        return sj.toString();
    }
}