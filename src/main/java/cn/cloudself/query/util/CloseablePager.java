package cn.cloudself.query.util;

import org.jetbrains.annotations.NotNull;

import java.io.Closeable;
import java.io.IOException;
import java.util.function.Supplier;

public class CloseablePager<T> extends Pageable<T> implements AutoCloseable {
    private final Closeable closeable;
    private boolean closed = false;

    public CloseablePager(Supplier<Long> count, Query<T> query, @NotNull Closeable onClose) {
        super(count, query);
        this.closeable = onClose;
    }

    @Override
    public void close() throws IOException {
        this.closed = true;
        this.closeable.close();
    }

    public boolean closed() {
        return closed;
    }
}