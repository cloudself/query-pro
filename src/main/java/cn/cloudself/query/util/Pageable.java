package cn.cloudself.query.util;


import cn.cloudself.util.iterator.AbstractIterator;
import cn.cloudself.util.iterator.ConcatenatedIterator;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

public class Pageable<T> {
    public interface Query<T> {
        List<T> query(long first, long limit);
    }
    public static <T> Pageable<T> create(Supplier<Long> count, Query<T> query) {
        return new Pageable<>(count, query);
    }
    private final Supplier<Long> count;
    private final Query<T> query;
    protected Pageable(Supplier<Long> count, Query<T> query) {
        this.count = count;
        this.query = query;
    }

    public Iterable<T> toIterable(int pageSize) {
        final Query<T> query = this.query;
        final Iterator<Iterator<T>> pages = new AbstractIterator<Iterator<T>>() {
            long page = 0;
            boolean end;
            @Override
            protected Iterator<T> computeNext() {
                if (end) {
                    return super.endOfData();
                }
                final List<T> rows = query.query(page * pageSize, pageSize);
                if (rows.size() < pageSize) {
                    end = true;
                }
                page++;
                return rows.iterator();
            }
        };
        return () -> new ConcatenatedIterator<>(pages);
    }

    public Long total() {
        return this.count.get();
    }

    public List<T> limit(long first, long limit) {
        return this.query.query(first, limit);
    }

    /**
     * @param page starts from 1
     * @param pageSize page size
     */
    public TotalCountModePage<T> totalCountMode(long page, int pageSize) {
        final Long totalCount = this.count.get();
        final long first = (page - 1) * pageSize;
        final List<T> rows = first >= totalCount ? Collections.emptyList() : limit(first, pageSize);
        return new TotalCountModePage<>(page, rows, totalCount);
    }

    public AsyncPage<T> asyncMode(long page, int pageSize) {
        final List<T> rows = limit((page - 1) * pageSize, pageSize);
        return new AsyncPage<>(page, rows, this.count);
    }

    public HasNextModePage<T> hasNextMode(long page, int pageSize) {
        final long first = (page - 1) * pageSize;
        final List<T> rows = limit(first, pageSize + 1);
        return new HasNextModePage<>(page, rows.subList(0, pageSize), rows.size() > pageSize);
    }

    public static class AsyncPage<T> {
        /** 从1开始 */
        private Long page;
        private List<T> rows;
        private Supplier<Long> total;

        public AsyncPage(Long page, List<T> rows, Supplier<Long> total) {
            this.page = page;
            this.rows = rows;
            this.total = total;
        }

        public Long getPage() {
            return page;
        }

        public void setPage(Long page) {
            this.page = page;
        }

        public List<T> getRows() {
            return rows;
        }

        public void setRows(List<T> rows) {
            this.rows = rows;
        }

        public Supplier<Long> getTotal() {
            return total;
        }

        public void setTotal(Supplier<Long> total) {
            this.total = total;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            AsyncPage<?> asyncPage = (AsyncPage<?>) o;
            return Objects.equals(page, asyncPage.page) && Objects.equals(rows, asyncPage.rows) && Objects.equals(total, asyncPage.total);
        }

        @Override
        public int hashCode() {
            return Objects.hash(page, rows, total);
        }

        @Override
        public String toString() {
            return "AsyncPage{" +
                    "page=" + page +
                    ", rows=" + rows +
                    ", total=" + total +
                    '}';
        }
    }

    public static class TotalCountModePage<T> {
        /** 从1开始 */
        private Long page;
        private List<T> rows;
        private Long total;

        public TotalCountModePage(Long page, List<T> rows, Long total) {
            this.page = page;
            this.rows = rows;
            this.total = total;
        }

        public Long getPage() {
            return page;
        }

        public void setPage(Long page) {
            this.page = page;
        }

        public List<T> getRows() {
            return rows;
        }

        public void setRows(List<T> rows) {
            this.rows = rows;
        }

        public Long getTotal() {
            return total;
        }

        public void setTotal(Long total) {
            this.total = total;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            TotalCountModePage<?> that = (TotalCountModePage<?>) o;
            return Objects.equals(page, that.page) && Objects.equals(rows, that.rows) && Objects.equals(total, that.total);
        }

        @Override
        public int hashCode() {
            return Objects.hash(page, rows, total);
        }

        @Override
        public String toString() {
            return "TotalCountModePage{" +
                    "page=" + page +
                    ", rows=" + rows +
                    ", total=" + total +
                    '}';
        }
    }

    public static class HasNextModePage<T> {
        private Long page;
        private List<T> rows;
        private Boolean hasNext;

        public HasNextModePage(Long page, List<T> rows, Boolean hasNext) {
            this.page = page;
            this.rows = rows;
            this.hasNext = hasNext;
        }

        public Long getPage() {
            return page;
        }

        public void setPage(Long page) {
            this.page = page;
        }

        public List<T> getRows() {
            return rows;
        }

        public void setRows(List<T> rows) {
            this.rows = rows;
        }

        public Boolean getHasNext() {
            return hasNext;
        }

        public void setHasNext(Boolean hasNext) {
            this.hasNext = hasNext;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            HasNextModePage<?> that = (HasNextModePage<?>) o;
            return Objects.equals(page, that.page) && Objects.equals(rows, that.rows) && Objects.equals(hasNext, that.hasNext);
        }

        @Override
        public int hashCode() {
            return Objects.hash(page, rows, hasNext);
        }

        @Override
        public String toString() {
            return "HasNextModePage{" +
                    "page=" + page +
                    ", rows=" + rows +
                    ", hasNext=" + hasNext +
                    '}';
        }
    }
}
