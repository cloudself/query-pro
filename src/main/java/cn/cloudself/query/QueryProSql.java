package cn.cloudself.query;

import cn.cloudself.query.config.*;
import cn.cloudself.query.config.IQueryProConfig;
import cn.cloudself.query.config.impl.QueryProConfigImpl;
import cn.cloudself.query.config.store.HashMapStore;
import cn.cloudself.exception.IllegalCall;
import cn.cloudself.exception.IllegalParameters;
import cn.cloudself.query.psi.structure.QueryStructureAction;
import cn.cloudself.query.resolver.DatabaseAccessor;
import cn.cloudself.query.resolver.ScriptResolver;
import cn.cloudself.query.util.CloseablePager;
import cn.cloudself.query.util.Pageable;
import cn.cloudself.util.ext.IOPro;
import cn.cloudself.util.ext.SqlPro;
import cn.cloudself.util.log.Log;
import cn.cloudself.util.log.LogFactory;
import org.intellij.lang.annotations.Language;
import org.jetbrains.annotations.NotNull;

import javax.sql.DataSource;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 用于：
 * 1. 直接执行`SQL`语句
 * 2. 动态生成插入语句并执行
 */
public class QueryProSql {
    private static final Log logger = LogFactory.getLog(QueryProSession.class);

    /**
     * 直接执行{@code sql}语句, 例子:
     * <pre>{@code
     * QueryProSql.create("SELECT * FROM user WHERE username = ?", "hb").query();
     * }</pre>
     * @param sql sql语句，参数使用 ? 的格式
     */
    public static Action create(@Language("SQL") @NotNull String sql, Object... params) {
        return new Action(sql, formatVararg(params));
    }

    /**
     * 直接执行{@code sql}语句
     * @param inputStream sql语句
     */
    public static Action create(@NotNull InputStream inputStream, Object... params) {
        return new Action(IOPro.fromInputStream(inputStream).transferToString(), formatVararg(params));
    }

    /**
     * 直接执行classpath下的{@code sql}语句(例如resources目录下), 例子:
     * <pre>{@code
     * // 例如，resources下有个sql文件夹，里面有个user.sql文件
     * QueryProSql.createFromClassPath("sql/user.sql").query();
     * }</pre>
     * @param path sql文件在 resources, src/main/java, src/main/kotlin等路径下的路径
     */
    public static Action createFromClassPath(@NotNull String path, Object... params) {
        try (final InputStream is = QueryProSql.class.getClassLoader().getResourceAsStream(path)) {
            if (is == null) {
                throw new IllegalParameters("路径{0}可能不是标准的ClassPath", path);
            }
            return create(is, params);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static DynamicAction create() {
        return new DynamicAction();
    }

    public static BatchAction createBatch() {
        return new BatchAction();
    }

    public static abstract class AbstractAction<T> {
        protected final HashMapStore store = new HashMapStore();
        protected final QueryProConfigImpl config = new QueryProConfigImpl(store);

        protected abstract T that();

        /**
         * 更改数据库类型，默认为mysql
         */
        public T dbType(IQueryProConfig.DatabaseType dbType) {
            this.config.dbType(dbType);
            return that();
        }

        /**
         * take 方法，用于写if 条件等
         */
        public T take(Function<T, T> factor) {
            return factor.apply(that());
        }

        /**
         * 临时切换数据源(使用Connection切换)
         * 相比起[ofConnection]更推荐使用[ofDataSource], 因为[ofDataSource]会自动管理链接的开启与关闭。
         * 而[ofConnection]需要手动管理链接的开启与关闭(这在存在事务的时候需要额外注意)
         */
        public T ofConnection(Connection connection) {
            this.config.connection(connection);
            return that();
        }

        /**
         * 临时切换数据源
         */
        public T ofDataSource(DataSource dataSource) {
            this.config.dataSource(dataSource);
            return that();
        }
    }

    public static class Action extends AbstractAction<Action> {
        @Language("SQL")
        private final String sql;
        private final Object[] params;
        public Action(@Language("SQL") String sql, Object[] params) {
            this.sql = sql;
            this.params = params;
        }
        @Override
        protected Action that() {
            return this;
        }

        /**
         * 查询单个对象
         *
         * @param clazz 支持JavaBean, 支持Map, 支持基本类型(Long, String, Date, Enum等, 具体参考[QueryProConfig.global.addResultSetParser])
         */
        public <T> T queryOne(@NotNull Class<T> clazz) {
            final List<T> rows = query(clazz);
            if (rows.isEmpty()) {
                return null;
            }
            return rows.get(0);
        }

        /**
         * 查询单个对象
         */
        public Map<String, Object> queryOne() {
            final List<Map<String, Object>> rows = query();
            if (rows.isEmpty()) {
                return null;
            }
            return rows.get(0);
        }

        /**
         * 查询多个对象
         *
         * @param clazz 支持JavaBean, 支持Map, 支持基本类型(Long, String, Date, Enum等, 具体参考[QueryProConfig.global.addResultSetParser])
         */
        public <T> List<T> query(@NotNull Class<T> clazz) {
            return QueryProConfig.computed.queryStructureResolver()
                    .resolve(store, QueryStructureAction.SELECT, sql, params, clazz);
        }

        /**
         * 查询多个对象
         */
        public List<Map<String, Object>> query() {
            return query(HashMap.class).stream().map(m -> {
                //noinspection unchecked
                return (Map<String, Object>) m;
            }).collect(Collectors.toList());
        }

        /**
         * 使用单条语句执行更新，创建，删除等非select语句
         */
        public int exec() {
            final List<Integer> size = QueryProConfig.computed.queryStructureResolver()
                    .resolve(store, QueryStructureAction.UPDATE, sql, params, Integer.class);
            return size.size() == 1 ? size.get(0) : 0;
        }

        public BatchAction autoSplit() {
            final BatchAction batchAction = new BatchAction().autoSplit(sql, params);
            batchAction.store.copyFrom(this.store);
            return batchAction;
        }
    }

    public static class BatchAction extends AbstractAction<BatchAction> {
        private final List<SqlAndParams> sqlAndParamsList = new ArrayList<>();

        @Override
        protected BatchAction that() {
            return this;
        }

        /**
         * 添加一个sql
         * @param sql sql
         * @param params 参数
         */
        public BatchAction add(@Language("SQL") String sql, Object... params) {
            sqlAndParamsList.add(new SqlAndParams(sql, params));
            return this;
        }

        /**
         * 使用含;的复合语句组合执行更新操作,
         * 该语句会对传入的sql按照;进行拆分
         *
         * @param sqlGroup 复合sql语句 e.g. INSERT INTO user (id, name) VALUES (?, ?); UPDATE user SET name = UPPER(name) WHERE id = ?
         * @param params e.g. [1, 'hb, 1]
         */
        public BatchAction autoSplit(@Language("SQL") String sqlGroup, Object... params) {
            final List<SqlPro.StatementSplitter.Split> splits = SqlPro.of(sqlGroup).splitter().bySemicolon().registerCounter('?').keepEmptyStatement().splitAndCount();

            int j = 0;
            for (SqlPro.StatementSplitter.Split split : splits) {
                @Language("SQL") final String sql = split.sql();
                final SqlAndParams sqlAndParams;
                if (split.empty()) {
                    sqlAndParams = new SqlAndParams(sql).emptyStatement(true);
                } else {
                    final int count = split.count('?');
                    sqlAndParams = new SqlAndParams(sql, Arrays.copyOfRange(params, j, j + count));
                    j += count;
                }
                sqlAndParamsList.add(sqlAndParams);
            }
            return this;
        }

        @SuppressWarnings("unused")
        public static class Plus {
            private final BatchAction batchAction;
            private final List<ScriptResolver.ShouldSkip> shouldSkips = new ArrayList<>();
            private final List<ScriptResolver.BeforeExecute> beforeExecutes = new ArrayList<>();
            private final List<ScriptResolver.AfterExecute> afterExecutes = new ArrayList<>();
            Plus(BatchAction batchAction) {
                this.batchAction = batchAction;
            }

            public Plus shouldSkip(ScriptResolver.ShouldSkip shouldSkip) {
                this.shouldSkips.add(shouldSkip);
                return this;
            }

            public Plus beforeExecute(ScriptResolver.BeforeExecute beforeExecute) {
                this.beforeExecutes.add(beforeExecute);
                return this;
            }

            public Plus afterExecute(ScriptResolver.AfterExecute afterExecute) {
                this.afterExecutes.add(afterExecute);
                return this;
            }

            final ScriptResolver.Around around = new ScriptResolver.Around() {
                @Override
                public boolean shouldSkip(DatabaseAccessor databaseAccessor, SqlAndParams sqlAndParams) {
                    for (ScriptResolver.ShouldSkip shouldSkip : shouldSkips) {
                        if (shouldSkip.shouldSkip(databaseAccessor, sqlAndParams)) {
                            return true;
                        }
                    }
                    return false;
                }

                @Override
                public SqlAndParams beforeExecute(DatabaseAccessor databaseAccessor, SqlAndParams sqlAndParams) {
                    for (ScriptResolver.BeforeExecute beforeExecute : beforeExecutes) {
                        sqlAndParams = beforeExecute.beforeExecute(databaseAccessor, sqlAndParams);
                    }
                    return sqlAndParams;
                }

                @Override
                public void afterExecute(SqlAndParams sqlAndParams) {
                    for (ScriptResolver.AfterExecute afterExecute : afterExecutes) {
                        afterExecute.afterExecute(sqlAndParams);
                    }
                }
            };

            public <T> Pageable.AsyncPage<T> page(long first, long limit) {
                //noinspection unchecked,rawtypes
                return (Pageable.AsyncPage) page(Map.class, first, limit);
            }

            public <T> Pageable.AsyncPage<T> page(Class<T> clazz, long first, long limit) {
                return usePager(clazz, (Function<Pageable<T>, Pageable.AsyncPage<T>>) pager -> new Pageable.AsyncPage<>(null, pager.limit(first, limit), pager::total));
            }

            public void usePager(Consumer<Pageable<Map<String, Object>>> pageableConsumer) {
                usePager(pager -> {
                    pageableConsumer.accept(pager);
                    return null;
                });
            }

            public <R> R usePager(Function<Pageable<Map<String, Object>>, R> pageableConsumer) {
                //noinspection unchecked,rawtypes
                return usePager(Map.class, pager -> (R) pageableConsumer.apply((Pageable) pager));
            }

            public <T> void usePager(Class<T> clazz, Consumer<Pageable<T>> pageableConsumer) {
                usePager(clazz, pager -> {
                    pageableConsumer.accept(pager);
                    return null;
                });
            }

            public <T, R> R usePager(Class<T> clazz, Function<Pageable<T>, R> pageableConsumer) {
                final CloseablePager<T> page = page(clazz, manualSessionTimeout);
                Throwable e = null;
                try {
                    return pageableConsumer.apply(page);
                } catch (Throwable ex) {
                    e = ex;
                    throw ex;
                } finally {
                    try {
                        page.close();
                    } catch (IOException ex) {
                        if (e != null) {
                            e.addSuppressed(ex);
                        }
                    }
                }
            }

            /**
             * 注意page方法会维持一个session直到超时或手动关闭，该期间可能导致连接数超出最大连接数。(Mysql: SHOW VARIABLES LIKE '%max_connections%';)
             * @param sessionTimeout 毫秒
             * @see #usePager(Consumer)
             * @see #usePager(Function)
             */
            protected CloseablePager<Map<String, Object>> page(long sessionTimeout) {
                //noinspection unchecked,rawtypes
                return (CloseablePager<Map<String, Object>>) (CloseablePager) page(Map.class, sessionTimeout);
            }

            private final static long manualSessionTimeout = -12356789;

            /**
             * 注意page方法会维持一个session直到超时或手动关闭，该期间可能导致连接数超出最大连接数。(Mysql: SHOW VARIABLES LIKE '%max_connections%';)
             * @param sessionTimeout 毫秒
             * @see #usePager(Class, Consumer)
             * @see #usePager(Class, Function)
             */
            protected  <T> CloseablePager<T> page(Class<T> clazz, long sessionTimeout) {
                final Closeable session = QueryProSession.openSession();
                if (sessionTimeout != manualSessionTimeout) {
                    logger.warn("注意page方法会维持一个session直到超时或手动关闭，该期间可能导致连接数超出最大连接数。(Mysql: SHOW VARIABLES LIKE '%max_connections%';)");
                    if (sessionTimeout < 0) {
                        throw new IllegalParameters("sessionTimeout不能为负");
                    }
                }

                final ScriptResolver.Actuator actuator = QueryProConfig.computed.scriptResolver()
                        .executor(batchAction.store, batchAction.sqlAndParamsList, around);

                while (!actuator.isLast()) {
                    actuator.executeNext();
                }

                if (sessionTimeout != manualSessionTimeout) {
                    new Thread(() -> {
                        try {
                            Thread.sleep(sessionTimeout);
                            session.close();
                        } catch (InterruptedException | IOException e) {
                            throw new RuntimeException(e);
                        }
                    }).start();
                }

                final SqlAndParams lastSqlAndParams = actuator.prepareAndGetSqlAndParams();
                final String lastSqlMaybeWithSemicolon = lastSqlAndParams.sql().trim();
                final String lastSql = lastSqlMaybeWithSemicolon.endsWith(";")
                        ? lastSqlMaybeWithSemicolon.substring(0, lastSqlMaybeWithSemicolon.length() - 1)
                        : lastSqlMaybeWithSemicolon;
                final Object[] lastParams = lastSqlAndParams.params();

                return new CloseablePager<>(
                        () -> {
                            @Language("SQL") final String counter = "select count(*) from (" + lastSql + ")";
                            final List<Long> results = actuator.runSql(new SqlAndParams(counter, lastParams), Long.class);
                            return results.get(0);
                        },
                        (first, limit) -> {
                            @Language("SQL") final String pager = lastSql + " LIMIT " + first + ", " + limit;
                            return actuator.runSql(new SqlAndParams(pager, lastParams), clazz);
                        },
                        session
                );
            }

            /**
             * 执行Sql脚本, 并返回最后一条sql语句的返回结果
             * <p>基于{@link cn.cloudself.query.resolver.QSRTmpl}的实现，batch 模式的生命周期方法会多次调用，每一句sql执行，均会调用 beforeRunSql等生命周期方法</p>
             *
             * @param clazz 支持的值有JavaBean, BasicType(e.g. {@link Integer}, {@link java.math.BigDecimal}, {@link Boolean}, {@link String}, etc.), {@link Map}.class(HashMap&lt;String, Object>)
             * @return 最后一条sql语句的返回结果
             */
            public <T> List<T> exec(Class<T> clazz) {
                return QueryProSession.withSession(
                        () -> QueryProConfig.computed.scriptResolver().resolve(batchAction.store, batchAction.sqlAndParamsList, clazz, around)
                );
            }

            /**
             * 执行Sql脚本, 并返回最后一条sql语句的返回结果
             * @return 最后一条sql语句的返回结果
             */
            public Map<String, Object> execForMap() {
                //noinspection unchecked
                return (Map<String, Object>) exec(Map.class);
            }

            /**
             * 执行sql 语句组
             */
            public void exec() {
                exec(void.class);
            }
        }

        public Plus plus() {
            return new Plus(this);
        }

        /**
         * 执行Sql脚本, 并返回最后一条sql语句的返回结果
         * <p>基于{@link cn.cloudself.query.resolver.QSRTmpl}的实现，batch 模式的生命周期方法会多次调用，每一句sql执行，均会调用 beforeRunSql等生命周期方法</p>
         *
         * @param clazz 支持的值有JavaBean, BasicType(e.g. {@link Integer}, {@link java.math.BigDecimal}, {@link Boolean}, {@link String}, etc.), {@link Map}.class(HashMap&lt;String, Object>)
         * @return 最后一条sql语句的返回结果
         */
        public <T> List<T> exec(Class<T> clazz) {
            return QueryProSession.withSession(
                    () -> QueryProConfig.computed.scriptResolver().resolve(store, sqlAndParamsList, clazz, null)
            );
        }

        /**
         * 执行Sql脚本, 并返回最后一条sql语句的返回结果
         * @return 最后一条sql语句的返回结果
         */
        public Map<String, Object> execForMap() {
            //noinspection unchecked
            return (Map<String, Object>) exec(Map.class);
        }

        /**
         * 执行sql 语句组
         */
        public void exec() {
            exec(void.class);
        }
    }

    public static class DynamicAction extends AbstractAction<DynamicAction> {
        @Override
        protected DynamicAction that() {
            return this;
        }

        public DynamicAction maxParameterSize(Integer size) {
            this.config.maxParameterSize(size);
            return that();
        }

        /**
         * 通过 objs 配合数据库表名，动态生成sql语句，并执行
         */
        @SafeVarargs
        public final void insert(String table, Map<String, ?>... objs) {
            insert(table, Arrays.asList(objs), null);
        }

        /**
         * 通过 objs 配合数据库表名，动态生成sql语句，并执行
         */
        public <V, M extends Map<String, V>> void insert(String table, Collection<? extends Map<String, ?>> objs) {
            insert(table, objs, null);
        }

        /**
         * 通过 objs 配合数据库表名，动态生成sql语句，并执行
         *
         * @param idColumnClazz 指定需要返回的ID的类型
         */
        public <ID> List<ID> insert(String table, Collection<? extends Map<String, ?>> objs, Class<ID> idColumnClazz) {
            return QueryProConfig.computed.dynamicActuator()
                    .insert(store, table, objs, false, idColumnClazz);
        }

        /**
         * 通过 objs 配合数据库表名，动态生成sql语句，并执行
         */
        @SafeVarargs
        public final <V> void replace(String table, Map<String, V>... objs) {
            replace(table, Arrays.asList(objs));
        }

        /**
         * 通过 objs 配合数据库表名，动态生成sql语句，并执行
         */
        public <V, M extends Map<String, V>> void replace(String table, Collection<M> objs) {
            QueryProConfig.computed.dynamicActuator().insert(store, table, objs, true, null);
        }

        private void removeByPrimaryKey(String table, String keyColumn, Object... primaryKeys) {}
        private void update(String table, String keyColumn, Collection<? extends Map<String, ?>> withPrimaryKeyPartialRows) {}
    }

    /**
     * 将 [Collection] 转为 Object[]
     */
    private static Object[] formatVararg(Object[] vararg) {
        if (vararg == null) {
            return null;
        }
        if (vararg.length > 0) {
            final Object first = vararg[0];
            if (first instanceof Collection) {
                if (vararg.length > 1) {
                    throw new IllegalCall("第一个参数为collection时，参数个数不能大于1。");
                }
                return ((Collection<?>) first).toArray();
            }
        }
        return vararg;
    }
}
