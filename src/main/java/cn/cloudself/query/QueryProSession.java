package cn.cloudself.query;

import cn.cloudself.exception.UnSupportException;
import cn.cloudself.util.log.Log;
import cn.cloudself.util.log.LogFactory;

import javax.sql.DataSource;
import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

/**
 * QueryPro默认支持Spring管理的事务（例如@Transactional注解）。
 * <p>该类适合没有spring的环境下或者spring事务未开启的情况下使用</p>，
 * <p>如果在spring事务中使用该组件，相当于新开启了一个事务。</p>
 * <br/>
 * 当 session 与 transaction 嵌套使用的时候，有很多限制，大多都是该版本的缺陷：
 * <ol>
 *     <li>Transaction中如果嵌套Session, Transaction不能指定数据源（即只能默认绑定所有数据源）。说明：因为单个事务中的所有语句本身就是处于同一个Session中的，这种场景下，Transaction内部的Session是可以忽略的，它可有可无，<i>QueryPro</i> 只是支持这种写法。</li>
 *     <li>Transaction无法嵌套</li>
 *     <li>Session中不支持嵌套Transaction，即无法在Session中开启一个事务(缺陷)</li>
 *     <li>Session不能指定数据源（即只能默认绑定所有数据源）</li>
 *     <li>Session之间支持任意嵌套</li>
 * </ol>
 *
 */
public class QueryProSession {
    private static final Log logger = LogFactory.getLog(QueryProSession.class);

    public static class __Instance {
        static Set<DataSource> ALL_ACTIVE = new HashSet<>();
        static __Instance ALL_DATASOURCE = new __Instance(ALL_ACTIVE);
        final Set<DataSource> limitedDataSources;
        __Instance(Set<DataSource> limitedDataSources) {
            this.limitedDataSources = limitedDataSources.size() == 0 ? ALL_ACTIVE : limitedDataSources;
        }

        enum SessionType {
            Session,
            Transaction,
            ALL
        }

        private static final ThreadLocal<SessionType> sessionType = new ThreadLocal<>();
        // 已经激活session的Datasource
        private static final ThreadLocal<Set<DataSource>> datasourceListWhichSessionIsActive = new ThreadLocal<>();
        private static final ThreadLocal<Map<DataSource, Connection>> connectionThreadLocal = new ThreadLocal<>();

        // 已经激活session的Datasource, 如果已经激活，不会对Connection进行回收
        public static boolean isActualSessionActive(DataSource dataSource) {
            final Set<DataSource> dataSources = datasourceListWhichSessionIsActive.get();
            if (dataSources == ALL_ACTIVE) {
                return true;
            }
            if (dataSources == null) {
                return false;
            }
            return dataSources.contains(dataSource);
        }

        public static Connection getConnection(DataSource datasource) {
            Map<DataSource, Connection> connections = connectionThreadLocal.get();
            if (connections != null) {
                final Connection connection = connections.get(datasource);
                if (connection != null) {
                    return connection;
                }
            } else {
                connections = new HashMap<>();
                connectionThreadLocal.set(connections);
            }
            final Connection connection;
            try {
                connection = datasource.getConnection();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            logger.debug("connection got.");
            try {
                final SessionType sessionType = __Instance.sessionType.get();
                if (sessionType == SessionType.Transaction || sessionType == SessionType.ALL) {
                    connection.setAutoCommit(false);
                }
            } catch (Throwable e) {
                int times = 3;
                while (times-- > 0) {
                    try {
                        connection.close();
                        break;
                    } catch (Throwable ex) {
                        logger.warn("try close failed, remainder times: " + times);
                        e.addSuppressed(ex);
                        try {
                            Thread.sleep(10000);
                        } catch (InterruptedException ie) {
                            e.addSuppressed(ie);
                        }
                    }
                }
                throw new RuntimeException(e);
            }
            connections.put(datasource, connection);
            return connection;
        }

        public interface Block {
            void exec() throws Exception;
        }

        public interface BlockR<R> {
            R exec() throws Exception;
        }

        public <R> R useTransaction(__Instance.BlockR<R> block) {
            if (datasourceListWhichSessionIsActive.get() == null) {
                datasourceListWhichSessionIsActive.set(this.limitedDataSources);
            }
            final SessionType sessionType = __Instance.sessionType.get();
            if (sessionType == null) {
                __Instance.sessionType.set(SessionType.Transaction);
            } else if (sessionType == SessionType.Session) {
                throw new UnSupportException("不支持在Session中开启事务");
            } else {
                throw new UnSupportException("不支持嵌套");
            }

            try {
                Throwable e = null;
                try {
                    logger.debug("connection managed by QueryProTransaction.");
                    final R result = block.exec();
                    final Map<DataSource, Connection> connections = connectionThreadLocal.get();
                    if (connections != null) {
                        for (Connection connection : connections.values()) {
                            connection.commit();
                        }
                        logger.debug("transaction committed.");
                    }
                    return result;
                } catch (Exception ex) {
                    final Map<DataSource, Connection> connections = connectionThreadLocal.get();
                    if (connections != null) {
                        logger.warn("遇到错误，准备回滚中");
                        for (Connection connection : connections.values()) {
                            connection.rollback();
                        }
                        logger.info("回滚完毕");
                    } else {
                        logger.info("遇到错误，错误发生在执行sql之前，无需回滚");
                    }
                    e = ex;
                    throw ex;
                } finally {
                    final Map<DataSource, Connection> connections = connectionThreadLocal.get();
                    if (connections != null) {
                        Throwable finalE = e;
                        closeConnections(connections.values(), ex -> {
                            if (finalE != null) {
                                finalE.addSuppressed(ex);
                            }
                        });
                    }
                    connectionThreadLocal.remove();
                    datasourceListWhichSessionIsActive.remove();
                    __Instance.sessionType.remove();
                }
            } catch (Exception e) {
                if (e instanceof RuntimeException) {
                    throw (RuntimeException) e;
                }
                throw new RuntimeException(e);
            }
        }

        public Closeable openSession() {
            if (this.limitedDataSources != ALL_ACTIVE) {
                throw new UnSupportException("Session不支持指定数据源");
            }
            if (datasourceListWhichSessionIsActive.get() == null) {
                datasourceListWhichSessionIsActive.set(this.limitedDataSources);
            }
            final AtomicReference<Boolean> reuse = new AtomicReference<>(false);
            final SessionType sessionType = __Instance.sessionType.get();
            if (sessionType == null) {
                __Instance.sessionType.set(SessionType.Session);
            } else if (sessionType == SessionType.Transaction) {
                __Instance.sessionType.set(SessionType.ALL);
            } else {
                reuse.set(true);
            }

            return () -> {
                if (reuse.get()) {
                    return;
                }
                logger.debug("session is closing.");
                final AtomicReference<IOException> e = new AtomicReference<>();
                if (__Instance.sessionType.get() == SessionType.ALL) {
                    __Instance.sessionType.set(SessionType.Transaction);
                } else {
                    final Map<DataSource, Connection> connections = connectionThreadLocal.get();
                    if (connections != null) {
                        closeConnections(connections.values(), ex -> {
                            if (e.get() == null) {
                                e.set(new IOException());
                            }
                            final Throwable t = e.get();
                            t.addSuppressed(ex);
                        });
                    }
                    connectionThreadLocal.remove();
                    datasourceListWhichSessionIsActive.remove();
                    __Instance.sessionType.remove();
                }
                if (e.get() != null) {
                    throw e.get();
                }
            };
        }

        public <R> R withSession(__Instance.BlockR<R> block) {
            try {
                try (Closeable ignored = openSession()) {
                    logger.debug("connection managed by QueryProTransaction.");
                    return block.exec();
                }
            } catch (Exception e) {
                if (e instanceof RuntimeException) {
                    throw (RuntimeException) e;
                } else {
                    throw new RuntimeException(e);
                }
            }
        }

        private void closeConnections(Collection<Connection> connections, Consumer<Throwable> onException) {
            for (Connection connection : connections) {
                try {
                    if (!connection.getAutoCommit()) {
                        connection.commit();
                    }
                } catch (Exception ex) {
                    if (onException != null) {
                        onException.accept(ex);
                    } else {
                        logger.error("auto commit failed.", ex);
                    }
                }
            }
            for (Connection connection : connections) {
                boolean failed = false;
                try {
                    connection.close();
                } catch (Exception ex) {
                    if (onException != null) {
                        onException.accept(ex);
                    } else {
                        failed = true;
                        logger.error("connection close failed.", ex);
                    }
                }
                if (!failed) {
                    logger.debug("connection closed.");
                }
            }
        }
    }

    /**
     * 包裹一个事务
     * <br/><br/>
     * <p><b>Examples:</b></p>
     * <pre>
     * {@code
     * QueryProSession.useTransaction(() -> {
     *   // ...doTransaction
     * });
     * }</pre>
     * <p><b>Notes：</b></p>
     * <ul>
     *     <li>事务仅对当前新线程生效，意味着<i> block </i>代码块中开启的新线程并不参与当前事务。</li>
     *     <li><i> block </i>代码块中如果存在多数据源，各数据源均会开启一个独立的事务，这是默认行为，如果需要指定数据源，使用:{@link #ofDataSource(DataSource...)}}.useTransaction。</li>
     * </ul>
     */
    public static void useTransaction(__Instance.Block block) {
        __Instance.ALL_DATASOURCE.useTransaction(() -> {
            block.exec();
            return null;
        });
    }

    /**
     * 包裹一个事务
     * <br/><br/>
     * <p><b>Examples:</b></p>
     * <pre>
     * {@code
     * QueryProSession.useTransaction(() -> {
     *   // ...doTransaction
     * });
     * }</pre>
     * <p><b>Notes：</b></p>
     * <ul>
     *     <li>事务仅对当前新线程生效，意味着<i> block </i>代码块中开启的新线程并不参与当前事务。</li>
     *     <li><i> block </i>代码块中如果存在多数据源，各数据源均会开启一个独立的事务，这是默认行为，如果需要指定数据源，使用:{@link #ofDataSource(DataSource...)}.useTransaction。</li>
     * </ul>
     */
    public static <R> R useTransaction(__Instance.BlockR<R> block) {
        return __Instance.ALL_DATASOURCE.useTransaction(block);
    }

    /**
     * 默认会开启代码块中所有数据源的事务，如果只想部分开启，使用该方法
     * @see #useTransaction(__Instance.Block)
     * @see #useTransaction(__Instance.BlockR)
     */
    public static __Instance ofDataSource(DataSource... dataSource) {
        return new __Instance(new HashSet<>(Arrays.asList(dataSource)));
    }

    public static <R> R withSession(__Instance.BlockR<R> block) {
        return __Instance.ALL_DATASOURCE.withSession(block);
    }

    public static Closeable openSession() {
        return __Instance.ALL_DATASOURCE.openSession();
    }
}
