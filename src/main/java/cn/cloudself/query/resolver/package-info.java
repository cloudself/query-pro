/**
 * <h2>resolver包</h2>
 * 将{@link cn.cloudself.query.psi.structure.QueryStructure}等结构转换为`SQL`并执行或直接执行`SQL`
 * <br/>
 * <ul>
 *     <li>{@link cn.cloudself.query.resolver.QueryStructureResolver} 主要接口,用于解析{@link cn.cloudself.query.psi.structure.QueryStructure}对象并执行或直接执行`SQL`</li>
 *     <li>{@link cn.cloudself.query.resolver.ScriptResolver} 用于执行SQL脚本(多语句)</li>
 *     <li>{@link cn.cloudself.query.resolver.DynamicActuator} 用于执行动态语句(根据表结构生成相应的SQL语句并执行)</li>
 *     <li>{@link cn.cloudself.query.resolver.QSRTmpl} 模版方法，已经实现{@link cn.cloudself.query.resolver.QueryStructureResolver}, {@link cn.cloudself.query.resolver.ScriptResolver}, {@link cn.cloudself.query.resolver.DynamicActuator} 以及添加了生命周期的支持，具体实现类仅需实现{@link cn.cloudself.query.resolver.DatabaseAccessor}接口即可</li>
 *     <li>{@link cn.cloudself.query.resolver.impl.JdbcQSR} 继承自{@link cn.cloudself.query.resolver.QSRTmpl}, {@link cn.cloudself.query.resolver.QueryStructureResolver}等接口的`JDBC`实现。</li>
 *     <li>{@link cn.cloudself.query.resolver.helper.ToSqlByQueryStructure} 工具类，目的是将{@link cn.cloudself.query.psi.structure.QueryStructure}转换为`JDBC`风格的`SQL`(`SQL`语句中参数占位符为`?`)及其参数。</li>
       <li>{@link cn.cloudself.query.resolver.helper.ToInsertSqlByObjects} 工具类，目的是根据需要`insert`的对象以及目标数据库表信息生成对应的`SQL`语句(同样是JDBC风格的，参数占位符为`?`)</li>
 * </ul>
 */
package cn.cloudself.query.resolver;
