package cn.cloudself.query.resolver;

import cn.cloudself.query.config.SqlAndParams;
import cn.cloudself.query.config.store.HashMapStore;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Map;

public interface ScriptResolver {
    interface ShouldSkip {
        boolean shouldSkip(DatabaseAccessor databaseAccessor, SqlAndParams sqlAndParams);
    }
    interface BeforeExecute {
        SqlAndParams beforeExecute(DatabaseAccessor databaseAccessor, SqlAndParams sqlAndParams);
    }
    interface AfterExecute {
        void afterExecute(SqlAndParams sqlAndParams);
    }
    interface Around extends ShouldSkip, BeforeExecute, AfterExecute {
        default boolean shouldSkip(DatabaseAccessor databaseAccessor, SqlAndParams sqlAndParams) { return false; }
        default SqlAndParams beforeExecute(DatabaseAccessor databaseAccessor, SqlAndParams sqlAndParams) { return sqlAndParams; }
        default void afterExecute(SqlAndParams sqlAndParams) {}
    }

    /**
     * 使用多条语句和参数执行更新，创建，删除等非select语句
     *
     * @param sqlAndParamsList 多对sql and params
     * @param resultClass 支持的值有JavaBean, BasicType(e.g. {@link Integer}, {@link java.math.BigDecimal}, {@link Boolean}, {@link String}, etc.), {@link Map}.class(HashMap&lt;String, Object>)
     * @return 最后一条sql语句的返回结果
     */
    @Nullable
    default <T> List<T> resolve(
            @NotNull HashMapStore configStore,
            @NotNull List<SqlAndParams> sqlAndParamsList,
            @NotNull Class<T> resultClass,
            @Nullable Around around
    ) {
        final Actuator actuator = executor(configStore, sqlAndParamsList, around);
        while (true) {
            final boolean last = actuator.isLast();
            if (!last) {
                actuator.executeNext();
            } else {
                return actuator.executeNext(resultClass);
            }
        }
    }

    interface Actuator {
        boolean isLast();

        void next();

        SqlAndParams prepareAndGetSqlAndParams();

        <T> List<T> runSql(@Nullable SqlAndParams sqlAndParams, Class<T> resultClass);

        default void executeNext() {
            executeNext(void.class);
        }

        /**
         * @param resultClass 支持的值有JavaBean, BasicType(e.g. {@link Integer}, {@link java.math.BigDecimal}, {@link Boolean}, {@link String}, etc.), {@link Map}.class(HashMap&lt;String, Object>)
         */
        default <T> List<T> executeNext(Class<T> resultClass) {
            prepareAndGetSqlAndParams();
            final List<T> result = runSql(null, resultClass);
            next();
            return result;
        }
    }

    /**
     * 使用多条语句和参数执行更新，创建，删除等非select语句
     *
     * @param sqlAndParamsList 多对sql and params
     * @return 执行器，需手动执行或者直接使用 {@link #resolve(HashMapStore, List, Class, Around)}
     */
    @NotNull
    Actuator executor(
            @NotNull HashMapStore configStore,
            @NotNull List<SqlAndParams> sqlAndParamsList,
            @Nullable Around around
    );
}
