package cn.cloudself.query.resolver;

import cn.cloudself.query.config.store.HashMapStore;
import cn.cloudself.query.psi.structure.QueryPayload;
import cn.cloudself.query.psi.structure.QueryStructure;
import cn.cloudself.query.psi.structure.QueryStructureAction;
import org.intellij.lang.annotations.Language;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * <h2>主要用于解析对象并执行或直接执行`SQL`</h2>
 * 推荐继承QSR模板以实现IQueryStructureResolver, 而不是直接实现该接口。
 * <br/>
 * 该接口包含：
 * <ol>
 *     <li>解析并执行`QueryStructure`接口</li>
 *     <li>直接执行单条`SQL`接口</li>
 *     <li>执行多条`SQL`查询语句接口</li>
 *     <li>动态插入接口（根据数据库结构以及提供的`JavaBean`生成`INSERT`语句并执行）</li>
 * </ol>
 */
public interface QueryStructureResolver {
    /**
     * 将{@link QueryStructure}解析至SQL并执行
     *
     * @param queryStructure {@link QueryStructure}
     * @param payload 载荷 {@link QueryPayload}
     * @param resultClass 返回结果的类型。{@link QSRTmpl}模版支持`JavaBean`, `Map`, 以及`Long`, `String`, `Boolean`等基本类型。另外，默认使用的{@link cn.cloudself.query.resolver.impl.JdbcQSR}(为{@link QSRTmpl}的子类)还额外支持{@link cn.cloudself.query.resolver.impl.JdbcQSR.IResultSetWalker}
     *              insert时为对象的类型
     * @return clazz为基本类型时，可能返回List&lt;T?> 当动作为insert时，返回值为List&lt;ID>
     * @param <T> see resultClass
     */
    @NotNull
    <T> List<T> resolve(
            @NotNull QueryStructure queryStructure,
            @NotNull QueryPayload payload,
            @NotNull Class<T> resultClass
    );

    /**
     * 将{@link QueryStructure}解析至SQL并执行(insert)
     * insert方法和resolve方法的区别是 返回结果不一样
     *
     * @param queryStructure {@link QueryStructure}
     * @param payload 载荷
     * @param javaBeanClass Class&lt;JavaBean> 由于插入对象支持map类型，所以需要一个参照的JavaBean.class对象
     * @return List&lt;ID> 插入数据的id列表(默认的实现不支持复合主键）
     */
    @NotNull
    List<Object> insert(
            @NotNull QueryStructure queryStructure,
            @NotNull QueryPayload payload,
            @NotNull Class<?> javaBeanClass
    );

    /**
     * 执行一个SQL语句
     *
     * @param configStore code 级别的配置, 使用 {@code QueryProConfig.code.use} 处理
     * @param sql 单条sql语句 e.g. SELECT * FROM user WHERE user.id = ?
     * @param params 参数数组 e.g. [1]
     * @param resultClass `JavaBean`, `Map`, and basic type e.g. `Long`, `String`, `Boolean` etc. for select by default; [Int] for update
     * @param action {@link QueryStructureAction}
     * @return clazz为基本类型时，可能返回List&lt;T?>
     * @param <T> see param resultClass
     */
    @NotNull
    <T> List<T> resolve(
            @NotNull HashMapStore configStore,
            @NotNull QueryStructureAction action,
            @NotNull @Language("SQL") String sql,
            @NotNull Object[] params,
            @NotNull Class<T> resultClass
    );
}
