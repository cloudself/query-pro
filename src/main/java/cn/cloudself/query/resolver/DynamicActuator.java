package cn.cloudself.query.resolver;

import cn.cloudself.query.config.store.HashMapStore;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 动态SQL行为
 */
public interface DynamicActuator {
    /**
     * 动态插入
     * 通过 objs 配合数据库表名，动态生成sql语句，并执行
     *
     * @param configStore code 级别的配置, 使用 {@code QueryProConfig.code.use} 处理
     * @param table 数据库表名
     * @param objs 需要插入的Map类型的集合
     * @param replace true 生成REPLACE语句, false 生成INSERT语句
     * @param idColumnClazz 如需返回ID，指定ID的类型，当操作为replace时，clazz只能为null或者void
     * @return id列表：只有当生成了id, 且clazz不为null, 不为void时，id才会返回; 否则, 对应记录会以null占位
     * @param <ID> see idColumnClazz
     */
    @NotNull
    <ID> List<ID> insert(
            @NotNull HashMapStore configStore,
            @NotNull String table,
            @NotNull Collection<? extends Map<String, ?>> objs,
            boolean replace,
            @Nullable Class<ID> idColumnClazz
    );

    /**
     * 动态删除行
     * @param configStore code 级别的配置, 使用 {@code QueryProConfig.code.use} 处理
     * @param table 数据库表名
     * @param primaryKeys 需要删除的主键值
     * @param keyColumn 需要删除的主键键名，如果为null，将从数据库表结构中获取
     * @param clazz int, boolean, void等
     * @return 影响的行数
     * @param <T> see clazz
     */
    @Nullable
    <T> T removeByPrimaryKey(
            @NotNull HashMapStore configStore,
            @NotNull String table,
            @NotNull Object[] primaryKeys,
            @Nullable String keyColumn,
            @Nullable Class<T> clazz
    );

    /**
     * 动态更新表
     * @param configStore code 级别的配置, 使用 {@code QueryProConfig.code.use} 处理
     * @param table 数据库表名
     * @param keyColumn 需要更新的主键键名
     * @param withPrimaryKeyPartialRows 行数据，注意每一行都至少存在两个键值，其中一个是id，如果要将数据更新为null，
     *                                  需要传入{@link cn.cloudself.query.psi.Const#NULL}或者将skipNull参数设置为false
     * @param skipNull 当行数据的的map-entry.value为null时，是否跳过该键值，如果选择跳过，仍然可以传入{@link cn.cloudself.query.psi.Const#NULL}将值更新为null
     * @param clazz int, boolean, void等
     * @return 影响的行数
     * @param <T> see clazz
     */
    @Nullable
    <T> T update(
            @NotNull HashMapStore configStore,
            @NotNull String table,
            @Nullable String keyColumn,
            @NotNull Collection<? extends Map<String, ?>> withPrimaryKeyPartialRows,
            boolean skipNull,
            @Nullable Class<T> clazz
    );
}
