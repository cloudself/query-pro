package cn.cloudself.query.resolver;

import cn.cloudself.exception.IllegalCall;
import cn.cloudself.exception.IllegalImplements;
import cn.cloudself.exception.IllegalParameters;
import cn.cloudself.exception.UnSupportException;
import cn.cloudself.query.config.QueryProConfig;
import cn.cloudself.query.config.SqlAndParams;
import cn.cloudself.query.config.store.HashMapStore;
import cn.cloudself.query.psi.Const;
import cn.cloudself.query.psi.structure.Insert;
import cn.cloudself.query.psi.structure.QueryPayload;
import cn.cloudself.query.psi.structure.QueryStructure;
import cn.cloudself.query.psi.structure.QueryStructureAction;
import cn.cloudself.query.resolver.helper.*;
import cn.cloudself.util.ext.ClassPro;
import cn.cloudself.util.ext.EntityProxy;
import cn.cloudself.util.ext.IterablePlus;
import cn.cloudself.util.log.Log;
import cn.cloudself.util.log.LogFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <h2>QSR的模板类</h2>
 * <p>一般继承该类重写 相应的抽象方法即可</p>
 * {@link #queryStructureToSql(QueryStructure)}, {@link #insertObjectToSql}, {@link #getColumnsDynamic}也是可以重写的
 * 如需自行实现{@link QueryStructureResolver}, 需要处理生命周期方法beforeRunSql, afterRunSql以及如果用到dryRun模式,需要添加相应的支持
 */
public abstract class QSRTmpl implements QueryStructureResolver, DatabaseAccessor, ScriptResolver, DynamicActuator {
    /**
     * 提供`QueryStructure`，将其解析为`SQL`
     * <br/>
     * `SQL`中的参数 默认的实现为`JDBC`支持的`?`的风格
     * <br/>
     * 该方法可重写
     * @see ToSqlByQueryStructure#toSqlWithIndexedParams()
     */
     protected SqlAndParams queryStructureToSql(QueryStructure queryStructure) {
         return new ToSqlByQueryStructure(queryStructure).toSqlWithIndexedParams();
     }

    /**
     * 提供：表名，列信息，
     * 将需要插入的对象转为sql语句
     * <br/>
     * 约定：当返回结果的size为1且groupedParams有值。表明数据量较大，使用批量插入模式
     * (可优化)
     *
     * @see ToInsertSqlByObjects#toSql(boolean, Collection, String, Collection)
     */
    protected List<SqlAndParams> insertObjectToSql(
            boolean replace,
            Collection<?> objs,
            String table,
            Collection<EntityProxy.Parser.Parsed.Column> columns
    ) {
        return ToInsertSqlByObjects.toSql(replace, objs, table, columns);
    }

    /**
     * 根据primaryKeys生成Delete语句
     * @param table 表名
     * @param keyColumn 主键键名
     * @param primaryKeys 需要删除的行的主键键值
     * @return SQL 语句以及其参数
     */
    protected SqlAndParams primaryKeysToDeleteSql(
            @NotNull String table,
            @Nullable String keyColumn,
            @NotNull Object[] primaryKeys
    ) {
        return ToDeleteSqlByPrimaryKeys.toSql(table, keyColumn, primaryKeys);
    }

    /**
     * 根据对象生成更新语句
     * @param table 表名
     * @param columns 对应表的列信息
     * @param keyColumn 主键键名
     * @param withPrimaryKeyPartialRows 代理对象集合 每个代理对象至少都有两个元素 其中一个是主键
     * @param skipNull 是否跳过null值
     * @return 列表 SQL语句以及其参数
     */
    protected List<SqlAndParams> updateObjectToSql(
            @NotNull String table,
            @NotNull Set<String> columns,
            @NotNull String keyColumn,
            @NotNull Collection<? extends Map<String, ?>> withPrimaryKeyPartialRows,
            boolean skipNull
    ) {
        return ToUpdateSqlByObjects.toSql(table, columns, keyColumn, withPrimaryKeyPartialRows, skipNull);
    }

    protected static class Column {
        protected final String column;
        protected final Function<Object, Object> getter;
        protected final boolean isId;
        protected final Boolean hasDefault;
        public Column(String column, Function<Object, Object> getter, boolean isId, Boolean hasDefault) {
            this.column = column;
            this.getter = getter;
            this.isId = isId;
            this.hasDefault = hasDefault;
        }
        private EntityProxy.Parser.Parsed.Column toColumn() {
            return new EntityProxy.Parser.Parsed.Column(column, column, Object.class, (o, v) -> {}, getter::apply).setPrimary(isId).setHasDefault(hasDefault);
        }
    }

    /**
     * 提供：表名
     * <br/>
     * 生成列信息（如需使用动态插入语句，例如 QueryProSql.create().insert(Map&gt;String, Object>，则必须重写以实现该功能，可参考{@link cn.cloudself.query.resolver.impl.JdbcQSR}的实现)
     * <br/>
     * 该方法可重写
     * @see  cn.cloudself.query.resolver.impl.JdbcQSR
     */
    protected Collection<Column> getColumnsDynamic(String table, boolean onlyPrimaryKeys) {
        throw new IllegalImplements("必须重写以实现动态获取列信息，才可调用动态插入语句。可参考JdbcQSR.getColumnsDynamic的实现。");
    }

    /**
     * 将{@link QueryStructure}解析至SQL并执行
     *
     * @param queryStructure {@link QueryStructure}
     * @param payload 载荷 {@link QueryPayload}
     * @param resultClass 返回结果的类型。{@link QSRTmpl}模版支持`JavaBean`, `Map`, 以及`Long`, `String`, `Boolean`等基本类型。另外，默认使用的{@link cn.cloudself.query.resolver.impl.JdbcQSR}(为{@link QSRTmpl}的子类)还额外支持{@link cn.cloudself.query.resolver.impl.JdbcQSR.IResultSetWalker}
     *              insert时为对象的类型
     * @return clazz为基本类型时，可能返回List&lt;T?> 当动作为insert时，返回值为List&lt;ID>
     * @param <T> see resultClass
     */
    @NotNull
    @Override
    public <T> List<T> resolve(@NotNull QueryStructure queryStructure, @NotNull QueryPayload payload, @NotNull Class<T> resultClass) {
        if (queryStructure.action() == QueryStructureAction.INSERT) {
            throw new IllegalCall("对于insert操作使用insert方法");
        }
        return withContext(resultClass, queryStructure, payload, (accessor, qs) -> {
            final SqlAndParams sqlAndParams = queryStructureToSql(queryStructure);
            return doResolveAction(payload.queryProClass(), resultClass).resolve(accessor, qs.action(), sqlAndParams);
        });
    }

    /**
     * 将{@link QueryStructure}解析至SQL并执行(insert)
     * insert方法和resolve方法的区别是 返回结果不一样
     *
     * @param queryStructure {@link QueryStructure}
     * @param payload 载荷
     * @param javaBeanClass Class&lt;JavaBean> 由于插入对象支持map类型，所以需要一个参照的JavaBean.class对象
     * @return List&lt;ID> 插入数据的id列表(默认的实现不支持复合主键）
     */
    @NotNull
    @Override
    public List<Object> insert(@NotNull QueryStructure queryStructure, @NotNull QueryPayload payload, @NotNull Class<?> javaBeanClass) {
        final Insert insert = queryStructure.insert();
        final Collection<?> objs = insert == null ? null : insert.data();
        if (objs == null) {
            throw new IllegalImplements("INSERT操作必须传qs.insert.data");
        }

        final boolean replace = QueryStructureAction.REPLACE.equals(queryStructure.action());

        final EntityProxy.Parser.Parsed parsedClass = EntityProxy.Parser.of(javaBeanClass).parse();
        final String tableName = parsedClass.name();
        final EntityProxy.Parser.Parsed.Column id = parsedClass.id();

        final Collection<EntityProxy.Parser.Parsed.Column> columns = parsedClass.columns().values();

        final Class<?> idColumnType;
        if (replace) {
            idColumnType = null;
        } else {
            idColumnType = id == null ? null : id.javaType();
            if (idColumnType == null) {
                logger.warn("没有找到主键或其对应的Class, 不会有返回结果");
            }
        }

        return withContext(idColumnType, queryStructure, payload, (actions, qs) -> {
            final List<SqlAndParams> sqlAndParamsList = insertObjectToSql(replace, objs, tableName, columns);
            final List<Object> results = new ArrayList<>();
            for (SqlAndParams sqlAndParams : sqlAndParamsList) {
                results.addAll(actions.doInsert(payload.queryProClass(), sqlAndParams, idColumnType));
            }
            return results;
        });
    }

    /**
     * 执行一个SQL语句
     *
     * @param configStore code 级别的配置, 使用 {@code QueryProConfig.code.use} 处理
     * @param sql 单条sql语句 e.g. SELECT * FROM user WHERE user.id = ?
     * @param params 参数数组 e.g. [1]
     * @param resultClass `JavaBean`, `Map`, and basic type e.g. `Long`, `String`, `Boolean` etc. for select by default; [Int] for update
     * @param action {@link QueryStructureAction}
     * @return clazz为基本类型时，可能返回List&lt;T?>
     * @param <T> see param resultClass
     */
    @NotNull
    @Override
    public <T> List<T> resolve(@NotNull HashMapStore configStore, @NotNull QueryStructureAction action, @NotNull String sql, @NotNull Object[] params, @NotNull Class<T> resultClass) {
        return withContext(configStore, accessor -> doResolveAction(null, resultClass).resolve(accessor, action, new SqlAndParams(sql, params)));
    }

    private interface Resolver<T> {
        List<T> resolve(DatabaseAccessor accessor, QueryStructureAction queryStructureAction, SqlAndParams sqlAndParams);
    }
    private <T> Resolver<T> doResolveAction(@Nullable Class<?> queryPro, Class<T> clazz) {
        return (actions, action, sqlAndParams) -> {
            switch (action) {
                case SELECT:
                    return actions.doSelect(queryPro, sqlAndParams, clazz);
                case UPDATE:
                    return listOf(actions.doUpdate(queryPro, sqlAndParams, clazz));
                case DELETE :
                    return listOf(actions.doDelete(queryPro, sqlAndParams, clazz));
                case INSERT:
                case REPLACE:
                    return actions.doInsert(queryPro, sqlAndParams, clazz);
                default:
                    throw new IllegalImplements("unknown action type");
            }
        };
    }

    private <T> List<T> listOf(T item) {
        final ArrayList<T> list = new ArrayList<>();
        list.add(item);
        return list;
    }

    /**
     * 使用多条语句和参数执行更新，创建，删除等非select语句
     *
     * @param sqlAndParamsList 多对sql and params
     * @return 执行器，需手动执行或者直接使用 {@link #resolve(HashMapStore, List, Class, Around)}
     */
    @NotNull
    @Override
    public Actuator executor(@NotNull HashMapStore configStore, @NotNull List<SqlAndParams> sqlAndParamsList, @Nullable Around around) {
        if (sqlAndParamsList.isEmpty()) {
            throw new IllegalParameters("空SQL，无法执行");
        }

        return withContext(configStore, lifecycledDatabaseAccessor -> new Actuator() {
            final int iMax = sqlAndParamsList.size() - 1;
            final int realMax; // 不算注释，真正的最后一行sql的下标
            {
                int max = 0;
                for (int i = 0; i <= iMax; i++) {
                    if (!sqlAndParamsList.get(i).emptyStatement()) {
                        max = i;
                    }
                }
                realMax = max;
            }
            private Object result;
            private int idx = 0;
            private SqlAndParams currentSqlAndParams;
            private final int StatusUninit = 0;
            private final int StatusPrepared = 1;
            private int status = StatusUninit;

            @Override
            public boolean isLast() {
                return idx == iMax;
            }

            @Override
            public void next() {
                idx++;
                this.status = StatusUninit;
            }

            @Override
            public SqlAndParams prepareAndGetSqlAndParams() {
                if (this.status != StatusUninit) {
                    throw new IllegalCall("prepare操作不能执行两遍");
                }
                currentSqlAndParams = sqlAndParamsList.get(idx);
                if (around != null) {
                    if (around.shouldSkip(lifecycledDatabaseAccessor, currentSqlAndParams)) {
                        next();
                        return prepareAndGetSqlAndParams();
                    }
                    currentSqlAndParams = around.beforeExecute(lifecycledDatabaseAccessor, currentSqlAndParams);
                }
                this.status = StatusPrepared;
                return currentSqlAndParams;
            }

            @Override
            public <T> List<T> runSql(@Nullable SqlAndParams sqlAndParams, Class<T> resultClass) {
                if (this.status < StatusPrepared) {
                    throw new IllegalCall("resolve前需先调用prepareAndGetSqlAndParams()");
                }
                if (sqlAndParams == null) {
                    sqlAndParams = currentSqlAndParams;
                }

                final boolean voidResult = resultClass == void.class || resultClass == Void.class;

                if (!sqlAndParams.emptyStatement()) {
                    if (idx != realMax || voidResult) {
                        lifecycledDatabaseAccessor.doUpdate(null, sqlAndParams, Void.class);
                    } else {
                        this.result = lifecycledDatabaseAccessor.doSelect(null, sqlAndParams, resultClass);
                    }
                }

                if (around != null) {
                    around.afterExecute(sqlAndParams);
                }
                if (voidResult) {
                    return null;
                }
                //noinspection unchecked
                return iMax == idx ? (List<T>) this.result : null;
            }
        });
    }

    /**
     * 动态插入
     * 通过 objs 配合数据库表名，动态生成sql语句，并执行
     *
     * @param configStore code 级别的配置, 使用 {@code QueryProConfig.code.use} 处理
     * @param table 数据库表名
     * @param objs 需要插入的Map类型的集合
     * @param replace true 生成REPLACE语句, false 生成INSERT语句
     * @param idColumnClazz 如需返回ID，指定ID的类型，当操作为replace时，clazz只能为null或者void
     * @return id列表：只有当生成了id, 且clazz不为null, 不为void时，id才会返回; 否则, 对应记录会以null占位
     * @param <ID> see idColumnClazz
     */
    @NotNull
    @Override
    public <ID> List<ID> insert(
            @NotNull HashMapStore configStore,
            @NotNull String table,
            @NotNull Collection<? extends Map<String, ?>> objs,
            boolean replace,
            @Nullable Class<ID> idColumnClazz
    ) {
        final List<EntityProxy.Parser.Parsed.Column> columns = getColumnsDynamic(table, false).stream().map(Column::toColumn).collect(Collectors.toList());
        final List<SqlAndParams> sqlAndParamsList = insertObjectToSql(replace, objs, table, columns);
        return withContext(configStore, accessor -> {
            final List<ID> results = new ArrayList<>();
            for (SqlAndParams sqlAndParams : sqlAndParamsList) {
                final List<ID> tempIds = accessor.doInsert(null, sqlAndParams, replace ? null : idColumnClazz);
                if (!replace) {
                    results.addAll(tempIds);
                }
            }
            return results;
        });
    }

    /**
     * 动态删除行
     *
     * @param configStore code 级别的配置, 使用 {@code QueryProConfig.code.use} 处理
     * @param table       数据库表名
     * @param primaryKeys 需要删除的主键值
     * @param keyColumn   需要删除的主键键名，如果为null，将从数据库表结构中获取
     * @param clazz       int, boolean, void等
     * @return 影响的行数
     */
    @Nullable
    @Override
    public <T> T removeByPrimaryKey(@NotNull HashMapStore configStore, @NotNull String table, @NotNull Object[] primaryKeys, @Nullable String keyColumn, @Nullable Class<T> clazz) {
        if (primaryKeys.length == 0) {
            throw new IllegalParameters("没有可以删除的对象，primaryKeys为空");
        }

        if (keyColumn == null) {
            //noinspection deprecation
            final Column id = QueryProConfig.code.use(configStore.toMap(), ctx -> IterablePlus.first(getColumnsDynamic(table, true)));
            if (id == null) {
                throw new IllegalParameters("获取不到主键键名，请指定参数keyColumn");
            }
            keyColumn = id.column;
        }

        final String finalKeyColumn = keyColumn;
        return withContext(configStore, accessor -> accessor.doDelete(primaryKeysToDeleteSql(table, finalKeyColumn, primaryKeys), clazz));
    }

    /**
     * 动态更新表
     *
     * @param configStore               code 级别的配置, 使用 {@code QueryProConfig.code.use} 处理
     * @param table                     数据库表名
     * @param keyColumn                 需要更新的主键键名
     * @param withPrimaryKeyPartialRows 行数据，注意每一行都至少存在两个键值，其中一个是id，如果要将数据更新为null，
     *                                  需要传入{@link Const#NULL}或者将skipNull参数设置为false
     * @param skipNull                  当行数据的的map-entry.value为null时，是否跳过该键值，如果选择跳过，仍然可以传入{@link Const#NULL}将值更新为null
     * @param clazz                     int, boolean, void等
     * @return 影响的行数
     */
    @Nullable
    @Override
    public <T> T update(
            @NotNull HashMapStore configStore,
            @NotNull String table,
            @Nullable String keyColumn,
            @NotNull Collection<? extends Map<String, ?>> withPrimaryKeyPartialRows,
            boolean skipNull,
            @Nullable Class<T> clazz
    ) {
        //noinspection deprecation
        final Collection<Column> columnObjects = QueryProConfig.code.use(configStore.toMap(), ctx -> getColumnsDynamic(table, false));
        final Set<String> columns = new HashSet<>();
        for (Column column : columnObjects) {
            columns.add(column.column);
            if (keyColumn == null && column.isId) {
                keyColumn = column.column;
            }
        }

        if (keyColumn == null) {
            throw new IllegalParameters("获取不到主键键名，请指定参数keyColumn");
        }

        final List<SqlAndParams> sqlAndParamsList = updateObjectToSql(table, columns, keyColumn, withPrimaryKeyPartialRows, skipNull);

        final boolean nullResult = clazz == null || clazz == void.class || clazz == Void.class;
        Class<?> finalClass = nullResult ? null : Integer.class;
        return (T) withContext(configStore, accessor -> {
            int count = 0;
            for (SqlAndParams sqlAndParams : sqlAndParamsList) {
                final Object o = accessor.doUpdate(sqlAndParams, finalClass);
                if (o instanceof Integer) {
                    count = count + (Integer) o;
                }
            }
            if (nullResult) {
                return null;
            }
            final ClassPro classPro = ClassPro.of(clazz);
            if (classPro.compatibleWithInt()) {
                return count;
            } else if (classPro.compatibleWithBool()) {
                return count > 0;
            } else {
                throw new UnSupportException("un support return type");
            }
        });
    }

    private <R> R withContext(HashMapStore store, Function<DatabaseAccessor, R> useResolver) {
        final Map<String, Object> config = store.toMap();

        // 创建resolver 代理，该代理包含对生命周期函数beforeRunSql的调用
        final DatabaseAccessor resolver = LifecycleHelpers.wrap(this);
        //noinspection deprecation
        return QueryProConfig.code.use(config, ctx -> useResolver.apply(resolver));
    }

    private interface UseResolver<R> {
        R use(DatabaseAccessor accessor, QueryStructure queryStructure);
    }
    private <R> R withContext(@Nullable Class<?> resultClazz, QueryStructure qs, QueryPayload payload, UseResolver<R> useResolver) {
        final Map<String, Object> config = payload.configs().toMap();

        // 创建resolver 代理，该代理包含对生命周期函数beforeRunSql的调用
        final DatabaseAccessor resolver = LifecycleHelpers.wrap(this);
        //noinspection deprecation
        return QueryProConfig.code.use(config, ctx -> {
            final QueryStructure transformedQs = LifecycleHelpers.beforeParsingQueryStructure(resultClazz, qs, payload).unwrap();
            final R res = useResolver.use(resolver, transformedQs);
            return LifecycleHelpers.afterQueryStructureResultQueried(resultClazz, transformedQs, payload, res).unwrap();
        });
    }

    private static final Log logger = LogFactory.getLog(QSRTmpl.class);
}
