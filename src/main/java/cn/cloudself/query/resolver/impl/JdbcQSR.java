package cn.cloudself.query.resolver.impl;

import cn.cloudself.query.QueryProSession;
import cn.cloudself.query.config.QueryProConfig;
import cn.cloudself.query.config.IOnlyGlobalConfig;
import cn.cloudself.query.config.IQueryProConfig;
import cn.cloudself.exception.ConfigException;
import cn.cloudself.exception.IllegalCall;
import cn.cloudself.exception.UnSupportException;
import cn.cloudself.query.config.SqlAndParams;
import cn.cloudself.query.psi.Const;
import cn.cloudself.query.resolver.*;
import cn.cloudself.util.ext.EntityProxy;
import cn.cloudself.util.ext.ClassPro;
import cn.cloudself.util.ext.ReflectPro;
import cn.cloudself.util.ext.SqlPro;
import cn.cloudself.util.framework.AutoRefreshDataSource;
import cn.cloudself.util.structure.ListEx;
import cn.cloudself.util.log.Log;
import cn.cloudself.util.log.LogFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import java.lang.reflect.Constructor;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.*;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import javax.sql.DataSource;

/**
 * `QSR`接口的`JDBC`实现
 */
public class JdbcQSR extends QSRTmpl {
    public interface IResultSetWalker {
        @SuppressWarnings("unused")
        void walk(ResultSet rs) throws Exception;
    }

    @Override public <T> List<T> doSelect(@Nullable Class<?> queryPro, @NotNull SqlAndParams sqlAndParams, @NotNull Class<T> clazz) {
        return tryUseConnection(queryPro, connection -> {
            final String sql = sqlAndParams.sql();
            final Object[] params = sqlAndParams.params();
            final List<T> resultList = new ArrayList<>();
            try (final PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                setParam(preparedStatement, params, OnNull.NULL);

                final ResultSet resultSet = preparedStatement.executeQuery();
                if (IResultSetWalker.class != clazz && IResultSetWalker.class.isAssignableFrom(clazz)) {
                    final Constructor<T> constructor = clazz.getDeclaredConstructor();
                    constructor.setAccessible(true);
                    final T r = constructor.newInstance();
                    ReflectPro.of(r).invoke("walk", resultSet);
                    return ListEx.of(r);
                }

                final EntityProxy<T> proxy = EntityProxy.fromClass(clazz);
                while (resultSet.next()) {
                    resultList.add(mapRow(proxy, resultSet));
                }
            }
            return resultList;
        });
    }

    @Override public <T> T doUpdate(@Nullable Class<?> queryPro, @NotNull SqlAndParams sqlAndParams, @Nullable Class<T> clazz) {
        return tryUseConnection(queryPro, connection -> {
            final String sql = sqlAndParams.sql();
            final Object[] params = sqlAndParams.params();

            final OnNull setParamMode = queryPro == null ? OnNull.NULL : OnNull.BREAK;
            try (final Statement statement = params.length == 0 ? connection.createStatement() : withSetParam(connection.prepareStatement(sql), params, setParamMode)) {
                if (params.length == 0) {
                    statement.execute(sql);
                } else {
                    ((PreparedStatement) statement).execute();
                }
                if (clazz == null) {
                    return null;
                }
                if (clazz == Void.class || clazz == void.class) {
                    return null;
                }
                final int updateCount = statement.getUpdateCount();

                final ClassPro classPro = ClassPro.of(clazz);
                if (classPro.compatibleWithInt()) {
                    //noinspection unchecked
                    return (T) (Object) updateCount;
                }
                if (classPro.compatibleWithBool()) {
                    //noinspection unchecked
                    return (T) (Object) (updateCount > 0);
                }
                throw new UnSupportException("不支持的class, 目前只支持List.class, Integer.class, int.class, Boolean.class, boolean.class, Void.class, void.class");
            }
        });
    }

    @Override
    public <T> T doDelete(@Nullable Class<?> queryPro, @NotNull SqlAndParams sqlAndParams, @Nullable Class<T> clazz) {
        return doUpdate(queryPro, sqlAndParams, clazz);
    }

    @Override
    public <ID> List<ID> doInsert(@Nullable Class<?> queryPro, @NotNull SqlAndParams sqlAndParams, @Nullable Class<ID> clazz) {
        final EntityProxy<ID> idColumnProxy = (clazz == null || void.class == clazz || Void.class == clazz)
                ? null
                : EntityProxy.fromClass(clazz);

        return tryUseConnection(queryPro, connection -> {
            final List<ID> results = new ArrayList<>();
            final String sql = sqlAndParams.sql();
            final Object[] params = sqlAndParams.params();
            try (final PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
                setParam(preparedStatement, params, OnNull.NULL);
                preparedStatement.execute();
                if (idColumnProxy != null) {
                    final ResultSet resultSet = preparedStatement.getGeneratedKeys();
                    while (resultSet.next()) {
                        results.add(mapRow(idColumnProxy, resultSet));
                    }
                }
            }
            if (results.isEmpty()) {
                results.add(null);
            }
            return results;
        });
    }

    @Override
    public Collection<QSRTmpl.Column> getColumnsDynamic(String table_, boolean onlyPrimaryKeys) {
        final IQueryProConfig.DatabaseType dbType = QueryProConfig.computed.dbType();
        final boolean isMsSql = IQueryProConfig.DatabaseType.MSSQL == dbType;
        return tryUseConnection(null, connection -> {
            final DatabaseMetaData metaData = connection.getMetaData();
            final String catalog = connection.getCatalog();
            final String schema;
            final String table;
            if (isMsSql) {
                final SqlPro.MsSchemaAndTable splitSchemaAndTable = SqlPro.of(table_).splitter().msTableToSchemaAndTable();
                schema = splitSchemaAndTable.schema();
                table = splitSchemaAndTable.table();
            } else {
                schema = connection.getSchema();
                table = table_;
            }

            // metadata https://dev.mysql.com/doc/refman/8.0/en/show-columns.html
            final ResultSet tableSet = metaData.getTables(catalog, schema, table, new String[]{"TABLE", "VIEW"});

            tableSet.next();

            final String tableName = tableSet.getString("TABLE_NAME");

            final List<Column> columns = new ArrayList<>();

            final ResultSet primaryKeys = metaData.getPrimaryKeys(catalog, schema, tableName);
            if (onlyPrimaryKeys) {
                while (primaryKeys.next()) {
                    final String id = primaryKeys.getString("COLUMN_NAME");
                    columns.add(new Column(
                            id,
                            it -> {
                                if (it instanceof Map) {
                                    return ((Map<?, ?>) it).get(id);
                                }
                                throw new IllegalCall("不支持非Map类型");
                            },
                            true,
                            null
                    ));
                }
                return columns;
            }

            String id = null;
            boolean idDefined = false;
            while (primaryKeys.next()) {
                if (idDefined) {
                    id = null;
                    logger.warn("[WARN] 目前仍不支持复合主键");
                } else {
                    id = primaryKeys.getString("COLUMN_NAME");
                    idDefined = true;
                }
            }

            final ResultSet columnSet = metaData.getColumns(catalog, schema, tableName, null);
            while (columnSet.next()) {
                final String columnName = columnSet.getString("COLUMN_NAME");
                if (columnName == null) {
                    throw new RuntimeException("找不到列名");
                }
                final boolean hasDefault = columnSet.getString("COLUMN_DEF") != null;
                final Column column = new Column(
                        columnName,
                        it -> {
                            if (it instanceof Map) {
                                return ((Map<?, ?>) it).get(columnName);
                            }
                            throw new IllegalCall("不支持非Map类型");
                        },
                        columnName.equals(id),
                        hasDefault
                );
                columns.add(column);
            }

            return columns;
        });
    }

    public enum OnNull {
        BREAK,
        NULL,
    }

    private PreparedStatement withSetParam(PreparedStatement preparedStatement, Object[] params, OnNull onNull) throws SQLException {
        setParam(preparedStatement, params, onNull);
        return preparedStatement;
    }
    public void setParam(PreparedStatement preparedStatement, Object[] params, OnNull onNull) throws SQLException {
        final int paramsLength = params.length;
        for (int i = 0; i < paramsLength; i++) {
            final Object param = params[i];
            if (Const.NULL.equals(param)) {
                preparedStatement.setNull(i + 1, Types.NULL);
            } else if (param instanceof BigDecimal) {
                preparedStatement.setBigDecimal(i + 1, (BigDecimal) param);
            } else if (param instanceof Boolean) {
                preparedStatement.setBoolean(i + 1, (Boolean) param);
            } else if (param instanceof Byte) {
                preparedStatement.setByte(i + 1, (Byte) param);
            } else if (param instanceof byte[]) {
                preparedStatement.setBytes(i + 1, (byte[]) param);
            } else if (param instanceof Time) {
                preparedStatement.setTime(i + 1, (Time) param);
            } else if (param instanceof Timestamp) {
                preparedStatement.setTimestamp(i + 1, (Timestamp) param);
            } else if (param instanceof java.sql.Date) {
                preparedStatement.setTimestamp(i + 1, new Timestamp(((Date) param).getTime()));
            } else if (param instanceof java.util.Date) {
                preparedStatement.setTimestamp(i + 1, new Timestamp(((java.util.Date) param).getTime()));
            } else if (param instanceof Double) {
                preparedStatement.setDouble(i + 1, (Double) param);
            } else if (param instanceof Enum) {
                preparedStatement.setString(i + 1, ((Enum<?>) param).name());
            } else if (param instanceof Float) {
                preparedStatement.setFloat(i + 1, (Float) param);
            } else if (param instanceof Integer) {
                preparedStatement.setInt(i + 1, (Integer) param);
            } else if (param instanceof LocalDate) {
                preparedStatement.setDate(i + 1, java.sql.Date.valueOf((LocalDate) param));
            } else if (param instanceof LocalTime) {
                preparedStatement.setTime(i + 1, Time.valueOf((LocalTime) param));
            } else if (param instanceof LocalDateTime) {
                preparedStatement.setTimestamp(i + 1, Timestamp.valueOf((LocalDateTime) param));
            } else if (param instanceof Long) {
                preparedStatement.setLong(i + 1, (Long) param);
            } else if (param instanceof Short) {
                preparedStatement.setShort(i + 1, (Short) param);
            } else if (param instanceof String) {
                preparedStatement.setString(i + 1, (String) param);
            } else if (param instanceof BigInteger) {
                preparedStatement.setObject(i + 1, param);
            } else {
                if (param == null) {
                    if (onNull == OnNull.NULL) {
                        preparedStatement.setNull(i + 1, Types.NULL);
                    } else {
                        throw new IllegalCall("不支持null, 使用Const.NULL代替");
                    }
                } else {
                    final Class<?> clazz = param.getClass();
                    final IOnlyGlobalConfig.SqlParamSetter<?> sqlParamSetter = QueryProConfig.computed.sqlParamSetter(clazz);
                    if (sqlParamSetter != null) {
                        //noinspection unchecked
                        ((IOnlyGlobalConfig.SqlParamSetter<Object>) sqlParamSetter).set(preparedStatement, i + 1, param);
                    } else {
                        logger.warn(
                            "为sql设置了一个可能不支持的参数(取决于JDBC的实现){0}，" +
                                "如果JDBC能正常处理该参数，可以使用" +
                                "QueryProConfig.global.addSqlParamSetter({1}.class, PreparedStatement::setObject);" +
                                "屏蔽该警告",
                            param, clazz.getName()
                        );
                        preparedStatement.setObject(i + 1, param);
                    }
                }
            }
        }
    }

    public  <T> T mapRow(EntityProxy<T> proxy, ResultSet resultSet) throws SQLException, ClassNotFoundException {
        final EntityProxy.BeanInstance<T, T> resultProxy = proxy.newInstance();

        final ResultSetMetaData metaData = resultSet.getMetaData();
        final int columnCount = metaData.getColumnCount();

        for (int i = 1; i <= columnCount; i++) {
            final String columnName = metaData.getColumnLabel(i);
            final String columnType = metaData.getColumnTypeName(i);
            Class<?> beanNeedType = resultProxy.getPropertyType(columnName);

            if (beanNeedType == null) {
                for (Map.Entry<Function<IOnlyGlobalConfig.DbColumnInfo, Boolean>, Class<?>> entry : QueryProConfig.computed.dbColumnInfoToJavaType().entrySet()) {
                    final Function<IOnlyGlobalConfig.DbColumnInfo, Boolean> tester = entry.getKey();
                    final Class<?> jt = entry.getValue();
                    if (tester.apply(new IOnlyGlobalConfig.DbColumnInfo(columnType, columnName))) {
                        beanNeedType = jt;
                        break;
                    }
                }
            }

            final Object value;
            if (beanNeedType == null) {
                value = resultSet.getObject(i);
            } else {
                final IOnlyGlobalConfig.ResultSetGetter<?> parser = QueryProConfig.computed.resultSetParser(beanNeedType);
                if (parser != null) {
                    value = parser.get(resultSet, i); /* value */
                } else {
                    //noinspection OptionalAssignedToNull
                    Optional<?> valueOpt = null;
                    for (IOnlyGlobalConfig.ResultSetParserEx resultSetParserEx : QueryProConfig.computed.resultSetParserEx()) {
                        final Optional<?> valueOptMay = resultSetParserEx.parse(resultSet, beanNeedType, i);
                        if (valueOptMay.isPresent()) {
                            valueOpt = valueOptMay;
                            break;
                        }
                    }
                    //noinspection OptionalAssignedToNull
                    if (valueOpt != null) {
                        value = valueOpt.get();
                    } else {
                        // 没有找到生成目标类型的配置，尝试使用数据库默认的类型转换成目标类型，如果不行，则抛出异常
                        final String couldConvertClassName = metaData.getColumnClassName(i);
                        if (beanNeedType.isAssignableFrom(Class.forName(couldConvertClassName))) {
                            value = resultSet.getObject(i);
                        } else {
                            throw new ConfigException("不支持将name: {0}, type: {1}转换为{2}, " +
                                    "使用QueryProConfig.global.addResultSetParser添加转换器",
                                columnName, columnType, beanNeedType.getName());
                        }
                    }
                }
            }

            resultProxy.setProperty(columnName, resultSet.wasNull() ? null : value);
        }

        return resultProxy.toBean();
    }

    private interface ThrowableFunction<T, R> {
        R apply(T arg) throws Exception;
    }

    private <R> R tryUseConnection(@Nullable Class<?> clazz, ThrowableFunction<Connection, R> autoUse) {
        try {
            final AtomicReference<Throwable> e = new AtomicReference<>();
            final R res = useConnection(clazz, t -> {
                try {
                    return autoUse.apply(t);
                } catch (Exception ex) {
                    e.set(ex);
                    return null;
                }
            });
            final Throwable throwable = e.get();
            if (throwable != null) {
                throw throwable;
            }
            return res;
        } catch (Throwable e) {
            if (e instanceof RuntimeException) {
                throw (RuntimeException) e;
            } else {
                throw new RuntimeException(e);
            }
        }
    }

    protected  <R> R useConnection(@Nullable Class<?> clazz, Function<Connection, R> autoUse) {
        final Connection connection = getConnection(clazz);
        if (shouldClose(clazz)) {
            Throwable e = null;
            try {
                return autoUse.apply(connection);
            } catch (Throwable t) {
                e = t;
                throw t;
            } finally {
                try {
                    connection.close();
                } catch (SQLException se) {
                    if (e != null) {
                        e.addSuppressed(se);
                    }
                    logger.warn("connection close failed.");
                }
                logger.debug("connection closed.");
            }
        } else {
            return autoUse.apply(connection);
        }
    }

    private final ThreadLocal<Boolean> connectionByConfig = new ThreadLocal<>();
    private Connection getConnection(@Nullable Class<?> clazz) {
        final Connection configuredConnection = QueryProConfig.computed.connection();
        if (configuredConnection != null) {
            connectionByConfig.set(true);
            return configuredConnection;
        }
        connectionByConfig.set(false);

        DataSource dataSource = QueryProConfig.computed.dataSource(clazz);
        if (dataSource == null) {
            dataSource = new AutoRefreshDataSource(
                    () -> {
                        throw new ConfigException("无法找到DataSource, 使用QueryProConfig.setDataSource设置");
                    }
            );
            QueryProConfig.global.dataSource(dataSource);
        }
        if (isDataSourceUtilsPresent && TransactionSynchronizationManager.isActualTransactionActive()) {
            return DataSourceUtils.getConnection(dataSource);
        }
        if (QueryProSession.__Instance.isActualSessionActive(dataSource)) {
            return QueryProSession.__Instance.getConnection(dataSource);
        } else {
            final Connection connection;
            try {
                connection = dataSource.getConnection();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            logger.debug("connection got.");
            return connection;
        }
    }

    private boolean shouldClose(@Nullable Class<?> clazz) {
        if (Boolean.TRUE.equals(connectionByConfig.get())) {
            return false;
        } else if (isDataSourceUtilsPresent) {
            if (TransactionSynchronizationManager.isActualTransactionActive()) {
                return false;
            } else {
                return !QueryProSession.__Instance.isActualSessionActive(QueryProConfig.computed.dataSource(clazz));
            }
        } else {
            return !QueryProSession.__Instance.isActualSessionActive(QueryProConfig.computed.dataSource(clazz));
        }
    }

    private static final Log logger = LogFactory.getLog(JdbcQSR.class);
    private static boolean isDataSourceUtilsPresent;
    static {
        try {
            Class.forName("org.springframework.jdbc.datasource.DataSourceUtils");
            isDataSourceUtilsPresent = true;
        } catch (Throwable e) {
            isDataSourceUtilsPresent = false;
        }
    }
}
