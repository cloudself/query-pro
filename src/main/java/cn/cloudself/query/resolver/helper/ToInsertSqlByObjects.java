package cn.cloudself.query.resolver.helper;

import cn.cloudself.query.config.QueryProConfig;
import cn.cloudself.query.config.SqlAndParams;
import cn.cloudself.query.config.IQueryProConfig;
import cn.cloudself.util.ext.EntityProxy;
import org.intellij.lang.annotations.Language;

import java.util.*;

/**
 * 工具类，目的是根据需要`insert`的对象以及目标数据库表信息生成对应的`SQL`语句(JDBC风格，参数占位符为`?`)
 * 约定：当返回结果的size为1且groupedParams有值。表明数据量较大，使用批量插入模式
 */
public class ToInsertSqlByObjects {
    public static List<SqlAndParams> toSql(
            boolean replace,
            Collection<?> objs,
            String table,
            Collection<EntityProxy.Parser.Parsed.Column> columnCollection
    ) {
        final IQueryProConfig.DatabaseType dbType = QueryProConfig.computed.dbType();
        final Integer maxParameterSize = QueryProConfig.computed.maxParameterSize();
        final boolean isMsSql = IQueryProConfig.DatabaseType.MSSQL == dbType;
        final char leftQuota = isMsSql ? '[' : '`';
        final char rightQuota = isMsSql ? ']' : '`';
        final List<SqlAndParams> sqlAndParamsList = new ArrayList<>();
        // 对象中是否存在ID，存在ID的对象和不存在ID的对象，不能放在同一条`INSERT`语句中插入，否则JDBC会返回错误的ID
        final boolean[] hasId = new boolean[objs.size()];
        // 有序列
        final List<EntityProxy.Parser.Parsed.Column> columns = new ArrayList<>(columnCollection);
        // 所有对象中全为NULL的字段
        final Map<String, Boolean> allNullColumnMap = new HashMap<>();
        // 初始化
        for (EntityProxy.Parser.Parsed.Column column : columns) {
            allNullColumnMap.put(column.name(), true);
        }
        // 按照columns顺序排列的值列表 列表
        final Object[][] paramsArr = new Object[objs.size()][];
        int oi = 0;
        final int columnSize = columns.size();
        for (Object obj : objs) {
            final Object[] item = new Object[columnSize];
            paramsArr[oi] = item;
            for (int j = 0; j < columnSize; j++) {
                final EntityProxy.Parser.Parsed.Column col = columns.get(j);
                final Object value = col.getter().get(obj);
                if (col.isPrimary() && value != null) {
                    hasId[oi] = true;
                }
                if (value != null) {
                    allNullColumnMap.put(col.name(), false);
                }
                item[j] = value;
            }
            oi++;
        }

        final StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append(replace ? "REPLACE INTO " : "INSERT INTO ");
        final boolean useQuota = table.charAt(0) != leftQuota;
        if (!isMsSql && useQuota) {
            sqlBuilder.append(leftQuota);
        }
        sqlBuilder.append(table);
        if (!isMsSql && useQuota) {
            sqlBuilder.append(rightQuota);
        }
        sqlBuilder.append(" (");
        boolean firstAppend = false;
        for (EntityProxy.Parser.Parsed.Column col : columns) {
            final String column = col.name();
            final Boolean value = allNullColumnMap.get(column);
            if (value != null && value) {
                continue;
            }
            if (firstAppend) {
                sqlBuilder.append(", ");
            } else {
                firstAppend = true;
            }
            sqlBuilder.append(leftQuota).append(column).append(rightQuota);
        }
        sqlBuilder.append(") VALUES ");
        final String sqlPrefix = sqlBuilder.toString();

        final int maxPageSize = 1000;

        int parameterCount = 0;
        List<Object> curParams = new ArrayList<>();
        boolean firstRow = true;
        int firstRowIndex = 0;
        boolean batchMode = false;
        boolean lastHasId = false;
        int fromRowNumber = 1;
        final int paramsArrayLength = paramsArr.length;
        for (int i = 0; i < paramsArrayLength; i++) {
            final Object[] params = paramsArr[i];
            if (
                (maxParameterSize != null && (parameterCount + params.length + 1) >= maxParameterSize) ||
                (sqlBuilder.length() > 1000 * 500) ||
                (i != 0 && i % maxPageSize == 0) ||
                (hasId[i] && !lastHasId && !firstRow) ||
                (lastHasId && !hasId[i])
            ) { // ~0.5M if all ascii char
                batchMode = true;
                sqlAndParamsList.add(new SqlAndParams("/*BATCH MODE " + fromRowNumber + " to " + i + "*/ " + sqlBuilder, curParams.toArray()));
                sqlBuilder.setLength(0);
                sqlBuilder.append(sqlPrefix);
                curParams = new ArrayList<>();
                firstRow = true;
                firstRowIndex = i;
                fromRowNumber = i;
                parameterCount = 0;
            }
            lastHasId = hasId[i];
            if (firstRow) {
                firstRow = false;
                sqlBuilder.append('(');
            } else {
                sqlBuilder.append(", (");
            }
            boolean firstCol = true;
            final int paramsLength = params.length;
            for (int j = 0; j < paramsLength; j++) {
                final Object param = params[j];
                final EntityProxy.Parser.Parsed.Column col = columns.get(j);
                boolean useDefault = (col.getHasDefault() != null && col.getHasDefault()) && param == null;
                final Boolean b = allNullColumnMap.get(col.name());
                if (b != null && b) {
                    continue;
                }
                if (!useDefault) {
                    parameterCount++;
                    curParams.add(param);
                }
                if (firstCol) {
                    firstCol = false;
                    sqlBuilder.append(useDefault ? "DEFAULT" : "?");
                } else {
                    sqlBuilder.append(useDefault ? ", DEFAULT" : ", ?");
                }
            }
            sqlBuilder.append(')');
        }

        if (!firstRow) {
            @Language("SQL") final String sql = batchMode
                    ? "/*BATCH MODE " + (firstRowIndex + 1) + " to " + paramsArr.length + "*/ " + sqlBuilder
                    : sqlBuilder.toString();
            final Object[] params = curParams.toArray();
            sqlAndParamsList.add(new SqlAndParams(sql, params));
        }

        return sqlAndParamsList;
    }
}