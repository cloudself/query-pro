package cn.cloudself.query.resolver.helper;

import cn.cloudself.query.config.SqlAndParams;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ToDeleteSqlByPrimaryKeys {
    public static SqlAndParams toSql(@NotNull String table, @Nullable String keyColumn, @NotNull Object[] primaryKeys) {
        final StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("DELETE FROM ").append(table).append(" WHERE ").append(keyColumn).append(" in (");
        for (int i = primaryKeys.length - 1; i > 0; i--) {
            sqlBuilder.append("?, ");
        }
        sqlBuilder.append("?)");
        return new SqlAndParams(sqlBuilder.toString(), primaryKeys);
    }
}
