package cn.cloudself.query.resolver.helper;

import cn.cloudself.exception.IllegalCall;
import cn.cloudself.query.config.SqlAndParams;
import cn.cloudself.query.psi.Const;
import cn.cloudself.util.log.Log;
import cn.cloudself.util.log.LogFactory;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class ToUpdateSqlByObjects {
    private static final Log logger = LogFactory.getLog(ToUpdateSqlByObjects.class);
    public static List<SqlAndParams> toSql(
            @NotNull String table,
            @NotNull Set<String> columns,
            @NotNull String keyColumn,
            @NotNull Collection<? extends Map<String, ?>> withPrimaryKeyPartialRows,
            boolean skipNull
    ) {
        final List<SqlAndParams> sqlAndParamsList = new ArrayList<>();

        for (Map<String, ?> row : withPrimaryKeyPartialRows) {
            final StringBuilder sqlBuilder = new StringBuilder("UPDATE " + table + " SET ");
            final List<Object> params = new ArrayList<>();

            Object primaryKeyValue = null;
            boolean first = true;
            for (Map.Entry<String, ?> columnAndValue : row.entrySet()) {
                final String column = columnAndValue.getKey();
                final Object value = columnAndValue.getValue();
                if (keyColumn.equals(column)) {
                    if (value == null) {
                        throw new IllegalCall("主键键值不能为null");
                    }
                    primaryKeyValue = value;
                    continue;
                } if (!columns.contains(column) || (value == null && skipNull)) {
                    continue;
                } if (first) {
                    first = false;
                } else {
                    sqlBuilder.append(',');
                }
                sqlBuilder.append('`').append(column).append("`= ? ");
                params.add(Const.NULL.equals(value) ? null : value);
            }
            if (!first) {
                if (primaryKeyValue == null) {
                    logger.info("key: {0}, row: {1}", keyColumn, row);
                    throw new RuntimeException("找不到主键，无法更新");
                }
                sqlBuilder.append(" where ").append(keyColumn).append(" = ?");
                params.add(primaryKeyValue);
            }
            sqlAndParamsList.add(new SqlAndParams(sqlBuilder.toString(), params.toArray()));
        }

        return sqlAndParamsList;
    }
}
