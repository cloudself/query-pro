package cn.cloudself.query.resolver.helper;

import cn.cloudself.query.config.QueryProConfig;
import cn.cloudself.query.config.SqlAndParams;
import cn.cloudself.exception.IllegalCall;
import cn.cloudself.exception.MissingParameter;
import cn.cloudself.exception.UnSupportException;
import cn.cloudself.query.psi.*;
import cn.cloudself.query.psi.structure.*;
import cn.cloudself.util.structure.Pair;
import cn.cloudself.util.ext.EntityProxy;
import org.jetbrains.annotations.Nullable;

import java.lang.StringBuilder;
import java.util.*;

/**
 * 目的是将`QueryStructure`转换为`JDBC`风格的`SQL`(`SQL`语句中参数占位符为`?`)及其参数。
 */
public class ToSqlByQueryStructure {
    private final QueryStructure qs;
    private final boolean beautify = QueryProConfig.computed.beautifySql();
    private final StringBuilder sql = new StringBuilder();
    private final List<Object> indexedParams = new ArrayList<>();
    public ToSqlByQueryStructure(QueryStructure qs) {
        this.qs = qs;
    }

    public SqlAndParams toSqlWithIndexedParams() {
        final QueryStructureAction action = qs.action();
        sql.append(action.name()).append(' ');
        if (qs.distinct()) {
            sql.append("DISTINCT ");
        }
        if (action == QueryStructureAction.SELECT) {
            buildFields(qs.fields());
            sql.append(beautify ? '\n' : ' ');
        }
        if (action == QueryStructureAction.SELECT || action == QueryStructureAction.DELETE) {
            sql.append("FROM ");
        }
        buildFromClause(qs.from());
        WhereClause idWhereClause = null;
        if (action == QueryStructureAction.UPDATE) {
            final Update update = qs.update();
            if (update == null) {
                throw new MissingParameter("updateSet缺少参数, 参考.updateSet(obj)");
            }
            idWhereClause = buildUpdateSetField(update);
        }
        final List<WhereClause> wheres;
        if (idWhereClause == null) {
            wheres = qs.where();
        } else {
            wheres = new ArrayList<>(qs.where());
            wheres.add(idWhereClause);
        }
        if (action == QueryStructureAction.UPDATE && wheres.isEmpty()) {
            throw new MissingParameter("updateSet缺少参数, 需指定id字段或者where条件");
        }
        buildWheresClause(wheres);
        buildOrderByClause(qs.orderBy());
        buildLimitClause(qs.limit());

        return new SqlAndParams(sql.toString(), indexedParams.toArray());
    }

    private void buildField(@Nullable Field field, boolean whereClauseUpper) {
        if (field == null) {
            return;
        }
        final boolean upper = field.commands() == FieldCommands.UPPER_CASE || whereClauseUpper;
        if (upper) {
            sql.append("UPPER(");
        }
        if (field.table() != null) {
            sql.append('`').append(field.table()).append('`').append('.');
        }
        final String column = field.column();
        final boolean isActualColumn = !"count(*)".equals(column);
        if (isActualColumn) {
            sql.append('`').append(column).append('`');
        } else {
            sql.append(column);
        }
        if (upper) {
            sql.append(')');
        }
    }

    private void buildValue(Object v, boolean upper) {
        if (v != null) {
            sql.append(upper ? "UPPER(?)" : '?');
            indexedParams.add(v);
        }
    }

    private void buildFields(List<Field> fields) {
        if (fields.isEmpty()) {
            if (!qs.from().joins().isEmpty()) {
                sql.append('`');
                final String alias = qs.from().alias();
                if (alias != null) {
                    sql.append(alias);
                } else {
                    sql.append(qs.from().getMain());
                }
                sql.append('`').append(".*");
            } else {
                sql.append('*');
            }
            return;
        }

        final int lastIndexOfFields = fields.size() - 1;
        for (int i = 0; i <= lastIndexOfFields; i++) {
            final Field field = fields.get(i);
            buildField(field, false);
            if (i != lastIndexOfFields) {
                sql.append(",");
                sql.append(beautify ? "\n       " : ' ');
            }
        }
    }

    private void buildFromClause(QueryStructureFrom from) {
        sql.append('`').append(from.getMain()).append( '`');
        if (from.alias() != null) {
            sql.append(' ').append('`').append(from.alias()).append('`');
        }
        for (FromJoiner joiner : from.joins()) {
            sql.append(beautify ? "\n    " : ' ');
            switch (joiner.type()) {
                case LEFT_JOIN  : sql.append("LEFT JOIN "); break;
                case RIGHT_JOIN : sql.append("RIGHT JOIN "); break;
                case INNER_JOIN : sql.append("INNER JOIN "); break;
            }
            sql.append('`').append(joiner.table()).append('`');
            if (joiner.alias() != null) {
                sql.append(' ').append('`').append(joiner.alias()).append('`');
            }
            sql.append(" ON ");
            int lastIndexOfJoinOn = joiner.on().size() - 1;
            final List<FromJoinerOn> on = joiner.on();
            for (int i = 0; i < on.size(); i++) {
                final FromJoinerOn joinOn = on.get(i);
                buildField(joinOn.left(), false);
                sql.append(" = ");
                buildField(joinOn.right(), false);
                if (i != lastIndexOfJoinOn) {
                    sql.append(" AND ");
                }
            }
        }
    }

    private WhereClause buildUpdateSetField(Update update) {
        sql.append(" SET");
        final Object data = update.data();
        if (data == null) {
            throw new MissingParameter(".updateSet(obj): obj不能为null");
        }
        final boolean override = update.override();

        boolean first = true;
        final Iterator<Map.Entry<String, Object>> columns = EntityProxy.fromBean(data).iterator();
        final String idColumn = update.id();
        WhereClause idWhereClause = null;
        while (columns.hasNext()) {
            final Map.Entry<String, Object> column = columns.next();
            final String field = column.getKey();
            final Object value = column.getValue();
            if (!override && value == null) {
                continue;
            }
            if (Objects.equals(field, idColumn)) {
                if (idColumn == null) {
                    throw new IllegalCall("id必须有值");
                }
                idWhereClause = new WhereClause(new Field(qs.from().getMain(), idColumn), Const.EQUAL, value);
                continue;
            }
            if (!first) {
                sql.append(",");
            }
            sql.append(" `").append(field).append("` = ?");
            if (value != null) {
                indexedParams.add(value);
            } else {
                indexedParams.add(Const.NULL);
            }
            first = false;
        }
        if (first) {
            throw new MissingParameter(".updateSet()没有设置内容或设置的内容全为空");
        }
        return idWhereClause;
    }

    private void buildWheresClause(List<WhereClause> wheres) {
        if (wheres.isEmpty()) {
            return;
        }

        sql.append(beautify ? '\n' : ' ');
        sql.append("WHERE ");
        int lastIndexOfWheres = wheres.size() - 1;
        int leftParCount = 0;
        for (int i = 0; i <= lastIndexOfWheres; i++) {
            final WhereClause whereClause = wheres.get(i);
            final String operator = whereClause.operator();
            if (beautify) {
                if (Const.OR.equals(operator)) {
                    sql.append("\n ");
                }
                if (Const.OPEN_PAR.equals(operator)) {
                    leftParCount++;
                }
                if (Const.CLOSE_PAR.equals(operator)) {
                    leftParCount--;
                }
            }

            parseWhereClause(whereClause);

            if (lastIndexOfWheres != i &&
                    !Const.OPEN_PAR.equals(operator) &&
                    !Const.CLOSE_PAR.equals(wheres.get(i + 1).operator()) &&
                    !Const.OR.equals(operator) &&
                     ! Const.OR.equals(wheres.get(i + 1).operator())
            ) {
                sql.append(beautify && leftParCount == 0 ? "\n  " : ' ');
                sql.append("AND ");
            }
        }
    }

    void parseWhereClause(WhereClause whereClause) {
        final boolean upper = whereClause.commands() == WhereClauseCommands.UPPER_CASE;
        final Field field = whereClause.field();
        final String operator = whereClause.operator();
        final Object value = whereClause.value();
        final String whereSql = whereClause.sql();

        buildField(field, upper);
        sql.append(' ').append(operator).append(' ');

        if (value instanceof List<?> || (value != null && value.getClass().isArray())) {
            final boolean isBetween = !Const.BETWEEN.equals(operator) && !Const.NOT_BETWEEN.equals(operator);
            if (isBetween) {
                sql.append('(');
            }

            final List<?> list;
            if (value instanceof List) {
                list = (List<?>) value;
                if (list.stream().noneMatch(Objects::nonNull) && !((List<?>) value).isEmpty()) {
                    sql.append("/*WARN each item of argument list is null*/");
                }
            } else {
                list = Arrays.asList((Object[]) value);
            }
            final int lastIndexOfValues = list.size() - 1;
            for (int i = 0; i <= lastIndexOfValues; i++) {
                final Object v = list.get(i);
                if (v instanceof WhereClause) {
                    parseWhereClause((WhereClause) v);
                } else {
                    buildValue(v, upper);
                }
                if (i != lastIndexOfValues) {
                    switch (operator) {
                        case Const.IN          :
                        case Const.NOT_IN      :
                            sql.append( ", "); break;
                        case Const.BETWEEN     :
                        case Const.NOT_BETWEEN :
                        case Const.OR          :
                            sql.append( " AND "); break;
                        default: throw new UnSupportException("未知的运算符{0}", operator);
                    }
                }
            }
            if (isBetween) sql.append(')');
        } else {
            if (whereSql != null) {
                sql.append(whereSql);
            } else {
                buildValue(value, upper);
            }
        }
    }

    private void buildOrderByClause(List<OrderByClause> orderBys) {
        if (orderBys.isEmpty()) {
            return;
        }
        sql.append(beautify ? '\n' : ' ');
        sql.append("ORDER BY ");
        final int lastIndexOfOrderBy = orderBys.size() - 1;
        for (int i = 0; i <= lastIndexOfOrderBy; i++) {
            final OrderByClause orderBy = orderBys.get(i);
            buildField(orderBy.field(), false);
            sql.append(' ');
            switch (orderBy.operator()) {
                case "desc" :
                    sql.append( "DESC"); break;
                case "asc"  :
                case ""     :
                    sql.append( "ASC"); break;
                default: throw new UnSupportException("不支持的order by操作符{0}", orderBy.operator());

            }
            if (i != lastIndexOfOrderBy) {
                sql.append(", ");
            }
        }
    }

    private void buildLimitClause(@Nullable Pair<Long, Long> limit) {
        if (limit == null) {
            return;
        }
        sql.append(beautify ? '\n' : ' ');
        sql.append("LIMIT ");
        if (limit.first() != 0) {
            sql.append(limit.first()).append(", ");
        }
        sql.append(limit.second());
    }
}
