package cn.cloudself.query.resolver.helper;

import cn.cloudself.query.config.Lifecycle;
import cn.cloudself.query.config.QueryProConfig;
import cn.cloudself.query.config.SqlAndParams;
import cn.cloudself.query.psi.Const;
import cn.cloudself.query.psi.structure.*;
import cn.cloudself.query.resolver.DatabaseAccessor;
import cn.cloudself.query.util.*;
import cn.cloudself.util.ext.EntityProxy;
import cn.cloudself.util.ext.IterablePlus;
import cn.cloudself.util.log.Log;
import cn.cloudself.util.log.LogFactory;
import cn.cloudself.util.structure.Result;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class LifecycleHelpers {
    private static final Log logger = LogFactory.getLog(LifecycleHelpers.class);

    public static DatabaseAccessor wrap(DatabaseAccessor databaseAccessor) {
        return new ResolverWrapper(databaseAccessor);
    }

    public static Result<QueryStructure, Throwable> beforeParsingQueryStructure(
        @Nullable Class<?> resultClass,
        QueryStructure queryStructure,
        QueryPayload payload
    ) {
        doLogicDelete(queryStructure, payload);

        final boolean printDebugLog = QueryProConfig.computed.printDebugLog();
        final boolean printQueryStructureWholly =
                    printDebugLog
                && (
                        Optional.ofNullable(queryStructure.insert()).map(Insert::data).map(Collection::size).orElse(0) <= 32
                    ||
                        QueryProConfig.computed.printLargeElementWholly()
                );
        if (printDebugLog) {
            if (printQueryStructureWholly) {
                logger.debug("query structure is transforming. {0}", queryStructure);
            } else {
                final Insert insert = queryStructure.insert();
                final Collection<?> data = insert == null ? null : insert.data();
                final QueryStructure simpleQueryStructure;
                if (insert != null && data != null) {
                    simpleQueryStructure = queryStructure.clone().insert(new Insert(Collections.singleton(IterablePlus.first(data))));
                } else {
                    simpleQueryStructure = queryStructure;
                }
                logger.debug(
                    "query structure is transforming. but the insert elements is so large, only first element is print: {0}",
                    simpleQueryStructure
                );
            }
        }

        QueryStructure transformedQueryStructure = queryStructure;

        final List<Lifecycle.QueryStructureTransformer> transformers = ((Lifecycle.Internal) QueryProConfig.computed.lifecycle()).getBeforeExecTransformers();
        for (Lifecycle.QueryStructureTransformer transformer : transformers) {
            final Result<QueryStructure, Throwable> transformed = transformer.transform(resultClass, transformedQueryStructure, payload);
            if (!transformed.ok()) {
                final Throwable it = transformed.error();
                logger.warn("beforeExec钩子阻止了本次操作", it);
                return Result.err(it);
            } else {
                transformedQueryStructure = transformed.unwrap();
            }
        }

        if (printDebugLog) {
            if (printQueryStructureWholly) {
                logger.debug("query structure transformed. {0}", transformedQueryStructure);
            } else {
                final Insert insert = transformedQueryStructure.insert();
                final Collection<?> data = insert == null ? null : insert.data();
                final QueryStructure simpleQueryStructure;
                if (insert != null && data != null) {
                    simpleQueryStructure = transformedQueryStructure.clone().insert(new Insert(Collections.singletonList(IterablePlus.first(data))));
                } else {
                    simpleQueryStructure = transformedQueryStructure;
                }
                logger.debug(
                    "query structure transformed. but the insert elements is so large, only first element will be print: {0}",
                    simpleQueryStructure
                );
            }
        }

        return Result.ok(transformedQueryStructure);
    }

    public static  <R> Result<R, Throwable> afterQueryStructureResultQueried(
        @Nullable Class<?> resultClass,
        QueryStructure queryStructure,
        QueryPayload payload,
        R result
    ) {
        if (result == null) {
            return Result.ok(null);
        }
        Object transformedResult = result;
        final List<Lifecycle.ResultWithQueryStructureTransformer> transformers = ((Lifecycle.Internal) QueryProConfig.computed.lifecycle()).getAfterExecTransformers();
        for (Lifecycle.ResultWithQueryStructureTransformer transformer : transformers) {
            final Result<?, Throwable> transformed = transformer.transform(resultClass, transformedResult, queryStructure, payload);
            if (!transformed.ok()) {
                final Throwable it = transformed.error();
                logger.warn("afterExec钩子阻止了本次操作", it);
                return Result.err(it);
            } else {
                transformedResult = transformed.unwrap();
            }
        }

        //noinspection unchecked
        return Result.ok((R) transformedResult);
    }

    private static void doLogicDelete(QueryStructure queryStructure, QueryPayload payload) {
        if (QueryProConfig.computed.logicDelete()) {
            final String logicDeleteField = QueryProConfig.computed.logicDeleteField();
            final boolean hasDeletedField = EntityProxy.Parser.of(payload.clazz()).parse().columns().get(logicDeleteField) != null;
            if (hasDeletedField) {
                if (queryStructure.action() == QueryStructureAction.DELETE) {
                    final Update update = new Update();
                    update.data(new HashMap<String, Boolean>() {{put(logicDeleteField, true);}});
                    update.override(false);
                    queryStructure.action(QueryStructureAction.UPDATE);
                    queryStructure.update(update);
                } else {
                    final String alias = queryStructure.from().alias();
                    final String mainTable = alias == null ? queryStructure.from().getMain() : alias;
                    final boolean hasOrClause = queryStructure.where().stream().anyMatch(it -> Const.OR.equals(it.operator()));
                    final Field field = new Field(mainTable, logicDeleteField);
                    final WhereClause noDeletedWhereClause = new WhereClause(field, Const.EQUAL, false);
                    final List<WhereClause> newWhereClauses = new ArrayList<>();
                    if (hasOrClause) {
                        newWhereClauses.add(new WhereClause(Const.OPEN_PAR));
                        newWhereClauses.addAll(queryStructure.where());
                        newWhereClauses.add(new WhereClause(Const.CLOSE_PAR));
                        newWhereClauses.add(noDeletedWhereClause);
                    } else {
                        newWhereClauses.addAll(queryStructure.where());
                        newWhereClauses.add(noDeletedWhereClause);
                    }
                    queryStructure.where(newWhereClauses);
                }
            }
        }
    }
}

/**
 * 针对SQL执行的包装器（代理DatabaseAccessor对象），含日志，生命周期方法
 */
class ResolverWrapper implements DatabaseAccessor {
    private static final Log logger = LogFactory.getLog(ResolverWrapper.class);

    private final DatabaseAccessor databaseAccessor;
    public ResolverWrapper(DatabaseAccessor databaseAccessor) {
        this.databaseAccessor = databaseAccessor;
    }

    @Override
    public <T> List<T> doSelect(@Nullable Class<?> queryPro, @NotNull SqlAndParams sqlAndParams, @NotNull Class<T> clazz) {
        final SqlAndParams newSqlAndParams = beforeRunSql(sqlAndParams).unwrap();
        final List<T> res = SqlLog.withQuery(logger, newSqlAndParams, () -> databaseAccessor.doSelect(queryPro, newSqlAndParams, clazz));
        return afterRunSql(res).unwrap();
    }

    @Override
    public <T> T doUpdate(@Nullable Class<?> queryPro, @NotNull SqlAndParams sqlAndParams, @Nullable Class<T> clazz) {
        final SqlAndParams newSqlAndParams = beforeRunSql(sqlAndParams).unwrap();
        final T res = SqlLog.withQuery(logger, newSqlAndParams, () -> databaseAccessor.doUpdate(queryPro, newSqlAndParams, clazz));
        return afterRunSql(res).unwrap();
    }

    @Override
    public <T> T doDelete(@Nullable Class<?> queryPro, @NotNull SqlAndParams sqlAndParams, @Nullable Class<T> clazz) {
        final SqlAndParams newSqlAndParams = beforeRunSql(sqlAndParams).unwrap();
        final T res = SqlLog.withQuery(logger, newSqlAndParams, () -> databaseAccessor.doDelete(queryPro, newSqlAndParams, clazz));
        return afterRunSql(res).unwrap();
    }

    @Override
    public <ID> List<ID> doInsert(@Nullable Class<?> queryPro, @NotNull SqlAndParams sqlAndParams, @Nullable Class<ID> clazz) {
        final SqlAndParams newSqlAndParams = beforeRunSql(sqlAndParams).unwrap();
        final List<ID> res = SqlLog.withQuery(logger, newSqlAndParams, () -> databaseAccessor.doInsert(queryPro, newSqlAndParams, clazz));
        return afterRunSql(res).unwrap();
    }

    private static Result<SqlAndParams, Throwable> beforeRunSql(SqlAndParams sqlAndParams) {
        SqlAndParams transformedSqlAndParams = sqlAndParams;

        final List<Lifecycle.SqlAndParamsTransformer> transformers = ((Lifecycle.Internal) QueryProConfig.computed.lifecycle()).getBeforeRunSqlTransformers();
        for (Lifecycle.SqlAndParamsTransformer transformer : transformers) {
            final Result<SqlAndParams, Throwable> transformed = transformer.transform(transformedSqlAndParams);
            if (!transformed.ok()) {
                logger.warn("beforeRunSql钩子阻止了本次操作", transformed.error());
                return transformed;
            } else {
                transformedSqlAndParams = transformed.unwrap();
            }
        }
        return Result.ok(transformedSqlAndParams);
    }

    private static <R> Result<R, Throwable> afterRunSql(R result) {
        final List<Lifecycle.ResultTransformer> transformers = ((Lifecycle.Internal) QueryProConfig.computed.lifecycle()).getAfterRunSqlTransformers();

        Object transformedResult = result;
        for (Lifecycle.ResultTransformer transformer : transformers) {
            final Result<Object, Throwable> transformed = transformer.transform(result);
            if (!transformed.ok()) {
                final Throwable err = transformed.error();
                logger.warn("afterRunSql钩子阻止了本次操作", err);
                return Result.err(err);
            } else {
                transformedResult = transformed.unwrap();
            }
        }

        //noinspection unchecked
        return Result.ok((R) transformedResult);
    }
}
