package cn.cloudself.query.resolver;

import cn.cloudself.query.config.SqlAndParams;
import org.intellij.lang.annotations.Language;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * 数据库访问器
 */
public interface DatabaseAccessor {
    /**
     * 执行查询语句
     * @param sqlAndParams sql和对应的参数
     * @param clazz 需返回的对象类型, 可以为JavaBean或者{@code Map.class}或者基本类型.class
     * @return 查询结果，当class为map时，返回List&lt;Map&lt;String, Object>>
     * @param <T> see clazz
     */
    default <T> List<T> doSelect(@NotNull SqlAndParams sqlAndParams, @NotNull Class<T> clazz) {
        return doSelect(null, sqlAndParams, clazz);
    }

    /**
     * 执行更新语句
     * @param sqlAndParams sql和对应的参数
     * @param clazz 需返回的对象的类型，一般添加对 Integer, Boolean, Void 的支持即可
     * @return int: 更新的条数, 其它: 并不规定，默认的boolean实现为更新的条数是否大于等于1
     * @param <T> see clazz
     */
    default <T> T doUpdate(@NotNull SqlAndParams sqlAndParams, @Nullable Class<T> clazz) {
        return doUpdate(null, sqlAndParams, clazz);
    }

    /**
     * 执行删除语句，
     * 与更新操作区别不大。
     * @param sqlAndParams sql和对应的参数
     * @param clazz 需返回的对象的类型，一般添加对 Integer, Boolean, Void 的支持即可
     * @return int: 删除的条数, 其它: 并不规定，默认的boolean实现为删除的条数是否大于等于1
     * @param <T> see clazz
     */
    default <T> T doDelete(@NotNull SqlAndParams sqlAndParams, @Nullable Class<T> clazz) {
        return doDelete(null, sqlAndParams, clazz);
    }

    /**
     * 执行插入语句，
     * 与更新操作的主要区别是返回结果不同。
     * @param sqlAndParams sql和对应的参数
     * @param clazz insert方法需返回id列表，clazz是指ID的类型 如Long, Integer, String, Void 等
     *              当操作为replace时，clazz可以为null或者void
     * @return id list, list.size() == 1。 当生成了id且clazz不为null,void时，返回生成的id; 其它情况(未生成id,clazz为null或者void)返回list[null]
     * @param <ID> see clazz
     */
    default <ID> List<ID> doInsert(@NotNull SqlAndParams sqlAndParams, @Nullable Class<ID> clazz) {
        return doInsert(null, sqlAndParams, clazz);
    }

    /**
     * 执行查询语句
     * @param queryPro QueryPro类是可以绑定数据源的, 通过{@code QueryProConfig.computed.dataSource(clazz)}可以获取到相应的数据源，clazz为null时返回默认的数据源。
     * @param sqlAndParams sql和对应的参数
     * @param clazz 需返回的对象类型, 可以为JavaBean或者{@code Map.class}或者基本类型.class
     * @return 查询结果，当class为map时，返回List&lt;Map&lt;String, Object>>
     * @param <T> see clazz
     */
    <T> List<T> doSelect(@Nullable Class<?> queryPro, @NotNull SqlAndParams sqlAndParams, @NotNull Class<T> clazz);

    /**
     * 执行更新语句
     * @param queryPro QueryPro类是可以绑定数据源的, 通过{@code QueryProConfig.computed.dataSource(clazz)}可以获取到相应的数据源，clazz为null时返回默认的数据源。
     * @param sqlAndParams sql和对应的参数
     * @param clazz 需返回的对象的类型，一般添加对 Integer, Boolean, Void 的支持即可
     * @return int: 更新的条数, 其它: 并不规定，默认的boolean实现为更新的条数是否大于等于1
     * @param <T> see clazz
     */
    <T> T doUpdate(@Nullable Class<?> queryPro, @NotNull SqlAndParams sqlAndParams, @Nullable Class<T> clazz);

    /**
     * 执行删除语句，
     * 与更新操作区别不大。
     * @param queryPro QueryPro类是可以绑定数据源的, 通过{@code QueryProConfig.computed.dataSource(clazz)}可以获取到相应的数据源，clazz为null时返回默认的数据源。
     * @param sqlAndParams sql和对应的参数
     * @param clazz 需返回的对象的类型，一般添加对 Integer, Boolean, Void 的支持即可
     * @return int: 删除的条数, 其它: 并不规定，默认的boolean实现为删除的条数是否大于等于1
     * @param <T> see clazz
     */
    <T> T doDelete(@Nullable Class<?> queryPro, @NotNull SqlAndParams sqlAndParams, @Nullable Class<T> clazz);

    /**
     * 执行插入语句，
     * 与更新操作的主要区别是返回结果不同。
     * @param queryPro QueryPro类是可以绑定数据源的, 通过{@code QueryProConfig.computed.dataSource(clazz)}可以获取到相应的数据源，clazz为null时返回默认的数据源。
     * @param sqlAndParams sql和对应的参数
     * @param clazz insert方法需返回id列表，clazz是指ID的类型 如Long, Integer, String, Void 等
     *              当操作为replace时，clazz可以为null或者void
     * @return id list, list.size() == 1。 当生成了id且clazz不为null,void时，返回生成的id; 其它情况(未生成id,clazz为null或者void)返回list[null]
     * @param <ID> see clazz
     */
    <ID> List<ID> doInsert(@Nullable Class<?> queryPro, @NotNull SqlAndParams sqlAndParams, @Nullable Class<ID> clazz);
}
