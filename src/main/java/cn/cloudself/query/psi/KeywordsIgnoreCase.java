package cn.cloudself.query.psi;

import cn.cloudself.exception.IllegalParameters;
import cn.cloudself.query.psi.structure.*;
import org.jetbrains.annotations.Contract;


public class KeywordsIgnoreCase<F extends AbstractExpressionOperators<?, ?, ?, ?, ?, ?>> {
    private final Field field;
    private final QueryStructure queryStructure;
    private final QueryPayload payload;
    private final QueryFieldCreator<F> queryFieldCreator;

    public KeywordsIgnoreCase(Field field, QueryStructure queryStructure, QueryPayload payload, QueryFieldCreator<F> queryFieldCreator) {
        this.field = field;
        this.queryStructure = queryStructure;
        this.payload = payload;
        this.queryFieldCreator = queryFieldCreator;
    }

    @Contract(pure = true)
    public F eq(Object value) {
        return with(new WhereClause(upperField(field), Const.EQUAL, value).commands(WhereClauseCommands.UPPER_CASE));
    }
    @Contract(pure = true)
    public F equalTo(Object value) {
        return with(new WhereClause(upperField(field), Const.EQUAL, value).commands(WhereClauseCommands.UPPER_CASE));
    }
    @Contract(pure = true)
    public F like(String str) {
        return with(new WhereClause(upperField(field), Const.LIKE, str).commands(WhereClauseCommands.UPPER_CASE));
    }
    @Contract(pure = true)
    public F in(Object... values) {
        if (values.length == 0) {
            throw new IllegalParameters("in查询+ignore case不能设置空参数");
        }
        return with(new WhereClause(upperField(field), Const.IN, values).commands(WhereClauseCommands.UPPER_CASE));
    }

    private Field upperField(Field field) {
        return new Field(field.table(), field.column());
    }
    private F with(WhereClause whereClause) {
        return queryFieldCreator.create(queryStructure.appendWhere(whereClause), payload);
    }
}
