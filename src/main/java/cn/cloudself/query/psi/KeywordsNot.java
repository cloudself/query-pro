package cn.cloudself.query.psi;

import cn.cloudself.exception.IllegalCall;
import cn.cloudself.exception.IllegalParameters;
import cn.cloudself.query.psi.structure.Field;
import cn.cloudself.query.psi.structure.QueryPayload;
import cn.cloudself.query.psi.structure.QueryStructure;
import cn.cloudself.query.psi.structure.WhereClause;
import org.jetbrains.annotations.Contract;

import java.util.Collection;

public class KeywordsNot<F extends AbstractExpressionOperators<?, ?, ?, ?, ?, ?>> {

    private final Field field;
    private final QueryStructure queryStructure;
    private final QueryPayload payload;
    private final QueryFieldCreator<F> queryFieldCreator;

    public KeywordsNot(Field field, QueryStructure queryStructure, QueryPayload payload, QueryFieldCreator<F> queryFieldCreator) {
        this.field = field;
        this.queryStructure = queryStructure;
        this.payload = payload;
        this.queryFieldCreator = queryFieldCreator;
    }

    @Contract(pure = true)
    public F eq(Object value) {
        return with(new WhereClause(field, "<>", value));
    }
    @Contract(pure = true)
    public F equalTo(Object value) {
        return with(new WhereClause(field, "<>", value));
    }
    @Contract(pure = true)
    public F between(Object start, Object end) {
        return with(new WhereClause(field, Const.NOT_BETWEEN, new Object[]{start, end}));
    }

    @Contract(pure = true)
    public F like(String str) {
        return with(new WhereClause(field, Const.NOT_LIKE, str));
    }

    @Contract(pure = true)
    public F in(Object... values) {
        final Object[] inArr;
        if (values.length != 0 && values[0] instanceof Collection<?>) {
            if (values.length > 1) {
                throw new IllegalCall("not in查询的第一个参数为collection时，不支持多个collection。");
            }
            inArr = ((Collection<?>) values[0]).toArray();
        } else {
            inArr = values;
        }
        if (inArr.length == 0) {
            throw new IllegalParameters("not in查询不能设置空参数");
        }
        return with(new WhereClause(field, Const.NOT_IN, inArr));
    }

    @Contract(pure = true)
    public F nul() {
        return with(new WhereClause(field, Const.IS_NOT_NULL));
    }

    private F with(WhereClause whereClause) {
        return queryFieldCreator.create(queryStructure.appendWhere(whereClause), payload);
    }
}