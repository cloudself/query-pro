package cn.cloudself.query.psi;

import cn.cloudself.query.config.QueryProConfig;
import cn.cloudself.exception.IllegalCall;
import cn.cloudself.exception.IllegalImplements;
import cn.cloudself.query.psi.structure.*;
import cn.cloudself.query.util.Pageable;
import cn.cloudself.util.ext.ClassPro;
import cn.cloudself.util.structure.Pair;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.stream.Collectors;

public abstract class FinalOperators<
        T,
        RUN_RES,
        COLUMN_LIMITER_FILED extends FinalOperators<T, RUN_RES, COLUMN_LIMITER_FILED, COLUMNS_LIMITER_FILED>,
        COLUMNS_LIMITER_FILED extends FinalOperators<T, RUN_RES, COLUMN_LIMITER_FILED, COLUMNS_LIMITER_FILED>
> {
    protected abstract QueryStructure getQueryStructure();
    protected abstract QueryPayload getPayload();
    protected abstract Class<T> getClazz();
    protected abstract COLUMN_LIMITER_FILED createColumnLimitField(QueryStructure qs, QueryPayload payload);
    protected abstract COLUMNS_LIMITER_FILED createColumnsLimitField(QueryStructure qs, QueryPayload payload);
    protected abstract FinalOperators<T, RUN_RES, COLUMN_LIMITER_FILED, COLUMNS_LIMITER_FILED> createField(QueryStructure qs, QueryPayload payload);

    private <R> List<R> resolve(Class<R> clazz) {
        return QueryProConfig.computed.queryStructureResolver().resolve(getQueryStructure(), getPayload(), clazz);
    }

    public FinalOperators<T, RUN_RES, COLUMN_LIMITER_FILED, COLUMNS_LIMITER_FILED> distinct() {
        final QueryStructure queryStructure = getQueryStructure();
        if (!queryStructure.orderBy().isEmpty()) {
            throw new IllegalCall("不支持 distinct + orderBy. 可使用Java代码手动去重。");
        }
        return createField(queryStructure.distinct(true), getPayload());
    }

    public FinalOperators<T, RUN_RES, COLUMN_LIMITER_FILED, COLUMNS_LIMITER_FILED> limit(int limit) {
        return limit(0, limit);
    }

    public FinalOperators<T, RUN_RES, COLUMN_LIMITER_FILED, COLUMNS_LIMITER_FILED> limit(long start, long limit) {
        final QueryStructure queryStructure = getQueryStructure();
        queryStructure.limit(Pair.of(start, limit));
        return createField(queryStructure, getPayload());
    }

    protected <S> List<S> getColumn(Field field, Class<S> clazz) {
        final QueryStructure queryStructure = getQueryStructure();
        ;
        final List<Map<String, Object>> rows = createField(queryStructure.appendField(field), getPayload()).runAsMap();
        return rows.stream().map(row -> {
            final Object f = row.get(field.column());
            if (f == null) {
                return null;
            }
            final Class<?> fieldJavaClass = f.getClass();
            if (clazz.isAssignableFrom(fieldJavaClass)) {
                //noinspection unchecked
                return (S) f;
            }
            final Class<?> targetPrimitiveType = ClassPro.of(clazz).toPrimitiveType();
            if (targetPrimitiveType == null) {
                return null;
            }
            final Class<?> objPrimitiveType = ClassPro.of(fieldJavaClass).toPrimitiveType();
            if (targetPrimitiveType == objPrimitiveType) {
                //noinspection unchecked
                return (S) f;
            } else {
                return null;
            }
        }).collect(Collectors.toList());
    }

    public COLUMNS_LIMITER_FILED columnsLimiter() {
        return createColumnsLimitField(getQueryStructure(), getPayload());
    }

    public COLUMN_LIMITER_FILED columnLimiter() {
        return createColumnLimitField(getQueryStructure(), getPayload());
    }

    public long count() {
        final QueryStructure queryStructure = getQueryStructure();
        if (queryStructure.action() != QueryStructureAction.SELECT) {
            throw new IllegalCall("非SELECT语句不能使用count方法");
        }
        queryStructure.fields(new ArrayList<>()).appendField(new Field("count(*)"));
        return resolve(long.class).get(0);
    }

    public Optional<T> runLimit1Opt() {
        return Optional.ofNullable(runLimit1());
    }

    @Nullable
    public T runLimit1() {
        final QueryStructure queryStructure = getQueryStructure();
        final List<T> results = createField(queryStructure.limit(Pair.of(0L, 1L)), getPayload()).queryAll();
        return results.isEmpty() ? null : results.get(0);
    }

    public RUN_RES run() {
        final QueryStructure queryStructure = getQueryStructure();
        switch (queryStructure.action()) {
            case SELECT:
                //noinspection unchecked
                return (RUN_RES) queryAll();
            case DELETE:
            case UPDATE:
                final List<T> results = queryAll();
                if (results.isEmpty())
                    throw new IllegalImplements("DELETE, UPDATE需返回长度为1的List<Boolean>");
                else
                    //noinspection unchecked
                    return (RUN_RES) results.get(0);
            case INSERT:
            case REPLACE:
                throw new IllegalCall("run方法不支持INSERT");
            default:
                throw new IllegalImplements("NEVER");
        }
    }

    public List<Map<String, Object>> runAsMap() {
        //noinspection unchecked,rawtypes
        return (List) resolve(Map.class);
    }

    private List<T> queryAll() {
        return resolve(getClazz());
    }

    public Pageable<T> pageable() {
        return Pageable.create(this::count, (start, limit) -> {
            getQueryStructure().limit(Pair.of(start, limit));
            return resolve(getClazz());
        });
    }
}