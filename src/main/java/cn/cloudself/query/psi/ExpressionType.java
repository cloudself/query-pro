package cn.cloudself.query.psi;

public enum ExpressionType {
    WHERE,
    ORDER_BY,
    COLUMN_LIMITER,
    COLUMNS_LIMITER,
}