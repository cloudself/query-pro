package cn.cloudself.query.psi;

import cn.cloudself.query.psi.structure.Field;
import cn.cloudself.query.psi.structure.OrderByClause;
import cn.cloudself.query.psi.structure.QueryPayload;
import cn.cloudself.query.psi.structure.QueryStructure;
import org.jetbrains.annotations.Contract;

public class KeywordsOrderBy<F extends AbstractExpressionOperators<?, ?, ?, ?, ?, ?>> {
    private final Field field;
    private final QueryStructure queryStructure;
    private final QueryPayload payload;
    private final QueryFieldCreator<F> queryFieldCreator;

    public KeywordsOrderBy(Field field, QueryStructure queryStructure, QueryPayload payload, QueryFieldCreator<F> queryFieldCreator) {
        this.field = field;
        this.queryStructure = queryStructure;
        this.payload = payload;
        this.queryFieldCreator = queryFieldCreator;
    }

    @Contract(pure = true)
    public F asc() {
        return with(new OrderByClause(field, "asc"));
    }
    @Contract(pure = true)
    public F desc() {
        return with(new OrderByClause(field, "desc"));
    }

    private F with(OrderByClause orderBy) {
        queryStructure.appendOrderBy(orderBy);
        return queryFieldCreator.create(queryStructure, payload);
    }
}
