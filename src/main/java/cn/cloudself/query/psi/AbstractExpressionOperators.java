package cn.cloudself.query.psi;

import cn.cloudself.query.psi.structure.*;

/**
 * <h2>表达式</h2>
 *
 * @param <T>                     the type parameter
 * @param <RUN_RES>               the type parameter
 * @param <WHERE_FIELD>           the type parameter
 * @param <ORDER_BY_FIELD>        the type parameter
 * @param <COLUMN_LIMITER_FILED>  the type parameter
 * @param <COLUMNS_LIMITER_FILED> the type parameter
 */
public abstract class AbstractExpressionOperators<
        T,
        RUN_RES,
        WHERE_FIELD extends AbstractExpressionOperators<T, RUN_RES, WHERE_FIELD, ORDER_BY_FIELD, COLUMN_LIMITER_FILED, COLUMNS_LIMITER_FILED>,
        ORDER_BY_FIELD extends AbstractExpressionOperators<T, RUN_RES, WHERE_FIELD, ORDER_BY_FIELD, COLUMN_LIMITER_FILED, COLUMNS_LIMITER_FILED>,
        COLUMN_LIMITER_FILED extends AbstractExpressionOperators<T, RUN_RES, WHERE_FIELD, ORDER_BY_FIELD, COLUMN_LIMITER_FILED, COLUMNS_LIMITER_FILED>,
        COLUMNS_LIMITER_FILED extends AbstractExpressionOperators<T, RUN_RES, WHERE_FIELD, ORDER_BY_FIELD, COLUMN_LIMITER_FILED, COLUMNS_LIMITER_FILED>
> extends FinalOperators<T, RUN_RES, COLUMN_LIMITER_FILED, COLUMNS_LIMITER_FILED> {

    /**
     * Gets field type.
     *
     * @return the field type
     */
    protected abstract ExpressionType getFieldType();

    /**
     * Create where field where field.
     *
     * @param qs      the qs
     * @param payload the payload
     * @return the where field
     */
    protected abstract WHERE_FIELD createWhereField(QueryStructure qs, QueryPayload payload);

    /**
     * Create order by field order by field.
     *
     * @param qs      the qs
     * @param payload the payload
     * @return the order by field
     */
    protected abstract ORDER_BY_FIELD createOrderByField(QueryStructure qs, QueryPayload payload);

    /**
     * private
     * @param qs {@link QueryStructure}
     * @param payload {@link QueryPayload}
     * @return WHERE
     */
    @Override
    public FinalOperators<T, RUN_RES, COLUMN_LIMITER_FILED, COLUMNS_LIMITER_FILED> createField(QueryStructure qs, QueryPayload payload) {
        return createWhereField(qs, payload);
    }
}
