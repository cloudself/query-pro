package cn.cloudself.query.psi;

import cn.cloudself.query.psi.structure.QueryPayload;
import cn.cloudself.query.psi.structure.QueryStructure;

public interface QueryFieldCreator<F> {
    F create(QueryStructure queryStructure, QueryPayload queryPayload);
}
