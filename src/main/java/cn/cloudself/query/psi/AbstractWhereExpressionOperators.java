package cn.cloudself.query.psi;

import cn.cloudself.exception.IllegalCall;
import cn.cloudself.query.psi.structure.*;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.function.Function;

/**
 * <h2>表达式连接符</h2>
 * <p>继承自表达式终止符</p>
 * 含{@link #and()}, {@link #or()}, {@link #par(Function)}等
 *
 * @param <T>                     the type parameter
 * @param <RUN_RES>               the type parameter
 * @param <WHERE_FIELD>           the type parameter
 * @param <ORDER_BY_FIELD>        the type parameter
 * @param <COLUMN_LIMITER_FILED>  the type parameter
 * @param <COLUMNS_LIMITER_FILED> the type parameter
 */
public abstract class AbstractWhereExpressionOperators<
        T,
        RUN_RES,
        WHERE_FIELD extends AbstractWhereExpressionOperators<T, RUN_RES, WHERE_FIELD, ORDER_BY_FIELD, COLUMN_LIMITER_FILED, COLUMNS_LIMITER_FILED>,
        ORDER_BY_FIELD extends AbstractExpressionOperators<T, RUN_RES, WHERE_FIELD, ORDER_BY_FIELD, COLUMN_LIMITER_FILED, COLUMNS_LIMITER_FILED>,
        COLUMN_LIMITER_FILED extends AbstractExpressionOperators<T, RUN_RES, WHERE_FIELD, ORDER_BY_FIELD, COLUMN_LIMITER_FILED, COLUMNS_LIMITER_FILED>,
        COLUMNS_LIMITER_FILED extends AbstractExpressionOperators<T, RUN_RES, WHERE_FIELD, ORDER_BY_FIELD, COLUMN_LIMITER_FILED, COLUMNS_LIMITER_FILED>
> extends AbstractExpressionOperators<T, RUN_RES,  WHERE_FIELD, ORDER_BY_FIELD, COLUMN_LIMITER_FILED, COLUMNS_LIMITER_FILED> {
    /**
     * 自定义列，需要自行注意SQL注入风险
     * @param column 列SQL
     * @return {@link Keywords}
     */
    public Keywords<WHERE_FIELD> customColumn(String column) {
        return new Keywords<>(new Field(column), getQueryStructure(), getPayload(), this::createWhereField);
    }

    /**
     * Where条件连接符，任何情况下都可省略
     * <p>注意order by之后不要使用</p>
     * @return the where field
     */
    @SuppressWarnings("unchecked")
    public WHERE_FIELD and() {
        if (getFieldType() != ExpressionType.WHERE) {
            throw new RuntimeException(getFieldType() + " can not call and, usage: .orderBy().id.desc().name.asc()");
        }
        //noinspection unchecked
        return (WHERE_FIELD) this;
    }

    /**
     * Where条件OR连接符
     * <p>注意order by之后不要使用</p>
     * @return the where field
     */
    public WHERE_FIELD or() {
        return or(null);
    }

    /**
     * Where条件OR连接符
     * <p>注意order by之后不要使用</p>
     * @param factor the factor
     * @return the where field
     */
    public WHERE_FIELD or(@Nullable Function<WHERE_FIELD, WHERE_FIELD> factor) {
        if (getFieldType() != ExpressionType.WHERE) {
            throw new RuntimeException(getFieldType() + " can not call and, usage: .orderBy().id.desc().name.asc()");
        }

        final QueryStructure queryStructure = getQueryStructure();
        final QueryPayload payload = getPayload();
        if (factor == null) {
            queryStructure.appendWhere(new WhereClause(Const.OR));
            return createWhereField(queryStructure, payload);
        }

        // v_temp会消失, 只取where
        final QueryStructure vTempQueryStructure = new QueryStructure().from(new QueryStructureFrom("v_temp"));
        final List<WhereClause> orWhereClauses = factor.apply(createWhereField(vTempQueryStructure, payload)).getQueryStructure().where();
        queryStructure.appendWhere(new WhereClause(Const.OR).value(orWhereClauses));
        return createWhereField(queryStructure, payload);
    }

    /**
     * 括号
     * @param factor the factor
     * @return the where field
     */
    public WHERE_FIELD par(Function<WHERE_FIELD, WHERE_FIELD> factor) {
        if (getFieldType() != ExpressionType.WHERE) {
            throw new RuntimeException(getFieldType() + " can not call and, usage: .orderBy().id.desc().name.asc()");
        }

        final QueryPayload payload = getPayload();
        // v_temp会消失, 只取where
        final QueryStructure vTempQueryStructure = new QueryStructure().from(new QueryStructureFrom("v_temp"));
        final List<WhereClause> parWhereClauses = factor.apply(createWhereField(vTempQueryStructure, payload)).getQueryStructure().where();
        final QueryStructure queryStructure = getQueryStructure();
        queryStructure
                .appendWhere(new WhereClause(Const.OPEN_PAR))
                .appendWheres(parWhereClauses)
                .appendWhere(new WhereClause(Const.CLOSE_PAR));
        return createWhereField(queryStructure, payload);
    }

    /**
     * 左括号
     * @return the where field
     */
    public WHERE_FIELD parLeft() {
        return createWhereField(getQueryStructure().appendWhere(new WhereClause(Const.OPEN_PAR)), getPayload());
    }

    /**
     * 右括号
     * @return the where field
     */
    public WHERE_FIELD parRight() {
        return createWhereField(getQueryStructure().appendWhere(new WhereClause(Const.CLOSE_PAR)), getPayload());
    }

    /**
     * 自定义SQL
     * @param sql the sql
     * @return the where field
     */
    public WHERE_FIELD sql(String sql) {
        return createWhereField(getQueryStructure().appendWhere(WhereClause.createBySql(sql)), getPayload());
    }

    /**
     * order by 语句
     * @return the order by field
     */
    public ORDER_BY_FIELD orderBy() {
        final QueryStructure queryStructure = getQueryStructure();
        if (queryStructure.distinct()) {
            throw new IllegalCall("不支持 distinct + orderBy. 可使用Java代码手动去重。");
        }
        return createOrderByField(queryStructure, getPayload());
    }
}
