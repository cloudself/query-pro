package cn.cloudself.query.psi;

import cn.cloudself.exception.IllegalCall;
import cn.cloudself.query.psi.structure.*;
import cn.cloudself.util.ext.StringPlus;
import org.jetbrains.annotations.Contract;

import java.util.Collection;


/**
 * <h2>关键字</h2>
 * <p>包含{@link #not()}, {@link #equalTo(Object)}, {@link #between(Object, Object)}, {@link #graterThan(Object)},
 * {@link #like(String)}, {@link #in(Object...)} 等</p>
 *
 * @param <F> the type parameter
 */
public class Keywords<F extends AbstractExpressionOperators<?, ?, ?, ?, ?, ?>> {
    private final Field field;
    private final QueryStructure queryStructure;
    private final QueryPayload payload;
    private final QueryFieldCreator<F> queryFieldCreator;

    public Keywords(Field field, QueryStructure queryStructure, QueryPayload payload, QueryFieldCreator<F> queryFieldCreator) {
        this.field = field;
        this.queryStructure = queryStructure;
        this.payload = payload;
        this.queryFieldCreator = queryFieldCreator;
    }

    @Contract(pure = true)
    public Keywords<F> is() {
        return this;
    }

    @Contract(pure = true)
    public KeywordsNot<F> not() {
        return new KeywordsNot<>(field, queryStructure, payload, queryFieldCreator);
    }

    @Contract(pure = true)
    public KeywordsIgnoreCase<F> ignoreCase() {
        return new KeywordsIgnoreCase<>(field, queryStructure, payload, queryFieldCreator);
    }

    @Contract(pure = true)
    public F eq(Object value) {
        return with(new WhereClause(field, Const.EQUAL, value));
    }
    @Contract(pure = true)
    public F equalTo(Object value) {
        return with(new WhereClause(field, Const.EQUAL, value));
    }
    @Contract(pure = true)
    public F between(Object start, Object end) {
        return with(new WhereClause(field, Const.BETWEEN, new Object[]{start, end}));
    }
    @Contract(pure = true)
    public F lessThan(Object value) {
        return with(new WhereClause(field, "<", value));
    }
    @Contract(pure = true)
    public F lessThanOrEqual(Object value) {
        return with(new WhereClause(field, "<=", value));
    }
    @Contract(pure = true)
    public F graterThan(Object value) {
        return with(new WhereClause(field, ">", value));
    }
    @Contract(pure = true)
    public F graterThanOrEqual(Object value) {
        return with(new WhereClause(field, ">=", value));
    }
    @Contract(pure = true)
    public F like(String str) {
        return with(new WhereClause(field, Const.LIKE, str));
    }

    /**
     * in 查询 支持vararg, eg: in(1, 2, 3), in(intArray), etc.  <br/>
     * 支持有且仅有一个Collection作为参数, eg: in(list), in(set), etc.
     */
    @Contract(pure = true)
    public F in(Object... values) {
        final Object[] inArr;
        if (values.length != 0 && values[0] instanceof Collection<?>) {
            if (values.length > 1) {
                throw new IllegalCall("in查询的第一个参数为collection时，不支持多个collection。");
            }
            inArr = ((Collection<?>) values[0]).toArray();
        } else {
            inArr = values;
        }
        if (inArr.length == 0) {
            final String table = field.table();
            final StringBuilder sqlPartBuilder = new StringBuilder("false/* WARN: ");
            if (table != null) {
                sqlPartBuilder.append(StringPlus.onlyAZaz_(table));
                sqlPartBuilder.append('.');
            }
            sqlPartBuilder.append(StringPlus.onlyAZaz_(field.column())).append(" in empty*/");
            final String sqlPart = sqlPartBuilder.toString();
            return with(WhereClause.createBySql(sqlPart));
        }

        return with(new WhereClause(field, Const.IN, inArr));
    }
    @Contract(pure = true)
    public F nul() {
        return with(new WhereClause(field, Const.IS_NULL));
    }
    @Contract(pure = true)
    public F isNull() {
        return with(new WhereClause(field, Const.IS_NULL));
    }
    @Contract(pure = true)
    public F isNotNull() {
        return with(new WhereClause(field, Const.IS_NOT_NULL));
    }
    @Contract(pure = true)
    public F sql(String sql) {
        return with(new WhereClause("").field(field).sql(sql));
    }

    private F with(WhereClause whereClause) {
        return queryFieldCreator.create(queryStructure.appendWhere(whereClause), payload);
    }
}
