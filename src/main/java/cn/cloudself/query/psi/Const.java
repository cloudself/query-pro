package cn.cloudself.query.psi;

/**
 * The type Const.
 */
public class Const {
    /**
     * The constant NULL.
     */
    public static final String NULL = "%^&*SyMmb0L(null)";
    /**
     * The constant SKIP.
     */
    public static final String SKIP = "%^&*SyMmb0L(skip)";

    /**
     * The constant OPEN_PAR.
     */
    public static final String OPEN_PAR = "(";
    /**
     * The constant CLOSE_PAR.
     */
    public static final String CLOSE_PAR = ")";
    /**
     * The constant EQUAL.
     */
    public static final String EQUAL  = "=";
    /**
     * The constant ASSIGN.
     */
    public static final String ASSIGN = "=";
    /**
     * The constant LT.
     */
    public static final String LT     = "<";
    /**
     * The constant LT_EQ.
     */
    public static final String LT_EQ  = "<=";
    /**
     * The constant GT.
     */
    public static final String GT     = ">";
    /**
     * The constant GT_EQ.
     */
    public static final String GT_EQ  = ">=";

    /**
     * The constant OR.
     */
    public static final String OR = "OR";
    /**
     * The constant AND.
     */
    public static final String AND = "AND";
    /**
     * The constant IN.
     */
    public static final String IN = "IN";
    /**
     * The constant NOT_IN.
     */
    public static final String NOT_IN = "NOT IN";
    /**
     * The constant BETWEEN.
     */
    public static final String BETWEEN = "BETWEEN";
    /**
     * The constant NOT_BETWEEN.
     */
    public static final String NOT_BETWEEN = "NOT BETWEEN";
    /**
     * The constant IS_NULL.
     */
    public static final String IS_NULL = "IS NULL";
    /**
     * The constant IS_NOT_NULL.
     */
    public static final String IS_NOT_NULL = "IS NOT NULL";

    /**
     * The constant LIKE.
     */
    public static final String LIKE = "LIKE";
    /**
     * The constant NOT_LIKE.
     */
    public static final String NOT_LIKE = "NOT LIKE";
}
