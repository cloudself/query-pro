package cn.cloudself.query.psi;

import cn.cloudself.query.psi.structure.QueryPayload;
import cn.cloudself.query.psi.structure.QueryStructure;
import cn.cloudself.query.psi.structure.QueryStructureAction;

public class UpdateSetDefinedExpression<UPDATE_BY_FIELD extends AbstractExpressionOperators<?, ?, ?, ?, ?, ?>> {
    private final QueryStructure queryStructure;
    private final QueryPayload payload;
    private final QueryFieldCreator<UPDATE_BY_FIELD> createUpdateByField;
    public UpdateSetDefinedExpression(QueryStructure queryStructure, QueryPayload payload, QueryFieldCreator<UPDATE_BY_FIELD> createUpdateByField) {
        this.queryStructure = queryStructure;
        this.payload = payload;
        this.createUpdateByField = createUpdateByField;
    }

    public UPDATE_BY_FIELD where() {
        return createUpdateByField.create(queryStructure.action(QueryStructureAction.UPDATE), payload);
    }

    public boolean run() {
        return (boolean) createUpdateByField.create(queryStructure, payload).run();
    }
}