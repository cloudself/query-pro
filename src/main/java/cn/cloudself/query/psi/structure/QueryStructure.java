package cn.cloudself.query.psi.structure;

import cn.cloudself.util.structure.Pair;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * 描述查询，更新，插入，删除的一个可序列化的结构
 */
@SuppressWarnings("UnusedReturnValue")
public class QueryStructure implements Cloneable {
    @NotNull
    private QueryStructureAction action = QueryStructureAction.SELECT;
    private Update update;
    private Insert insert;
    private boolean distinct;
    @NotNull
    private List<Field> fields = new ArrayList<>();
    @NotNull
    private QueryStructureFrom from = new QueryStructureFrom();
    @NotNull
    private List<WhereClause> where = new ArrayList<>();
    @NotNull
    private List<OrderByClause> orderBy = new ArrayList<>();
    @Nullable
    private Pair<Long, Long> limit;

    @NotNull
    public QueryStructureAction action() {
        return action;
    }

    public QueryStructure action(@NotNull QueryStructureAction action) {
        this.action = action;
        return this;
    }

    public Update update() {
        return update;
    }

    public QueryStructure update(Update update) {
        this.update = update;
        return this;
    }

    public Insert insert() {
        return insert;
    }

    public QueryStructure insert(Insert insert) {
        this.insert = insert;
        return this;
    }

    public boolean distinct() {
        return distinct;
    }

    public QueryStructure distinct(boolean distinct) {
        this.distinct = distinct;
        return this;
    }

    @NotNull
    public List<Field> fields() {
        return fields;
    }

    public QueryStructure fields(@NotNull List<Field> fields) {
        this.fields = fields;
        return this;
    }

    public QueryStructure appendField(Field field) {
        this.fields.add(field);
        return this;
    }

    @NotNull
    public QueryStructureFrom from() {
        return from;
    }

    public QueryStructure from(@NotNull QueryStructureFrom from) {
        this.from = from;
        return this;
    }

    @NotNull
    public List<WhereClause> where() {
        return where;
    }

    public QueryStructure where(@NotNull List<WhereClause> where) {
        this.where = where;
        return this;
    }

    public QueryStructure appendWheres(@NotNull Collection<WhereClause> wheres) {
        this.where.addAll(wheres);
        return this;
    }

    public QueryStructure appendWhere(@NotNull WhereClause where) {
        this.where.add(where);
        return this;
    }

    @NotNull
    public List<OrderByClause> orderBy() {
        return orderBy;
    }

    public QueryStructure orderBy(@NotNull List<OrderByClause> orderBy) {
        this.orderBy = orderBy;
        return this;
    }

    public QueryStructure appendOrderBy(OrderByClause orderBy) {
        this.orderBy.add(orderBy);
        return this;
    }

    @Nullable
    public Pair<Long, Long> limit() {
        return limit;
    }

    public QueryStructure limit(@Nullable Pair<Long, Long> limit) {
        this.limit = limit;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QueryStructure that = (QueryStructure) o;
        return distinct == that.distinct && action == that.action && Objects.equals(update, that.update) && Objects.equals(insert, that.insert) && Objects.equals(fields, that.fields) && Objects.equals(from, that.from) && Objects.equals(where, that.where) && Objects.equals(orderBy, that.orderBy) && Objects.equals(limit, that.limit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(action, update, insert, distinct, fields, from, where, orderBy, limit);
    }

    @Override
    public String toString() {
        return "QueryStructure{" +
                "action=" + action +
                ", update=" + update +
                ", insert=" + insert +
                ", distinct=" + distinct +
                ", fields=" + fields +
                ", from=" + from +
                ", where=" + where +
                ", orderBy=" + orderBy +
                ", limit=" + limit +
                '}';
    }

    @Override
    public QueryStructure clone() {
        try {
            return (QueryStructure) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}
