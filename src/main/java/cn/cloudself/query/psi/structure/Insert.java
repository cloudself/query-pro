package cn.cloudself.query.psi.structure;

import java.util.Collection;
import java.util.Objects;

public class Insert {
    private Collection<?> data;

    public Insert(Collection<?> data) {
        this.data = data;
    }

    public Collection<?> data() {
        return data;
    }

    public void data(Collection<?> data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Insert insert = (Insert) o;
        return Objects.equals(data, insert.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data);
    }

    @Override
    public String toString() {
        return "Insert{" +
                "data=" + data +
                '}';
    }
}
