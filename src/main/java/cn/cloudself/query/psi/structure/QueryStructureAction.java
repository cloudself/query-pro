package cn.cloudself.query.psi.structure;

public enum QueryStructureAction {
    SELECT,
    UPDATE,
    DELETE,
    INSERT,
    REPLACE
}