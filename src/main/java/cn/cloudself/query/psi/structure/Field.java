package cn.cloudself.query.psi.structure;

import java.util.Objects;

public class Field {
    private String table = null;
    private String column;
    private FieldCommands commands = null;

    public Field(String column) {
        this.column = column;
    }

    public Field(String table, String column) {
        this.table = table;
        this.column = column;
    }

    public String table() {
        return table;
    }

    public Field table(String table) {
        this.table = table;
        return this;
    }

    public String column() {
        return column;
    }

    public Field column(String column) {
        this.column = column;
        return this;
    }

    public FieldCommands commands() {
        return commands;
    }

    public Field commands(FieldCommands commands) {
        this.commands = commands;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Field field = (Field) o;
        return Objects.equals(table, field.table) && Objects.equals(column, field.column) && commands == field.commands;
    }

    @Override
    public int hashCode() {
        return Objects.hash(table, column, commands);
    }

    @Override
    public String toString() {
        return "Field{" +
                "table='" + table + '\'' +
                ", column='" + column + '\'' +
                ", commands=" + commands +
                '}';
    }
}
