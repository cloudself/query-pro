package cn.cloudself.query.psi.structure;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class QueryStructureFrom {
    private String main = "?";
    private String alias = null;
    private List<FromJoiner> joins = new ArrayList<>();

    public QueryStructureFrom() {
    }

    public QueryStructureFrom(String main) {
        this.main = main;
    }

    public String getMain() {
        return main;
    }

    public QueryStructureFrom setMain(String main) {
        this.main = main;
        return this;
    }

    public String alias() {
        return alias;
    }

    public QueryStructureFrom alias(String alias) {
        this.alias = alias;
        return this;
    }

    public List<FromJoiner> joins() {
        return joins;
    }

    public QueryStructureFrom joins(List<FromJoiner> joins) {
        this.joins = joins;
        return this;
    }

    public QueryStructureFrom appendJoin(FromJoiner join) {
        this.joins.add(join);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QueryStructureFrom that = (QueryStructureFrom) o;
        return Objects.equals(main, that.main) && Objects.equals(alias, that.alias) && Objects.equals(joins, that.joins);
    }

    @Override
    public int hashCode() {
        return Objects.hash(main, alias, joins);
    }

    @Override
    public String toString() {
        return "QueryStructureFrom{" +
                "main='" + main + '\'' +
                ", alias='" + alias + '\'' +
                ", joins=" + joins +
                '}';
    }
}
