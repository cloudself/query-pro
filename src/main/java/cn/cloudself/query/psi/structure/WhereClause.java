package cn.cloudself.query.psi.structure;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public class WhereClause {
    @Nullable
    private Field field = null;
    @NotNull
    private String operator;
    @Nullable
    private Object value = null; // null arrayOr<string boolean integer long date> List<WhereClause>
    @Nullable
    private WhereClauseCommands commands = null;
    @Nullable
    private String sql = null;
    public static WhereClause createBySql(@Nullable String sql) {
        return new WhereClause("").sql(sql);
    }
    public WhereClause(@NotNull String operator) {
        this.operator = operator;
    }
    public WhereClause(@Nullable Field field, @NotNull String operator) {
        this.field = field;
        this.operator = operator;
    }
    public WhereClause(@Nullable Field field, @NotNull String operator, @Nullable Object value) {
        this.field = field;
        this.operator = operator;
        this.value = value;
    }

    @Nullable
    public Field field() {
        return field;
    }

    public WhereClause field(@Nullable Field field) {
        this.field = field;
        return this;
    }

    @NotNull
    public String operator() {
        return operator;
    }

    public WhereClause operator(@NotNull String operator) {
        this.operator = operator;
        return this;
    }

    @Nullable
    public Object value() {
        return value;
    }

    public WhereClause value(@Nullable Object value) {
        this.value = value;
        return this;
    }

    @Nullable
    public WhereClauseCommands commands() {
        return commands;
    }

    public WhereClause commands(@Nullable WhereClauseCommands commands) {
        this.commands = commands;
        return this;
    }

    @Nullable
    public String sql() {
        return sql;
    }

    public WhereClause sql(@Nullable String sql) {
        this.sql = sql;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WhereClause that = (WhereClause) o;
        return Objects.equals(field, that.field) && Objects.equals(operator, that.operator) && Objects.equals(value, that.value) && commands == that.commands && Objects.equals(sql, that.sql);
    }

    @Override
    public int hashCode() {
        return Objects.hash(field, operator, value, commands, sql);
    }

    @Override
    public String toString() {
        return "WhereClause{" +
                "field=" + field +
                ", operator='" + operator + '\'' +
                ", value=" + value +
                ", commands=" + commands +
                ", sql='" + sql + '\'' +
                '}';
    }
}