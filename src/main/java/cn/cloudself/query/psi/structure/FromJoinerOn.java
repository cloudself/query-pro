package cn.cloudself.query.psi.structure;

import java.util.Objects;

public class FromJoinerOn {
    private Field left;
    private Field right;

    public FromJoinerOn(Field left, Field right) {
        this.left = left;
        this.right = right;
    }

    public Field left() {
        return left;
    }

    public FromJoinerOn left(Field left) {
        this.left = left;
        return this;
    }

    public Field right() {
        return right;
    }

    public FromJoinerOn right(Field right) {
        this.right = right;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FromJoinerOn that = (FromJoinerOn) o;
        return Objects.equals(left, that.left) && Objects.equals(right, that.right);
    }

    @Override
    public int hashCode() {
        return Objects.hash(left, right);
    }

    @Override
    public String toString() {
        return "FromJoinerOn{" +
                "left=" + left +
                ", right=" + right +
                '}';
    }
}
