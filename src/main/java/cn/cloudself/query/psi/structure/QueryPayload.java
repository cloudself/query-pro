package cn.cloudself.query.psi.structure;

import cn.cloudself.query.config.store.HashMapStore;

import java.util.Objects;

/**
 * <h2>载荷</h2>
 * 含绑定的类对象，绑定的QueryPro对象，以及code级别的配置
 */
public class QueryPayload {
    private Class<?> clazz;
    private Class<?> queryProClass;
    private HashMapStore configs;

    /**
     * Instantiates a new Query payload.
     *
     * @param clazz         the bound class 绑定的类对象
     * @param queryProClass the query pro class
     * @param configs       the code level configs
     */
    public QueryPayload(Class<?> clazz, Class<?> queryProClass, HashMapStore configs) {
        this.clazz = clazz;
        this.queryProClass = queryProClass;
        this.configs = configs;
    }

    /**
     * Clazz class.
     *
     * @return the class 绑定的类对象
     */
    public Class<?> clazz() {
        return clazz;
    }

    /**
     * Clazz query payload.
     *
     * @param clazz the clazz
     * @return the query payload
     */
    public QueryPayload clazz(Class<?> clazz) {
        this.clazz = clazz;
        return this;
    }

    /**
     * Query pro class class.
     *
     * @return the class QueryPro对象
     */
    public Class<?> queryProClass() {
        return queryProClass;
    }

    /**
     * Query pro class query payload.
     *
     * @param queryProClass the query pro class
     * @return the query payload
     */
    public QueryPayload queryProClass(Class<?> queryProClass) {
        this.queryProClass = queryProClass;
        return this;
    }

    /**
     * code level config
     *
     * @return code level config
     */
    public HashMapStore configs() {
        return configs;
    }

    /**
     * Configs query payload.
     *
     * @param configs the configs
     * @return the query payload
     */
    public QueryPayload configs(HashMapStore configs) {
        this.configs = configs;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QueryPayload that = (QueryPayload) o;
        return Objects.equals(clazz, that.clazz) && Objects.equals(queryProClass, that.queryProClass) && Objects.equals(configs, that.configs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clazz, queryProClass, configs);
    }

    @Override
    public String toString() {
        return "QueryPayload{" +
                "clazz=" + clazz +
                ", queryProClass=" + queryProClass +
                ", configs=" + configs +
                '}';
    }
}
