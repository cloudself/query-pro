package cn.cloudself.query.psi.structure;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FromJoiner {
    private JoinType type;
    private String table;
    private List<FromJoinerOn> on;
    @Nullable
    private String alias;

    public FromJoiner(JoinType type, String table, List<FromJoinerOn> on) {
        this(type, table, on, null);
    }

    public FromJoiner(JoinType type, String table, FromJoinerOn on, @Nullable String alias) {
        this(type, table, new ArrayList<>(), alias);
        this.on.add(on);
    }

    public FromJoiner(JoinType type, String table, List<FromJoinerOn> on, @Nullable String alias) {
        this.type = type;
        this.table = table;
        this.on = on;
        this.alias = alias;
    }

    public JoinType type() {
        return type;
    }

    public FromJoiner type(JoinType type) {
        this.type = type;
        return this;
    }

    public String table() {
        return table;
    }

    public FromJoiner table(String table) {
        this.table = table;
        return this;
    }

    public List<FromJoinerOn> on() {
        return on;
    }

    public FromJoiner on(List<FromJoinerOn> on) {
        this.on = on;
        return this;
    }

    @Nullable
    public String alias() {
        return alias;
    }

    public FromJoiner alias(@Nullable String alias) {
        this.alias = alias;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FromJoiner that = (FromJoiner) o;
        return type == that.type && Objects.equals(table, that.table) && Objects.equals(on, that.on) && Objects.equals(alias, that.alias);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, table, on, alias);
    }

    @Override
    public String toString() {
        return "FromJoiner{" +
                "type=" + type +
                ", table='" + table + '\'' +
                ", on=" + on +
                ", alias='" + alias + '\'' +
                '}';
    }
}
