package cn.cloudself.query.psi.structure;

import java.util.Objects;

public class OrderByClause {
    private Field field;
    private String operator;

    public OrderByClause(Field field, String operator) {
        this.field = field;
        this.operator = operator;
    }

    public Field field() {
        return field;
    }

    public OrderByClause field(Field field) {
        this.field = field;
        return this;
    }

    public String operator() {
        return operator;
    }

    public OrderByClause operator(String operator) {
        this.operator = operator;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderByClause that = (OrderByClause) o;
        return Objects.equals(field, that.field) && Objects.equals(operator, that.operator);
    }

    @Override
    public int hashCode() {
        return Objects.hash(field, operator);
    }

    @Override
    public String toString() {
        return "OrderByClause{" +
                "field=" + field +
                ", operator='" + operator + '\'' +
                '}';
    }
}
