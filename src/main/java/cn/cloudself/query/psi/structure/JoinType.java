package cn.cloudself.query.psi.structure;

public enum JoinType {
    LEFT_JOIN,
    RIGHT_JOIN,
    INNER_JOIN,
}