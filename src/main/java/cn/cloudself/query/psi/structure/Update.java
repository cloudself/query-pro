package cn.cloudself.query.psi.structure;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Update {
    private Object data = null;
    private List<String> columns = new ArrayList<>();
    private boolean override = false;
    private String id = null;

    public Object data() {
        return data;
    }

    public Update data(Object data) {
        this.data = data;
        return this;
    }

    public List<String> columns() {
        return columns;
    }

    public Update columns(List<String> columns) {
        this.columns = columns;
        return this;
    }

    public boolean override() {
        return override;
    }

    public Update override(boolean override) {
        this.override = override;
        return this;
    }

    public String id() {
        return id;
    }

    public Update id(String id) {
        this.id = id;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Update update = (Update) o;
        return override == update.override && Objects.equals(data, update.data) && Objects.equals(columns, update.columns) && Objects.equals(id, update.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data, columns, override, id);
    }

    @Override
    public String toString() {
        return "Update{" +
                "data=" + data +
                ", columns=" + columns +
                ", override=" + override +
                ", id='" + id + '\'' +
                '}';
    }
}
