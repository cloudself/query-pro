package cn.cloudself.query.plus.col;


/**
 * The interface Get 1.
 *
 * @param <T> the type parameter
 * @param <V> the type parameter
 */
public interface Get1<T, V> extends Get<T, V> {
}
