package cn.cloudself.query.plus.col;

import java.io.Serializable;

/**
 * The interface Col new.
 *
 * @param <T> the type parameter
 */
public interface ColNew<T> extends Serializable {
    /**
     * Gets .
     *
     * @param table the table
     * @return the
     */
    Object getter(T table);
}
