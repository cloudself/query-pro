package cn.cloudself.query.plus.col;


/**
 * The interface Get 3.
 *
 * @param <T> the type parameter
 * @param <V> the type parameter
 */
public interface Get3<T, V> extends Get<T, V> {
}
