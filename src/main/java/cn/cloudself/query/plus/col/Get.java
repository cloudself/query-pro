package cn.cloudself.query.plus.col;


import java.io.Serializable;

/**
 * The interface Get.
 *
 * @param <T> the type parameter
 * @param <V> the type parameter
 */
public interface Get<T, V> extends Serializable {
    /**
     * Gets .
     *
     * @param table the table
     * @return the
     */
    Object getter(T table);
}
