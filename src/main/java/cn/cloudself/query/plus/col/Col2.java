package cn.cloudself.query.plus.col;

import java.io.Serializable;

/**
 * The interface Col 2.
 *
 * @param <T> the type parameter
 */
public interface Col2<T> extends Serializable {
    /**
     * Gets .
     *
     * @param table the table
     * @return the
     */
    Object getter(T table);
}
