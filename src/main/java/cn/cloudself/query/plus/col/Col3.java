package cn.cloudself.query.plus.col;

import java.io.Serializable;

/**
 * The interface Col 3.
 *
 * @param <T> the type parameter
 */
public interface Col3<T> extends Serializable {
    /**
     * Gets .
     *
     * @param table the table
     * @return the
     */
    Object getter(T table);
}
