package cn.cloudself.query.plus.col;


/**
 * The interface Get 4.
 *
 * @param <T> the type parameter
 * @param <V> the type parameter
 */
public interface Get4<T, V> extends Get<T, V> {
}
