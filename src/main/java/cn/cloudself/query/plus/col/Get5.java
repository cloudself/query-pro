package cn.cloudself.query.plus.col;


/**
 * The interface Get 5.
 *
 * @param <T> the type parameter
 * @param <V> the type parameter
 */
public interface Get5<T, V> extends Get<T, V> {
}
