package cn.cloudself.query.plus.col;


/**
 * The interface Get 2.
 *
 * @param <T> the type parameter
 * @param <V> the type parameter
 */
public interface Get2<T, V> extends Get<T, V> {
}
