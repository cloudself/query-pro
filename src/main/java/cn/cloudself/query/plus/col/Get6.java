package cn.cloudself.query.plus.col;


/**
 * The interface Get 6.
 *
 * @param <T> the type parameter
 * @param <V> the type parameter
 */
public interface Get6<T, V> extends Get<T, V> {
}
