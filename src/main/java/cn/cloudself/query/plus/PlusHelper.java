package cn.cloudself.query.plus;


import cn.cloudself.query.plus.col.Get;
import cn.cloudself.query.psi.structure.Field;
import cn.cloudself.util.ext.CasePlus;

import javax.persistence.Table;
import java.io.Serializable;
import java.lang.invoke.SerializedLambda;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class PlusHelper {
    public static String getTableName(Class<?> clazz) {
        String name = null;
        try {
            final Table table = clazz.getAnnotation(Table.class);
            if (table != null) {
                name = table.name();
            }
        } catch (Exception ignored) {
        }
        if (name != null) {
            return name;
        }
        return CasePlus.to_snake_case(clazz.getSimpleName());
    }

    public static Field toField(Serializable serializable) {
        return toField(serializable, null);
    }
    public static Field toField(Serializable serializable, String alias) {
        final SerializedLambda sl = getSerializedLambda(serializable);
        final String className = sl.getImplClass().replace('/', '.');
        final String table = alias == null ? PlusHelper.getTableName(unsafeForName(className)) : alias;
        final String getter = sl.getImplMethodName();
        final String field;
        if (getter.startsWith("get")) {
            field = getter.substring(3);
        } else if (getter.startsWith("is")) {
            field = getter.substring(2);
        } else {
            field = getter;
        }
        final String snake_case_field = CasePlus.to_snake_case(field);
        return new Field(table, snake_case_field);
    }

    public static <T, V> Class<V> getReturnType(Get<T, V> get) {
        final SerializedLambda sl = getSerializedLambda(get);
        final String implMethodSignature = sl.getImplMethodSignature();
        final String returnType = implMethodSignature.substring(implMethodSignature.lastIndexOf(')') + 1);

        if ('L' == returnType.charAt(0)) {
            final String clazzStr = returnType.substring(1, returnType.length() - 1).replace('/', '.');
            return unsafeForName(clazzStr);
        } else {
            //noinspection unchecked
            return (Class<V>) JNI_FIELD_DESC_MAP.get(returnType);
        }
    }

    private static <T> Class<T> unsafeForName(String className) {
        try {
            //noinspection unchecked
            return (Class<T>) Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private static SerializedLambda getSerializedLambda(Serializable serializable) {
        final Class<?> clazz = serializable.getClass();
        final Method writeReplace;
        try {
            writeReplace = clazz.getDeclaredMethod("writeReplace");
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
        writeReplace.setAccessible(true);
        // SerializedLambda structure, see JavaNative Interface FieldDescriptors
        try {
            return  (SerializedLambda) writeReplace.invoke(serializable);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    private static final Map<String, Class<?>> JNI_FIELD_DESC_MAP = new HashMap<>();
    static {
        JNI_FIELD_DESC_MAP.put("Z", boolean.class);
        JNI_FIELD_DESC_MAP.put("B", byte.class);
        JNI_FIELD_DESC_MAP.put("C", char.class);
        JNI_FIELD_DESC_MAP.put("S", short.class);
        JNI_FIELD_DESC_MAP.put("I", int.class);
        JNI_FIELD_DESC_MAP.put("J", long.class);
        JNI_FIELD_DESC_MAP.put("F", float.class);
        JNI_FIELD_DESC_MAP.put("D", double.class);
        JNI_FIELD_DESC_MAP.put("V", void.class);
    }
}
