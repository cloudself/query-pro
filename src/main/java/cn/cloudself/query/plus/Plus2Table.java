package cn.cloudself.query.plus;

import cn.cloudself.query.plus.col.*;
import cn.cloudself.query.psi.*;
import cn.cloudself.query.psi.structure.*;
import cn.cloudself.util.structure.ListEx;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class Plus2Table<MAIN, T1, T2> {
    private final QueryStructure queryStructure;
    private final QueryPayload payload;
    private final Class<MAIN> clazz;
    public Plus2Table(QueryStructure queryStructure, QueryPayload payload, Class<MAIN> clazz) {
        this.queryStructure = queryStructure;
        this.payload = payload;
        this.clazz = clazz;
    }
    public class On {
        private final JoinType joinType;
        private final Class<?> newClazz;
        @Nullable
        private final String alias;
        public On(JoinType joinType, Class<?> newClazz)  {
            this(joinType, newClazz, null);
        }
        public On(JoinType joinType, Class<?> newClazz, @Nullable String alias) {
            this.joinType = joinType;
            this.newClazz = newClazz;
            this.alias = alias;
        }

        private <_T1, _T2> Plus2Table<MAIN, _T1, _T2>.Joined on(cn.cloudself.query.psi.structure.Field left, cn.cloudself.query.psi.structure.Field right) {
            final String table = PlusHelper.getTableName(newClazz);
            queryStructure.from().appendJoin(new FromJoiner(joinType, table, new FromJoinerOn(left, right), alias));
            return new Plus2Table<MAIN, _T1, _T2>(queryStructure, payload, clazz).new Joined();
        }

        public Plus2Table<MAIN, T1, T2>.Joined on(Col1<T1> column, Col2<T2> newTableColumn) {
            return this.<T1, T2>on(PlusHelper.toField(column), PlusHelper.toField(newTableColumn));
        }

        public Plus2Table<MAIN, T1, T2>.Joined on(Col2<T2> newTableColumn, Col1<T1> column) {
            return this.<T1, T2>on(PlusHelper.toField(column), PlusHelper.toField(newTableColumn));
        }

        public <TAny> Plus2Table<MAIN, T1, Alias>.Joined on(Col1<T1> table1column, String table2alias, Col2<TAny> table2column) {
            return this.<T1, Alias>on(PlusHelper.toField(table1column), PlusHelper.toField(table2column, table2alias));
        }
        public <TAny> Plus2Table<MAIN, Alias, T2>.Joined on(Col2<T2> table2column, String table1alias, Col1<TAny> table1column) {
            return this.<Alias, T2>on(PlusHelper.toField(table1column, table1alias), PlusHelper.toField(table2column));
        }
        public <TAny> Plus2Table<MAIN, T1, Alias>.Joined on(String table2alias, Col2<TAny> table2column, Col1<T1> table1column) {
            return this.<T1, Alias>on(PlusHelper.toField(table1column), PlusHelper.toField(table2column, table2alias));
        }
        public <TAny> Plus2Table<MAIN, Alias, T2>.Joined on(String table1alias, Col1<TAny> table1column, Col2<T2> table2column) {
            return this.<Alias, T2>on(PlusHelper.toField(table1column, table1alias), PlusHelper.toField(table2column));
        }
        public <TA, TN> Plus2Table<MAIN, Alias, Alias>.Joined on(String table1alias, Col1<TA> table1column, String table2alias, Col2<TN> table2column) {
            return this.<Alias, Alias>on(PlusHelper.toField(table1column, table1alias), PlusHelper.toField(table2column, table2alias));
        }
    }

    public class WhereField<RUN_RES> extends AbstractWhereExpressionOperators<MAIN, RUN_RES, WhereField<RUN_RES>, OrderByField<RUN_RES>, ColumnLimiterField<RUN_RES>, ColumnsLimiterField<RUN_RES>> {
        private final QueryStructure queryStructure;
        private final QueryPayload payload;
        private final Class<MAIN> clazz;
        public WhereField(QueryStructure queryStructure, QueryPayload payload, Class<MAIN> clazz) {
            this.queryStructure = queryStructure;
            this.payload = payload;
            this.clazz = clazz;
        }
        @NotNull @Override protected QueryStructure getQueryStructure() { return queryStructure; }
        @NotNull @Override protected QueryPayload getPayload() { return payload; }
        @NotNull @Override protected Class<MAIN> getClazz() { return clazz; }
        @NotNull @Override protected WhereField<RUN_RES> createWhereField(@NotNull QueryStructure qs, @NotNull QueryPayload payload) { return new WhereField<>(qs, payload, clazz); }
        @NotNull @Override protected OrderByField<RUN_RES> createOrderByField(@NotNull QueryStructure qs, @NotNull QueryPayload payload) { return new OrderByField<>(qs, payload, clazz); }
        @NotNull @Override protected ColumnLimiterField<RUN_RES> createColumnLimitField(@NotNull QueryStructure qs, @NotNull QueryPayload payload) { return new ColumnLimiterField<>(qs, payload, clazz); }
        @NotNull @Override protected ColumnsLimiterField<RUN_RES> createColumnsLimitField(@NotNull QueryStructure qs, @NotNull QueryPayload payload) { return new ColumnsLimiterField<>(qs, payload, clazz); }
        @NotNull @Override protected ExpressionType getFieldType() { return ExpressionType.WHERE; }
        private Keywords<WhereField<RUN_RES>> makeKeywords(cn.cloudself.query.psi.structure.Field column) {
            return new Keywords<>(column, queryStructure, payload, this::createWhereField);
        }
        public <A> Keywords<WhereField<RUN_RES>> column(String alias, ColNew<A> column) {
            return makeKeywords(PlusHelper.toField(column, alias));
        }
        public Keywords<WhereField<RUN_RES>> column(Col1<T1> column) {
            return makeKeywords(PlusHelper.toField(column));
        }
        public Keywords<WhereField<RUN_RES>> column(Col2<T2> column) {
            return makeKeywords(PlusHelper.toField(column));
        }
    }

    public abstract class Field<RUN_RES> extends AbstractExpressionOperators<MAIN, RUN_RES, WhereField<RUN_RES>, OrderByField<RUN_RES>, ColumnLimiterField<RUN_RES>, ColumnsLimiterField<RUN_RES>> {
        private final QueryStructure queryStructure;
        private final QueryPayload payload;
        private final Class<MAIN> clazz;
        public Field(@NotNull QueryStructure queryStructure, @NotNull QueryPayload payload, @NotNull Class<MAIN> clazz) {
            this.queryStructure = queryStructure;
            this.payload = payload;
            this.clazz = clazz;
        }
        @NotNull @Override protected QueryStructure getQueryStructure() { return queryStructure; }
        @NotNull @Override protected QueryPayload getPayload() { return payload; }
        @NotNull @Override protected Class<MAIN> getClazz() { return clazz; }
        @NotNull @Override protected WhereField<RUN_RES> createWhereField(@NotNull QueryStructure qs, @NotNull QueryPayload payload) { return new WhereField<>(qs, payload, clazz); }
        @NotNull @Override protected OrderByField<RUN_RES> createOrderByField(@NotNull QueryStructure qs, @NotNull QueryPayload payload) { return new OrderByField<>(qs, payload, clazz); }
        @NotNull @Override protected ColumnLimiterField<RUN_RES> createColumnLimitField(@NotNull QueryStructure qs, @NotNull QueryPayload payload) { return new ColumnLimiterField<>(qs, payload, clazz); }
        @NotNull @Override protected ColumnsLimiterField<RUN_RES> createColumnsLimitField(@NotNull QueryStructure qs, @NotNull QueryPayload payload) { return new ColumnsLimiterField<>(qs, payload, clazz); }
    }

    public class OrderByField<RUN_RES> extends Field<RUN_RES> {
        public OrderByField(QueryStructure queryStructure, QueryPayload payload, Class<MAIN> clazz) {
            super(queryStructure, payload, clazz);
        }
        private KeywordsOrderBy<OrderByField<RUN_RES>> makeKeywords(cn.cloudself.query.psi.structure.Field column) {
            return new KeywordsOrderBy<>(column, queryStructure, payload, this::createOrderByField);
        }
        @NotNull @Override protected ExpressionType getFieldType() { return ExpressionType.ORDER_BY; }

        public <A> KeywordsOrderBy<OrderByField<RUN_RES>> column(String alias, ColNew<A> column) {
            return makeKeywords(PlusHelper.toField(column, alias));
        }
        public KeywordsOrderBy<OrderByField<RUN_RES>> column(Col1<T1> column) {
            return makeKeywords(PlusHelper.toField(column));
        }
        public KeywordsOrderBy<OrderByField<RUN_RES>> column(Col2<T2> column) {
            return makeKeywords(PlusHelper.toField(column));
        }
    }

    public class ColumnLimiterField<RUN_RES> extends Field<RUN_RES> {
        public ColumnLimiterField(QueryStructure queryStructure, QueryPayload payload, Class<MAIN> clazz) { super(queryStructure, payload, clazz); }
        private <V> ListEx<V> queryWithColumnLimiter(Class<V> clazz, cn.cloudself.query.psi.structure.Field column) { return new ListEx<>(getColumn(column, clazz)); }
        @NotNull @Override protected ExpressionType getFieldType() { return ExpressionType.COLUMN_LIMITER; }

        public <TA, V> ListEx<V> column(String alias, Get<TA, V> column) {
            return queryWithColumnLimiter(PlusHelper.getReturnType(column), PlusHelper.toField(column, alias));
        }
        public <V> ListEx<V> column(Get1<T1, V> column) {
            return queryWithColumnLimiter(PlusHelper.getReturnType(column), PlusHelper.toField(column));
        }
        public <V> ListEx<V> column(Get2<T2, V> column) {
            return queryWithColumnLimiter(PlusHelper.getReturnType(column), PlusHelper.toField(column));
        }
    }

    public class ColumnsLimiterField<RUN_RES> extends Field<RUN_RES> {
        public ColumnsLimiterField(QueryStructure queryStructure, QueryPayload payload, Class<MAIN> clazz) {
            super(queryStructure, payload, clazz);
        }
        private ColumnsLimiterField<RUN_RES> makeColumnsLimiter(cn.cloudself.query.psi.structure.Field field)  {
            queryStructure.fields().add(field);
            return this;
        }
        @NotNull @Override protected ExpressionType getFieldType() { return ExpressionType.COLUMNS_LIMITER; }

        public <A> ColumnsLimiterField<RUN_RES> column(String alias, ColNew<A> column) {
            return makeColumnsLimiter(PlusHelper.toField(column, alias));
        }
        public ColumnsLimiterField<RUN_RES> column(Col1<T1> column) {
            return makeColumnsLimiter(PlusHelper.toField(column));
        }
        public ColumnsLimiterField<RUN_RES> column(Col2<T2> column) {
            return makeColumnsLimiter(PlusHelper.toField(column));
        }
    }

    public class Joined {
        public <NEW> Plus3Table<MAIN, T1, T2, NEW>.On leftJoin (Class<NEW> entity) {
            return new Plus3Table<MAIN, T1, T2, NEW>(queryStructure, payload, clazz).new On(JoinType.LEFT_JOIN, entity);
        }
        public <NEW> Plus3Table<MAIN, T1, T2, NEW>.On rightJoin(Class<NEW> entity) {
            return new Plus3Table<MAIN, T1, T2, NEW>(queryStructure, payload, clazz).new On(JoinType.RIGHT_JOIN, entity);
        }
        public <NEW> Plus3Table<MAIN, T1, T2, NEW>.On innerJoin(Class<NEW> entity) {
            return new Plus3Table<MAIN, T1, T2, NEW>(queryStructure, payload, clazz).new On(JoinType.INNER_JOIN, entity);
        }
        public <NEW> Plus3Table<MAIN, T1, T2, Alias>.On leftJoin (Class<NEW> entity, String alias) {
            return new Plus3Table<MAIN, T1, T2, Alias>(queryStructure, payload, clazz).new On(JoinType.LEFT_JOIN, entity, alias);
        }
        public <NEW> Plus3Table<MAIN, T1, T2, Alias>.On rightJoin(Class<NEW> entity, String alias) {
            return new Plus3Table<MAIN, T1, T2, Alias>(queryStructure, payload, clazz).new On(JoinType.RIGHT_JOIN, entity, alias);
        }
        public <NEW> Plus3Table<MAIN, T1, T2, Alias>.On innerJoin(Class<NEW> entity, String alias) {
            return new Plus3Table<MAIN, T1, T2, Alias>(queryStructure, payload, clazz).new On(JoinType.INNER_JOIN, entity, alias);
        }

        public WhereField<List<MAIN>> where() {
            return new WhereField<>(queryStructure, payload, clazz);
        }
    }
}
