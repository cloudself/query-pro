package cn.cloudself.query.plus;

import cn.cloudself.query.psi.structure.JoinType;
import cn.cloudself.query.psi.structure.QueryPayload;
import cn.cloudself.query.psi.structure.QueryStructure;

public class QueryProPlus<MAIN, T> {
    private final QueryStructure queryStructure;
    private final QueryPayload payload;
    private final Class<MAIN> clazz;
    public QueryProPlus(QueryStructure queryStructure, QueryPayload payload, Class<MAIN> clazz) {
        this(queryStructure, payload, clazz, null);
    }
    public QueryProPlus(QueryStructure queryStructure, QueryPayload payload, Class<MAIN> clazz, String alias) {
        this.queryStructure = queryStructure;
        this.payload = payload;
        this.clazz = clazz;
        if (alias != null) {
            queryStructure.from().alias(alias);
        }
    }

    public <NEW> Plus2Table<MAIN, T, NEW>.On leftJoin(Class<NEW> entity) {
        return new Plus2Table<MAIN, T, NEW>(queryStructure, payload, clazz).new On(JoinType.LEFT_JOIN, entity);
    }
    public <NEW> Plus2Table<MAIN, T, NEW>.On rightJoin(Class<NEW> entity) {
        return new Plus2Table<MAIN, T, NEW>(queryStructure, payload, clazz).new On(JoinType.RIGHT_JOIN, entity);
    }
    public <NEW> Plus2Table<MAIN, T, NEW>.On innerJoin(Class<NEW> entity) {
        return new Plus2Table<MAIN, T, NEW>(queryStructure, payload, clazz).new On(JoinType.INNER_JOIN, entity);
    }
    public <NEW> Plus2Table<MAIN, T, Alias>.On leftJoin(Class<NEW> entity, String alias) {
        return new Plus2Table<MAIN, T, Alias>(queryStructure, payload, clazz).new On(JoinType.LEFT_JOIN, entity, alias);
    }
    public <NEW> Plus2Table<MAIN, T, Alias>.On rightJoin(Class<NEW> entity, String alias) {
        return new Plus2Table<MAIN, T, Alias>(queryStructure, payload, clazz).new On(JoinType.RIGHT_JOIN, entity, alias);
    }
    public <NEW> Plus2Table<MAIN, T, Alias>.On innerJoin(Class<NEW> entity, String alias) {
        return new Plus2Table<MAIN, T, Alias>(queryStructure, payload, clazz).new On(JoinType.INNER_JOIN, entity, alias);
    }
}
