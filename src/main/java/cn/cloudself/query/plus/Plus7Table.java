package cn.cloudself.query.plus;

import cn.cloudself.query.plus.col.*;
import cn.cloudself.query.psi.structure.*;
import org.jetbrains.annotations.Nullable;

public class Plus7Table<MAIN, T1, T2, T3, T4, T5, T6, T7>  {
    private final QueryStructure queryStructure;
    private final QueryPayload payload;
    private final Class<MAIN> clazz;
    public Plus7Table(QueryStructure queryStructure, QueryPayload payload, Class<MAIN> clazz) {
        this.queryStructure = queryStructure;
        this.payload = payload;
        this.clazz = clazz;
    }

    public class On {
        private final JoinType joinType;
        private final Class<?> newClazz;
        @Nullable
        private final String alias;
        public On(JoinType joinType, Class<?> newClazz)  {
            this(joinType, newClazz, null);
        }
        public On(JoinType joinType, Class<?> newClazz, @Nullable String alias) {
            this.joinType = joinType;
            this.newClazz = newClazz;
            this.alias = alias;
        }
        private PlusManyTable<MAIN>.Joined on(cn.cloudself.query.psi.structure.Field left, cn.cloudself.query.psi.structure.Field right) {
            final String table = PlusHelper.getTableName(newClazz);
            queryStructure.from().appendJoin(new FromJoiner(joinType, table, new FromJoinerOn(left, right), alias));
            return new PlusManyTable<>(queryStructure, payload, clazz).new Joined();
        }

        public PlusManyTable<MAIN>.Joined on(Col1<T1> t1, ColNew<T7> newCol) { return on(PlusHelper.toField(t1), PlusHelper.toField(newCol)); }
        public PlusManyTable<MAIN>.Joined on(Col2<T2> t2, ColNew<T7> newCol) { return on(PlusHelper.toField(t2), PlusHelper.toField(newCol)); }
        public PlusManyTable<MAIN>.Joined on(Col3<T3> t3, ColNew<T7> newCol) { return on(PlusHelper.toField(t3), PlusHelper.toField(newCol)); }
        public PlusManyTable<MAIN>.Joined on(Col4<T4> t4, ColNew<T7> newCol) { return on(PlusHelper.toField(t4), PlusHelper.toField(newCol)); }
        public PlusManyTable<MAIN>.Joined on(Col5<T5> t5, ColNew<T7> newCol) { return on(PlusHelper.toField(t5), PlusHelper.toField(newCol)); }
        public PlusManyTable<MAIN>.Joined on(Col6<T6> t6, ColNew<T7> newCol) { return on(PlusHelper.toField(t6), PlusHelper.toField(newCol)); }
        public PlusManyTable<MAIN>.Joined on(ColNew<T7> newCol, Col1<T1> t1) { return on(t1, newCol); }
        public PlusManyTable<MAIN>.Joined on(ColNew<T7> newCol, Col2<T2> t2) { return on(t2, newCol); }
        public PlusManyTable<MAIN>.Joined on(ColNew<T7> newCol, Col3<T3> t3) { return on(t3, newCol); }
        public PlusManyTable<MAIN>.Joined on(ColNew<T7> newCol, Col4<T4> t4) { return on(t4, newCol); }

        public <TN> PlusManyTable<MAIN>.Joined on(Col1<T1> table1column, String tableNewAlias, ColNew<TN> tableNewColumn) { return on(PlusHelper.toField(table1column), PlusHelper.toField(tableNewColumn, tableNewAlias)); }
        public <TN> PlusManyTable<MAIN>.Joined on(Col2<T2> table2column, String tableNewAlias, ColNew<TN> tableNewColumn) { return on(PlusHelper.toField(table2column), PlusHelper.toField(tableNewColumn, tableNewAlias)); }
        public <TN> PlusManyTable<MAIN>.Joined on(Col3<T3> table3column, String tableNewAlias, ColNew<TN> tableNewColumn) { return on(PlusHelper.toField(table3column), PlusHelper.toField(tableNewColumn, tableNewAlias)); }
        public <TN> PlusManyTable<MAIN>.Joined on(Col4<T4> table4column, String tableNewAlias, ColNew<TN> tableNewColumn) { return on(PlusHelper.toField(table4column), PlusHelper.toField(tableNewColumn, tableNewAlias)); }
        public <TN> PlusManyTable<MAIN>.Joined on(Col5<T5> table5column, String tableNewAlias, ColNew<TN> tableNewColumn) { return on(PlusHelper.toField(table5column), PlusHelper.toField(tableNewColumn, tableNewAlias)); }
        public <TN> PlusManyTable<MAIN>.Joined on(Col6<T6> table6column, String tableNewAlias, ColNew<TN> tableNewColumn) { return on(PlusHelper.toField(table6column), PlusHelper.toField(tableNewColumn, tableNewAlias)); }
        public <TN> PlusManyTable<MAIN>.Joined on(String tableNewAlias, ColNew<TN> tableNewColumn, Col1<T1> table1column) { return on(PlusHelper.toField(table1column), PlusHelper.toField(tableNewColumn, tableNewAlias)); }
        public <TN> PlusManyTable<MAIN>.Joined on(String tableNewAlias, ColNew<TN> tableNewColumn, Col2<T2> table2column) { return on(PlusHelper.toField(table2column), PlusHelper.toField(tableNewColumn, tableNewAlias)); }
        public <TN> PlusManyTable<MAIN>.Joined on(String tableNewAlias, ColNew<TN> tableNewColumn, Col3<T3> table3column) { return on(PlusHelper.toField(table3column), PlusHelper.toField(tableNewColumn, tableNewAlias)); }
        public <TN> PlusManyTable<MAIN>.Joined on(String tableNewAlias, ColNew<TN> tableNewColumn, Col4<T4> table4column) { return on(PlusHelper.toField(table4column), PlusHelper.toField(tableNewColumn, tableNewAlias)); }
        public <TN> PlusManyTable<MAIN>.Joined on(String tableNewAlias, ColNew<TN> tableNewColumn, Col5<T5> table5column) { return on(PlusHelper.toField(table5column), PlusHelper.toField(tableNewColumn, tableNewAlias)); }
        public <TN> PlusManyTable<MAIN>.Joined on(String tableNewAlias, ColNew<TN> tableNewColumn, Col6<T6> table6column) { return on(PlusHelper.toField(table6column), PlusHelper.toField(tableNewColumn, tableNewAlias)); }
        public <TA> PlusManyTable<MAIN>.Joined on(String table1alias, Col1<TA> table1column, Col7<T7> tableNewColumn) { return on(PlusHelper.toField(table1column, table1alias), PlusHelper.toField(tableNewColumn)); }
        public <TA> PlusManyTable<MAIN>.Joined on(Col7<T7> tableNewColumn, String table1alias, Col1<TA> table1column) { return on(PlusHelper.toField(table1column, table1alias), PlusHelper.toField(tableNewColumn)); }
        public <TA, TN> PlusManyTable<MAIN>.Joined on(String table1alias, Col1<TA> table1column, String table2alias, Col2<TN> table2column) { return on(PlusHelper.toField(table1column, table1alias), PlusHelper.toField(table2column, table2alias)); }
    }
}
