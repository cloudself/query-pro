package cn.cloudself.query.generator;

import cn.cloudself.query.QueryPro;
import cn.cloudself.exception.IllegalCall;
import cn.cloudself.exception.IllegalTemplate;
import cn.cloudself.query.generator.misc.*;
import cn.cloudself.query.psi.AbstractExpressionOperators;
import cn.cloudself.query.psi.annotations.JvHidden;
import cn.cloudself.query.psi.annotations.PureContract;
import cn.cloudself.util.ext.IOPro;
import cn.cloudself.util.log.Log;
import cn.cloudself.util.log.LogFactory;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.sql.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class QueryProFileMaker {
    private final JavaFilePath[] templateFileNameAndPaths;
    private final Log logger = LogFactory.getLog(QueryProFileMaker.class);
    private final List<String> QueryProKeywords = Arrays.stream(AbstractExpressionOperators.class.getMethods())
            .map(Method::getName).collect(Collectors.toList());
    {
        System.out.println("Keywords: " + QueryProKeywords);
    }
    private QueryProFileMaker(JavaFilePath... templateFileNameAndPaths) {
        this.templateFileNameAndPaths = templateFileNameAndPaths;
    }

    /**
     * 生成entity和dao至两个文件 Java版, 参考 [QueryProFileMaker.entityAndDaoMode]
     * 注意对于特殊构造的数据库结构，可能被注入Java代码到生成的(Entity, Dao)源文件中，所以可能有任意代码执行的风险
     * @param filePathResolver [FilePathResolver] 文件位置解析器，即指示生成的文件应该放在哪里。可使用[PathFrom]生成
     */
    public static QueryProFileMaker entityAndDaoMode(FilePathResolver filePathResolver) {
        return new QueryProFileMaker(
                filePathResolver.resolve("DaoJava.ftl"),
                filePathResolver.resolve("EntityJava.ftl")
        );
    }

    private final String KEYWORDS_SUFFIX = "_$col$umn";
    private boolean debug = false;
    private boolean useLogger = false;
    private DbInfo db = null;
    private Set<String> tables = new HashSet<>(); {
        tables.add("");
    }
    private Set<String> excludeTables = new HashSet<>();
    private List<Predicate<String>> excludeTableFilters = new ArrayList<>();
    private String daoExCodes = "";
    private String entityExCodes = "";
    private Function<TemplateModel.Column, List<String>> entityExPropertyAnnotationsGenerator = null;
    private String defaultDataSource = null;
    private boolean replaceMode = false;
    private boolean skipReplaceEntity = false;
    private boolean chainForModel = false;
    private boolean swaggerSupport = false;
    private String entityFileTemplatePath = null;
    private final Map<String, KtJavaType> dbMetaTypeMapKtJavaType = new HashMap<String, KtJavaType>() {{
        put("BIGINT", new KtJavaType("Long"));
        put("VARCHAR", new KtJavaType("String"));
        put("CHAR", new KtJavaType("String"));
        put("BIT", new KtJavaType("Boolean"));
        put("TINYINT", new KtJavaType("Short"));
        put("DATE", new KtJavaType("Date"));
        put("DATETIME", new KtJavaType("Date"));
        put("DECIMAL", new KtJavaType("BigDecimal"));
        put("FLOAT", new KtJavaType("Float"));
        put("DOUBLE", new KtJavaType("Double"));
        put("SMALLINT", new KtJavaType("Int", "Integer"));
        put("INT", new KtJavaType("Int", "Integer"));
        put("TEXT", new KtJavaType("String"));
        put("MEDIUMTEXT", new KtJavaType("String"));
        put("LONGTEXT", new KtJavaType("String"));
        put("BLOB", new KtJavaType("ByteArray", "byte[]"));
        put("LONGBLOB", new KtJavaType("ByteArray", "byte[]"));
        put("JSON", new KtJavaType("String"));
        put("BINARY", new KtJavaType("ByteArray", "byte[]"));
        put("TIMESTAMP", new KtJavaType("Date"));
    }};

    private final Set<String> javaKeywords = new HashSet<>(Arrays.asList(
            "abstract", "continue", "for", "new", "switch", "assert", "default", "goto", "package", "synchronized",
            "boolean", "do", "if", "private", "this", "break", "double", "implements", "protected", "throw",
            "byte", "else", "import", "public", "throws", "case", "enum", "instanceof", "return", "transient",
            "catch", "extends", "int", "short", "try", "char", "final", "interface", "static", "void",
            "class", "finally", "long", "strictfp", "volatile", "const", "float", "native", "super", "while"
    ));

    private NameConverter nameConverter = DbNameToJava.createDefault().getConverter();

    /**
     * 生成的JavaBean允许链式set
     */
    public QueryProFileMaker chain() {
        this.chainForModel = true;
        return this;
    }

    /**
     * 显示更多输出
     */
    public QueryProFileMaker debug() {
        return debug(false);
    }

    /**
     * 显示更多输出
     */
    public QueryProFileMaker debug(boolean useLogger) {
        this.debug = true;
        this.useLogger = useLogger;
        return this;
    }

    /**
     * 指定db
     * @see DbInfoBuilder#mysql(String, String)
     */
    public QueryProFileMaker db(DbInfo db) {
        this.db = db;
        return this;
    }

    /**
     * 指定需要生成QueryPro文件的表名，允许为"*"，代表所有，
     * 注意对于特殊构造的数据库结构，可能被注入Java代码到生成的(Entity, Dao)源文件中，所以可能有任意代码执行的风险
     */
    public QueryProFileMaker tables(String... tables) {
        this.tables = new HashSet<>(Arrays.asList(tables));
        return this;
    }

    /**
     * 指定需要排除生成QueryPro文件的表名
     */
    public QueryProFileMaker excludeTables(String... tables) {
        this.excludeTables = new HashSet<>(Arrays.asList(tables));
        return this;
    }

    /**
     * 指定需要排除生成QueryPro文件的表名
     */
    public QueryProFileMaker excludeTables(Predicate<String> filter) {
        this.excludeTableFilters.add(filter);
        return this;
    }

    /**
     * 加入dao中的额外方法
     */
    public QueryProFileMaker daoExCodes(String codes) {
        this.daoExCodes = codes;
        return this;
    }

    /**
     * 加入实体类的额外方法
     */
    public QueryProFileMaker entityExMethods(String codes) {
        this.entityExCodes = codes;
        return this;
    }

    public QueryProFileMaker entityExPropertyAnnotations(Function<TemplateModel.Column, List<String>> annotationGenerator) {
        this.entityExPropertyAnnotationsGenerator = annotationGenerator;
        return this;
    }

    /**
     * 默认的DataSource获取方法
     */
    public QueryProFileMaker defaultDataSource(String code) {
        this.defaultDataSource = code;
        return this;
    }

    /**
     * 是否替换掉已有的文件 默认false
     */
    public QueryProFileMaker replaceMode() {
        return replaceMode(true);
    }

    /**
     * 是否替换掉已有的文件 默认false
     */
    public QueryProFileMaker replaceMode(boolean replaceMode) {
        this.replaceMode = replaceMode;
        return this;
    }

    public QueryProFileMaker skipReplaceEntity() {
        return skipReplaceEntity(true);
    }

    public QueryProFileMaker skipReplaceEntity(boolean skipReplaceEntity) {
        this.skipReplaceEntity = skipReplaceEntity;
        return this;
    }

    /**
     * 自定义名称转换器(用于转换数据库table, column名称至java类名，属性名)
     * @param nameConverter [NameConverter]
     * @see DbNameToJava#createDefault()
     */
    public QueryProFileMaker dbJavaNameConverter(NameConverter nameConverter) {
        this.nameConverter = nameConverter;
        return this;
    }

    /**
     * 添加swagger的支持(包括但不限于在Entity上面添加ApiModelProperty注解)
     */
    public QueryProFileMaker swaggerSupport(Boolean swaggerSupport) {
        this.swaggerSupport = swaggerSupport;
        return this;
    }

    /**
     * 自定义entity的模板
     */
    public QueryProFileMaker entityFileTemplatePath(String path) {
        this.entityFileTemplatePath = path;
        return this;
    }

    public void create() {
        final Map<String, TemplateModel> modelsFromDb = getModelsFromDb();
        debugPrint(modelsFromDb);
        modelsFromDb.entrySet().parallelStream().forEach(entry -> {
            final String db_name = entry.getKey();
            final TemplateModel model = entry.getValue();

            final JavaFilePath entityFilePath = Arrays.stream(templateFileNameAndPaths)
                    .filter(it -> it.getTemplateName().startsWith("Entity"))
                    .findAny().orElse(null);

            for (JavaFilePath javaFilePath : templateFileNameAndPaths) {
                debugPrint(javaFilePath);
                final String templateName = javaFilePath.getTemplateName();

                final String noExtTemplateName = templateName.substring(0, templateName.lastIndexOf("."));
                final boolean areEntity = templateName.startsWith("Entity");

                final String ext = ".java";

                final String ClassName = nameConverter.convert(new ConvertInfo(db_name, JavaNameType.ClassName, templateName));

                final Map<String, Object> data = new HashMap<>();
                data.put("m", model);

                model.set_EntityName(nameConverter.convert(
                    new ConvertInfo(
                        db_name,
                        JavaNameType.ClassName,
                        "EntityJava.ftl"
                    )
                ));
                if (entityFilePath != null) {
                    model.setEntityPackage(entityFilePath.getPackagePath());
                }

                model.setChainForModel(chainForModel);
                model.set_ClassName(ClassName);
                model.setPackagePath(javaFilePath.getPackagePath());
                model.setSwaggerSupport(swaggerSupport);
                final String templateDir = "templates";
                final List<TemplateModel.DelegateInfo> delegateInfo = delegateQueryPro(templateName, () -> {
                    final InputStream is = QueryProFileMaker.class.getClassLoader().getResourceAsStream(templateDir + "/" + templateName);
                    if (is == null) {
                        throw new IllegalCall("没有找到相应的模板");
                    }
                    return is;
                });
                debugPrint(delegateInfo);
                model.setQueryProDelegate(delegateInfo);
                for (TemplateModel.Column column : model.getColumns()) {
                    final String propertyName = nameConverter.convert(
                            new ConvertInfo(column.getDb_name(), JavaNameType.propertyName, templateName)
                    );

                    if (javaKeywords.contains(propertyName)) {
                        column.setPropertyName(propertyName + KEYWORDS_SUFFIX);
                    } else if (QueryProKeywords.contains(propertyName)) {
                        column.setPropertyName(propertyName + KEYWORDS_SUFFIX);
                    } else {
                        column.setPropertyName(propertyName);
                    }

                    if (this.entityExPropertyAnnotationsGenerator != null) {
                        final List<String> annotations = this.entityExPropertyAnnotationsGenerator.apply(column);
                        if (annotations != null) {
                            column.setAnnotations(annotations);
                        }
                    }
                }

                final Configuration configuration = new Configuration(Configuration.VERSION_2_3_30);
                configuration.setClassLoaderForTemplateLoading(QueryProFileMaker.class.getClassLoader(), templateDir);
                final Template template;
                try {
                    template = configuration.getTemplate(templateName);
                    Files.createDirectories(javaFilePath.getDir());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                final List<OpenOption> openOptions = new ArrayList<>();
                openOptions.add(StandardOpenOption.CREATE);
                if (replaceMode) {
                    openOptions.add(StandardOpenOption.TRUNCATE_EXISTING);
                }
                if (areEntity && skipReplaceEntity) {
                    openOptions.clear();
                    openOptions.add(StandardOpenOption.CREATE_NEW);
                }
                final Path filePath = Paths.get(javaFilePath.getDir().toAbsolutePath().toString(), ClassName + ext);
                try (final OutputStream os = Files.newOutputStream(filePath, openOptions.toArray(new OpenOption[0]))) {
                    template.process(data, new OutputStreamWriter(os, StandardCharsets.UTF_8));
                } catch (FileAlreadyExistsException e) {
                    warn("文件已存在: " + filePath + ", e: " + e.getMessage());
                } catch (IOException | TemplateException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        info("all done");
    }

    private Map<String, TemplateModel> getModelsFromDb() {
        if (db == null) {
            throw new RuntimeException("db信息没有注册，参考，需使用.db方法注册");
        }
        try {
            Class.forName(db.driver());
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        final Map<String, TemplateModel> tableNameMapTemplateModel = new HashMap<>();
        try (final Connection connection = DriverManager.getConnection(db.url(), db.username(), db.password())) {
            if (tables.contains("*")) {
                readTableAsModel(connection, null, tableNameMapTemplateModel);
            } else {
                for (String table : tables) {
                    readTableAsModel(connection, table, tableNameMapTemplateModel);
                }
            }

            for (String excludeTable : excludeTables) {
                tableNameMapTemplateModel.remove(excludeTable);
            }

            for (Predicate<String> excludeTableFilter : excludeTableFilters) {
                tableNameMapTemplateModel.entrySet().removeIf(it -> excludeTableFilter.test(it.getKey()));
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return tableNameMapTemplateModel;
    }

    private void readTableAsModel(Connection connection, @Nullable String tableNamePattern, Map<String, TemplateModel> tableNameMapTemplateModel) throws SQLException {
        final DatabaseMetaData metaData = connection.getMetaData();
        final String catalog = connection.getCatalog();
        final String schema = connection.getSchema();

        // metadata https://dev.mysql.com/doc/refman/8.0/en/show-columns.html
        final ResultSet tableSet = metaData.getTables(catalog, schema, tableNamePattern, new String[]{"TABLE", "VIEW"});

        while (tableSet.next()) {
            final String tableName = tableSet.getString("TABLE_NAME");
            final String tableRemark = tableSet.getString("REMARKS");

            final List<TemplateModel.EnumInfo> enums = new ArrayList<>();
            final List<TemplateModel.Column> modelColumns = new ArrayList<>();

            /**
             * 获取主键
             */
            TemplateModel.ModelId id = null;
            boolean idDefined = false;
            final ResultSet primaryKeys = metaData.getPrimaryKeys(catalog, schema, tableName);
            while (primaryKeys.next()) {
                if (idDefined) {
                    id = null;
                    warn("[WARN] 目前仍不支持复合主键");
                } else {
                    final String columnName = primaryKeys.getString("COLUMN_NAME");
                    id = new TemplateModel.ModelId(columnName);
                    idDefined = true;
                }
            }

            final ResultSet columnSet = metaData.getColumns(catalog, schema, tableName, null);
            while (columnSet.next()) {
                final String columnName = columnSet.getString("COLUMN_NAME");
                if (columnName == null) {
                    throw new RuntimeException("找不到列名");
                }
                final String typeName = columnSet.getString("TYPE_NAME");
                final String remarks = columnSet.getString("REMARKS");

                KtJavaType ktJavaType;
                if ("ENUM".equals(typeName)) {
                    ktJavaType = new KtJavaType("String");
                } else {
                    ktJavaType = dbMetaTypeMapKtJavaType.get(typeName);
                    if (ktJavaType == null) {
                        ktJavaType = dbMetaTypeMapKtJavaType.get(typeName.replace(" UNSIGNED", ""));
                    }
                    if (ktJavaType == null) {
                        throw new RuntimeException("找不到数据库类型" + typeName + "对应的kt或java类型, 列名" + columnName);
                    }
                }

                if (id != null && columnName.equals(id.getColumn())) {
                    id.setAutoIncrement("YES".equals(columnSet.getString("IS_AUTOINCREMENT")));
                }

                final Set<String> isPrimaryMap = new HashSet<String>() {{
                    add("Boolean");
                    add("Char");
                    add("Byte");
                    add("Short");
                    add("Int");
                    add("Float");
                    add("Long");
                    add("Double");
                }};

                final String ktTypeStr = ktJavaType.getKtType();
                final boolean primary = isPrimaryMap.contains(ktTypeStr);
                final String remark = remarks.replace("*/", "").replace("/*", "");
                modelColumns.add(new TemplateModel.Column(columnName, ktTypeStr, ktJavaType.getJavaType(), primary, remark));
            }

            final String idColumnStr = id == null ? null : id.getColumn();
            if (id != null) {
                final TemplateModel.Column idColumn = modelColumns.stream().filter(it -> idColumnStr.equals(it.getDb_name())).findAny().orElse(null);
                if (idColumn != null) {
                    id.setKtTypeStr(idColumn.getKtTypeStr());
                    id.setJavaTypeStr(idColumn.getJavaTypeStr());
                }
            }
            final TemplateModel templateModel = new TemplateModel();
            templateModel.setDb_name(tableName);
            templateModel.setRemark(tableRemark);
            templateModel.setColumns(modelColumns);
            templateModel.setId(id);
            templateModel.setHasBigDecimal(modelColumns.stream().anyMatch(it -> "BigDecimal".equals(it.getJavaTypeStr())));
            templateModel.setHasDate(modelColumns.stream().anyMatch(it -> "Date".equals(it.getJavaTypeStr())));
            templateModel.setEntityExCodes(entityExCodes);
            templateModel.setEnums(enums);
            tableNameMapTemplateModel.put(tableName, templateModel);
        }
    }

    private final Pattern QueryPositionPattern = Pattern.compile("static QueryPro<([\\s\\S]+)>\\s+createQuery\\(\\)\\s+\\{");


    @SuppressWarnings("rawtypes")
    private List<TemplateModel.DelegateInfo> delegateQueryPro(String templateName, Supplier<InputStream> templateLoader) {
        if (!templateName.startsWith("DaoJava")) {
            return new ArrayList<>();
        }

        final boolean java = true;

        final String daoJavaTemplate = IOPro.fromInputStream(templateLoader.get()).transferToString();

        final Matcher matcher = QueryPositionPattern.matcher(daoJavaTemplate);
        if (!matcher.find()) {
            throw new IllegalTemplate("找不到queryPro定义的位置");
        }
        final String actualGenericTypeStr = matcher.group(1);

        final List<String> actualGenericTypes = Arrays.stream(actualGenericTypeStr.split(",\n")).filter(it -> !it.trim().isEmpty()).collect(Collectors.toList());
        final TypeVariable<Class<QueryPro>>[] genericTypes = QueryPro.class.getTypeParameters();

        if (genericTypes.length != actualGenericTypes.size()) {
            throw new IllegalTemplate("模板中QueryPro的参数长度与QueryPro的参数长度不一致");
        }
        final Map<String, String> genericTypeMapActualGenericType = new HashMap<>();
        for (int i = 0; i < genericTypes.length; i++) {
            final TypeVariable<Class<QueryPro>> genericType = genericTypes[i];
            genericTypeMapActualGenericType.put(genericType.getName(), actualGenericTypes.get(i).trim());
        }

        final Function<String, String> toActualType = (genericType) -> {
            final String actualType = genericTypeMapActualGenericType.get(genericType);
            if (actualType != null) {
                return actualType;
            }
            if (genericType.endsWith(">") || genericType.endsWith(">[]")) {
                final boolean areArr = genericType.endsWith("]");
                final int indexOfLt = genericType.indexOf('<');
                final String paramsStr = genericType.substring(indexOfLt + 1, areArr ? genericType.length() - 3 : genericType.length() - 1);

                final StringJoiner sj = new StringJoiner(", ");
                for (String param : paramsStr.split(",")) {
                    final String formatted = param.replace("? extends ", "").trim();
                    final String actual = genericTypeMapActualGenericType.get(formatted);
                    if (actual != null) {
                        sj.add(actual);
                    } else {
                        sj.add(param.trim());
                    }
                }

                return genericType.substring(0, indexOfLt + 1) + sj + ">" + (areArr ? "[]" : "");
            }
            if (genericType.endsWith("[]")) {
                final String t = genericTypeMapActualGenericType.get(genericType.substring(0, genericType.length() - 2));
                return t + "[]";
            }
            return null;
        };

        final Function<String, String> noPackage = (className) -> className.replace("java.lang.", "");

        final Method[] declaredMethods = QueryPro.class.getDeclaredMethods();

        return Arrays.stream(declaredMethods)
                .filter(it -> {
                    if (!Modifier.isPublic(it.getModifiers())) {
                        return false;
                    }
                    if (it.isSynthetic()) {
                        return false;
                    }
                    return !it.isAnnotationPresent(JvHidden.class);
                })
                .map(method -> {
                    final Parameter[] parameters = method.getParameters();

                    final SafeVarargs safeVarargs = method.getAnnotation(SafeVarargs.class);
                    final PureContract contract = method.getAnnotation(PureContract.class);

                    final boolean pure = contract != null;

                    final List<String> annotations = new ArrayList<>();
                    if (pure) annotations.add("@Contract(pure = true)");
                    if (safeVarargs != null) annotations.add("@SafeVarargs");

                    final String withPackageReturnType = Optional.ofNullable(toActualType.apply(method.getGenericReturnType().getTypeName()))
                            .orElseGet(() -> method.getReturnType().getTypeName());
                    final String returnType = noPackage.apply(withPackageReturnType);

                    AtomicInteger i = new AtomicInteger();
                    final List<TemplateModel.DelegateInfo.Arg> args = Arrays.stream(parameters).map(param -> {
                        final String paramType = Optional.ofNullable(toActualType.apply(param.getParameterizedType().getTypeName()))
                                .orElseGet(() -> param.getType().getTypeName().replace('$', '.'));
                        final boolean isVararg = param.isVarArgs();
                        final String finalParamType;
                        if (isVararg) {
                            if (!paramType.endsWith("[]")) {
                                throw new IllegalTemplate("vararg参数类型不可以不是数组");
                            }
                            finalParamType = noPackage.apply(paramType.substring(0, paramType.length() - 2));
                        } else {
                            finalParamType = noPackage.apply(paramType);
                        }
                        final String paramName = Optional.ofNullable(param.getName()).orElseGet(() -> "obj" + i.getAndIncrement());
                        return new TemplateModel.DelegateInfo.Arg(finalParamType, isVararg, null, paramName);
                    }).collect(Collectors.toList());

                    return new TemplateModel.DelegateInfo(
                        "public static",
                        returnType,
                        method.getName(),
                        args,
                        annotations
                    );
                })
                .sorted(Comparator.comparing(d -> d.getMethod() + d.getArgs()))
                .collect(Collectors.toList());
    }

    private void info(Object obj) {
        if (this.useLogger) {
            logger.info(obj);
        } else {
            System.out.println(obj);
        }
    }

    private void warn(Object obj) {
        if (this.useLogger) {
            logger.warn(obj);
        } else {
            System.out.println("[warn]" + obj.toString());
        }
    }

    @SuppressWarnings("UnusedReturnValue")
    private <T> T debugPrint(T t) {
        if (debug) {
            if (t instanceof Iterable) {
                ((Iterable<?>) t).forEach(this::info);
            } else {
                info(t);
            }
        }
        return t;
    }
}
