package cn.cloudself.query.generator;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TemplateModel {
    private String db_name;
    @Nullable
    private String remark;
    @Nullable
    private ModelId id;
    private boolean hasBigDecimal = false;
    private boolean hasDate = false;
    private boolean chainForModel = false;
    private String entityPackage = null;
    private String _EntityName = null;
    private String _ClassName = null;
    private String packagePath = null;
    private Boolean noArgMode = null;
    private Boolean swaggerSupport = false;
    private String daoExCodes = null;
    private String entityExCodes = null;
    private List<Column> columns = new ArrayList<>();
    private List<DelegateInfo> queryProDelegate = new ArrayList<>();
    private List<EnumInfo> enums = new ArrayList<>();

    public String getDb_name() {
        return db_name;
    }

    public void setDb_name(String db_name) {
        this.db_name = db_name;
    }

    @Nullable
    public String getRemark() {
        return remark;
    }

    public void setRemark(@Nullable String remark) {
        this.remark = remark;
    }

    @Nullable
    public ModelId getId() {
        return id;
    }

    public void setId(@Nullable ModelId id) {
        this.id = id;
    }

    public boolean isHasBigDecimal() {
        return hasBigDecimal;
    }

    public void setHasBigDecimal(boolean hasBigDecimal) {
        this.hasBigDecimal = hasBigDecimal;
    }

    public boolean isHasDate() {
        return hasDate;
    }

    public void setHasDate(boolean hasDate) {
        this.hasDate = hasDate;
    }

    public boolean isChainForModel() {
        return chainForModel;
    }

    public void setChainForModel(boolean chainForModel) {
        this.chainForModel = chainForModel;
    }

    public String getEntityPackage() {
        return entityPackage;
    }

    public void setEntityPackage(String entityPackage) {
        this.entityPackage = entityPackage;
    }

    public String get_EntityName() {
        return _EntityName;
    }

    public void set_EntityName(String _EntityName) {
        this._EntityName = _EntityName;
    }

    public String get_ClassName() {
        return _ClassName;
    }

    public void set_ClassName(String _ClassName) {
        this._ClassName = _ClassName;
    }

    public String getPackagePath() {
        return packagePath;
    }

    public void setPackagePath(String packagePath) {
        this.packagePath = packagePath;
    }

    public Boolean getNoArgMode() {
        return noArgMode;
    }

    public void setNoArgMode(Boolean noArgMode) {
        this.noArgMode = noArgMode;
    }

    public Boolean getSwaggerSupport() {
        return swaggerSupport;
    }

    public void setSwaggerSupport(Boolean swaggerSupport) {
        this.swaggerSupport = swaggerSupport;
    }

    public String getDaoExCodes() {
        return daoExCodes;
    }

    public void setDaoExCodes(String daoExCodes) {
        this.daoExCodes = daoExCodes;
    }

    public String getEntityExCodes() {
        return entityExCodes;
    }

    public void setEntityExCodes(String entityExCodes) {
        this.entityExCodes = entityExCodes;
    }

    public List<Column> getColumns() {
        return columns;
    }

    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    public List<DelegateInfo> getQueryProDelegate() {
        return queryProDelegate;
    }

    public void setQueryProDelegate(List<DelegateInfo> queryProDelegate) {
        this.queryProDelegate = queryProDelegate;
    }

    public List<EnumInfo> getEnums() {
        return enums;
    }

    public void setEnums(List<EnumInfo> enums) {
        this.enums = enums;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TemplateModel that = (TemplateModel) o;
        return hasBigDecimal == that.hasBigDecimal && hasDate == that.hasDate && chainForModel == that.chainForModel && Objects.equals(db_name, that.db_name) && Objects.equals(remark, that.remark) && Objects.equals(id, that.id) && Objects.equals(entityPackage, that.entityPackage) && Objects.equals(_EntityName, that._EntityName) && Objects.equals(_ClassName, that._ClassName) && Objects.equals(packagePath, that.packagePath) && Objects.equals(noArgMode, that.noArgMode) && Objects.equals(swaggerSupport, that.swaggerSupport) && Objects.equals(daoExCodes, that.daoExCodes) && Objects.equals(entityExCodes, that.entityExCodes) && Objects.equals(columns, that.columns) && Objects.equals(queryProDelegate, that.queryProDelegate) && Objects.equals(enums, that.enums);
    }

    @Override
    public int hashCode() {
        return Objects.hash(db_name, remark, id, hasBigDecimal, hasDate, chainForModel, entityPackage, _EntityName, _ClassName, packagePath, noArgMode, swaggerSupport, daoExCodes, entityExCodes, columns, queryProDelegate, enums);
    }

    @Override
    public String toString() {
        return "TemplateModel{" +
                "db_name='" + db_name + '\'' +
                ", remark='" + remark + '\'' +
                ", id=" + id +
                ", hasBigDecimal=" + hasBigDecimal +
                ", hasDate=" + hasDate +
                ", chainForModel=" + chainForModel +
                ", entityPackage='" + entityPackage + '\'' +
                ", _EntityName='" + _EntityName + '\'' +
                ", _ClassName='" + _ClassName + '\'' +
                ", packagePath='" + packagePath + '\'' +
                ", noArgMode=" + noArgMode +
                ", swaggerSupport=" + swaggerSupport +
                ", daoExCodes='" + daoExCodes + '\'' +
                ", entityExCodes='" + entityExCodes + '\'' +
                ", columns=" + columns +
                ", queryProDelegate=" + queryProDelegate +
                ", enums=" + enums +
                '}';
    }

    public static class ModelId {
        private String column;
        private String ktTypeStr = null;
        private String javaTypeStr = null;
        private boolean autoIncrement = false;

        public ModelId(String column) {
            this.column = column;
        }

        public String getColumn() {
            return column;
        }

        public void setColumn(String column) {
            this.column = column;
        }

        public String getKtTypeStr() {
            return ktTypeStr;
        }

        public void setKtTypeStr(String ktTypeStr) {
            this.ktTypeStr = ktTypeStr;
        }

        public String getJavaTypeStr() {
            return javaTypeStr;
        }

        public void setJavaTypeStr(String javaTypeStr) {
            this.javaTypeStr = javaTypeStr;
        }

        public boolean isAutoIncrement() {
            return autoIncrement;
        }

        public void setAutoIncrement(boolean autoIncrement) {
            this.autoIncrement = autoIncrement;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ModelId modelId = (ModelId) o;
            return autoIncrement == modelId.autoIncrement && Objects.equals(column, modelId.column) && Objects.equals(ktTypeStr, modelId.ktTypeStr) && Objects.equals(javaTypeStr, modelId.javaTypeStr);
        }

        @Override
        public int hashCode() {
            return Objects.hash(column, ktTypeStr, javaTypeStr, autoIncrement);
        }

        @Override
        public String toString() {
            return "ModelId{" +
                    "column='" + column + '\'' +
                    ", ktTypeStr='" + ktTypeStr + '\'' +
                    ", javaTypeStr='" + javaTypeStr + '\'' +
                    ", autoIncrement=" + autoIncrement +
                    '}';
        }
    }

    public static class Column {
        private String db_name;
        private String ktTypeStr;
        private String javaTypeStr;
        private boolean primary;
        private List<String> annotations = new ArrayList<>();
        private String remark = null;
        private String propertyName = null;

        public Column(String db_name, String ktTypeStr, String javaTypeStr, boolean primary, String remark) {
            this.db_name = db_name;
            this.ktTypeStr = ktTypeStr;
            this.javaTypeStr = javaTypeStr;
            this.primary = primary;
            this.remark = remark;
        }

        public String getDb_name() {
            return db_name;
        }

        public void setDb_name(String db_name) {
            this.db_name = db_name;
        }

        public String getKtTypeStr() {
            return ktTypeStr;
        }

        public void setKtTypeStr(String ktTypeStr) {
            this.ktTypeStr = ktTypeStr;
        }

        public String getJavaTypeStr() {
            return javaTypeStr;
        }

        public void setJavaTypeStr(String javaTypeStr) {
            this.javaTypeStr = javaTypeStr;
        }

        public boolean isPrimary() {
            return primary;
        }

        public void setPrimary(boolean primary) {
            this.primary = primary;
        }

        public List<String> getAnnotations() {
            return annotations;
        }

        public void setAnnotations(List<String> annotations) {
            this.annotations = annotations;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getPropertyName() {
            return propertyName;
        }

        public void setPropertyName(String propertyName) {
            this.propertyName = propertyName;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Column that = (Column) o;
            return primary == that.primary && Objects.equals(db_name, that.db_name) && Objects.equals(ktTypeStr, that.ktTypeStr) && Objects.equals(javaTypeStr, that.javaTypeStr) && Objects.equals(annotations, that.annotations) && Objects.equals(remark, that.remark) && Objects.equals(propertyName, that.propertyName);
        }

        @Override
        public int hashCode() {
            return Objects.hash(db_name, ktTypeStr, javaTypeStr, primary, annotations, remark, propertyName);
        }

        @Override
        public String toString() {
            return "TemplateModelColumn{" +
                    "db_name='" + db_name + '\'' +
                    ", ktTypeStr='" + ktTypeStr + '\'' +
                    ", javaTypeStr='" + javaTypeStr + '\'' +
                    ", primary=" + primary +
                    ", annotations=" + annotations +
                    ", remark='" + remark + '\'' +
                    ", propertyName='" + propertyName + '\'' +
                    '}';
        }
    }

    public static class DelegateInfo {
        private String modifiers;
        private String returnType;
        private String method;
        private List<Arg> args;
        private List<String> annotations = new ArrayList<>();

        public DelegateInfo(String modifiers, String returnType, String method, List<Arg> args, List<String> annotations) {
            this.modifiers = modifiers;
            this.returnType = returnType;
            this.method = method;
            this.args = args;
            this.annotations = annotations;
        }

        public String getModifiers() {
            return modifiers;
        }

        public void setModifiers(String modifiers) {
            this.modifiers = modifiers;
        }

        public String getReturnType() {
            return returnType;
        }

        public void setReturnType(String returnType) {
            this.returnType = returnType;
        }

        public String getMethod() {
            return method;
        }

        public void setMethod(String method) {
            this.method = method;
        }

        public List<Arg> getArgs() {
            return args;
        }

        public void setArgs(List<Arg> args) {
            this.args = args;
        }

        public List<String> getAnnotations() {
            return annotations;
        }

        public void setAnnotations(List<String> annotations) {
            this.annotations = annotations;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DelegateInfo that = (DelegateInfo) o;
            return Objects.equals(modifiers, that.modifiers) && Objects.equals(returnType, that.returnType) && Objects.equals(method, that.method) && Objects.equals(args, that.args) && Objects.equals(annotations, that.annotations);
        }

        @Override
        public int hashCode() {
            return Objects.hash(modifiers, returnType, method, args, annotations);
        }

        @Override
        public String toString() {
            return "DelegateInfo{" +
                    "modifiers='" + modifiers + '\'' +
                    ", returnType='" + returnType + '\'' +
                    ", method='" + method + '\'' +
                    ", args=" + args +
                    ", annotations=" + annotations +
                    '}';
        }

        public static class Arg {
            private String variableType;
            private boolean vararg;
            private String defaultValue;
            private String variableName;

            public Arg(String variableType, boolean vararg, String defaultValue, String variableName) {
                this.variableType = variableType;
                this.vararg = vararg;
                this.defaultValue = defaultValue;
                this.variableName = variableName;
            }

            public String getVariableType() {
                return variableType;
            }

            public void setVariableType(String variableType) {
                this.variableType = variableType;
            }

            public boolean isVararg() {
                return vararg;
            }

            public void setVararg(boolean vararg) {
                this.vararg = vararg;
            }

            public String getDefaultValue() {
                return defaultValue;
            }

            public void setDefaultValue(String defaultValue) {
                this.defaultValue = defaultValue;
            }

            public String getVariableName() {
                return variableName;
            }

            public void setVariableName(String variableName) {
                this.variableName = variableName;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Arg that = (Arg) o;
                return vararg == that.vararg && Objects.equals(variableType, that.variableType) && Objects.equals(defaultValue, that.defaultValue) && Objects.equals(variableName, that.variableName);
            }

            @Override
            public int hashCode() {
                return Objects.hash(variableType, vararg, defaultValue, variableName);
            }

            @Override
            public String toString() {
                return "DelegateInfoArg{" +
                        "variableType='" + variableType + '\'' +
                        ", vararg=" + vararg +
                        ", defaultValue='" + defaultValue + '\'' +
                        ", variableName='" + variableName + '\'' +
                        '}';
            }
        }
    }

    public class EnumInfo {
        private String name;
        private List<String> enums;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<String> getEnums() {
            return enums;
        }

        public void setEnums(List<String> enums) {
            this.enums = enums;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            EnumInfo enumInfo = (EnumInfo) o;
            return Objects.equals(name, enumInfo.name) && Objects.equals(enums, enumInfo.enums);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, enums);
        }

        @Override
        public String toString() {
            return "EnumInfo{" +
                    "name='" + name + '\'' +
                    ", enums=" + enums +
                    '}';
        }
    }
}