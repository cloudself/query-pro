package cn.cloudself.query.generator.misc;

import java.util.Objects;

public class KtJavaType {
    private String ktType;
    private String javaType;

    public KtJavaType(String ktType, String javaType) {
        this.ktType = ktType;
        this.javaType = javaType;
    }

    public KtJavaType(String type) {
        this(type, type);
    }

    public String getKtType() {
        return ktType;
    }

    public void setKtType(String ktType) {
        this.ktType = ktType;
    }

    public String getJavaType() {
        return javaType;
    }

    public void setJavaType(String javaType) {
        this.javaType = javaType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KtJavaType that = (KtJavaType) o;
        return Objects.equals(ktType, that.ktType) && Objects.equals(javaType, that.javaType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ktType, javaType);
    }

    @Override
    public String toString() {
        return "KtJavaType{" +
                "ktType='" + ktType + '\'' +
                ", javaType='" + javaType + '\'' +
                '}';
    }
}