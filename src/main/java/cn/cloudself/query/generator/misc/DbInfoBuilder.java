package cn.cloudself.query.generator.misc;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * 用于生成[DbInfo], 主要是生成其中的url属性
 */
public class DbInfoBuilder {
    public static DbInfoBuilder mysql(String host, String schema) {
        return new DbInfoBuilder("mysql", host, schema);
    }
    private String protocol;
    private String host;
    private String schema;
    private Integer port = null;
    private String driver = "com.mysql.cj.jdbc.Driver";
    private DbInfoBuilder(String protocol, String host, String schema) {
        this.protocol = protocol;
        this.host = host;
        this.schema = schema;
    }

    private Map<String, String> params = new HashMap<String, String>() {{
        put("useUnicode", "true");
        put("characterEncoding", "utf-8");
        put("serverTimezone", "Asia/Shanghai");
        put("useInformationSchema", "true");
        put("allowPublicKeyRetrieval", "true");
    }};

    public DbInfoBuilder protocol(String protocol) {
        this.protocol = protocol;
        return this;
    }

    public DbInfoBuilder host(String host) {
        this.host = host;
        return this;
    }

    public DbInfoBuilder schema(String schema) {
        this.schema = schema;
        return this;
    }

    /**
     * 设置端口
     */
    public DbInfoBuilder port(int port) {
        this.port = port;
        return this;
    }

    /**
     * 设置驱动
     * [com.mysql.jdbc.Driver]和 mysql-connector-java 5一起用。
     * [com.mysql.cj.jdbc.Driver]和 mysql-connector-java 6一起用。
     */
    public DbInfoBuilder driver(String driver) {
        this.driver = driver;
        return this;
    }

    /**
     * 设置连接参数
     * <br/>
     * 存在一些默认的参数 [params] 比如使用utf8, 时区+8, useInformationSchema
     */
    public DbInfoBuilder params(Function<Map<String, String>, Map<String, String>> converter) {
        this.params = converter.apply(this.params);
        return this;
    }

    public DbInfo toDbInfo(String username, String password) {
        final StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append("jdbc:").append(protocol).append("://").append(host);
        if (port != null) {
            urlBuilder.append(':').append(port);
        }
        urlBuilder.append('/').append(schema);
        boolean first = true;
        for (Map.Entry<String, String> entry : this.params.entrySet()) {
            if (first) {
                urlBuilder.append('?');
                first = false;
            } else {
                urlBuilder.append('&');
            }
            urlBuilder.append(entry.getKey()).append('=').append(entry.getValue());
        }
        return new DbInfo(urlBuilder.toString(), username, password, driver);
    }
}