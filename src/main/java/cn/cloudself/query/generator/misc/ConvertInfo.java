package cn.cloudself.query.generator.misc;

import java.util.Objects;

/**
 * The type Convert info.
 */
public class ConvertInfo {
    private String name;
    private JavaNameType toType;
    private String templateName;

    /**
     * Instantiates a new Convert info.
     *
     * @param name         the name
     * @param toType       the to type
     * @param templateName the template name
     */
    public ConvertInfo(String name, JavaNameType toType, String templateName) {
        this.name = name;
        this.toType = toType;
        this.templateName = templateName;
    }

    /**
     * Copy convert info.
     *
     * @return the convert info
     */
    public ConvertInfo copy() {
        return new ConvertInfo(name, toType, templateName);
    }

    /**
     * Name string.
     *
     * @return the string
     */
    public String name() {
        return name;
    }

    /**
     * Name convert info.
     *
     * @param name the name
     * @return the convert info
     */
    public ConvertInfo name(String name) {
        this.name = name;
        return this;
    }

    /**
     * To type java name type.
     *
     * @return the java name type
     */
    public JavaNameType toType() {
        return toType;
    }

    /**
     * To type convert info.
     *
     * @param toType the to type
     * @return the convert info
     */
    public ConvertInfo toType(JavaNameType toType) {
        this.toType = toType;
        return this;
    }

    /**
     * Template name string.
     *
     * @return the string
     */
    public String templateName() {
        return templateName;
    }

    /**
     * Template name.
     *
     * @param templateName the template name
     */
    public void templateName(String templateName) {
        this.templateName = templateName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConvertInfo that = (ConvertInfo) o;
        return Objects.equals(name, that.name) && toType == that.toType && Objects.equals(templateName, that.templateName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, toType, templateName);
    }

    @Override
    public String toString() {
        return "ConvertInfo{" +
                "name='" + name + '\'' +
                ", toType=" + toType +
                ", templateName='" + templateName + '\'' +
                '}';
    }
}
