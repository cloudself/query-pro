package cn.cloudself.query.generator.misc;

import java.util.Objects;

public class DbInfo {
    private String url;
    private String username;
    private String password;
    private String driver;

    public DbInfo(String url, String username, String password, String driver) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.driver = driver;
    }

    public String url() {
        return url;
    }

    public void url(String url) {
        this.url = url;
    }

    public String username() {
        return username;
    }

    public void username(String username) {
        this.username = username;
    }

    public String password() {
        return password;
    }

    public void password(String password) {
        this.password = password;
    }

    public String driver() {
        return driver;
    }

    public void driver(String driver) {
        this.driver = driver;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DbInfo dbInfo = (DbInfo) o;
        return Objects.equals(url, dbInfo.url) && Objects.equals(username, dbInfo.username) && Objects.equals(password, dbInfo.password) && Objects.equals(driver, dbInfo.driver);
    }

    @Override
    public int hashCode() {
        return Objects.hash(url, username, password, driver);
    }

    @Override
    public String toString() {
        return "DbInfo{" +
                "url='" + url + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", driver='" + driver + '\'' +
                '}';
    }
}