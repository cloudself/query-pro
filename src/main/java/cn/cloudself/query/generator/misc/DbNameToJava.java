package cn.cloudself.query.generator.misc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 数据库名称转为Java名称(类名,属性名等)
 */
public class DbNameToJava {
    /**
     * 该默认方法会把
     * 表名 user_info 转换为 UserInfo
     * 列名 create_by 转换为 createBy
     * @see cn.cloudself.query.generator.QueryProFileMaker#dbJavaNameConverter
     */
    public static DbNameToJava createDefault() {
        return new DbNameToJava();
    }
    private DbNameToJava() {
    }

    private final List<NameConverter> preHandles = new ArrayList<>();
    private final List<NameConverter> postHandles = new ArrayList<>();

    private final NameConverter preHandle = (convertInfo) -> {
        String name = convertInfo.name();
        for (NameConverter preHandle : preHandles) {
            name = preHandle.convert(convertInfo.copy().name(name));
        }
        return name;
    };

    private final NameConverter postHandle = (convertInfo) -> {
        String tName = convertInfo.name();
        for (NameConverter postHandle : postHandles) {
            tName = postHandle.convert(convertInfo.copy().name(tName));
        }
        return tName;
    };

    public DbNameToJava addPrefixToClassNameBeforeConvert(String prefix) {
        preHandles.add(it -> it.toType() == JavaNameType.ClassName ? prefix + it.name() : it.name());
        return this;
    }

    public DbNameToJava removePrefixToClassNameBeforeConvert() {
        preHandles.add(it ->it.toType() == JavaNameType.ClassName ? it.name().replaceFirst("[^_]+_", "") : it.name());
        return this;
    }

    public DbNameToJava addSuffixToEntity(String suffix) {
        postHandles.add(it -> {
            if (it.templateName().startsWith("Entity") && it.toType() == JavaNameType.ClassName)
                return it.name() + suffix;
            else
                return it.name();
        });
        return this;
    }

    public DbNameToJava addPreHandle(NameConverter preHandle) {
        preHandles.add(preHandle);
        return this;
    }

    public DbNameToJava addPostHandle(NameConverter postHandle) {
        postHandles.add(postHandle);
        return this;
    }

    public NameConverter getConverter() {
        return convertInfo -> {
            String result = Arrays.stream(preHandle.convert(convertInfo).split("_"))
                    .map(s -> s.isEmpty() ? "" : Character.toUpperCase(s.charAt(0)) + s.substring(1))
                    .collect(Collectors.joining(""))
                    .replaceAll("[\\x00-\\x2F\\x3A-\\x40\\x5B-\\x60\\x7B-\\x7F]", "_");
            if (convertInfo.toType() == JavaNameType.propertyName) {
                result = Character.toLowerCase(result.charAt(0)) + result.substring(1);
            } else {
                if (!convertInfo.templateName().startsWith("Entity")) {
                    result = result + "QueryPro";
                }
            }
            return postHandle.convert(convertInfo.copy().name(result));
        };
    }
}