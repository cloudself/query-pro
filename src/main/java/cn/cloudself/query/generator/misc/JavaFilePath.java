package cn.cloudself.query.generator.misc;

import java.nio.file.Path;
import java.util.Objects;

public class JavaFilePath {
    private String templateName;
    private Path dir;
    private String packagePath;

    public JavaFilePath(String templateName, Path dir, String packagePath) {
        this.templateName = templateName;
        this.dir = dir;
        this.packagePath = packagePath;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public Path getDir() {
        return dir;
    }

    public void setDir(Path dir) {
        this.dir = dir;
    }

    public String getPackagePath() {
        return packagePath;
    }

    public void setPackagePath(String packagePath) {
        this.packagePath = packagePath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JavaFilePath that = (JavaFilePath) o;
        return Objects.equals(templateName, that.templateName) && Objects.equals(dir, that.dir) && Objects.equals(packagePath, that.packagePath);
    }

    @Override
    public int hashCode() {
        return Objects.hash(templateName, dir, packagePath);
    }

    @Override
    public String toString() {
        return "JavaFilePath{" +
                "templateName='" + templateName + '\'' +
                ", dir=" + dir +
                ", packagePath='" + packagePath + '\'' +
                '}';
    }
}