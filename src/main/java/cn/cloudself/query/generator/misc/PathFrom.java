package cn.cloudself.query.generator.misc;

import java.io.File;
import java.nio.file.Paths;

/**
 * 用于生成 [FilePathResolver]
 */
public class PathFrom {
    private PathFrom() {
    }
    /**
     * 使用builder模式创建一个PathFrom, 另外还有两个快捷方法 [PathFrom.ktPackageName], [PathFrom.javaPackageName]
     */
    public static PathFrom create() {
        return new PathFrom();
    }

    /**
     * 指示生成的文件应该放在哪个包下面
     * <p></p>注意该方法会自动在包后面加上 dao.zz 或 entity， 如需使用绝对包名，可以加上 [abs]，详见第二个示例
     */
    public static FilePathResolver ktPackage(String packageName) {
        return create().ktPackageName(packageName).getResolver();
    }

    /**
     * 指示生成的文件应该放在哪个包下面
     */
    public static FilePathResolver javaPackage(String packageName) {
        return create().javaPackageName(packageName).getResolver();
    }

    private String subModuleName = "";
    private String lang = "kotlin";
    private String mainDir = "main";
    private String packageName = "cn.cloudself";
    private String entityPackage = "entity";
    private String daoPackage = "dao.zz";
    private String absPath = null;


    public PathFrom dirTest() {
        return dirTest("test");
    }

    public PathFrom dirTest(String dir) {
        this.mainDir = dir;
        return this;
    }

    /**
     * 如果项目存在子模块, 使用这个设置项目的子模块
     */
    public PathFrom subModule(String subModuleName) {
        this.subModuleName = subModuleName;
        return this;
    }

    public PathFrom absSrc(String path) {
        this.absPath = path;
        return this;
    }

    /**
     * 指示生成的文件应该放在packageName指定的包下面,
     * 存在以下简写 [PathFrom.ktPackageName]
     */
    public PathFrom ktPackageName(String packageName) {
        lang = "kotlin";
        this.packageName = packageName;
        return this;
    }

    /**
     * 指示生成的文件应该放在packageName指定的包下面,
     * 存在以下简写 [PathFrom.javaPackageName]
     */
    public PathFrom javaPackageName(String packageName) {
        lang = "java";
        this.packageName = packageName;
        return this;
    }

    /**
     * 设置生成的entity文件放在哪个包下 默认: entity。
     * 如果是singleFileMode(单文件模式) 该配置不会起任何作用
     */
    public PathFrom entityPackage(String entityPackage) {
        this.entityPackage = entityPackage;
        return this;
    }

    /**
     * 设置生成的dao文件放在哪个包下 默认: dao.zz
     */
    public PathFrom daoPackage(String daoPackage) {
        this.daoPackage = daoPackage;
        return this;
    }

    public FilePathResolver getResolver() {
        return templateName -> {
            final String workspace = System.getProperty("user.dir");

            final String entityOrDao = templateName.startsWith("Entity") ? entityPackage : daoPackage;
            final StringBuilder packagePathBuilder = new StringBuilder();
            packagePathBuilder.append(packageName);
            if (entityOrDao != null && !entityOrDao.isEmpty()) {
                packagePathBuilder.append('.');
                packagePathBuilder.append(entityOrDao);
            }
            final String packagePath = packagePathBuilder.toString();
            return new JavaFilePath(
                templateName,
                Paths.get(workspace, subModuleName, "src", mainDir, lang, packageNameToPath(packagePath)),
                    packagePath
            );
        };
    }

    private String packageNameToPath(String packageName) {
        return packageName.replace(".", File.separator);
    }
}