package cn.cloudself.query.generator.misc;

/**
 * 文件位置解析器，即指示生成的文件应该放在哪里。<br/>
 * 函数参数templateName: 模板文件的名称 DaoKt, EntityKt, DaoJava, EntityJava, SingleFileKt, SingleFileJava等。<br/>
 * 需要返回的是：生成的文件应该放在哪个文件夹里面。
 */
public interface FilePathResolver {
    JavaFilePath resolve(String templateName);
}
