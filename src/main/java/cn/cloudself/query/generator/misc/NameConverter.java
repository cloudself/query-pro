package cn.cloudself.query.generator.misc;

/**
 * 将数据库名称转为Java名称
 * 一般数据库名称是下_划_线式的, Java类名是驼峰式的, 建议转换
 * 函数参数: dbName 数据库中的名称, type 表或列
 * 需要返回的是
 */
public interface NameConverter {
    String convert(ConvertInfo convertInfo);
}
