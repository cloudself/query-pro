package cn.cloudself.query.config.supports;

import org.jetbrains.annotations.Nullable;

import javax.sql.DataSource;

/**
 * 根据不同的QueryPro文件名称，使用不同的默认数据源
 * <br/>
 * Spring环境下只需继承该接口，并实现[getDefault]方法。
 * 其它环境暂不支持
 * <br/>
 * 例:
 * ```
 * QueryProFileMaker
 *          .javaEntityAndDaoMode(PathFrom.create().javaPackageName("cn.cloudself.start").getResolver())
 *          .db(DbInfoBuilder.mysql("127.0.0.1", "zz_start").toDbInfo("root", Password.password1))
 *          .dbJavaNameConverter(DbNameToJava.createDefault().addPrefixToClassNameBeforeConvert("Zs").getConverter())
 *          .create();
 * <br/>
 * QueryProFileMaker
 *          .javaEntityAndDaoMode(PathFrom.create().javaPackageName("cn.cloudself.start").getResolver())
 *          .db(DbInfoBuilder.mysql("127.0.0.1", "zz_biz").toDbInfo("root", Password.password1))
 *          .dbJavaNameConverter(DbNameToJava.createDefault().addPrefixToClassNameBeforeConvert("Zb").getConverter())
 *          .create();
 * <br/>
 * public class QueryProConfigure implements DefaultDataSource {
 *     public DataSource getDefault(Class&lt;?> clazz) {
 *         if (clazz == null) {
 *             return null;
 *         }
 *         final String simpleName = clazz.getSimpleName();
 *         if (simpleName.startsWith("Zs")) {
 *             return DataSources.START;
 *         }
 *         if (simpleName.startsWith("Zb")) {
 *             return DataSources.BIZ;
 *         }
 *         return null;
 *     }
 * }
 * ```
 */
public interface DefaultDataSource {
    /**
     * @param clazz XxxQueryPro.class Xxx一般为表名，用于区分默认的数据源，如果数据库表名不足以区分数据源，
     * 可以在QueryProFileMaker生成文件时，为每个数据源添加默认的前缀
     *
     * @return 为空代表QueryProConfig.global中配置的数据源（或者DataSource这个Spring Bean）
     */
    @Nullable DataSource getDefault(@Nullable Class<?> clazz);
}

