package cn.cloudself.query.config.supports;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class DefaultDataSourceWrapper {
    @Nullable
    private static DefaultDataSource defaultDataSource = null;

    @Nullable
    public static DataSource getDefault(@Nullable Class<?> clazz) {
        if (defaultDataSource == null) {
            return null;
        }
        return defaultDataSource.getDefault(clazz);
    }

    public DefaultDataSourceWrapper(@Nullable @Autowired(required = false) DefaultDataSource defaultDataSource) {
        DefaultDataSourceWrapper.defaultDataSource = defaultDataSource;
    }
}