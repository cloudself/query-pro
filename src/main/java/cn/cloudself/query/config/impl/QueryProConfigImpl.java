package cn.cloudself.query.config.impl;

import cn.cloudself.query.config.IQueryProConfig;
import cn.cloudself.query.config.store.Store;
import cn.cloudself.util.log.LogLevel;
import org.jetbrains.annotations.Nullable;

import javax.sql.DataSource;
import java.sql.Connection;

/**
 * 通用的`DB`实现(构造函数的参数中需传入一个`Store`)
 */
public class QueryProConfigImpl extends IQueryProConfig.Writeable.Default<QueryProConfigImpl> implements IQueryProConfig {
    public QueryProConfigImpl(Store store) {
        super(store);
    }

    @Override
    protected QueryProConfigImpl that() {
        return this;
    }

    @Override
    @Nullable
    public DatabaseType dbType() {
        return (DatabaseType) store.get("dbType");
    }

    @Override
    @Nullable
    public Integer maxParameterSize() {
        return (Integer) store.get("maxParameterSize");
    }

    /**
     * 配置connection，与dataSource二选一即可。
     * <br/>
     * 相比起[connection]更推荐使用[dataSource], 因为[dataSource]会自动管理链接的开启与关闭。
     * 而[connection]需要手动管理链接的开启与关闭(这在存在事务的时候需要额外注意)
     */
    @Nullable
    @Override
    public Connection connection() {
        return (Connection) store.get("connection");
    }

    @Nullable
    @Override
    public DataSource dataSource() {
        return (DataSource) store.get("dataSource");
    }

    @Nullable
    @Override
    public Boolean beautifySql() {
        return (Boolean) store.get("beautifySql");
    }

    @Nullable
    @Override
    public Boolean printLog() {
        return (Boolean) store.get("printLog");
    }

    @Nullable
    @Override
    public LogLevel printLogLevel() {
        return (LogLevel) store.get("printLogLevel");
    }

    @Nullable
    @Override
    public Boolean printLargeElementWholly() {
        return (Boolean) store.get("printLargeElementWholly");
    }

    @Nullable
    @Override
    public Boolean printCallByInfo() {
        return (Boolean) store.get("printCallByInfo");
    }

    @Nullable
    @Override
    public LogLevel printCallByInfoLevel() {
        return (LogLevel) store.get("printCallByInfoLevel");
    }

    @Nullable
    @Override
    public Boolean printResult() {
        return (Boolean) store.get("printResult");
    }

    @Nullable
    @Override
    public LogLevel printResultLevel() {
        return (LogLevel) store.get("printResultLevel");
    }

    @Nullable
    @Override
    public Boolean dryRun() {
        return (Boolean) store.get("dryRun");
    }

    @Nullable
    @Override
    public Boolean logicDelete() {
        return (Boolean) store.get("logicDelete");
    }

    @Nullable
    @Override
    public String logicDeleteField() {
        return (String) store.get("logicDeleteField");
    }

    @Nullable
    @Override
    public Object logicDeleteTrue() {
        return store.get("logicDeleteTrue");
    }

    @Nullable
    @Override
    public Object logicDeleteFalse() {
        return store.get("logicDeleteFalse");
    }
}
