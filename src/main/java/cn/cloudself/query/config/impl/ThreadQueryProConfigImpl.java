package cn.cloudself.query.config.impl;

import cn.cloudself.query.config.IQueryProConfig;
import cn.cloudself.util.log.LogLevel;
import org.jetbrains.annotations.Nullable;

import javax.sql.DataSource;
import java.sql.Connection;

/**
 * 继承自`QueryProConfigDb`, 添加了一些方法，需手动管理`ThreadLocal`中数据的初始化和销毁，也可以用`use`方法管理。
 */
public class ThreadQueryProConfigImpl implements IQueryProConfig, IQueryProConfig.Writeable<QueryProConfigImpl> {
    protected final TransmittableQueryProConfig configDb;
    public ThreadQueryProConfigImpl() {
        this(true);
    }
    public ThreadQueryProConfigImpl(boolean subThreadSupports) {
        this.configDb = new TransmittableQueryProConfig(subThreadSupports);
    }

    public void init() {
        configDb.init();
    }

    public void clean() {
        configDb.clean();
    }

    public interface Use {
        void call(TransmittableQueryProConfig context) throws Exception;
    }

    public interface UseWithReturn<T> {
        T call(TransmittableQueryProConfig context) throws Exception;
    }

    /**
     * 在回调函数中，维持一个query pro配置的上下文
     * <p><b>Examples:</b></p>
     * <pre>
     * {@code
     * QueryProConfig.context.use(context -> {
     *   context.beautifySql();
     *   UserQueryPro.selectBy().id().equalTo(1);
     * });
     * }</pre>
     */
    public void use(Use func) {
        use(it -> {
            func.call(it);
            return null;
        });
    }

    /**
     * 在回调函数中，维持一个query pro配置的上下文
     * <p><b>Examples:</b></p>
     * <pre>
     * {@code
     * return QueryProConfig.context.use(context -> {
     *   context.beautifySql();
     *   return UserQueryPro.selectBy().id().equalTo(1);
     * });
     * }</pre>
     */
    public <T> T use(UseWithReturn<T> func) {
        TransmittableQueryProConfig transmittableQueryProConfig = configDb.initAndCapture();
        T result;
        try {
            result = func.call(transmittableQueryProConfig);
        } catch (Exception e) {
            if (e instanceof RuntimeException) {
                throw (RuntimeException) e;
            } else {
                throw new RuntimeException(e);
            }
        } finally {
            configDb.clean();
        }
        return result;
    }

    @Nullable
    @Override
    public IQueryProConfig.DatabaseType dbType() {
        return configDb.dbType();
    }

    @Nullable
    @Override
    public Integer maxParameterSize() {
        return configDb.maxParameterSize();
    }

    @Nullable
    @Override
    public Connection connection() {
        return configDb.connection();
    }

    @Nullable
    @Override
    public DataSource dataSource() {
        return configDb.dataSource();
    }

    @Nullable
    @Override
    public Boolean beautifySql() {
        return configDb.beautifySql();
    }

    @Nullable
    @Override
    public Boolean printLog() {
        return configDb.printLog();
    }

    @Nullable
    @Override
    public LogLevel printLogLevel() {
        return configDb.printLogLevel();
    }

    @Nullable
    @Override
    public Boolean printLargeElementWholly() {
        return configDb.printLargeElementWholly();
    }

    @Nullable
    @Override
    public Boolean printCallByInfo() {
        return configDb.printCallByInfo();
    }

    @Nullable
    @Override
    public LogLevel printCallByInfoLevel() {
        return configDb.printCallByInfoLevel();
    }

    @Nullable
    @Override
    public Boolean printResult() {
        return configDb.printResult();
    }

    @Nullable
    @Override
    public LogLevel printResultLevel() {
        return configDb.printResultLevel();
    }

    @Nullable
    @Override
    public Boolean dryRun() {
        return configDb.dryRun();
    }

    @Nullable
    @Override
    public Boolean logicDelete() {
        return configDb.logicDelete();
    }

    @Nullable
    @Override
    public String logicDeleteField() {
        return configDb.logicDeleteField();
    }

    @Nullable
    @Override
    public Object logicDeleteTrue() {
        return configDb.logicDeleteTrue();
    }

    @Nullable
    @Override
    public Object logicDeleteFalse() {
        return configDb.logicDeleteFalse();
    }

    @Override
    public QueryProConfigImpl dbType(IQueryProConfig.DatabaseType dbType) {
        return configDb.dbType(dbType);
    }

    @Override
    public QueryProConfigImpl maxParameterSize(int size) {
        return configDb.maxParameterSize(size);
    }

    @Override
    public QueryProConfigImpl connection(Connection connection) {
        return configDb.connection(connection);
    }

    @Override
    public QueryProConfigImpl dataSource(DataSource dataSource) {
        return configDb.dataSource(dataSource);
    }

    @Override
    public QueryProConfigImpl beautifySql(boolean enable) {
        return configDb.beautifySql(enable);
    }

    @Override
    public QueryProConfigImpl printLog(boolean enable) {
        return configDb.printLog(enable);
    }

    @Override
    public QueryProConfigImpl printLog(boolean enable, LogLevel level) {
        return configDb.printLog(enable, level);
    }

    @Override
    public QueryProConfigImpl printLargeElementWholly(boolean print) {
        return configDb.printLargeElementWholly(print);
    }

    @Override
    public QueryProConfigImpl printCallByInfo(boolean enable) {
        return configDb.printCallByInfo(enable);
    }

    @Override
    public QueryProConfigImpl printCallByInfo(boolean enable, LogLevel level) {
        return configDb.printCallByInfo(enable, level);
    }

    @Override
    public QueryProConfigImpl printResult(boolean enable) {
        return configDb.printResult(enable);
    }

    @Override
    public QueryProConfigImpl printResult(boolean enable, LogLevel level) {
        return configDb.printResult(enable, level);
    }

    @Override
    public QueryProConfigImpl dryRun(boolean enable) {
        return configDb.dryRun(enable);
    }

    @Override
    public QueryProConfigImpl logicDelete(boolean enable) {
        return configDb.logicDelete(enable);
    }

    @Override
    public QueryProConfigImpl logicDelete(boolean enable, String field, @Nullable Object trueValue, @Nullable Object falseValue) {
        return configDb.logicDelete(enable, field, trueValue, falseValue);
    }
}
