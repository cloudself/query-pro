package cn.cloudself.query.config.impl;

import cn.cloudself.query.config.supports.DefaultDataSourceWrapper;
import cn.cloudself.query.config.Lifecycle;
import cn.cloudself.query.config.QueryProConfig;
import cn.cloudself.query.config.IOnlyGlobalConfig;
import cn.cloudself.query.config.IQueryProConfig;
import cn.cloudself.exception.IllegalCall;
import cn.cloudself.exception.IllegalImplements;
import cn.cloudself.query.resolver.DynamicActuator;
import cn.cloudself.query.resolver.QueryStructureResolver;
import cn.cloudself.query.resolver.ScriptResolver;
import cn.cloudself.util.log.LogLevel;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

/**
 * 继承自`QueryProConfigDb`, 添加了一些方法，需手动管理`ThreadLocal`中数据的初始化和销毁，也可以用`use`方法管理。
 */
public class FinalQueryProConfigImpl implements IQueryProConfig, IOnlyGlobalConfig {
    private final IQueryProConfig[] configs;
    public FinalQueryProConfigImpl(IQueryProConfig[] configs) {
        this.configs = configs;
    }

    @NotNull
    private <R> R getBy(Function<IQueryProConfig, R> getter) {
        final R res = getByNullable(getter);
        if (res == null) {
            throw new IllegalImplements("遍历了所有配置但仍然无法找到有效配置");
        }
        return res;
    }

    private <R> R getByNullable(Function<IQueryProConfig, R> getter) {
        for (IQueryProConfig config : configs) {
            final R res = getter.apply(config);
            if (res != null) {
                return res;
            }
        }
        return null;
    }

    @Nullable
    public DataSource dataSource(@Nullable Class<?> clazz) {
        final DataSource defaultDataSource = DefaultDataSourceWrapper.getDefault(clazz);
        if (defaultDataSource != null) {
            return defaultDataSource;
        }

        final int size = configs.length;
        final IQueryProConfig last = configs[size - 1];
        if (!(last instanceof GlobalQueryProConfigImpl)) {
            throw new IllegalImplements("最后一项配置必须为GlobalQueryProConfigDb");
        }
        for (int i = 0; i < size - 1; i++) {
            final DataSource res = configs[i].dataSource();
            if (res != null) {
                return res;
            }
        }
        return last.dataSource();
    }

    @Nullable
    @Override
    public DatabaseType dbType() {
        return getByNullable(IQueryProConfig::dbType);
    }

    @Nullable
    @Override
    public Integer maxParameterSize() {
        return getByNullable(IQueryProConfig::maxParameterSize);
    }

    @Nullable
    @Override
    public Connection connection() {
        return getByNullable(IQueryProConfig::connection);
    }

    @Nullable
    @Override
    public DataSource dataSource() {
        throw new IllegalCall("不可直接调用dataSource()，使用dataSource(Class<?>)替代");
    }

    @Override @NotNull public Boolean beautifySql() {
        return getBy(IQueryProConfig::beautifySql);
    }

    @NotNull
    @Override
    public Boolean printLog() {
        return getBy(IQueryProConfig::printLog);
    }

    @NotNull
    @Override
    public LogLevel printLogLevel() {
        return getBy(IQueryProConfig::printLogLevel);
    }

    @NotNull
    @Override
    public Boolean printLargeElementWholly() {
        return getBy(IQueryProConfig::printLargeElementWholly);
    }

    public boolean printDebugLog() {
        final Boolean printLog = printLog();
        return Boolean.TRUE.equals(printLog) || printLogLevel() != LogLevel.DEBUG;
    }

    @NotNull
    @Override
    public Boolean printCallByInfo() {
        return getBy(IQueryProConfig::printCallByInfo);
    }

    @NotNull
    @Override
    public LogLevel printCallByInfoLevel() {
        return getBy(IQueryProConfig::printCallByInfoLevel);
    }

    @NotNull
    @Override
    public Boolean printResult() {
        return getBy(IQueryProConfig::printResult);
    }

    @NotNull
    @Override
    public LogLevel printResultLevel() {
        return getBy(IQueryProConfig::printResultLevel);
    }

    @NotNull
    @Override
    public Boolean dryRun() {
        return getBy(IQueryProConfig::dryRun);
    }

    @NotNull
    @Override
    public Boolean logicDelete() {
        return getBy(IQueryProConfig::logicDelete);
    }

    @NotNull
    @Override
    public String logicDeleteField() {
        return getBy(IQueryProConfig::logicDeleteField);
    }

    @Nullable
    @Override
    public Object logicDeleteTrue() {
        for (IQueryProConfig config : configs) {
            if (config.logicDeleteField() != null) {
                return config.logicDeleteTrue();
            }
        }
        throw new IllegalImplements("遍历了所有配置但仍然无法找到有效配置");
    }

    @Nullable
    @Override
    public Object logicDeleteFalse() {
        for (IQueryProConfig config : configs) {
            if (config.logicDeleteField() != null) {
                return config.logicDeleteFalse();
            }
        }
        throw new IllegalImplements("遍历了所有配置但仍然无法找到有效配置");
    }

    @Override
    public Lifecycle lifecycle() {
        return QueryProConfig.global.lifecycle();
    }

    @Override
    public DataSourceGetter defaultDataSource() {
        return QueryProConfig.global.defaultDataSource();
    }

    @Override
    public Set<String> shouldIgnoreFields() {
        return QueryProConfig.global.shouldIgnoreFields();
    }

    @Override
    public Set<Class<?>> supportedColumnType() {
        return QueryProConfig.global.supportedColumnType();
    }

    @Override
    public Map<Function<DbColumnInfo, Boolean>, Class<?>> dbColumnInfoToJavaType() {
        return QueryProConfig.global.dbColumnInfoToJavaType();
    }

    @Nullable
    @Override
    public <T> SqlParamSetter<T> sqlParamSetter(Class<T> clazz) {
        return QueryProConfig.global.sqlParamSetter(clazz);
    }

    @Nullable
    @Override
    public <T> ResultSetGetter<T> resultSetParser(Class<T> clazz) {
        return QueryProConfig.global.resultSetParser(clazz);
    }

    @Override
    public List<ResultSetParserEx> resultSetParserEx() {
        return QueryProConfig.global.resultSetParserEx();
    }

    @Override
    public QueryStructureResolver queryStructureResolver() {
        return QueryProConfig.global.queryStructureResolver();
    }

    @Override
    public ScriptResolver scriptResolver() {
        return QueryProConfig.global.scriptResolver();
    }

    @Override
    public DynamicActuator dynamicActuator() {
        return QueryProConfig.global.dynamicActuator();
    }
}


