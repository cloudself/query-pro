package cn.cloudself.query.config.impl;

import cn.cloudself.query.config.store.Store;
import cn.cloudself.query.config.store.ThreadContextStore;

import java.util.Map;

/**
 * 通用的`DB`实现(构造函数的参数中需传入一个`Store`)
 */
public class TransmittableQueryProConfig extends QueryProConfigImpl {
    protected final ThreadContextStore store;
    private final Map<String, Object> storeCaptured;
    public TransmittableQueryProConfig(boolean subThreadSupports) {
        this(new ThreadContextStore(subThreadSupports), null);
    }
    private TransmittableQueryProConfig(ThreadContextStore store, Map<String, Object> storeCaptured) {
        super(store);
        this.store = store;
        this.storeCaptured = storeCaptured;
    }

    public void init() {
        this.store.init();
    }
    public void clean() {
        this.store.clean();
    }

    public TransmittableQueryProConfig initAndCapture() {
        this.init();
        return new TransmittableQueryProConfig(this.store, this.store.getStore());
    }

    public void linkToCurrentThread() {
        this.store.setStore(storeCaptured);
    }
}
