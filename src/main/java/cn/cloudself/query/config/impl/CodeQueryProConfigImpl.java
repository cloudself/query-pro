package cn.cloudself.query.config.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * 继承自`QueryProConfigDb`, 添加了一些方法，需手动管理`ThreadLocal`中数据的初始化和销毁，也可以用`use`方法管理。
 */
public class CodeQueryProConfigImpl extends ThreadQueryProConfigImpl {
    public CodeQueryProConfigImpl() {
        super(false);
    }

    public <T> T use(Map<String, Object> store, Function<QueryProConfigImpl, T> func) {
        return use(it -> {
            this.configDb.store.setStore(new HashMap<>(store));
            return func.apply(it);
        });
    }
}