package cn.cloudself.query.config.impl;

import cn.cloudself.query.config.Lifecycle;
import cn.cloudself.query.config.IOnlyGlobalConfig;
import cn.cloudself.query.config.store.HashMapStore;
import cn.cloudself.exception.IllegalCall;
import cn.cloudself.query.resolver.DynamicActuator;
import cn.cloudself.query.resolver.QueryStructureResolver;
import cn.cloudself.query.resolver.ScriptResolver;
import cn.cloudself.util.ext.ClassPro;
import org.jetbrains.annotations.Nullable;

import javax.sql.DataSource;
import java.util.*;
import java.util.function.Function;

/**
 * 继承自`QueryProConfigDb`, 额外引入了一些仅在全局作用域下支持的配置。
 */
public class GlobalQueryProConfigImpl extends QueryProConfigImpl implements IOnlyGlobalConfig {
    public GlobalQueryProConfigImpl() {
        super(new HashMapStore());
    }
    private final Lifecycle lifecycle = new Lifecycle.Internal();
    private final Set<Class<?>> supportedColumnType = new HashSet<>();
    private final Map<Class<?>, SqlParamSetter<?>> sqlParamSetter = new HashMap<>();
    private final Map<Class<?>, ResultSetGetter<?>> resultSetParser = new HashMap<>();
    private DataSourceGetter defaultDataSource = new DataSourceGetter() {
        @Nullable
        @Override
        public DataSource get(@Nullable Class<?> clazz) {
            return null;
        }
    };
    private QueryStructureResolver queryStructureResolver = null;
    private ScriptResolver scriptResolver = null;
    private DynamicActuator dynamicActuator = null;
    List<ResultSetParserEx> resultSetParserEx = new ArrayList<>();
    Map<Function<DbColumnInfo, Boolean>, Class<?>> dbColumnInfoToJavaType = new HashMap<>();
    Set<String> shouldIgnoreFields = new HashSet<>();

    @Override
    public Lifecycle lifecycle() {
        return lifecycle;
    }

    @Override
    public Set<String> shouldIgnoreFields() {
        return shouldIgnoreFields;
    }

    @Override
    public Set<Class<?>> supportedColumnType() {
        return supportedColumnType;
    }

    @Override
    public List<ResultSetParserEx> resultSetParserEx() {
        return resultSetParserEx;
    }

    @Override
    public Map<Function<DbColumnInfo, Boolean>, Class<?>> dbColumnInfoToJavaType() {
        return dbColumnInfoToJavaType;
    }

    @Override
    public DataSourceGetter defaultDataSource() {
        return defaultDataSource;
    }

    @Override
    public QueryStructureResolver queryStructureResolver() {
        if (queryStructureResolver == null) {
            throw new IllegalCall("unknown query structure resolver.");
        }
        return queryStructureResolver;
    }

    @Override
    public DynamicActuator dynamicActuator() {
        if (dynamicActuator == null) {
            throw new IllegalCall("unknown dynamic actuator.");
        }
        return dynamicActuator;
    }

    @Override
    public ScriptResolver scriptResolver() {
        if (scriptResolver == null) {
            throw new IllegalCall("unknown script resolver.");
        }
        return scriptResolver;
    }

    /**
     * 读取用户自定义的SqlParamSetter解析器(内置的支持无法读取（出于性能优化目的）)
     */
    @Override
    @Nullable
    public <T> SqlParamSetter<T> sqlParamSetter(Class<T> clazz) {
        //noinspection unchecked
        return (SqlParamSetter<T>) this.sqlParamSetter.get(clazz);
    }

    /**
     * ResultSet解析器
     * 内置支持的类型有:
     * BigDecimal, Byte, ByteArray, Date, LocalDate, LocalTime, LocalDateTime, java.sql.Date, Double, Float, Int,
     * Long, Time, Timestamp, Short, String,
     */
    @Override
    @Nullable
    public <T>  ResultSetGetter<T> resultSetParser(Class<T> clazz) {
        //noinspection unchecked
        ResultSetGetter<T> res = (ResultSetGetter<T>) this.resultSetParser.get(clazz);
        if (res == null) {
            for (Map.Entry<Class<?>, ResultSetGetter<?>> entry : this.resultSetParser.entrySet()) {
                if (entry.getKey().isAssignableFrom(clazz)) {
                    //noinspection unchecked
                    res = (ResultSetGetter<T>) entry.getValue();
                    break;
                }
            }
        }
        return res;
    }

    /**
     * 添加一个ResultSet解析器(字段解析器)
     * @param clazz 需要解析至的class, 例如: LocalDate.class
     * @param value 例子 rs -> i -> rs.getDate(i).toLocalDate()
     *
     * @see #resultSetParser
     * @see #resultSetParserEx
     */
    public <T> GlobalQueryProConfigImpl addResultSetParser(Class<T> clazz, ResultSetGetter<T> value) {
        resultSetParser.put(clazz, value);
        supportedColumnType.add(clazz);
        return this;
    }

    /**
     * 添加一个SqlParamSetter
     */
    public <T> GlobalQueryProConfigImpl addSqlParamSetter(Class<T> clazz, SqlParamSetter<T> value) {
        sqlParamSetter.put(clazz, value);
        return this;
    }

    /**
     * 添加一个ResultSet解析器，与addResultSetParser功能相似，但更推荐使用addResultSetParser，因为性能略好
     */
    public GlobalQueryProConfigImpl addResultSetParserEx(ResultSetParserEx parser) {
        resultSetParserEx.add(parser);
        return this;
    }

    @SuppressWarnings("UnusedReturnValue")
    public GlobalQueryProConfigImpl setQueryStructureResolver(QueryStructureResolver qsr) {
        queryStructureResolver = qsr;
        return this;
    }

    public GlobalQueryProConfigImpl setScriptResolver(ScriptResolver sr) {
        this.scriptResolver = sr;
        return this;
    }

    public GlobalQueryProConfigImpl setDynamicActuator(DynamicActuator da) {
        dynamicActuator = da;
        return this;
    }

    public GlobalQueryProConfigImpl setDefaultDataSource(DataSourceGetter getter) {
        defaultDataSource = getter;
        return this;
    }

    @SuppressWarnings("UnusedReturnValue")
    public <T> GlobalQueryProConfigImpl putToResultSetParser(Class<T> clazz, ResultSetGetter<T> value) {
        final ClassPro classPro = ClassPro.of(clazz);
        final Class<?> primitiveType = classPro.toPrimitiveType();
        if (primitiveType != null && primitiveType != clazz) {
            supportedColumnType.add(primitiveType);
            resultSetParser.put(primitiveType, value);
        }
        final Class<?> objectType = classPro.toObjectType();
        if (objectType != clazz) {
            supportedColumnType.add(objectType);
            resultSetParser.put(objectType, value);
        }
        supportedColumnType.add(clazz);
        resultSetParser.put(clazz, value);
        return this;
    }
}

