package cn.cloudself.query.config;

import cn.cloudself.query.config.store.Store;
import cn.cloudself.exception.UnSupportException;
import cn.cloudself.util.log.LogLevel;
import org.jetbrains.annotations.Nullable;

import javax.sql.DataSource;
import java.sql.Connection;

/**
 * 通用的配置选项(读取接口)
 */
public interface IQueryProConfig {
    @Nullable
    DatabaseType dbType();

    /**
     * 读取Connection配置（如有），与dataSource二选一即可
     */
    @Nullable
    Integer maxParameterSize();

    @Nullable
    Connection connection();

    @Nullable
    DataSource dataSource();

    @Nullable
    Boolean beautifySql();

    @Nullable
    Boolean printLog();

    @Nullable
    LogLevel printLogLevel();

    @Nullable
    Boolean printLargeElementWholly();

    @Nullable
    Boolean printCallByInfo();

    @Nullable
    LogLevel printCallByInfoLevel();

    @Nullable
    Boolean printResult();

    @Nullable
    LogLevel printResultLevel();

    @Nullable
    Boolean dryRun();

    @Nullable
    Boolean logicDelete();

    @Nullable
    String logicDeleteField();

    @Nullable
    Object logicDeleteTrue();

    @Nullable
    Object logicDeleteFalse();

    enum DatabaseType {
        MySQL,
        MSSQL
    }

    /**
     * 通用的配置选项(写入接口)
     */
    interface Writeable<T extends Writeable<T>> {
        T dbType(DatabaseType dbType);
        T maxParameterSize(int size);
        T connection(Connection connection);
        T dataSource(DataSource dataSource);
        T beautifySql(boolean enable);
        T printLog(boolean enable);
        T printLog(boolean enable, LogLevel level);
        T printLargeElementWholly(boolean print);
        T printCallByInfo(boolean enable);
        T printCallByInfo(boolean enable, LogLevel level);
        T printResult(boolean enable);
        T printResult(boolean enable, LogLevel level);

        /**
         * 占位，未充分测试，不推荐使用
         */
        T dryRun(boolean enable);
        T logicDelete(boolean enable);
        T logicDelete(boolean enable, String field, @Nullable Object trueValue, @Nullable Object falseValue);

        /**
         * 通用的`DB`实现(写入部分)(构造函数的参数中需传入一个`Store`)
         */
        abstract class Default<T extends Writeable<T>> implements Writeable<T> {
            protected final Store store;
            public Default(Store store) {
                this.store = store;
            }
            protected abstract T that();
            /* 更改数据库类型，默认为mysql */
            @Override public T dbType(DatabaseType dbType) { store.set("dbType", dbType); return that(); }
            @Override public T maxParameterSize(int size) { store.set("maxParameterSize", size); return that(); }
            @Override public T connection(Connection connection) { store.set("connection", connection); return that(); }
            @Override public T dataSource(DataSource dataSource) { store.set("dataSource", dataSource); return that(); }
            @Override public T beautifySql(boolean enable) { store.set("beautifySql", enable); return that(); }
            @Override public T printLog(boolean enable) { store.set("printLog", enable); store.set("printLogLevel", LogLevel.INFO); return that(); }
            @Override public T printLog(boolean enable, LogLevel level) { store.set("printLog", enable); store.set("printLogLevel", level); return that(); }
            @Override public T printLargeElementWholly(boolean print) { store.set("printLargeElementWholly", print); return that(); }
            @Override public T printCallByInfo(boolean enable) { store.set("printCallByInfo", enable); return that(); }
            @Override public T printCallByInfo(boolean enable, LogLevel level) { store.set("printCallByInfoLevel", level); return printCallByInfo(enable); }
            @Override public T printResult(boolean enable) { store.set("printResult", enable); return that(); }
            @Override public T printResult(boolean enable, LogLevel level) { store.set("printResult", enable); store.set("printResultLevel", level); return that(); }
            @Override public T dryRun(boolean enable) { if (enable) throw new UnSupportException("占位，未充分测试，不推荐使用"); store.set("dryRun", enable); return that(); }
            @Override public T logicDelete(boolean enable) { store.set("logicDelete", enable); return that(); }
            @Override public T logicDelete(boolean enable, String field, @Nullable Object trueValue, @Nullable Object falseValue) {
                store.set("logicDelete", enable);
                store.set("logicDeleteField", field);
                store.set("logicDeleteTrue", trueValue);
                store.set("logicDeleteFalse", falseValue);
                return that();
            }
        }
    }
}