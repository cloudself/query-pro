package cn.cloudself.query.config;

import cn.cloudself.exception.ConfigException;
import cn.cloudself.exception.IllegalImplements;
import cn.cloudself.query.QueryPro;
import cn.cloudself.query.psi.Const;
import cn.cloudself.query.psi.structure.*;
import cn.cloudself.util.ext.EntityProxy;
import cn.cloudself.util.structure.Result;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * <h2>生命周期定义工具</h2>
 *
 * 对于
 * <ul>
 *     <li>{@link QueryPro#selectBy()}</li>
 *     <li>{@link QueryPro#updateSet()}</li>
 *     <li>{@link QueryPro#updateSet(Map)}</li>
 *     <li>{@link QueryPro#updateSet(Object)}</li>
 *     <li>{@link QueryPro#updateSet(Object, boolean)}</li>
 *     <li>{@link QueryPro#deleteBy()}</li>
 *     <li>{@link QueryPro#insert(Map[])}</li>
 *     <li>{@link QueryPro#insert(Collection)}</li>
 *     <li>{@link QueryPro#insert(Object)}</li>
 *     <li>{@link QueryPro#insert(Object[])}</li>
 *     <li>{@link QueryPro#replace(Map[])}</li>
 *     <li>{@link QueryPro#replace(Object[])}</li>
 *     <li>{@link QueryPro#replace(Collection)}</li>
 *     <li>等</li>
 * </ul>
 * {@link QueryPro}下定义的方法(含生成的委托类,
 * 特点是通过{@link QueryStructure}传递信息，并由{@link cn.cloudself.query.resolver.QueryStructureResolver}生成并执行SQL)，
 * 生命周期为:
 * <ol>
 *     <li>{@link #beforeExec(QueryStructureTransformer)}; action based lifecycle, e.g. {@link #beforeSelect(Function)}, {@link #beforeUpdate(Function)}, {@link #beforeInsert(Function)}, {@link #beforeDelete(Function)}(取决于先调用哪个)</li>
 *     <li>{@link #beforeRunSql(SqlAndParamsTransformer)}</li>
 *     <li>{@link #afterRunSql(ResultTransformer)}</li>
 *     <li>{@link #afterExec(ResultWithQueryStructureTransformer)}; action based lifecycle, e.g. {@link #afterSelect(Function)}, etc.</li>
 * </ol>
 * <hr/>
 * 对于
 * {@link cn.cloudself.query.QueryProSql}下定义的方法(特点是不通过{@link QueryStructure}直接执行SQL或通过数据库表结构直接生成并执行SQL)，
 * 生命周期为:
 * <ol>
 *     <li>{@link #beforeRunSql(SqlAndParamsTransformer)}</li>
 *     <li>{@link #afterRunSql(ResultTransformer)}</li>
 * </ol>
 * <hr/>
 * 该定义工具最终生成并对内暴露{@link Internal}含以下四个方法:
 * <ul>
 *     <li>{@link Internal#getBeforeExecTransformers()}</li>
 *     <li>{@link Internal#getAfterExecTransformers()}</li>
 *     <li>{@link Internal#getBeforeRunSqlTransformers()}</li>
 *     <li>{@link Internal#getAfterRunSqlTransformers()}</li>
 * </ul>
 */
@SuppressWarnings("unused")
public class Lifecycle {

    /**
     * 对内暴露的最终生命周期周期方法，在内部适当的地方强转即可
     */
    public static class Internal extends Lifecycle {
        public List<QueryStructureTransformer> getBeforeExecTransformers() {
            return beforeExecTransformers;
        }

        public List<ResultWithQueryStructureTransformer> getAfterExecTransformers() {
            return afterExecTransformers;
        }

        public List<SqlAndParamsTransformer> getBeforeRunSqlTransformers() {
            return beforeRunSqlTransformers;
        }

        public List<ResultTransformer> getAfterRunSqlTransformers() {
            return afterRunSqlTransformers;
        }
    }
    // query pro
    protected final List<QueryStructureTransformer> beforeExecTransformers = new ArrayList<>();
    protected final List<ResultWithQueryStructureTransformer> afterExecTransformers = new ArrayList<>();
    // query pro sql & query pro
    protected final List<SqlAndParamsTransformer> beforeRunSqlTransformers = new ArrayList<>();
    protected final List<ResultTransformer> afterRunSqlTransformers = new ArrayList<>();

    /**
     * <h2>SQL和参数转换器</h2>
     * <p>可以使用该转换器转换返回结果或者返回一个异常</p>
     * @see #transform(SqlAndParams)
     */
    public interface SqlAndParamsTransformer {
        /**
         * 转换SQL或者参数
         * @param sqlAndParams 原SQL和参数
         * @return 根据 {@link cn.cloudself.query.resolver.QSRTmpl}的实现，如果返回{@link Result#err(Throwable)}，相关操作会终止执行并抛出异常；否则，替换SQL和参数为为{@link Result#ok(Object)}
         */
        Result<SqlAndParams, Throwable> transform(SqlAndParams sqlAndParams);
    }

    /**
     * <h2>返回结果转换器</h2>
     * <p>可以使用该转换器转换返回结果或者返回一个异常</p>
     * @see #transform(Object)
     */
    public interface ResultTransformer {
        /**
         * <p><b>转换返回结果</b></p>
         * @param result 原返回结果
         * @return 根据 {@link cn.cloudself.query.resolver.QSRTmpl}的实现，如果返回{@link Result#err(Throwable)}，相关操作会终止执行并抛出异常；否则，替换返回结果为{@link Result#ok(Object)}
         */
        Result<Object, Throwable> transform(@Nullable Object result);
    }

    /**
     * <h2>{@link QueryStructure}转换器</h2>
     * <p>可以使用该转换器转换{@link QueryStructure}或者返回一个异常</p>
     * @see #transform(Class, QueryStructure, QueryPayload)
     */
    public interface QueryStructureTransformer {
        /**
         * <p><b>转换{@link QueryStructure}</b></p>
         *
         * @param resultClass 需要返回的对象的Class类型
         * @param qs {@link QueryStructure}
         * @param payload {@link QueryPayload}
         * @return 根据 {@link cn.cloudself.query.resolver.QSRTmpl}的实现，如果返回{@link Result#err(Throwable)}，相关操作会终止执行并抛出异常
         */
        Result<QueryStructure, Throwable> transform(@Nullable Class<?> resultClass, QueryStructure qs, QueryPayload payload);
    }

    /**
     * <h2>返回结果转换器</h2>
     * <p>可以使用该转换器转换返回结果或者返回一个异常</p>
     * @see #transform(Class, Object, QueryStructure, QueryPayload)
     */
    public interface ResultWithQueryStructureTransformer {
        /**
         *
         * @param resultClass 需要返回的对象的Class类型
         * @param result 原返回结果
         * @param qs {@link QueryStructure}
         * @param payload {@link QueryPayload}
         * @return 根据 {@link cn.cloudself.query.resolver.QSRTmpl}的实现，如果返回{@link Result#err(Throwable)}，相关操作会终止执行并抛出异常；否则，替换返回结果为{@link Result#ok(Object)}
         */
        Result<?, Throwable> transform(@Nullable Class<?> resultClass, Object result, QueryStructure qs, QueryPayload payload);
    }

    protected static abstract class BaseQsTransformersBuilder {
        private final QueryStructureAction action;
        public BaseQsTransformersBuilder(QueryStructureAction action) {
            this.action = action;
        }

        private final List<QueryStructureTransformer> transformers = new ArrayList<>();
        @SuppressWarnings("UnusedReturnValue")
        protected BaseQsTransformersBuilder addTransformerWhenActionMatched(QueryStructureTransformer transformer) {
            transformers.add((clazz, queryStructure, payload) -> {
                if (queryStructure.action() == action) {
                    return transformer.transform(clazz, queryStructure, payload);
                } else {
                    return Result.ok(queryStructure);
                }
            });
            return this;
        }
        public abstract BaseQsTransformersBuilder addTransformer(QueryStructureTransformer transformer);
        public List<QueryStructureTransformer> build() {
            return transformers;
        }
    }

    /**
     * 添加，修改，删除返回结果时的过滤器
     * 一般作为可选参数出现
     * 当返回结果为false时代表不执行相应的操作
     */
    public interface BeforeInsertPredicate {
        /**
         * 过滤器，true 继续执行, false 终止执行
         * @param bean bean代理 {@link cn.cloudself.util.ext.EntityProxy.BeanInstance}
         * @param qs {@link QueryStructure}
         * @param payload {@link QueryPayload}
         * @return true 继续执行, false 终止执行
         */
        boolean predicate(EntityProxy.BeanInstance<?, Object> bean, QueryStructure qs, QueryPayload payload);
    }

    /**
     * <h2>插入(INSERT &amp; REPLACE)前，对{@link QueryStructure}进行转换</h2>
     * 例如，添加创建时间，创建人等
     *
     * @see #addField(String, Class, Supplier)
     * @see #overrideField(String, Class, Supplier)
     */
    public static class BeforeInsertTransformersBuilder extends BaseQsTransformersBuilder {
        BeforeInsertTransformersBuilder(boolean replace) {
            super(replace ? QueryStructureAction.REPLACE : QueryStructureAction.INSERT);
        }

        /**
         * <p><b>添加一个{@link QueryStructureTransformer}转换器</b></p>
         * <br/>
         * 根据 {@link cn.cloudself.query.resolver.QSRTmpl}的实现，如果返回{@link Result#err(Throwable)}，相关操作会终止执行并抛出异常
         *
         * @param transformer {@link QueryStructureTransformer#transform(Class, QueryStructure, QueryPayload)}
         * @return 自身，Builder模式，方便链式调用
         */
        @Override
        public BeforeInsertTransformersBuilder addTransformer(QueryStructureTransformer transformer) {
            addTransformerWhenActionMatched(transformer);
            return this;
        }

        /**
         * <p><b>插入前添加某字段，例如create_time等</b></p>
         * <p>当目标字段存在时，会跳过该字段的填充，如果需要覆盖该字段，使用{@link #overrideField(String, Class, Supplier)}</p>
         * <p>不会处理{@link cn.cloudself.query.QueryProSql}定义的插入操作</p>
         * @param field 需要添加的字段名
         * @param fieldClass 有时候，相同的 field 会有多种类型的取值，使用该字段可以区分这种情况
         * @param value Supplier，需要返回的是 需要添加的字段值
         * @return 自身，Builder模式，方便链式调用
         * @param <T> see fieldClass
         */
        public <T> BeforeInsertTransformersBuilder addField(String field, Class<T> fieldClass, Supplier<T> value) {
            return addField(field, fieldClass, value, (a, b, c) -> true);
        }

        /**
         * <p><b>插入前添加某字段，例如create_time等</b></p>
         * <p>当目标字段存在时，会跳过该字段的填充，如果需要覆盖该字段，使用{@link #overrideField(String, Class, Supplier, BeforeInsertPredicate)}</p>
         * <p>不会处理{@link cn.cloudself.query.QueryProSql}定义的插入操作</p>
         * @param field 需要添加的字段名
         * @param fieldClass 有时候，相同的 field 会有多种类型的取值，使用该字段可以区分这种情况
         * @param value Supplier，需要返回的是 需要添加的字段值
         * @param predicate 可选的过滤器，返回结果为false时，该字段不会被添加
         * @return 自身，Builder模式，方便链式调用
         * @param <T> see fieldClass
         */
        public <T> BeforeInsertTransformersBuilder addField(String field, Class<T> fieldClass, Supplier<T> value, BeforeInsertPredicate predicate) {
            overrideField(field, fieldClass, value, false, predicate);
            return this;
        }

        /**
         * <p><b>插入前添加某字段，例如create_time等</b></p>
         * <p>当目标字段存在时，会覆盖该字段，如果需要仅当目标字段为null时填充相应字段，使用{@link #addField(String, Class, Supplier)}</p>
         * <p>不会处理{@link cn.cloudself.query.QueryProSql}定义的插入操作</p>
         * @param field 需要添加的字段名
         * @param fieldClass 有时候，相同的 field 会有多种类型的取值，使用该字段可以区分这种情况
         * @param value Supplier，需要返回的是 需要添加的字段值
         * @return 自身，Builder模式，方便链式调用
         * @param <T> see fieldClass
         */
        public <T> BeforeInsertTransformersBuilder overrideField(String field, Class<?> fieldClass, Supplier<T> value) {
            return overrideField(field, fieldClass, value, (a, b, c) -> true);
        }

        /**
         * <p><b>插入前添加某字段，例如create_time等</b></p>
         * <p>当目标字段存在时，会覆盖该字段，如果需要仅当目标字段为null时填充相应字段，使用{@link #addField(String, Class, Supplier, BeforeInsertPredicate)}</p>
         * <p>不会处理{@link cn.cloudself.query.QueryProSql}定义的插入操作</p>
         * @param field 需要添加的字段名
         * @param fieldClass 有时候，相同的 field 会有多种类型的取值，使用该字段可以区分这种情况
         * @param value Supplier，需要返回的是 需要添加的字段值
         * @param predicate 可选的过滤器，返回结果为false时，该字段不会被添加
         * @return 自身，Builder模式，方便链式调用
         * @param <T> see fieldClass
         */
        public <T> BeforeInsertTransformersBuilder overrideField(String field, Class<?> fieldClass, Supplier<T> value, BeforeInsertPredicate predicate) {
            overrideField(field, fieldClass, value, true, predicate);
            return this;
        }

        private <T> void overrideField(String field, Class<?> fieldClass, Supplier<T> getValue, Boolean override, BeforeInsertPredicate predicate) {
            addTransformerWhenActionMatched((__, queryStructure, payload) -> {
                final Insert insert = queryStructure.insert();
                final Collection<?> objs = insert == null ? null : insert.data();
                if (objs == null) {
                    throw new IllegalImplements("insert 传入了空值。");
                }
                for (Object obj : objs) {
                    final EntityProxy.BeanInstance<?, Object> beanInstance = EntityProxy.fromClass(payload.clazz()).newInstance(obj);
                    if (!predicate.predicate(beanInstance, queryStructure, payload)) {
                        return Result.ok(queryStructure);
                    }

                    final Class<?> fieldType = beanInstance.getPropertyType(field);
                    if (fieldType == null || fieldType != fieldClass) {
                        continue;
                    }

                    if (beanInstance.getProperty(field) == null || override) {
                        final T value = getValue.get();
                        if (value != Const.SKIP) {
                            if (value == Const.NULL) {
                                beanInstance.setProperty(field, null);
                            } else {
                                beanInstance.setProperty(field, value);
                            }
                        }
                    }
                }
                return Result.ok(queryStructure);
            });
        }
    }

    /**
     * 添加，修改，删除返回结果时的过滤器
     * 一般作为可选参数出现
     * 当返回结果为false时代表不执行相应的操作
     */
    public interface BeforeUpdatePredicate {
        boolean predicate(EntityProxy.BeanInstance<?, Object> bean, QueryStructure qs, QueryPayload payload);
    }

    /**
     * 更新前，对{@link QueryStructure}进行转换，
     * 例如，添加更新时间，更新人等
     *
     * @see #addField(String, Class, Supplier, BeforeUpdatePredicate)
     * @see #overrideField(String, Class, Supplier)
     */
    public static class BeforeUpdateTransformersBuilder extends BaseQsTransformersBuilder {
        BeforeUpdateTransformersBuilder() {
            super(QueryStructureAction.UPDATE);
        }

        /**
         * <p><b>添加一个{@link QueryStructureTransformer}转换器</b></p>
         * <br/>
         * 根据 {@link cn.cloudself.query.resolver.QSRTmpl}的实现，如果返回{@link Result#err(Throwable)}，相关操作会终止执行并抛出异常
         *
         * @param transformer {@link QueryStructureTransformer#transform(Class, QueryStructure, QueryPayload)}
         * @return 自身，Builder模式，方便链式调用
         */
        public BeforeUpdateTransformersBuilder addTransformer(QueryStructureTransformer transformer) {
            addTransformerWhenActionMatched(transformer);
            return this;
        }

        /**
         * <p><b>更新前添加字段，当未指定某字段的值时，为该字段添加默认值</b></p>
         * <p>当目标字段存在时，会跳过该字段的填充，如果需要覆盖该字段，使用{@link #overrideField(String, Class, Supplier)}</p>
         * <p>不会处理{@link cn.cloudself.query.QueryProSql}定义的更新操作</p>
         * @param field 需要更新的字段名
         * @param fieldClass 有时候，相同的 field 会有多种类型的取值，使用该字段可以区分这种情况
         * @param value Supplier，需要返回的是 需要更新的字段值
         * @return 自身，Builder模式，方便链式调用
         * @param <T> see fieldClass
         */
        public <T> BeforeUpdateTransformersBuilder addField(String field, Class<T> fieldClass, Supplier<T> value) {
            return addField(field, fieldClass, value, (a, b, c) -> true);
        }

        /**
         * <p><b>更新前添加字段，当未指定某字段的值时，为该字段添加默认值</b></p>
         * <p>当目标字段存在时，会跳过该字段的填充，如果需要覆盖该字段，使用{@link #overrideField(String, Class, Supplier, BeforeUpdatePredicate)}</p>
         * <p>不会处理{@link cn.cloudself.query.QueryProSql}定义的更新操作</p>
         * @param field 需要更新的字段名
         * @param fieldClass 有时候，相同的 field 会有多种类型的取值，使用该字段可以区分这种情况
         * @param value Supplier，需要返回的是 需要更新的字段值
         * @param predicate 可选的过滤器，返回结果为false时，该字段不会被更新
         * @return 自身，Builder模式，方便链式调用
         * @param <T> see fieldClass
         */
        public <T> BeforeUpdateTransformersBuilder addField(String field, Class<T> fieldClass, Supplier<T> value, BeforeUpdatePredicate predicate) {
            overrideField(field, fieldClass, value, false, predicate);
            return this;
        }

        /**
         * <p><b>更新前添加字段，无论是否指定此字段，始终覆盖，例如：更新时间等</b></p>
         * <p>当目标字段存在时，会覆盖该字段的填充，如果不需要该行为，使用{@link #addField(String, Class, Supplier)}</p>
         * <p>不会处理{@link cn.cloudself.query.QueryProSql}定义的更新操作</p>
         * <p>可返回 {@link Const#NULL} 作为value实现deleteField</p>
         * @param field 需要更新的字段名
         * @param fieldClass 有时候，相同的 field 会有多种类型的取值，使用该字段可以区分这种情况
         * @param value Supplier，需要返回的是 需要更新的字段值
         * @return 自身，Builder模式，方便链式调用
         * @param <T> see fieldClass
         */
        public <T> BeforeUpdateTransformersBuilder overrideField(String field, Class<T> fieldClass, Supplier<T> value) {
            return overrideField(field, fieldClass, value, (a, b, c) -> true);
        }

        /**
         * <p><b>更新前添加字段，无论是否指定此字段，始终覆盖，例如：更新时间等</b></p>
         * <p>当目标字段存在时，会覆盖该字段的填充，如果不需要该行为，使用{@link #addField(String, Class, Supplier, BeforeUpdatePredicate)}</p>
         * <p>不会处理{@link cn.cloudself.query.QueryProSql}定义的更新操作</p>
         * <p>可返回 {@link Const#NULL} 作为value实现deleteField</p>
         * @param field 需要更新的字段名
         * @param fieldClass 有时候，相同的 field 会有多种类型的取值，使用该字段可以区分这种情况
         * @param value Supplier，需要返回的是 需要更新的字段值
         * @param predicate 可选的过滤器，返回结果为false时，该字段不会被更新
         * @return 自身，Builder模式，方便链式调用
         * @param <T> see fieldClass
         */
        public <T> BeforeUpdateTransformersBuilder overrideField(String field, Class<T> fieldClass, Supplier<T> value, BeforeUpdatePredicate predicate) {
            overrideField(field, fieldClass, value, true, predicate);
            return this;
        }


        /**
         * <p><b>更新时忽略某字段</b></p>
         * <p>不会处理{@link cn.cloudself.query.QueryProSql}定义的更新操作</p>
         * @param field 需要删除的字段名
         * @param fieldClass 有时候，相同的 field 会有多种类型的取值，使用该字段可以区分这种情况，可为null，为null时代表不区分类型
         * @return 自身，Builder模式，方便链式调用
         */
        public BeforeUpdateTransformersBuilder deleteField(String field, @Nullable Class<?> fieldClass) {
            return deleteField(field, fieldClass, (a, b, c) -> true);
        }

        /**
         * <p><b>更新时忽略某字段</b></p>
         * <p>不会处理{@link cn.cloudself.query.QueryProSql}定义的更新操作</p>
         * @param field 需要删除的字段名
         * @param fieldClass 有时候，相同的 field 会有多种类型的取值，使用该字段可以区分这种情况，可为null，为null时代表不区分类型
         * @param predicate 可选的过滤器，返回结果为false时，该字段不会被移除
         * @return 自身，Builder模式，方便链式调用
         */
        public BeforeUpdateTransformersBuilder deleteField(String field, @Nullable Class<?> fieldClass, BeforeUpdatePredicate predicate) {
            overrideField(field, fieldClass, () -> Const.NULL, true, predicate);
            return this;
        }

        private <T> void overrideField(String field, @Nullable Class<?> fieldClass, Supplier<T> getValue, boolean override, BeforeUpdatePredicate predicate) {
            addTransformerWhenActionMatched((__, queryStructure, payload) -> {
                final Update update = queryStructure.update();
                final Object data = update == null ? null : update.data();
                if (data == null) {
                    throw new IllegalImplements("update 传入了空值。");
                }

                @SuppressWarnings("DuplicatedCode") final EntityProxy.BeanInstance<?, Object> beanInstance = EntityProxy.fromClass(payload.clazz()).newInstance(data);
                if (!predicate.predicate(beanInstance, queryStructure, payload)) {
                    return Result.ok(queryStructure);
                }

                final Class<?> fieldType = beanInstance.getPropertyType(field);
                if (fieldType == null) {
                    return Result.ok(queryStructure);
                }
                if (fieldClass != null && fieldType != fieldClass) {
                    return Result.ok(queryStructure);
                }

                final Object oldValue = beanInstance.getProperty(field);
                if (oldValue != null && !override) {
                    return Result.ok(queryStructure);
                }
                final T value = getValue.get();
                if (value == null) {
                    throw new ConfigException("beforeUpdate.add(override)Field, 不能传入null值, 如需将值更新为null，使用Const.NULL");
                }
                if (value != Const.SKIP) {
                    if (value == Const.NULL) {
                        beanInstance.setProperty(field, null);
                    } else {
                        beanInstance.setProperty(field, value);
                    }
                }
                return Result.ok(queryStructure);
            });
        }
    }

    /**
     * Select前，对{@link QueryStructure}进行转换
     */
    public static class BeforeSelectTransformersBuilder extends BaseQsTransformersBuilder {
        BeforeSelectTransformersBuilder() {
            super(QueryStructureAction.SELECT);
        }

        /**
         * <p><b>添加一个{@link QueryStructureTransformer}转换器</b></p>
         * <br/>
         * 根据 {@link cn.cloudself.query.resolver.QSRTmpl}的实现，如果返回{@link Result#err(Throwable)}，相关操作会终止执行并抛出异常
         *
         * @param transformer {@link QueryStructureTransformer#transform(Class, QueryStructure, QueryPayload)}
         * @return 自身，Builder模式，方便链式调用
         */
        @Override
        public BeforeSelectTransformersBuilder addTransformer(QueryStructureTransformer transformer) {
            addTransformerWhenActionMatched(transformer);
            return this;
        }
    }

    /**
     * 执行删除操作前，对{@link QueryStructure}进行转换
     */
    public static class BeforeDeleteTransformersBuilder extends BaseQsTransformersBuilder {
        BeforeDeleteTransformersBuilder() {
            super(QueryStructureAction.DELETE);
        }

        /**
         * <p><b>添加一个{@link QueryStructureTransformer}转换器</b></p>
         * <br/>
         * 根据 {@link cn.cloudself.query.resolver.QSRTmpl}的实现，如果返回{@link Result#err(Throwable)}，相关操作会终止执行并抛出异常
         *
         * @param transformer {@link QueryStructureTransformer#transform(Class, QueryStructure, QueryPayload)}
         * @return 自身，Builder模式，方便链式调用
         */
        @Override
        public BeforeDeleteTransformersBuilder addTransformer(QueryStructureTransformer transformer) {
            addTransformerWhenActionMatched(transformer);
            return this;
        }
    }

    /**
     * <p><b>生命周期回调之: Before Select</b></p>
     * <br/>
     * <p>会在{@link QueryPro#selectBy()} 的{@link QueryStructure}生成之后，`SQL`生成之前调用</p>
     * <br/>
     * <p>具体的生命周期顺序，参考{@link Lifecycle}</p>
     * <p><b>Examples: </b></p>
     * <pre>{@code
     * QueryProConfig.global
     *         .lifecycle()
     *         .beforeSelect(builder -> builder.addTransformer(((resultClass, qs, payload) -> {
     *             // do any thing
     *             return Result.ok(qs);
     *         })));
     * }</pre>
     *
     * @param builder {@link BeforeSelectTransformersBuilder}
     * @return 自身，Builder模式，方便链式调用
     */
    public Lifecycle beforeSelect(Function<BeforeSelectTransformersBuilder, BeforeSelectTransformersBuilder> builder) {
        beforeExecTransformers.addAll(builder.apply(new BeforeSelectTransformersBuilder()).build());
        return this;
    }

    /**
     * <p><b>生命周期回调之: Before Update</b></p>
     * <br/>
     * <p>会在{@link QueryPro#updateSet()} 等方法的{@link QueryStructure}生成之后，`SQL`生成之前调用</p>
     * <br/>
     * <p>具体的生命周期顺序，参考{@link Lifecycle}</p>
     * <p><b>Examples: </b></p>
     * <pre>{@code
     * QueryProConfig.global
     *         .lifecycle()
     *         .beforeUpdate(
     *                 builder -> builder
     *                         .overrideField("update_time", Date.class, Date::new)
     *                         .overrideField("update_by", String.class, () -> "test")
     *         );
     * }</pre>
     *
     * @param builder {@link BeforeUpdateTransformersBuilder}
     * @return 自身，Builder模式，方便链式调用
     */
    public Lifecycle beforeUpdate(Function<BeforeUpdateTransformersBuilder, BeforeUpdateTransformersBuilder> builder) {
        beforeExecTransformers.addAll(builder.apply(new BeforeUpdateTransformersBuilder()).build());
        return this;
    }

    /**
     * <p><b>生命周期回调之: Before Delete</b></p>
     * <br/>
     * <p>会在{@link QueryPro#deleteBy()} 的{@link QueryStructure}生成之后，`SQL`生成之前调用</p>
     * <br/>
     * <p>具体的生命周期顺序，参考{@link Lifecycle}</p>
     * <p><b>Examples: </b></p>
     * <pre>{@code
     * QueryProConfig.global
     *         .lifecycle()
     *         .beforeDelete(builder -> builder.addTransformer(((resultClass, qs, payload) -> {
     *             // do any thing
     *             return Result.ok(qs);
     *         })));
     * }</pre>
     *
     * @param builder {@link BeforeDeleteTransformersBuilder}
     * @return 自身，Builder模式，方便链式调用
     */
    public Lifecycle beforeDelete(Function<BeforeDeleteTransformersBuilder, BeforeDeleteTransformersBuilder> builder) {
        beforeExecTransformers.addAll(builder.apply(new BeforeDeleteTransformersBuilder()).build());
        return this;
    }

    /**
     * <p><b>生命周期回调之: Before Insert</b></p>
     * <br/>
     * <p>会在{@link QueryPro#insert(Object)} 等方法的{@link QueryStructure}生成之后，`SQL`生成之前调用</p>
     * <br/>
     * <p>具体的生命周期顺序，参考{@link Lifecycle}</p>
     * <p><b>Examples: </b></p>
     * <pre>{@code
     * QueryProConfig.global
     *         .lifecycle()
     *         .beforeInsert(builder -> builder.addField("deleted", Boolean.class, () -> false).overrideField("age", Integer.class, () -> 18))
     * }</pre>
     *
     * @param builder {@link BeforeInsertTransformersBuilder}
     * @return 自身，Builder模式，方便链式调用
     */
    public Lifecycle beforeInsert(Function<BeforeInsertTransformersBuilder, BeforeInsertTransformersBuilder> builder) {
        beforeExecTransformers.addAll(builder.apply(new BeforeInsertTransformersBuilder(false)).build());
        return this;
    }

    /**
     * <p><b>生命周期回调之: Before Replace</b></p>
     * <br/>
     * <p>会在{@link QueryPro#replace(Object[])} 等方法的{@link QueryStructure}生成之后，`SQL`生成之前调用</p>
     * <br/>
     * <p>具体的生命周期顺序，参考{@link Lifecycle}</p>
     * <p><b>Examples: </b></p>
     * <pre>{@code
     * QueryProConfig.global
     *         .lifecycle()
     *         .beforeReplace(builder -> builder.addField("deleted", Boolean.class, () -> false).overrideField("age", Integer.class, () -> 18))
     * }</pre>
     *
     * @param builder {@link BeforeInsertTransformersBuilder}
     * @return 自身，Builder模式，方便链式调用
     */
    public Lifecycle beforeReplace(Function<BeforeInsertTransformersBuilder, BeforeInsertTransformersBuilder> builder) {
        beforeExecTransformers.addAll(builder.apply(new BeforeInsertTransformersBuilder(true)).build());
        return this;
    }

    /**
     * <p><b>生命周期回调之: Before Exec</b></p>
     * <br/>
     * <p>会在{@link QueryPro#selectBy()}, {@link QueryPro#insert(Object)} 等操作的{@link QueryStructure}生成之后，`SQL`生成之前调用</p>
     * <br/>
     * <p>具体的生命周期顺序，参考{@link Lifecycle}</p>
     * @param transformer {@link QueryStructureTransformer}
     * @return 自身，Builder模式，方便链式调用
     */
    public Lifecycle beforeExec(QueryStructureTransformer transformer) {
        beforeExecTransformers.add(transformer);
        return this;
    }

    protected static abstract class BaseResultTransformersBuilder {
        private final QueryStructureAction action;
        public BaseResultTransformersBuilder(QueryStructureAction action) {
            this.action = action;
        }

        private final List<ResultWithQueryStructureTransformer> transformers = new ArrayList<>();
        @SuppressWarnings("UnusedReturnValue")
        protected BaseResultTransformersBuilder addTransformerWhenActionMatched(ResultWithQueryStructureTransformer transformer) {
            transformers.add((clazz, result, queryStructure, payload) -> {
                if (queryStructure.action() == action) {
                    return transformer.transform(clazz, result, queryStructure, payload);
                } else {
                    return Result.ok(result);
                }
            });
            return this;
        }
        public abstract BaseResultTransformersBuilder addTransformer(ResultWithQueryStructureTransformer transformer);
        public List<ResultWithQueryStructureTransformer> build() {
            return transformers;
        }
    }

    /**
     * 添加，修改，删除返回结果时的过滤器
     * 一般作为可选参数出现
     * 当返回结果为false时代表不执行相应的操作
     */
    public interface AfterSelectPredicate {
        /**
         * 过滤器，true 继续执行, false 终止执行
         * @param beanInstance bean代理 {@link cn.cloudself.util.ext.EntityProxy.BeanInstance}
         * @param qs {@link QueryStructure}
         * @param payload {@link QueryPayload}
         * @return true 继续执行, false 终止执行
         */
        boolean predicate(@Nullable EntityProxy.BeanInstance<?, Object> beanInstance, QueryStructure qs, QueryPayload payload);
    }

    /**
     * <h2>查询结果返回后，对返回结果进行转换</h2>
     *
     * @see #addField(String, Class, Supplier)
     * @see #overrideField(String, Class, Supplier)
     * @see #deleteField(String, Class)
     */
    public static class AfterSelectTransformerBuilder extends BaseResultTransformersBuilder {
        AfterSelectTransformerBuilder() {
            super(QueryStructureAction.SELECT);
        }

        /**
         * <p><b>添加一个返回结果转换器</b></p>
         * <br/>
         * 根据 {@link cn.cloudself.query.resolver.QSRTmpl}的实现，如果返回{@link Result#err(Throwable)}，相关操作会终止执行并抛出异常
         *
         * @param transformer {@link ResultWithQueryStructureTransformer#transform(Class, Object, QueryStructure, QueryPayload)}
         * @return 自身，Builder模式，方便链式调用
         */
        @Override
        public AfterSelectTransformerBuilder addTransformer(ResultWithQueryStructureTransformer transformer) {
            addTransformerWhenActionMatched(transformer);
            return this;
        }

        /**
         * 为select的返回结果添加某属性
         * 不会处理{@link cn.cloudself.query.QueryProSql}的返回结果
         * @param field 需要添加的字段名
         * @param fieldClass 有时候，相同的 field 会有多种类型的取值，使用该字段可以区分这种情况
         * @param value Supplier，需要返回的是 需要添加的字段值
         * @return 自身，Builder模式，方便链式调用
         * @param <T> see fieldClass
         */
        public <T> AfterSelectTransformerBuilder addField(String field, Class<?> fieldClass, Supplier<T> value) {
            return addField(field, fieldClass, value, (a, b, c) -> true);
        }

        /**
         * 为select的返回结果添加某属性
         * 不会处理{@link cn.cloudself.query.QueryProSql}的返回结果
         * @param field 需要添加的字段名
         * @param fieldClass 有时候，相同的 field 会有多种类型的取值，使用该字段可以区分这种情况
         * @param value Supplier，需要返回的是 需要添加的字段值
         * @param predicate 可选的过滤器，返回结果为false时，该字段不会被添加
         * @return 自身，Builder模式，方便链式调用
         * @param <T> see fieldClass
         */
        public <T> AfterSelectTransformerBuilder addField(String field, Class<?> fieldClass, Supplier<T> value, AfterSelectPredicate predicate) {
            overrideField(field, fieldClass, value, false, predicate);
            return this;
        }

        /**
         * 为select的返回结果重写某属性
         * 不会处理{@link cn.cloudself.query.QueryProSql}的返回结果
         * @param field 需要重写的字段名
         * @param fieldClass 有时候，相同的 field 会有多种类型的取值，使用该字段可以区分这种情况
         * @param value Supplier，需要返回的是 需要重写的字段值
         * @return 自身，Builder模式，方便链式调用
         * @param <T> see fieldClass
         */
        public <T> AfterSelectTransformerBuilder overrideField(String field, Class<?> fieldClass, Supplier<T> value) {
            return overrideField(field, fieldClass, value, (a, b, c) -> true);
        }

        /**
         * 为select的返回结果重写某属性
         * 不会处理{@link cn.cloudself.query.QueryProSql}的返回结果
         * @param field 需要添加的字段名
         * @param fieldClass 有时候，相同的 field 会有多种类型的取值，使用该字段可以区分这种情况
         * @param value Supplier，需要返回的是 需要重写的字段值
         * @param predicate 可选的过滤器，返回结果为false时，该字段不会被重写
         * @return 自身，Builder模式，方便链式调用
         * @param <T> see fieldClass
         */
        public <T> AfterSelectTransformerBuilder overrideField(String field, Class<?> fieldClass, Supplier<T> value, AfterSelectPredicate predicate) {
            overrideField(field, fieldClass, value, true, predicate);
            return this;
        }

        /**
         * 为select的返回结果删除某属性
         * 不会处理{@link cn.cloudself.query.QueryProSql}的返回结果
         * @param field 需要删除的字段名
         * @param fieldClass 有时候，相同的 field 会有多种类型的取值，使用该字段可以区分这种情况
         * @return 自身，Builder模式，方便链式调用
         */
        public AfterSelectTransformerBuilder deleteField(String field, @Nullable Class<?> fieldClass) {
            return deleteField(field, fieldClass, (a, b, c) -> true);
        }

        /**
         * 为select的返回结果删除某属性
         * 不会处理{@link cn.cloudself.query.QueryProSql}的返回结果
         * @param field 需要删除的字段名
         * @param fieldClass 有时候，相同的 field 会有多种类型的取值，使用该字段可以区分这种情况
         * @param predicate 可选的过滤器，返回结果为false时，该字段不会被移除
         * @return 自身，Builder模式，方便链式调用
         */
        public AfterSelectTransformerBuilder deleteField(String field, @Nullable Class<?> fieldClass, AfterSelectPredicate predicate) {
            overrideField(field, fieldClass, () -> Const.NULL, true, predicate);
            return this;
        }

        private <T> void overrideField(String field, @Nullable Class<?> fieldClass, Supplier<T> getValue, boolean override, AfterSelectPredicate predicate) {
            super.addTransformerWhenActionMatched((resultClass, result, queryStructure, payload) -> {
                final Consumer<Object> doTransform = (row) -> {
                    if (resultClass == null) {
                        return;
                    }
                    if (row == null) {
                        return;
                    }
                    final EntityProxy.BeanInstance<?, Object> beanInstance = EntityProxy.fromClass(resultClass).newInstance(row);
                    if (!predicate.predicate(beanInstance, queryStructure, payload)) {
                        return;
                    }

                    final Class<?> fieldType = beanInstance.getPropertyType(field);
                    if (fieldType == null) {
                        return;
                    }
                    if (fieldClass != null && fieldType != fieldClass) {
                        return;
                    }

                    final Object oldValue = beanInstance.getProperty(field);
                    if (oldValue != null && !override) {
                        return;
                    }
                    final T value = getValue.get();
                    if (value == null) {
                        throw new ConfigException("afterSelect.add(override,delete)Field, 不能传入null值, 如需将值更新为null，使用QueryProConst(Kt).NULL");
                    }
                    if (value != Const.SKIP) {
                        if (value == Const.NULL) {
                            beanInstance.setProperty(field, null);
                        } else {
                            beanInstance.setProperty(field, value);
                        }
                    }
                };

                if (result instanceof Iterable) {
                    ((Iterable<?>) result).forEach(doTransform);
                } else {
                    doTransform.accept(result);
                }
                return Result.ok(result);
            });
        }
    }

    /**
     * <h2>更新操作的结果返回后，对返回结果进行转换</h2>
     */
    public static class AfterUpdateTransformerBuilder extends BaseResultTransformersBuilder {
        AfterUpdateTransformerBuilder() {
            super(QueryStructureAction.UPDATE);
        }
        /**
         * <p><b>添加一个返回结果转换器</b></p>
         * <br/>
         * 根据 {@link cn.cloudself.query.resolver.QSRTmpl}的实现，如果返回{@link Result#err(Throwable)}，相关操作会终止执行并抛出异常
         *
         * @param transformer {@link ResultWithQueryStructureTransformer#transform(Class, Object, QueryStructure, QueryPayload)}
         * @return 自身，Builder模式，方便链式调用
         */
        @Override
        public AfterUpdateTransformerBuilder addTransformer(ResultWithQueryStructureTransformer transformer) {
            addTransformerWhenActionMatched(transformer);
            return this;
        }
    }

    /**
     * <h2>删除操作的结果返回后，对返回结果进行转换</h2>
     */
    public static class AfterDeleteTransformerBuilder extends BaseResultTransformersBuilder {
        AfterDeleteTransformerBuilder() {
            super(QueryStructureAction.DELETE);
        }

        /**
         * <p><b>添加一个返回结果转换器</b></p>
         * <br/>
         * 根据 {@link cn.cloudself.query.resolver.QSRTmpl}的实现，如果返回{@link Result#err(Throwable)}，相关操作会终止执行并抛出异常
         *
         * @param transformer {@link ResultWithQueryStructureTransformer#transform(Class, Object, QueryStructure, QueryPayload)}
         * @return 自身，Builder模式，方便链式调用
         */
        @Override
        public AfterDeleteTransformerBuilder addTransformer(ResultWithQueryStructureTransformer transformer) {
            addTransformerWhenActionMatched(transformer);
            return this;
        }
    }

    /**
     * <h2>插入操作的结果返回后，对返回结果进行转换</h2>
     */
    public static class AfterInsertTransformerBuilder extends BaseResultTransformersBuilder {
        AfterInsertTransformerBuilder() {
            super(QueryStructureAction.INSERT);
        }

        /**
         * <p><b>添加一个返回结果转换器</b></p>
         * <br/>
         * 根据 {@link cn.cloudself.query.resolver.QSRTmpl}的实现，如果返回{@link Result#err(Throwable)}，相关操作会终止执行并抛出异常
         *
         * @param transformer {@link ResultWithQueryStructureTransformer#transform(Class, Object, QueryStructure, QueryPayload)}
         * @return 自身，Builder模式，方便链式调用
         */
        @Override public AfterInsertTransformerBuilder addTransformer(ResultWithQueryStructureTransformer transformer) {
            addTransformerWhenActionMatched(transformer);
            return this;
        }
    }

    /**
     * <h2>Replace操作的结果返回后，对返回结果进行转换</h2>
     */
    public static class AfterReplaceTransformerBuilder extends BaseResultTransformersBuilder {
        AfterReplaceTransformerBuilder() {
            super(QueryStructureAction.REPLACE);
        }

        /**
         * <p><b>添加一个返回结果转换器</b></p>
         * <br/>
         * 根据 {@link cn.cloudself.query.resolver.QSRTmpl}的实现，如果返回{@link Result#err(Throwable)}，相关操作会终止执行并抛出异常
         *
         * @param transformer {@link ResultWithQueryStructureTransformer#transform(Class, Object, QueryStructure, QueryPayload)}
         * @return 自身，Builder模式，方便链式调用
         */
        @Override public AfterReplaceTransformerBuilder addTransformer(ResultWithQueryStructureTransformer transformer) {
            addTransformerWhenActionMatched(transformer);
            return this;
        }
    }

    /**
     * <p><b>生命周期回调之: After Select</b></p>
     * <br/>
     * <p>会在{@link QueryPro#selectBy()}等方法的SQL语句执行并获取返回结果之后，方法返回之前调用</p>
     * <br/>
     * <p>具体的生命周期顺序，参考{@link Lifecycle}</p>
     * <p><b>Examples: </b></p>
     * <pre>{@code
     * QueryProConfig.global
     *         .lifecycle()
     *         .afterSelect(builder -> builder.addField("deleted", Boolean.class, () -> false).overrideField("age", Integer.class, () -> 18))
     * }</pre>
     *
     * @param builder {@link AfterSelectTransformerBuilder}
     * @return 自身，Builder模式，方便链式调用
     */
    public Lifecycle afterSelect(Function<AfterSelectTransformerBuilder, AfterSelectTransformerBuilder> builder) {
        afterExecTransformers.addAll(builder.apply(new AfterSelectTransformerBuilder()).build());
        return this;
    }

    /**
     * <p><b>生命周期回调之: After Update</b></p>
     * <br/>
     * <p>会在{@link QueryPro#updateSet()}等方法的SQL语句执行并获取返回结果之后，方法返回之前调用</p>
     * <br/>
     * <p>具体的生命周期顺序，参考{@link Lifecycle}</p>
     * <p><b>Examples: </b></p>
     * <pre>{@code
     * QueryProConfig.global
     *         .lifecycle()
     *         .afterUpdate(builder -> builder.addTransformer(((resultClass, result, qs, payload) -> Result.ok(result))))
     * }</pre>
     *
     * @param builder {@link AfterUpdateTransformerBuilder}
     * @return 自身，Builder模式，方便链式调用
     */
    public Lifecycle afterUpdate(Function<AfterUpdateTransformerBuilder, AfterUpdateTransformerBuilder> builder) {
        afterExecTransformers.addAll(builder.apply(new AfterUpdateTransformerBuilder()).build());
        return this;
    }

    /**
     * <p><b>生命周期回调之: After Delete</b></p>
     * <br/>
     * <p>会在{@link QueryPro#deleteBy()}等方法的SQL语句执行并获取返回结果之后，方法返回之前调用</p>
     * <br/>
     * <p>具体的生命周期顺序，参考{@link Lifecycle}</p>
     * <p><b>Examples: </b></p>
     * <pre>{@code
     * QueryProConfig.global
     *         .lifecycle()
     *         .afterDelete(builder -> builder.addTransformer(((resultClass, result, qs, payload) -> Result.ok(result))))
     * }</pre>
     *
     * @param builder {@link AfterDeleteTransformerBuilder}
     * @return 自身，Builder模式，方便链式调用
     */
    public Lifecycle afterDelete(Function<AfterDeleteTransformerBuilder, AfterDeleteTransformerBuilder> builder) {
        afterExecTransformers.addAll(builder.apply(new AfterDeleteTransformerBuilder()).build());
        return this;
    }

    /**
     * <p><b>生命周期回调之: After Insert</b></p>
     * <br/>
     * <p>会在{@link QueryPro#insert(Object)}等方法的SQL语句执行并获取返回结果之后，方法返回之前调用</p>
     * <br/>
     * <p>具体的生命周期顺序，参考{@link Lifecycle}</p>
     * <p><b>Examples: </b></p>
     * <pre>{@code
     * QueryProConfig.global
     *         .lifecycle()
     *         .afterInsert(builder -> builder.addTransformer(((resultClass, result, qs, payload) -> Result.ok(result))))
     * }</pre>
     *
     * @param builder {@link AfterInsertTransformerBuilder}
     * @return 自身，Builder模式，方便链式调用
     */
    public Lifecycle afterInsert(Function<AfterInsertTransformerBuilder, AfterInsertTransformerBuilder> builder) {
        afterExecTransformers.addAll(builder.apply(new AfterInsertTransformerBuilder()).build());
        return this;
    }

    /**
     * <p><b>生命周期回调之: After Replace</b></p>
     * <br/>
     * <p>会在{@link QueryPro#replace(Object[])}等方法的SQL语句执行并获取返回结果之后，方法返回之前调用</p>
     * <br/>
     * <p>具体的生命周期顺序，参考{@link Lifecycle}</p>
     * <p><b>Examples: </b></p>
     * <pre>{@code
     * QueryProConfig.global
     *         .lifecycle()
     *         .afterReplace(builder -> builder.addTransformer(((resultClass, result, qs, payload) -> Result.ok(result))))
     * }</pre>
     *
     * @param builder {@link AfterReplaceTransformerBuilder}
     * @return 自身，Builder模式，方便链式调用
     */
    public Lifecycle afterReplace(Function<AfterReplaceTransformerBuilder, AfterReplaceTransformerBuilder> builder) {
        afterExecTransformers.addAll(builder.apply(new AfterReplaceTransformerBuilder()).build());
        return this;
    }

    /**
     * <p><b>生命周期回调之: After Exec</b></p>
     * <br/>
     * <p>会在{@link QueryPro#insert(Object)}, {@link QueryPro#selectBy()}等方法的SQL语句执行并获取返回结果之后，方法返回之前调用</p>
     * <br/>
     * <p>具体的生命周期顺序，参考{@link Lifecycle}</p>
     * @param transformer {@link ResultWithQueryStructureTransformer#transform(Class, Object, QueryStructure, QueryPayload)}
     * @return 自身，Builder模式，方便链式调用
     */
    public Lifecycle afterExec(ResultWithQueryStructureTransformer transformer) {
        afterExecTransformers.add(transformer);
        return this;
    }

    /**
     * <p><b>生命周期回调之: Before RunSql</b></p>
     * <br/>
     * <p>会在执行`SQL`之前调用，以便记录`SQL`信息或对`SQL`的参数做一些转换或阻止`SQL`的执行</p>
     * <br/>
     * <p>具体的生命周期顺序，参考{@link Lifecycle}</p>
     * <pre>{@code
     * QueryProConfig.global
     *         .lifecycle()
     *         .beforeRunSql(sqlAndParams -> sqlAndParams.sql().startsWith("SELECT") ? Result.ok(sqlAndParams) : Result.err(new UnSupportException("unsupport")));
     * }</pre>
     * @param transformer {@link SqlAndParamsTransformer#transform(SqlAndParams)}
     * @return 自身，Builder模式，方便链式调用
     */
    public Lifecycle beforeRunSql(SqlAndParamsTransformer transformer) {
        beforeRunSqlTransformers.add(transformer);
        return this;
    }

    /**
     * <p><b>生命周期回调之: After RunSql</b></p>
     * <br/>
     * <p>会在执行`SQL`之后调用，以便</p>
     * <ul>
     *     <li>对返回结果进行记录</li>
     *     <li>对返回结果进行转换</li>
     *     <li>阻止结果的返回</li>
     * </ul>
     * <br/>
     * <p>具体的生命周期顺序，参考{@link Lifecycle}</p>
     * <pre>{@code
     * QueryProConfig.global
     *         .lifecycle()
     *         .afterRunSql(result -> Result.ok(new Wrapper(result)));
     * }</pre>
     * @param transformer {@link ResultTransformer#transform(Object)}
     * @return 自身，Builder模式，方便链式调用
     */
    public Lifecycle afterRunSql(ResultTransformer transformer) {
        afterRunSqlTransformers.add(transformer);
        return this;
    }
}