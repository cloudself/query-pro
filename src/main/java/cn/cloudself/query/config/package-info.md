## 配置以及生命周期

`QueryPro`支持很多不同级别的配置（`全局`、`请求`、`线程`、`上下文`以及`单次查询`）该包主要用于处理这些配置信息。

* [QueryProConfig.kt](QueryProConfig.java) 该包最终对外暴露的类，使用`QueryProConfig.` `final`(只读), `global`或`request`或`thread`或`context`或`code`读取,写入配置信息。
* [dbs.kt](dbs.kt) 包含`Store`(存储器)和`DB`
  * [Store](dbs.kt) `Store`接口，仅包含一个 `get()` 和一个 `set()` 方法。
  * [HashMapStore](dbs.kt) `hash map` 实现的 `Store` 用于存储全局`global`的配置。
  * [RequestContextStore](dbs.kt) `spring` `RequestContextHolder` 实现的 `Store`, 用于存储`request`级的配置。
  * [ThreadContextStore](dbs.kt) `ThreadLocal` 实现的 `Store`, 用于存储 `thread`, `context`, `code`级的配置。
  * [IQueryProConfigDb](dbs.kt) 通用的配置选项(读取接口)。
  * [IQueryProConfigDbWriteable](dbs.kt) 通用的配置选项(写入接口)。
  * [IOnlyGlobalConfig](dbs.kt) 仅用于全局的配置选项。
  * [QueryProConfigDb](dbs.kt) 通用的`DB`实现(构造函数的参数中需传入一个`Store`)。
  * [GlobalQueryProConfigDb](dbs.kt) 继承自`QueryProConfigDb`, 额外引入了一些仅在全局作用域下支持的配置。
  * [ThreadQueryProConfigDb](dbs.kt) 继承自`QueryProConfigDb`, 添加了一些方法，需手动管理`ThreadLocal`中数据的初始化和销毁，也可以用`use`方法管理。
  * [CodeQueryProConfigDb](dbs.kt) 继承自`ThreadQueryProConfigDb`, 方便快速从`HashMap`复制配置到`ThreadLocal`中。
  * [FinalQueryProConfigDb](dbs.kt) Final级别的`DB`实现（通过迭代构造函数中传入的`DB`数组，尝试从中寻找配置）。
* [DefaultDataSource.kt](DefaultDataSource.java) `QueryPro`支持多数据源，可以使用`DefaultDataSource`根据生成`QueryPro`文件的类名指定不同数据源(使用方法参考文档其它部分或源文件内的注释)，该数据源配置会被除`global`以外任意级别的配置覆盖。
* [DS.kt](DS.java) 暂未实现
* [Lifecycle.kt](Lifecycle.java) 生命周期
  * 对于 `.selectBy()...`, `.updateSet()...`, `.deleteBy()...`, `(QueryPro).insert...`的生命周期：
    ```
    [beforeExec, beforeSelect](取决于先调用哪个) ->
    [beforeRunSql, runSqlAround] ->
    [afterRunSql, runSqlAround] ->
    [afterExec, afterSelect] ->
    return result
    ```
  * 对于`QueryProSql...`的生命周期
    ```
    [beforeRunSql, runSqlAround] ->
    [afterRunSql, runSqlAround] ->
    return result
    ```
