package cn.cloudself.query.config.store;

import org.jetbrains.annotations.Nullable;

public interface Store {
    @Nullable
    Object get(String key);
    void set(String key, @Nullable Object value);
}