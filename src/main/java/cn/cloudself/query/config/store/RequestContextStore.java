package cn.cloudself.query.config.store;

import org.jetbrains.annotations.Nullable;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

/**
 * `spring` `RequestContextHolder` 实现的 `Store`, 用于存储`request`级的配置。
 */
public class RequestContextStore implements Store {
    private static final String KEY_PREFIX = "QUERY_PRO_CONFIG:REQUEST_CONTEXT:";
    private static boolean isRequestContextHolderPresent;
    static {
        try {
            Class.forName("org.springframework.web.context.request.RequestContextHolder");
            isRequestContextHolderPresent = true;
        } catch (Throwable e) {
            isRequestContextHolderPresent = false;
        }

    }

    @Nullable
    @Override
    public Object get(String key) {
        if (!isRequestContextHolderPresent) {
            return null;
        }

        try {
            return RequestContextHolder.currentRequestAttributes().getAttribute(KEY_PREFIX + key, RequestAttributes.SCOPE_REQUEST);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void set(String key, @Nullable Object value) {
        //noinspection DataFlowIssue
        RequestContextHolder.currentRequestAttributes().setAttribute(KEY_PREFIX + key, value, RequestAttributes.SCOPE_REQUEST);
    }
}
