package cn.cloudself.query.config.store;

import cn.cloudself.exception.IllegalCall;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * `ThreadLocal` 实现的 `Store`, 用于存储 `thread`, `context`, `code`级的配置。
 */
public class ThreadContextStore implements Store {
    // 注意该检测全应用只需调用一次，只是为了提醒用户，并不能保证一定初始化了。
    private static boolean initCalled = false;

    private final ThreadLocal<Map<String, Object>> store;
    private final boolean subThreadSupports;

    public ThreadContextStore(boolean subThreadSupports) {
        this.subThreadSupports = subThreadSupports;
        store = subThreadSupports ? new InheritableThreadLocal<>() : new ThreadLocal<>();
    }

    public void init() {
        store.set(subThreadSupports ? new ConcurrentHashMap<>() : new HashMap<>());
        initCalled = true;
    }

    public void clean() {
        store.remove();
    }

    @Override
    @Nullable
    public Object get(String key) {
        final Map<String, Object> map = store.get();
        if (map == null) {
            return null;
        }
        return map.get(key);
    }

    @Override
    public void set(String key, @Nullable Object value) {
        if (!initCalled) {
            throw new IllegalCall("使用thread进行配置时，因为线程池的问题，" +
                    "必须在线程初始化后调用initThreadContextStore，线程结束时调用cleanThreadContextStore，" +
                    "所以spring环境更推荐使用request进行配置。"
            );
        }
        store.get().put(key, value);
    }

    @Nullable
    public Map<String, Object> getStore() {
        return store.get();
    }

    public void setStore(Map<String, Object> map) {
        store.set(map);
    }
}
