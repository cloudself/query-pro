package cn.cloudself.query.config.store;

import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

/**
 * `hash map` 实现的 `Store` 用于存储全局`global`的配置。
 */
public class HashMapStore implements Store {
    private final Map<String, Object> store = new HashMap<>();

    @Nullable
    @Override
    public Object get(String key) {
        return store.get(key);
    }

    @Override
    public void set(String key, @Nullable Object value) {
        store.put(key, value);
    }

    public HashMapStore copyFrom(HashMapStore hashMapStore) {
        store.clear();
        store.putAll(hashMapStore.store);
        return this;
    }

    public Map<String, Object> toMap() {
        final Map<String, Object> map = new HashMap<>();
        for (String key : store.keySet()) {
            map.put(key, this.get(key));
        }
        return map;
    }
}
