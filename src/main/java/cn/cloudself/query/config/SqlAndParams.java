package cn.cloudself.query.config;

import org.intellij.lang.annotations.Language;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Objects;

public class SqlAndParams {
    @Language("SQL")
    @NotNull
    private String sql;
    @NotNull
    private Object[] params;
    private boolean emptyStatement;

    public SqlAndParams(@Language("SQL") @NotNull String sql, @NotNull Object[] params) {
        this.sql = sql;
        this.params = params;
    }

    public SqlAndParams(@Language("SQL") @NotNull String sql) {
        this.sql = sql;
        this.params = new Object[0];
    }

    @Language("SQL")
    public String sql() {
        return sql;
    }

    public SqlAndParams sql(@Language("SQL") String sql) {
        this.sql = sql;
        return this;
    }

    public Object[] params() {
        return params;
    }

    public SqlAndParams params(Object[] params) {
        this.params = params;
        return this;
    }

    public boolean emptyStatement() {
        return emptyStatement;
    }

    public SqlAndParams emptyStatement(boolean emptyStatement) {
        this.emptyStatement = emptyStatement;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SqlAndParams that = (SqlAndParams) o;
        return Objects.equals(sql, that.sql) && Arrays.equals(params, that.params);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(sql);
        result = 31 * result + Arrays.hashCode(params);
        return result;
    }

    @Override
    public String toString() {
        return "SqlAndParams{" +
                "sql='" + sql + '\'' +
                ", params=" + Arrays.toString(params) +
                ", emptyStatement=" + emptyStatement +
                '}';
    }
}