package cn.cloudself.query.config;

import cn.cloudself.query.resolver.DynamicActuator;
import cn.cloudself.query.resolver.QueryStructureResolver;
import cn.cloudself.query.resolver.ScriptResolver;
import org.jetbrains.annotations.Nullable;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.function.Function;

/**
 * 仅用于全局的配置选项
 */
public interface IOnlyGlobalConfig {
    interface DataSourceGetter {
        @Nullable
        DataSource get(@Nullable Class<?> clazz);
    }
    interface ResultSetParserEx {
        Optional<?> parse(ResultSet rs, Class<?> clazz, int i) throws SQLException;
    }
    interface ResultSetGetter<T> {
        @Nullable
        T get(ResultSet rs, int i) throws SQLException;
    }

    interface SqlParamSetter<T> {
        void set(PreparedStatement ps, int i, T obj) throws SQLException;
    }

    public class DbColumnInfo {
        private String type;
        private String label;

        public DbColumnInfo(String type, String label) {
            this.type = type;
            this.label = label;
        }

        public String type() {
            return type;
        }

        public DbColumnInfo type(String type) {
            this.type = type;
            return this;
        }

        public String label() {
            return label;
        }

        public DbColumnInfo label(String label) {
            this.label = label;
            return this;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DbColumnInfo that = (DbColumnInfo) o;
            return Objects.equals(type, that.type) && Objects.equals(label, that.label);
        }

        @Override
        public int hashCode() {
            return Objects.hash(type, label);
        }

        @Override
        public String toString() {
            return "DbColumnInfo{" +
                    "type='" + type + '\'' +
                    ", label='" + label + '\'' +
                    '}';
        }
    }

    Lifecycle lifecycle();
    DataSourceGetter defaultDataSource();
    Set<String> shouldIgnoreFields();
    Set<Class<?>> supportedColumnType();
    Map<Function<DbColumnInfo, Boolean>, Class<?>> dbColumnInfoToJavaType();
    @Nullable
    <T> SqlParamSetter<T> sqlParamSetter(Class<T> clazz);
    @Nullable
    <T> ResultSetGetter<T> resultSetParser(Class<T> clazz);
    List<ResultSetParserEx> resultSetParserEx();
    QueryStructureResolver queryStructureResolver();
    ScriptResolver scriptResolver();
    DynamicActuator dynamicActuator();
}