package cn.cloudself.query.config;

import cn.cloudself.query.config.impl.*;
import cn.cloudself.query.config.store.RequestContextStore;
import cn.cloudself.query.resolver.impl.JdbcQSR;
import cn.cloudself.util.ext.EntityProxy;
import cn.cloudself.util.log.LogLevel;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

/**
 * 该包最终对外暴露的类，使用`QueryProConfig.` `final`(只读), `global`或`request`或`thread`或`context`或`code`读取,写入配置信息。
 */
public class QueryProConfig {
    public static GlobalQueryProConfigImpl global = new GlobalQueryProConfigImpl();
    static {
        global.dbType(IQueryProConfig.DatabaseType.MySQL);
        global.beautifySql(true);
        global.printLog(true, LogLevel.INFO);
        global.printLargeElementWholly(false);
        global.printCallByInfo(true, LogLevel.INFO);
        global.printResult(true, LogLevel.INFO);
        global.dryRun(false);
        global.logicDelete(true, "deleted", true, false);
        final JdbcQSR jdbcQsr = new JdbcQSR();
        global.setScriptResolver(jdbcQsr);
        global.setQueryStructureResolver(jdbcQsr);
        global.setDynamicActuator(jdbcQsr);

        global.shouldIgnoreFields().add("serialVersionUID");

        // 数据库字段转Java字段 key: columnTester, value: JavaClass
        global.dbColumnInfoToJavaType().put(
                // 将BIGINT类型的id列设置为Long类型
                (IOnlyGlobalConfig.DbColumnInfo column) -> ("id".equals(column.label()) || column.label().endsWith("_id")) && column.type().startsWith("BIGINT"),
                long.class
        );

        /* jdbc查询的结果: resultSet转enum */
        global.resultSetParserEx().add(((rs, clazz, i) -> {
            if (!clazz.isEnum()) {
                return Optional.empty();
            } else {
                //noinspection unchecked
                final Class<Enum<?>> enumClass = (Class<Enum<?>>) clazz;
                return Optional.of(enumValueOfAny(enumClass, rs.getString(i))) ;
            }
        }));

        global.putToResultSetParser(boolean.class, ResultSet::getBoolean);
        global.putToResultSetParser(BigDecimal.class, ResultSet::getBigDecimal);
        global.putToResultSetParser(byte.class, ResultSet::getByte);
        global.putToResultSetParser(byte[].class, ResultSet::getBytes);
        global.putToResultSetParser(double.class, ResultSet::getDouble);
        global.putToResultSetParser(float.class, ResultSet::getFloat);
        global.putToResultSetParser(int.class, ResultSet::getInt);
        global.putToResultSetParser(long.class, ResultSet::getLong);
        global.putToResultSetParser(Time.class, ResultSet::getTime);
        global.putToResultSetParser(Timestamp.class, ResultSet::getTimestamp);
        global.putToResultSetParser(Short.class, ResultSet::getShort);
        global.putToResultSetParser(String.class, ResultSet::getString);
        global.putToResultSetParser(Date.class, ResultSet::getTimestamp);
        global.putToResultSetParser(java.sql.Date.class, ResultSet::getDate);
        global.putToResultSetParser(LocalDate.class, (rs, i) -> {
            final java.sql.Date date = rs.getDate(i);
            if (date == null) {
                return null;
            }
            return date.toLocalDate();
        });
        global.putToResultSetParser(LocalTime.class, (rs, i) -> {
            final Time time = rs.getTime(i);
            if (time == null) {
                return null;
            }
            return time.toLocalTime();
        });
        global.putToResultSetParser(LocalDateTime.class, (rs, i) -> {
            final Timestamp timestamp = rs.getTimestamp(i);
            if (timestamp == null) {
                return null;
            }
            return timestamp.toLocalDateTime();
        });
    }

    static {
        EntityProxy.areBasicType = clazz -> QueryProConfig.computed.supportedColumnType().stream().anyMatch(it -> it.isAssignableFrom(clazz));
        EntityProxy.shouldIgnoreFields = () -> QueryProConfig.computed.shouldIgnoreFields();
    }

    public static QueryProConfigImpl request = new QueryProConfigImpl(new RequestContextStore());

    /**
     * 不推荐使用，优先使用global或request或者context
     * <br/>
     * 因为存在线程池复用线程, threadLocal不释放问题
     * 所以必须在线程初始化后调用QueryProConfig.thread.init()
     * 必须在线程结束后调用QueryProConfig.thread.clean()
     * 之后，才能针对对thread进行配置
     * <br/>
     * 另外：同时存在thread配置与request配置的时候，会使用request中的配置
     * <br/>
     * 该配置在子线程中依旧生效
     */
    @SuppressWarnings("DeprecatedIsStillUsed")
    @Deprecated
    public static ThreadQueryProConfigImpl thread = new ThreadQueryProConfigImpl();

    /**
     * 在回调函数中，维持一个query pro配置的上下文。
     * <p><b>Examples:</b></p>
     * <pre>
     * {@code
     * QueryProConfig.context.use(context -> {
     *   context.beautifySql();
     *   UserQueryPro.selectBy().id().equalTo(1);
     * });
     * }</pre>
     */
    public static ThreadQueryProConfigImpl context = new ThreadQueryProConfigImpl();

    /**
     * 单次查询作用域
     * 代码内部使用
     */
    /*TODO 这个改为内部使用*/
    @Deprecated
    public static CodeQueryProConfigImpl code = new CodeQueryProConfigImpl();

    public static FinalQueryProConfigImpl computed = new FinalQueryProConfigImpl(new IQueryProConfig[] { code, context, request, thread, global });

    private static <T extends Enum<?>> T enumValueOfAny(Class<T> clazz, String name) {
        //noinspection unchecked,rawtypes
        return (T) Enum.valueOf((Class) clazz, name);
    }
}
